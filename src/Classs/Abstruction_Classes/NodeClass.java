package Classs.Abstruction_Classes;

import Visitor.AST_Visitor.AstVisitorInterface;

public abstract class NodeClass {
    private int row;
    private int column;

    public NodeClass(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public abstract void accept(AstVisitorInterface astVisitor);


    // Getter And Setter
    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }
}
