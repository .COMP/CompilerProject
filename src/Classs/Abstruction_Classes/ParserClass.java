package Classs.Abstruction_Classes;

import Visitor.AST_Visitor.AstVisitorInterface;
import Classs.Java_Classes.Java_ClassesParse_Classess.AllStatementsStartClass;

import java.util.ArrayList;
import java.util.List;

public class ParserClass extends StatementClass {
    public ParserClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
        this.allStatementClasses = new ArrayList<>();
    }

    private List<AllStatementsStartClass> allStatementClasses;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.allStatementClasses != null) {
            for (AllStatementsStartClass allStatementClass : this.allStatementClasses) {
                allStatementClass.accept(astVisitor);
            }
        }
    }


    // Getter And Setter
    public List<AllStatementsStartClass> getAllStatementClasses() {
        return allStatementClasses;
    }

    public void setAllStatementClasses(List<AllStatementsStartClass> allStatementClasses) {
        this.allStatementClasses = allStatementClasses;
    }


}
