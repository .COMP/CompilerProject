package Classs.Abstruction_Classes;

public abstract class StatementClass extends NodeClass {
    public StatementClass(int row, int column, String statement_name) {
        super(row, column);
        this.statement_name = statement_name;
    }

    private String statement_name;

    public String getStatement_name() {
        return statement_name;
    }

    public void setStatement_name(String statement_name) {
        this.statement_name = statement_name;
    }


}
