package Classs.Java_Classes.All_Statement_Start_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Global_Classes.JavaStatementClass;
import Classs.Java_Classes.Prototype_Function_Classes.PrototypeFunctionClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class JavaStatementsListClass extends StatementClass {
    public JavaStatementsListClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private JavaStatementClass java_statement_class;
    private PrototypeFunctionClass prototype_function;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.java_statement_class != null) {
            this.java_statement_class.accept(astVisitor);
        }

        if (this.prototype_function != null) {
            this.prototype_function.accept(astVisitor);
        }

    }

    // Getter and Setter
    public JavaStatementClass getJava_statement_class() {
        return java_statement_class;
    }

    public void setJava_statement_class(JavaStatementClass java_statement_class) {
        this.java_statement_class = java_statement_class;
    }

    public PrototypeFunctionClass getPrototype_function() {
        return prototype_function;
    }

    public void setPrototype_function(PrototypeFunctionClass prototype_function) {
        this.prototype_function = prototype_function;
    }
}
