package Classs.Java_Classes.Break_Continue_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class BreakStatementClass extends StatementClass {
    public BreakStatementClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
    }
}
