package Classs.Java_Classes.Break_Continue_Classes;

import Visitor.AST_Visitor.AstVisitorInterface;
import Classs.Abstruction_Classes.StatementClass;

public class ContinueStatementClass extends StatementClass {
    public ContinueStatementClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
    }
}
