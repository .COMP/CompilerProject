package Classs.Java_Classes.Callbakc_Function_Classes;

import Visitor.AST_Visitor.AstVisitorInterface;
import Classs.Abstruction_Classes.StatementClass;

public class CallbackFunctionClass extends StatementClass {
    public CallbackFunctionClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private CallbackFunctionWithOutScolClass callback_function_without_scol;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.callback_function_without_scol != null) {
            this.callback_function_without_scol.accept(astVisitor);
        }
    }

    public CallbackFunctionWithOutScolClass getCallback_function_without_scol() {
        return callback_function_without_scol;
    }

    public void setCallback_function_without_scol(CallbackFunctionWithOutScolClass callback_function_without_scol) {
        this.callback_function_without_scol = callback_function_without_scol;
    }
}
