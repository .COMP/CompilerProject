package Classs.Java_Classes.Callbakc_Function_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class CallbackFunctionHeaderClass extends StatementClass {
    public CallbackFunctionHeaderClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private List<CallbackFunctionParameterInHeaderClass> callback_function_parameter_in_header;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.callback_function_parameter_in_header != null) {
            for (CallbackFunctionParameterInHeaderClass callbackFunctionParameterInHeaderClass : this.callback_function_parameter_in_header) {
                callbackFunctionParameterInHeaderClass.accept(astVisitor);
            }
        }
    }


    public List<CallbackFunctionParameterInHeaderClass> getCallback_function_parameter_in_header() {
        return callback_function_parameter_in_header;
    }

    public void setCallback_function_parameter_in_header(List<CallbackFunctionParameterInHeaderClass> callback_function_parameter_in_header) {
        this.callback_function_parameter_in_header = callback_function_parameter_in_header;
    }
}
