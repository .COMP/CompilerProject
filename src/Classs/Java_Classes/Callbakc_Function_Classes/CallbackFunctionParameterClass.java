package Classs.Java_Classes.Callbakc_Function_Classes;

import Visitor.AST_Visitor.AstVisitorInterface;
import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Expression_Classes.ExpressionJacaClass;

public class CallbackFunctionParameterClass extends StatementClass {
    public CallbackFunctionParameterClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private String primitive_parameter;
    private FunctionParameterClass function_parameter;
    private CallbackFunctionWithOutScolClass callback_function_without_scol;
    private ExpressionJacaClass expr_java;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.function_parameter != null) {
            this.function_parameter.accept(astVisitor);
        }
        if (this.callback_function_without_scol != null) {
            this.callback_function_without_scol.accept(astVisitor);
        }
        if (this.expr_java != null) {
            this.expr_java.accept(astVisitor);
        }
    }


    public FunctionParameterClass getFunction_parameter() {
        return function_parameter;
    }

    public void setFunction_parameter(FunctionParameterClass function_parameter) {
        this.function_parameter = function_parameter;
    }

    public String getPrimitive_parameter() {
        return primitive_parameter;
    }

    public void setPrimitive_parameter(String primitive_parameter) {
        this.primitive_parameter = primitive_parameter;
    }

    public CallbackFunctionWithOutScolClass getCallback_function_without_scol() {
        return callback_function_without_scol;
    }

    public void setCallback_function_without_scol(CallbackFunctionWithOutScolClass callback_function_without_scol) {
        this.callback_function_without_scol = callback_function_without_scol;
    }

    public ExpressionJacaClass getExpr_java() {
        return expr_java;
    }

    public void setExpr_java(ExpressionJacaClass expr_java) {
        this.expr_java = expr_java;
    }
}
