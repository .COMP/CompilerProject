package Classs.Java_Classes.Callbakc_Function_Classes;

import Visitor.AST_Visitor.AstVisitorInterface;
import Classs.Abstruction_Classes.StatementClass;

import java.util.List;


public class CallbackFunctionParameterInHeaderClass extends StatementClass {
    public CallbackFunctionParameterInHeaderClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private List<CallbackFunctionParameterClass> callback_function_parameter;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.callback_function_parameter != null) {
            for (CallbackFunctionParameterClass callbackFunctionParameterClass : this.callback_function_parameter) {
                callbackFunctionParameterClass.accept(astVisitor);
            }
        }
    }


    public List<CallbackFunctionParameterClass> getCallback_function_parameter() {
        return callback_function_parameter;
    }

    public void setCallback_function_parameter(List<CallbackFunctionParameterClass> callback_function_parameter) {
        this.callback_function_parameter = callback_function_parameter;
    }
}
