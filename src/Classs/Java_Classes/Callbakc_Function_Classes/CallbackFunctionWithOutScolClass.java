package Classs.Java_Classes.Callbakc_Function_Classes;

import Visitor.AST_Visitor.AstVisitorInterface;
import Classs.Abstruction_Classes.StatementClass;

public class CallbackFunctionWithOutScolClass extends StatementClass {
    public CallbackFunctionWithOutScolClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private String function_name;
    private CallbackFunctionHeaderClass callback_function_header;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.callback_function_header != null) {
            this.callback_function_header.accept(astVisitor);
        }
    }


    public String getFunction_name() {
        return function_name;
    }

    public void setFunction_name(String function_name) {
        this.function_name = function_name;
    }

    public CallbackFunctionHeaderClass getCallback_function_header() {
        return callback_function_header;
    }

    public void setCallback_function_header(CallbackFunctionHeaderClass callback_function_header) {
        this.callback_function_header = callback_function_header;
    }
}
