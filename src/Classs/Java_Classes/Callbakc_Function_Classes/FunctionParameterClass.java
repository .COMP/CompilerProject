package Classs.Java_Classes.Callbakc_Function_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Global_Classes.BodyFunctionClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class FunctionParameterClass extends StatementClass {
    public FunctionParameterClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private VarNameInParameterClass var_name_in_parameter;
    private BodyFunctionClass body_function;
    private String scope_id;
    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.var_name_in_parameter != null) {
            this.var_name_in_parameter.accept(astVisitor);
        }
        if (this.body_function != null) {
            this.body_function.accept(astVisitor);
        }
    }


    public BodyFunctionClass getBody_function() {
        return body_function;
    }

    public void setBody_function(BodyFunctionClass body_function) {
        this.body_function = body_function;
    }

    public VarNameInParameterClass getVar_name_in_parameter() {
        return var_name_in_parameter;
    }

    public void setVar_name_in_parameter(VarNameInParameterClass var_name_in_parameter) {
        this.var_name_in_parameter = var_name_in_parameter;
    }

    public String getScope_id() {
        return scope_id;
    }

    public void setScope_id(String scope_id) {
        this.scope_id = scope_id;
    }
}
