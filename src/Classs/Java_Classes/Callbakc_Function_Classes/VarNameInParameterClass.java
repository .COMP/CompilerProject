package Classs.Java_Classes.Callbakc_Function_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class VarNameInParameterClass extends StatementClass {
    public VarNameInParameterClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private List<String> variable_parameters;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
    }

    public List<String> getVariable_parameters() {
        return variable_parameters;
    }

    public void setVariable_parameters(List<String> variable_parameters) {
        this.variable_parameters = variable_parameters;
    }
}
