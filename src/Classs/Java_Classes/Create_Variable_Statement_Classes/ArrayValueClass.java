package Classs.Java_Classes.Create_Variable_Statement_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Callbakc_Function_Classes.CallbackFunctionWithOutScolClass;
import Classs.Java_Classes.Expression_Classes.NumberValueJavaClass;
import Classs.Java_Classes.Global_Classes.VariableValueClass;
import Classs.SQL_Classes.Select_Statement.SelectStmtClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class ArrayValueClass extends StatementClass {
    public ArrayValueClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private NumberValueJavaClass number_value;
    private List<VariableValueClass> variables_value;
    private DirectArrayValueClass direct_array_value;
    private CallbackFunctionWithOutScolClass callback_function_without_scol;
    private SelectStmtClass select_statement;

    // Select statement;
    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.number_value != null) {
            this.number_value.accept(astVisitor);
        }
        if (this.variables_value != null) {
            for (VariableValueClass variableValueClass : this.variables_value) {
                variableValueClass.accept(astVisitor);
            }
        }
        if (this.direct_array_value != null) {
            this.direct_array_value.accept(astVisitor);
        }
        if (this.callback_function_without_scol != null) {
            this.callback_function_without_scol.accept(astVisitor);
        }
        if (this.select_statement != null) {
            this.select_statement.accept(astVisitor);
        }
    }

    public SelectStmtClass getSelect_statement() {
        return select_statement;
    }

    public void setSelect_statement(SelectStmtClass select_statement) {
        this.select_statement = select_statement;
    }

    public NumberValueJavaClass getNumber_value() {
        return number_value;
    }

    public void setNumber_value(NumberValueJavaClass number_value) {
        this.number_value = number_value;
    }

    public List<VariableValueClass> getVariables_value() {
        return variables_value;
    }

    public void setVariables_value(List<VariableValueClass> variables_value) {
        this.variables_value = variables_value;
    }

    public DirectArrayValueClass getDirect_array_value() {
        return direct_array_value;
    }

    public void setDirect_array_value(DirectArrayValueClass direct_array_value) {
        this.direct_array_value = direct_array_value;
    }

    public CallbackFunctionWithOutScolClass getCallback_function_without_scol() {
        return callback_function_without_scol;
    }

    public void setCallback_function_without_scol(CallbackFunctionWithOutScolClass callback_function_without_scol) {
        this.callback_function_without_scol = callback_function_without_scol;
    }
}
