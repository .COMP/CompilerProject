package Classs.Java_Classes.Create_Variable_Statement_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class AssignArrayClass extends StatementClass {
    public AssignArrayClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private PostSquareClass post_square;
    private PreviousSquareClass previous_square;
    private ArrayValueClass array_value;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.post_square != null) {
            this.post_square.accept(astVisitor);
        }
        if (this.previous_square != null) {
            this.previous_square.accept(astVisitor);
        }
        if (this.array_value != null) {
            this.array_value.accept(astVisitor);
        }
    }

    public PostSquareClass getPost_square() {
        return post_square;
    }

    public void setPost_square(PostSquareClass post_square) {
        this.post_square = post_square;
    }

    public PreviousSquareClass getPrevious_square() {
        return previous_square;
    }

    public void setPrevious_square(PreviousSquareClass previous_square) {
        this.previous_square = previous_square;
    }

    public ArrayValueClass getArray_value() {
        return array_value;
    }

    public void setArray_value(ArrayValueClass array_value) {
        this.array_value = array_value;
    }
}
