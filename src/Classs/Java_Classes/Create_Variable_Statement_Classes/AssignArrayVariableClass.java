package Classs.Java_Classes.Create_Variable_Statement_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class AssignArrayVariableClass extends StatementClass {
    public AssignArrayVariableClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private AssignVariableClass assign_variable;
    private AssignArrayClass assign_array;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.assign_variable != null) {
            this.assign_variable.accept(astVisitor);
        }
        if (this.assign_array != null) {
            this.assign_array.accept(astVisitor);
        }
    }

    public AssignVariableClass getAssign_variable() {
        return assign_variable;
    }

    public void setAssign_variable(AssignVariableClass assign_variable) {
        this.assign_variable = assign_variable;
    }

    public AssignArrayClass getAssign_array() {
        return assign_array;
    }

    public void setAssign_array(AssignArrayClass assign_array) {
        this.assign_array = assign_array;
    }
}
