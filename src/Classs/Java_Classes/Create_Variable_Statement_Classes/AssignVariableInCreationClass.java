package Classs.Java_Classes.Create_Variable_Statement_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class AssignVariableInCreationClass extends StatementClass {
    public AssignVariableInCreationClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private List<AssignArrayVariableClass> assign_array_variable;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.assign_array_variable != null) {
            for (AssignArrayVariableClass assignArrayVariableClass : this.assign_array_variable) {
                assignArrayVariableClass.accept(astVisitor);
            }
        }
    }

    // Getter And Setter

    public List<AssignArrayVariableClass> getAssign_array_variable() {
        return assign_array_variable;
    }

    public void setAssign_array_variable(List<AssignArrayVariableClass> assign_array_variable) {
        this.assign_array_variable = assign_array_variable;
    }
}
