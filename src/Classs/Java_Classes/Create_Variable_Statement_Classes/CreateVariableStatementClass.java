package Classs.Java_Classes.Create_Variable_Statement_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;
import org.antlr.v4.runtime.misc.Pair;

public class CreateVariableStatementClass extends StatementClass {
    public CreateVariableStatementClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private AssignVariableInCreationClass assign_variable_class;
    private Pair<String , String > symbol_id;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.assign_variable_class != null) {
            this.assign_variable_class.accept(astVisitor);
        }
    }

    // Getter And Setter

    public AssignVariableInCreationClass getAssign_variable_class() {
        return assign_variable_class;
    }

    public void setAssign_variable_class(AssignVariableInCreationClass assign_variable_class) {
        this.assign_variable_class = assign_variable_class;
    }

    public Pair<String, String> getSymbol_id() {
        return symbol_id;
    }

    public void setSymbol_id(Pair<String, String> symbol_id) {
        this.symbol_id = symbol_id;
    }
}
