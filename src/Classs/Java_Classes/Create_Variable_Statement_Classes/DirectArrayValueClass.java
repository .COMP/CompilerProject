package Classs.Java_Classes.Create_Variable_Statement_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Global_Classes.VariableValueClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class DirectArrayValueClass extends StatementClass {
    public DirectArrayValueClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private List<VariableValueClass> variables_value;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.variables_value != null) {
            for (VariableValueClass variableValueClass : this.variables_value) {
                variableValueClass.accept(astVisitor);
            }
        }
    }

    public List<VariableValueClass> getVariables_value() {
        return variables_value;
    }

    public void setVariables_value(List<VariableValueClass> variables_value) {
        this.variables_value = variables_value;
    }
}
