package Classs.Java_Classes.Expression_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Full_Condition_Classes.OperationInJava;
import SymbolTable.TypeEnum;
import TableManagment.SymbolTableManager;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class ExpressionJacaClass extends StatementClass {
    public ExpressionJacaClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private List<ExpressionJacaClass> expression_java;
    private OperationInJava operation_in_java;
    private NumberValueJavaClass number_value;
    private boolean is_found = false;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.expression_java != null) {
            for (ExpressionJacaClass expressionJacaClass : this.expression_java) {
                expressionJacaClass.accept(astVisitor);
            }
        }
        if (this.operation_in_java != null) {
            this.operation_in_java.accept(astVisitor);
        }
        if (this.number_value != null) {
            this.number_value.accept(astVisitor);
        }
    }

    public String getTypeOfExpression(ExpressionJacaClass expression_java) {

        if (expression_java.getNumber_value() != null) {
            if (expression_java.getNumber_value().getVariable_value_in_quotes() != null) {
                is_found = true;
            } else if (expression_java.getNumber_value().getVariable_name() != null) {
                String type = SymbolTableManager.getInstance().getTypeOfSymbol(expression_java.getNumber_value().getVariable_name());
                if(type!= null && type.equals(TypeEnum.String.toString())){
                    is_found = true;
                }
            }
        } else {
            for (ExpressionJacaClass expression : expression_java.getExpression_java()) {
                getTypeOfExpression(expression);
            }
        }
        if (is_found) {
            return TypeEnum.String.toString();
        }
        return TypeEnum.Number.toString();
    }

    // Getter And Setter

    public List<ExpressionJacaClass> getExpression_java() {
        return expression_java;
    }

    public void setExpression_java(List<ExpressionJacaClass> expression_java) {
        this.expression_java = expression_java;
    }

    public OperationInJava getOperation_in_java() {
        return operation_in_java;
    }

    public void setOperation_in_java(OperationInJava operation_in_java) {
        this.operation_in_java = operation_in_java;
    }

    public NumberValueJavaClass getNumber_value() {
        return number_value;
    }

    public void setNumber_value(NumberValueJavaClass number_value) {
        this.number_value = number_value;
    }
}
