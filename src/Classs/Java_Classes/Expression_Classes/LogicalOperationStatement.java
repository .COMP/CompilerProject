package Classs.Java_Classes.Expression_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Full_Condition_Classes.LogicalInJava;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class LogicalOperationStatement extends StatementClass {
    public LogicalOperationStatement(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private List<LogicalOperationStatement> logical_operation;
    private LogicalInJava logical_in_java;
    private String variable_name;
    private String variable_value_in_quotes;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.logical_operation != null) {
            for (LogicalOperationStatement logicalOperationStatement : this.logical_operation) {
                logicalOperationStatement.accept(astVisitor);
            }
        }
        if (this.logical_in_java != null) {
            this.logical_in_java.accept(astVisitor);
        }
    }


    public List<LogicalOperationStatement> getLogical_operation() {
        return logical_operation;
    }

    public void setLogical_operation(List<LogicalOperationStatement> logical_operation) {
        this.logical_operation = logical_operation;
    }

    public LogicalInJava getLogical_in_java() {
        return logical_in_java;
    }

    public void setLogical_in_java(LogicalInJava logical_in_java) {
        this.logical_in_java = logical_in_java;
    }

    public String getVariable_name() {
        return variable_name;
    }

    public void setVariable_name(String variable_name) {
        this.variable_name = variable_name;
    }

    public String getVariable_value_in_quotes() {
        return variable_value_in_quotes;
    }

    public void setVariable_value_in_quotes(String variable_value_in_quotes) {
        this.variable_value_in_quotes = variable_value_in_quotes;
    }
}
