package Classs.Java_Classes.Expression_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Full_Condition_Classes.ComparisonInJava;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class NumberConditionClass extends StatementClass {
    public NumberConditionClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private List<ExpressionJacaClass> exprs_java;
    private ComparisonInJava comparison_in_java;


    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.exprs_java != null) {
            for (ExpressionJacaClass expressionJacaClass : this.exprs_java) {
                expressionJacaClass.accept(astVisitor);
            }
        }
        if (this.comparison_in_java != null) {
            this.comparison_in_java.accept(astVisitor);
        }
    }


    public List<ExpressionJacaClass> getExprs_java() {
        return exprs_java;
    }

    public void setExprs_java(List<ExpressionJacaClass> exprs_java) {
        this.exprs_java = exprs_java;
    }

    public ComparisonInJava getComparison_in_java() {
        return comparison_in_java;
    }

    public void setComparison_in_java(ComparisonInJava comparison_in_java) {
        this.comparison_in_java = comparison_in_java;
    }
}
