package Classs.Java_Classes.Expression_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Callbakc_Function_Classes.CallbackFunctionWithOutScolClass;
import Classs.Java_Classes.Global_Classes.VariableNameItemInArrayClass;
import Classs.Java_Classes.Global_Classes.VariableValueClass;
import Classs.Java_Classes.Increase_Type_Classes.AdvancedIncreaseClass;
import Classs.Java_Classes.Increase_Type_Classes.DelayedIncreaseClass;
import Classs.Java_Classes.Json.Access_To_Json_Data_Classes.AccessJsonDataClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class NumberValueJavaClass extends StatementClass {
    public NumberValueJavaClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private String number_value;
    private String variable_name;
    private CallbackFunctionWithOutScolClass callback_function_without_scol;
    private AccessJsonDataClass access_json_data;
    private VariableNameItemInArrayClass var_name_item_in_array;
    private String variable_value_in_quotes;
    private AdvancedIncreaseClass advanced_increase;
    private DelayedIncreaseClass delayed_increase;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.callback_function_without_scol != null) {
            this.callback_function_without_scol.accept(astVisitor);
        }
        if (this.access_json_data != null) {
            this.access_json_data.accept(astVisitor);
        }
        if (this.var_name_item_in_array != null){
            this.var_name_item_in_array.accept(astVisitor);
        }
        if (this.advanced_increase != null) {
            this.advanced_increase.accept(astVisitor);
        }
        if (this.delayed_increase != null) {
            this.delayed_increase.accept(astVisitor);
        }
    }


    public String getNumber_value() {
        return number_value;
    }

    public void setNumber_value(String number_value) {
        this.number_value = number_value;
    }

    public String getVariable_name() {
        return variable_name;
    }

    public void setVariable_name(String variable_name) {
        this.variable_name = variable_name;
    }

    public CallbackFunctionWithOutScolClass getCallback_function_without_scol() {
        return callback_function_without_scol;
    }

    public void setCallback_function_without_scol(CallbackFunctionWithOutScolClass callback_function_without_scol) {
        this.callback_function_without_scol = callback_function_without_scol;
    }

    public AccessJsonDataClass getAccess_json_data() {
        return access_json_data;
    }

    public void setAccess_json_data(AccessJsonDataClass access_json_data) {
        this.access_json_data = access_json_data;
    }

    public VariableNameItemInArrayClass getVar_name_item_in_array() {
        return var_name_item_in_array;
    }

    public void setVar_name_item_in_array(VariableNameItemInArrayClass var_name_item_in_array) {
        this.var_name_item_in_array = var_name_item_in_array;
    }

    public String getVariable_value_in_quotes() {
        return variable_value_in_quotes;
    }

    public void setVariable_value_in_quotes(String variable_value_in_quotes) {
        this.variable_value_in_quotes = variable_value_in_quotes;
    }

    public AdvancedIncreaseClass getAdvanced_increase() {
        return advanced_increase;
    }

    public void setAdvanced_increase(AdvancedIncreaseClass advanced_increase) {
        this.advanced_increase = advanced_increase;
    }

    public DelayedIncreaseClass getDelayed_increase() {
        return delayed_increase;
    }

    public void setDelayed_increase(DelayedIncreaseClass delayed_increase) {
        this.delayed_increase = delayed_increase;
    }
}
