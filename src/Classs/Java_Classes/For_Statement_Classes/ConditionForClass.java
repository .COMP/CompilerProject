package Classs.Java_Classes.For_Statement_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Full_Condition_Classes.FullConditionClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class ConditionForClass extends StatementClass {
    public ConditionForClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private List<FullConditionClass> full_conditions;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.full_conditions != null) {
            for (FullConditionClass fullConditionClass : this.full_conditions) {
                fullConditionClass.accept(astVisitor);
            }
        }
    }

    public List<FullConditionClass> getFull_conditions() {
        return full_conditions;
    }

    public void setFull_conditions(List<FullConditionClass> full_conditions) {
        this.full_conditions = full_conditions;
    }
}
