package Classs.Java_Classes.For_Statement_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class ForParameterClass extends StatementClass {
    public ForParameterClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private InitialStartCountInForClass initial_start_count_in_for;
    private ConditionForClass condition_for;
    private IncrementCountForClass increment_count_for;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.initial_start_count_in_for != null) {
            this.initial_start_count_in_for.accept(astVisitor);
        }
        if (this.condition_for != null) {
            this.condition_for.accept(astVisitor);
        }
        if (this.increment_count_for != null) {
            this.increment_count_for.accept(astVisitor);
        }
    }

    public InitialStartCountInForClass getInitial_start_count_in_for() {
        return initial_start_count_in_for;
    }

    public void setInitial_start_count_in_for(InitialStartCountInForClass initial_start_count_in_for) {
        this.initial_start_count_in_for = initial_start_count_in_for;
    }

    public ConditionForClass getCondition_for() {
        return condition_for;
    }

    public void setCondition_for(ConditionForClass condition_for) {
        this.condition_for = condition_for;
    }

    public IncrementCountForClass getIncrement_count_for() {
        return increment_count_for;
    }

    public void setIncrement_count_for(IncrementCountForClass increment_count_for) {
        this.increment_count_for = increment_count_for;
    }
}
