package Classs.Java_Classes.For_Statement_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Increase_Type_Classes.IncrementTypeClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class IncrementCountForClass extends StatementClass {
    public IncrementCountForClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private List<IncrementTypeClass> increments_type;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.increments_type != null) {
            for (IncrementTypeClass incrementTypeClass : this.increments_type) {
                incrementTypeClass.accept(astVisitor);
            }
        }
    }

    public List<IncrementTypeClass> getIncrements_type() {
        return increments_type;
    }

    public void setIncrements_type(List<IncrementTypeClass> increments_type) {
        this.increments_type = increments_type;
    }
}
