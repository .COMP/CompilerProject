package Classs.Java_Classes.For_Statement_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Expression_Classes.NumberValueJavaClass;
import Classs.Java_Classes.Global_Classes.InitialVariableDeclarationClass;
import Visitor.AST_Visitor.AstVisitorInterface;
import org.antlr.v4.runtime.misc.Pair;

import java.util.List;

public class InitialStartCountInForClass extends StatementClass {
    public InitialStartCountInForClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private InitialVariableDeclarationClass initial_variable_declaration;
    private NumberValueJavaClass number_value;
    private List<InitialStartCountInForClass> initial_start_counts_in_for;
    private Pair<String , String > symbol_id;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.initial_variable_declaration != null) {
            this.initial_variable_declaration.accept(astVisitor);
        }
        if (this.number_value != null) {
            this.number_value.accept(astVisitor);
        }
        if (this.initial_start_counts_in_for != null) {
            for (InitialStartCountInForClass initialStartCountInForClass : this.initial_start_counts_in_for) {
                initialStartCountInForClass.accept(astVisitor);
            }
        }
    }

    public InitialVariableDeclarationClass getInitial_variable_declaration() {
        return initial_variable_declaration;
    }

    public void setInitial_variable_declaration(InitialVariableDeclarationClass initial_variable_declaration) {
        this.initial_variable_declaration = initial_variable_declaration;
    }

    public NumberValueJavaClass getNumber_value() {
        return number_value;
    }

    public void setNumber_value(NumberValueJavaClass number_value) {
        this.number_value = number_value;
    }

    public List<InitialStartCountInForClass> getInitial_start_counts_in_for() {
        return initial_start_counts_in_for;
    }

    public void setInitial_start_counts_in_for(List<InitialStartCountInForClass> initial_start_counts_in_for) {
        this.initial_start_counts_in_for = initial_start_counts_in_for;
    }

    public Pair<String, String> getSymbol_id() {
        return symbol_id;
    }

    public void setSymbol_id(Pair<String, String> symbol_id) {
        this.symbol_id = symbol_id;
    }
}
