//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package Classs.Java_Classes.Full_Condition_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class ComparisonInJava extends StatementClass {
    public ComparisonInJava(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private String operation;

    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
    }


    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }
}
