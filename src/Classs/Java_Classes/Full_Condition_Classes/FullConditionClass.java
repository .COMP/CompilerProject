package Classs.Java_Classes.Full_Condition_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class FullConditionClass extends StatementClass {
    public FullConditionClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private OneConditionJavaClass one_condition_java;
    private List<MultiConditionJavaClass> multi_condition_java;
    private FullConditionClass full_condition;


    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.one_condition_java != null) {
            this.one_condition_java.accept(astVisitor);
        }
        if (this.multi_condition_java != null) {
            for (MultiConditionJavaClass multiConditionJavaClass : this.multi_condition_java) {
                multiConditionJavaClass.accept(astVisitor);
            }
        }
        if (this.full_condition != null) {
            this.full_condition.accept(astVisitor);
        }
    }

    // Getter And Setter

    public OneConditionJavaClass getOne_condition_java() {
        return one_condition_java;
    }

    public void setOne_condition_java(OneConditionJavaClass one_condition_java) {
        this.one_condition_java = one_condition_java;
    }

    public List<MultiConditionJavaClass> getMulti_condition_java() {
        return multi_condition_java;
    }

    public void setMulti_condition_java(List<MultiConditionJavaClass> multi_condition_java) {
        this.multi_condition_java = multi_condition_java;
    }

    public FullConditionClass getFull_condition() {
        return full_condition;
    }

    public void setFull_condition(FullConditionClass full_condition) {
        this.full_condition = full_condition;
    }
}
