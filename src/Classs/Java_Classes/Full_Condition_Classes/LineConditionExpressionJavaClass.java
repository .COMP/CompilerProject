package Classs.Java_Classes.Full_Condition_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class LineConditionExpressionJavaClass extends StatementClass {
    public LineConditionExpressionJavaClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private FullConditionClass full_condition;
    private List<LineConditionResultClass> lines_condition_result;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.full_condition != null) {
            this.full_condition.accept(astVisitor);
        }
        if (this.lines_condition_result != null) {
            for (LineConditionResultClass lineConditionResultClass : this.lines_condition_result) {
                lineConditionResultClass.accept(astVisitor);
            }
        }
    }

    public FullConditionClass getFull_condition() {
        return full_condition;
    }

    public void setFull_condition(FullConditionClass full_condition) {
        this.full_condition = full_condition;
    }

    public List<LineConditionResultClass> getLines_condition_result() {
        return lines_condition_result;
    }

    public void setLines_condition_result(List<LineConditionResultClass> lines_condition_result) {
        this.lines_condition_result = lines_condition_result;
    }
}
