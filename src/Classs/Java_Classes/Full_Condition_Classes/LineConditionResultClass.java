package Classs.Java_Classes.Full_Condition_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Global_Classes.VariableValueClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class LineConditionResultClass extends StatementClass {
    public LineConditionResultClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private VariableValueClass variable_value;
    private LineConditionExpressionJavaClass line_condition_expression;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.variable_value != null) {
            this.variable_value.accept(astVisitor);
        }
        if (this.line_condition_expression != null) {
            this.line_condition_expression.accept(astVisitor);
        }
    }

    public VariableValueClass getVariable_value() {
        return variable_value;
    }

    public void setVariable_value(VariableValueClass variable_value) {
        this.variable_value = variable_value;
    }

    public LineConditionExpressionJavaClass getLine_condition_expression() {
        return line_condition_expression;
    }

    public void setLine_condition_expression(LineConditionExpressionJavaClass line_condition_expression) {
        this.line_condition_expression = line_condition_expression;
    }
}
