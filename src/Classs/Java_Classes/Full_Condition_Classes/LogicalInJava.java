package Classs.Java_Classes.Full_Condition_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class LogicalInJava extends StatementClass {
    public LogicalInJava(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private String operation;


    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }
}
