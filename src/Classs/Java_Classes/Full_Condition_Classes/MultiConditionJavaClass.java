package Classs.Java_Classes.Full_Condition_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class MultiConditionJavaClass extends StatementClass {
    public MultiConditionJavaClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private LogicalInJava logical_in_java;
    private OneConditionJavaClass ond_condition_java;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.ond_condition_java != null) {
            this.ond_condition_java.accept(astVisitor);
        }
        if (this.logical_in_java != null) {
            this.logical_in_java.accept(astVisitor);
        }
    }

    public LogicalInJava getLogical_in_java() {
        return logical_in_java;
    }

    public void setLogical_in_java(LogicalInJava logical_in_java) {
        this.logical_in_java = logical_in_java;
    }

    public OneConditionJavaClass getOnd_condition_java() {
        return ond_condition_java;
    }

    public void setOnd_condition_java(OneConditionJavaClass ond_condition_java) {
        this.ond_condition_java = ond_condition_java;
    }
}
