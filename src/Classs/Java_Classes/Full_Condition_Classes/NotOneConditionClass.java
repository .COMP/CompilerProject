package Classs.Java_Classes.Full_Condition_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class NotOneConditionClass extends StatementClass {
    public NotOneConditionClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private String operation;
    private OneConditionJavaClass one_condition_java;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.one_condition_java != null) {
            this.one_condition_java.accept(astVisitor);
        }
    }


    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public OneConditionJavaClass getOne_condition_java() {
        return one_condition_java;
    }

    public void setOne_condition_java(OneConditionJavaClass one_condition_java) {
        this.one_condition_java = one_condition_java;
    }
}
