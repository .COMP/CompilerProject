package Classs.Java_Classes.Full_Condition_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Callbakc_Function_Classes.CallbackFunctionWithOutScolClass;
import Classs.Java_Classes.Expression_Classes.LogicalConditionClass;
import Classs.Java_Classes.Expression_Classes.NumberConditionClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class OneConditionJavaClass extends StatementClass {
    public OneConditionJavaClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private NotVarNameClass not_var_name;
    private NotOneConditionClass not_one_condition;
    private NumberConditionClass number_condition;
    private LogicalConditionClass logical_condition;
    private CallbackFunctionWithOutScolClass callback_with_out_scol;
    private String operand;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.not_var_name != null) {
            this.not_var_name.accept(astVisitor);
        }
        if (this.not_one_condition != null) {
            this.not_one_condition.accept(astVisitor);
        }
        if (this.number_condition != null) {
            this.number_condition.accept(astVisitor);
        }
        if (this.logical_condition != null) {
            this.logical_condition.accept(astVisitor);
        }
        if (this.callback_with_out_scol != null) {
            this.callback_with_out_scol.accept(astVisitor);
        }
    }


    // Getter And Setter

    public NotVarNameClass getNot_var_name() {
        return not_var_name;
    }

    public void setNot_var_name(NotVarNameClass not_var_name) {
        this.not_var_name = not_var_name;
    }

    public NotOneConditionClass getNot_one_condition() {
        return not_one_condition;
    }

    public void setNot_one_condition(NotOneConditionClass not_one_condition) {
        this.not_one_condition = not_one_condition;
    }

    public NumberConditionClass getNumber_condition() {
        return number_condition;
    }

    public void setNumber_condition(NumberConditionClass number_condition) {
        this.number_condition = number_condition;
    }

    public LogicalConditionClass getLogical_condition() {
        return logical_condition;
    }

    public void setLogical_condition(LogicalConditionClass logical_condition) {
        this.logical_condition = logical_condition;
    }

    public CallbackFunctionWithOutScolClass getCallback_with_out_scol() {
        return callback_with_out_scol;
    }

    public void setCallback_with_out_scol(CallbackFunctionWithOutScolClass callback_with_out_scol) {
        this.callback_with_out_scol = callback_with_out_scol;
    }

    public String getOperand() {
        return operand;
    }

    public void setOperand(String operand) {
        this.operand = operand;
    }
}
