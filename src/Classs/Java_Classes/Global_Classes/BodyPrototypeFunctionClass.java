package Classs.Java_Classes.Global_Classes;

import Visitor.AST_Visitor.AstVisitorInterface;
import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Return_Classes.ReturnStatementClass;

import java.util.List;

public class BodyPrototypeFunctionClass extends StatementClass {
    public BodyPrototypeFunctionClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private List<JavaStatementClass> java_statements;
    private ReturnStatementClass return_statement;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.java_statements != null) {
            for (JavaStatementClass java_statement : this.java_statements) {
                java_statement.accept(astVisitor);
            }
        }
        if (this.return_statement != null) {
            return_statement.accept(astVisitor);
        }
    }

    public List<JavaStatementClass> getJava_statements() {
        return java_statements;
    }

    public void setJava_statements(List<JavaStatementClass> java_statements) {
        this.java_statements = java_statements;
    }

    public ReturnStatementClass getReturn_statement() {
        return return_statement;
    }

    public void setReturn_statement(ReturnStatementClass return_statement) {
        this.return_statement = return_statement;
    }
}
