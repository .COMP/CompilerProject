package Classs.Java_Classes.Global_Classes;

import Visitor.AST_Visitor.AstVisitorInterface;
import Classs.Abstruction_Classes.StatementClass;

public class InitialVariableDeclarationClass extends StatementClass {
    public InitialVariableDeclarationClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private String variable_name;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
    }

    public String getVariable_name() {
        return variable_name;
    }

    public void setVariable_name(String variable_name) {
        this.variable_name = variable_name;
    }

}
