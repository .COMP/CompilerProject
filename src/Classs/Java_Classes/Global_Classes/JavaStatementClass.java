package Classs.Java_Classes.Global_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Break_Continue_Classes.BreakStatementClass;
import Classs.Java_Classes.Break_Continue_Classes.ContinueStatementClass;
import Classs.Java_Classes.Callbakc_Function_Classes.CallbackFunctionClass;
import Classs.Java_Classes.Create_Variable_Statement_Classes.AssignVariableWithoutVarClass;
import Classs.Java_Classes.Create_Variable_Statement_Classes.CreateVariableStatementClass;
import Classs.Java_Classes.Do_While_Statement_Classes.DoWhileStatementClass;
import Classs.Java_Classes.For_Statement_Classes.ForStatementClass;
import Classs.Java_Classes.Foreach_Statement_Classess.ForeachStatementClass;
import Classs.Java_Classes.If_Statement_Classes.IfStatementClass;
import Classs.Java_Classes.Increase_Type_Classes.IncrementStatementClass;
import Classs.Java_Classes.Print_Statement_Classes.PrintStatementClass;
import Classs.Java_Classes.Scope_Statement_Classes.ScopeStatementClass;
import Classs.Java_Classes.Switch_Statement_Classes.SwitchStatementClass;
import Classs.Java_Classes.While_Statement_Classes.WhileStatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class JavaStatementClass extends StatementClass {
    public JavaStatementClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private CreateVariableStatementClass create_variable_statement;
    private AssignVariableWithoutVarClass assign_variable_with_out_var;
    private IfStatementClass if_statement;
    private ForStatementClass for_statement;
    private WhileStatementClass while_statement;
    private DoWhileStatementClass do_while_statement;
    private CallbackFunctionClass callback_function;
    private SwitchStatementClass switch_statement;
    private ForeachStatementClass foreach_statement;
    private PrintStatementClass print_statement;
    private BreakStatementClass break_statement;
    private ContinueStatementClass continue_statement;
    private IncrementStatementClass increment_statement;
    ScopeStatementClass scope_statement;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.create_variable_statement != null) {
            this.create_variable_statement.accept(astVisitor);
        }
        if (this.for_statement != null) {
            this.for_statement.accept(astVisitor);
        }
        if (this.if_statement != null) {
            this.if_statement.accept(astVisitor);
        }
        if (this.callback_function != null) {
            this.callback_function.accept(astVisitor);
        }
        if (this.while_statement != null) {
            this.while_statement.accept(astVisitor);
        }
        if (this.do_while_statement != null) {
            this.do_while_statement.accept(astVisitor);
        }
        if (this.switch_statement != null) {
            this.switch_statement.accept(astVisitor);
        }
        if (this.foreach_statement != null) {
            this.foreach_statement.accept(astVisitor);
        }
        if (this.print_statement != null) {
            this.print_statement.accept(astVisitor);
        }
        if (this.break_statement != null) {
            this.break_statement.accept(astVisitor);
        }
        if (this.continue_statement != null) {
            this.continue_statement.accept(astVisitor);
        }
        if (this.assign_variable_with_out_var != null) {
            this.assign_variable_with_out_var.accept(astVisitor);
        }
        if (this.increment_statement != null) {
            this.increment_statement.accept(astVisitor);
        }
        if (this.scope_statement != null) {
            this.scope_statement.accept(astVisitor);
        }
    }

    // Getter And Setter
    public CreateVariableStatementClass getCreate_variable_statement() {
        return create_variable_statement;
    }

    public void setCreate_variable_statement(CreateVariableStatementClass create_variable_statement) {
        this.create_variable_statement = create_variable_statement;
    }

    public AssignVariableWithoutVarClass getAssign_variable_with_out_var() {
        return assign_variable_with_out_var;
    }

    public void setAssign_variable_with_out_var(AssignVariableWithoutVarClass assign_variable_with_out_var) {
        this.assign_variable_with_out_var = assign_variable_with_out_var;
    }

    public IfStatementClass getIf_statement() {
        return if_statement;
    }

    public void setIf_statement(IfStatementClass if_statement) {
        this.if_statement = if_statement;
    }

    public ForStatementClass getFor_statement() {
        return for_statement;
    }

    public void setFor_statement(ForStatementClass for_statement) {
        this.for_statement = for_statement;
    }

    public WhileStatementClass getWhile_statement() {
        return while_statement;
    }

    public void setWhile_statement(WhileStatementClass while_statement) {
        this.while_statement = while_statement;
    }

    public DoWhileStatementClass getDo_while_statement() {
        return do_while_statement;
    }

    public void setDo_while_statement(DoWhileStatementClass do_while_statement) {
        this.do_while_statement = do_while_statement;
    }

    public CallbackFunctionClass getCallback_function() {
        return callback_function;
    }

    public void setCallback_function(CallbackFunctionClass callback_function) {
        this.callback_function = callback_function;
    }

    public SwitchStatementClass getSwitch_statement() {
        return switch_statement;
    }

    public void setSwitch_statement(SwitchStatementClass switch_statement) {
        this.switch_statement = switch_statement;
    }

    public ForeachStatementClass getForeach_statement() {
        return foreach_statement;
    }

    public void setForeach_statement(ForeachStatementClass foreach_statement) {
        this.foreach_statement = foreach_statement;
    }

    public PrintStatementClass getPrint_statement() {
        return print_statement;
    }

    public void setPrint_statement(PrintStatementClass print_statement) {
        this.print_statement = print_statement;
    }

    public BreakStatementClass getBreak_statement() {
        return break_statement;
    }

    public void setBreak_statement(BreakStatementClass break_statement) {
        this.break_statement = break_statement;
    }

    public ContinueStatementClass getContinue_statement() {
        return continue_statement;
    }

    public void setContinue_statement(ContinueStatementClass continue_statement) {
        this.continue_statement = continue_statement;
    }

    public IncrementStatementClass getIncrement_statement() {
        return increment_statement;
    }

    public void setIncrement_statement(IncrementStatementClass increment_statement) {
        this.increment_statement = increment_statement;
    }

    public ScopeStatementClass getScope_statement() {
        return scope_statement;
    }

    public void setScope_statement(ScopeStatementClass scope_statement) {
        this.scope_statement = scope_statement;
    }
}
