package Classs.Java_Classes.Global_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class ValueOfOneItemArrayClass extends StatementClass {
    public ValueOfOneItemArrayClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private VariableValueClass variable_value;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.variable_value != null){
            this.variable_value.accept(astVisitor);
        }
    }

    public VariableValueClass getVariable_value() {
        return variable_value;
    }

    public void setVariable_value(VariableValueClass variable_value) {
        this.variable_value = variable_value;
    }
}
