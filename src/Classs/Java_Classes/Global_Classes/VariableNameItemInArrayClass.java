package Classs.Java_Classes.Global_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class VariableNameItemInArrayClass extends StatementClass {
    public VariableNameItemInArrayClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private String variable_item_name;
    private ValueOfOneItemArrayClass value_of_one_item_array;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.value_of_one_item_array != null) {
            this.value_of_one_item_array.accept(astVisitor);
        }
    }

    public String getVariable_item_name() {
        return variable_item_name;
    }

    public void setVariable_item_name(String variable_item_name) {
        this.variable_item_name = variable_item_name;
    }

    public ValueOfOneItemArrayClass getValue_of_one_item_array() {
        return value_of_one_item_array;
    }

    public void setValue_of_one_item_array(ValueOfOneItemArrayClass value_of_one_item_array) {
        this.value_of_one_item_array = value_of_one_item_array;
    }
}
