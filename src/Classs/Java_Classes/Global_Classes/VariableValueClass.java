package Classs.Java_Classes.Global_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Callbakc_Function_Classes.CallbackFunctionWithOutScolClass;
import Classs.Java_Classes.Create_Variable_Statement_Classes.ArrayValueClass;
import Classs.Java_Classes.Expression_Classes.ExpressionJacaClass;
import Classs.Java_Classes.Expression_Classes.LogicalConditionClass;
import Classs.Java_Classes.Expression_Classes.NumberConditionClass;
import Classs.Java_Classes.Full_Condition_Classes.LineConditionExpressionJavaClass;
import Classs.Java_Classes.Increase_Type_Classes.IncrementTypeClass;
import Classs.Java_Classes.Json.Access_To_Json_Data_Classes.AccessJsonDataClass;
import Classs.Java_Classes.Json.Array_Json_Declaration_Classes.ArrayJsonDeclarationClass;
import Classs.Java_Classes.Json.Json_Declaration_Classes.JsonDeclarationClass;
import Classs.SQL_Classes.Select_Statement.SelectStmtClass;
import SymbolTable.TypeEnum;
import Visitor.AST_Visitor.AstVisitorInterface;

public class VariableValueClass extends StatementClass {
    public VariableValueClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    public String type_value;
    private String number_literal;
    private String variable_name;
    private String true_false_null_value;
    private String value_in_quote;
    private VariableNameItemInArrayClass variable_name_item_in_array;
    private CallbackFunctionWithOutScolClass callback_function_without_scol;
    private ArrayValueClass array_value;
    private ExpressionJacaClass expression_java;
    private NumberConditionClass number_condition;
    private LogicalConditionClass logical_condition;
    private IncrementTypeClass increment_type;
    private LineConditionExpressionJavaClass line_condition_expression;
    private JsonDeclarationClass json_declaration;
    private ArrayJsonDeclarationClass array_json_declaration;
    private SelectStmtClass select_statement;
    private AccessJsonDataClass access_json_data;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.variable_name_item_in_array != null) {
            this.variable_name_item_in_array.accept(astVisitor);
        }
        if (this.callback_function_without_scol != null) {
            this.callback_function_without_scol.accept(astVisitor);
        }
        if (this.array_value != null) {
            this.array_value.accept(astVisitor);
        }
        if (this.expression_java != null) {
            this.expression_java.accept(astVisitor);
        }
        if (this.number_condition != null) {
            this.number_condition.accept(astVisitor);
        }
        if (this.logical_condition != null) {
            this.logical_condition.accept(astVisitor);
        }
        if (this.increment_type != null) {
            this.increment_type.accept(astVisitor);
        }
        if (this.line_condition_expression != null) {
            this.line_condition_expression.accept(astVisitor);
        }
        if (this.json_declaration != null) {
            this.json_declaration.accept(astVisitor);
        }
        if (this.array_json_declaration != null) {
            this.array_json_declaration.accept(astVisitor);
        }
        if (this.select_statement != null) {
            this.select_statement.accept(astVisitor);
        }
        if (this.access_json_data != null) {
            this.access_json_data.accept(astVisitor);
        }
    }

    public SelectStmtClass getSelect_statement() {
        return select_statement;
    }

    public void setSelect_statement(SelectStmtClass select_statement) {
        this.select_statement = select_statement;
    }

    public String getVariable_name() {
        return variable_name;
    }

    public void setVariable_name(String variable_name) {
        this.variable_name = variable_name;
    }

    public String getNumber_literal() {
        return number_literal;
    }

    public void setNumber_literal(String number_literal) {
        this.number_literal = number_literal;
    }

    public CallbackFunctionWithOutScolClass getCallback_function_without_scol() {
        return callback_function_without_scol;
    }

    public void setCallback_function_without_scol(CallbackFunctionWithOutScolClass callback_function_without_scol) {
        this.callback_function_without_scol = callback_function_without_scol;
    }

    public NumberConditionClass getNumber_condition() {
        return number_condition;
    }

    public void setNumber_condition(NumberConditionClass number_condition) {
        this.number_condition = number_condition;
    }

    public LogicalConditionClass getLogical_condition() {
        return logical_condition;
    }

    public void setLogical_condition(LogicalConditionClass logical_condition) {
        this.logical_condition = logical_condition;
    }

    public String getValue_in_quote() {
        return value_in_quote;
    }

    public void setValue_in_quote(String value_in_quote) {
        this.value_in_quote = value_in_quote;
    }

    public IncrementTypeClass getIncrement_type() {
        return increment_type;
    }

    public void setIncrement_type(IncrementTypeClass increment_type) {
        this.increment_type = increment_type;
    }

    public LineConditionExpressionJavaClass getLine_condition_expression() {
        return line_condition_expression;
    }

    public void setLine_condition_expression(LineConditionExpressionJavaClass line_condition_expression) {
        this.line_condition_expression = line_condition_expression;
    }

    public VariableNameItemInArrayClass getVariable_name_item_in_array() {
        return variable_name_item_in_array;
    }

    public void setVariable_name_item_in_array(VariableNameItemInArrayClass variable_name_item_in_array) {
        this.variable_name_item_in_array = variable_name_item_in_array;
    }

    public ArrayValueClass getArray_value() {
        return array_value;
    }

    public void setArray_value(ArrayValueClass array_value) {
        this.array_value = array_value;
    }

    public ExpressionJacaClass getExpression_java() {
        return expression_java;
    }

    public void setExpression_java(ExpressionJacaClass expression_java) {
        this.expression_java = expression_java;
    }

    public JsonDeclarationClass getJson_declaration() {
        return json_declaration;
    }

    public void setJson_declaration(JsonDeclarationClass json_declaration) {
        this.json_declaration = json_declaration;
    }

    public ArrayJsonDeclarationClass getArray_json_declaration() {
        return array_json_declaration;
    }

    public void setArray_json_declaration(ArrayJsonDeclarationClass array_json_declaration) {
        this.array_json_declaration = array_json_declaration;
    }

    public String getTrue_false_null_value() {
        return true_false_null_value;
    }

    public void setTrue_false_null_value(String true_false_null_value) {
        this.true_false_null_value = true_false_null_value;
    }

    public AccessJsonDataClass getAccess_json_data() {
        return access_json_data;
    }

    public void setAccess_json_data(AccessJsonDataClass access_json_data) {
        this.access_json_data = access_json_data;
    }

    public String getType_value() {
        return type_value;
    }

    public void setType_value(String type_value) {
        this.type_value = type_value;
    }
}
