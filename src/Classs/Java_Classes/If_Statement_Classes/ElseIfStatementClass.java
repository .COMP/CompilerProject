package Classs.Java_Classes.If_Statement_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Global_Classes.BodyFunctionClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class ElseIfStatementClass extends StatementClass {
    public ElseIfStatementClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private HeaderIfClass header_if;
    private BodyFunctionClass body_function;
    private String scope_id;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.header_if != null) {
            this.header_if.accept(astVisitor);
        }
        if (this.body_function != null) {
            this.body_function.accept(astVisitor);
        }
    }

    public HeaderIfClass getHeader_if() {
        return header_if;
    }

    public void setHeader_if(HeaderIfClass header_if) {
        this.header_if = header_if;
    }

    public BodyFunctionClass getBody_function() {
        return body_function;
    }

    public void setBody_function(BodyFunctionClass body_function) {
        this.body_function = body_function;
    }

    public String getScope_id() {
        return scope_id;
    }

    public void setScope_id(String scope_id) {
        this.scope_id = scope_id;
    }
}
