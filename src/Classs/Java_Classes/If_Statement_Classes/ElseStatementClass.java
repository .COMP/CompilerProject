package Classs.Java_Classes.If_Statement_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Global_Classes.BodyFunctionClass;
import Classs.Java_Classes.Global_Classes.JavaStatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class ElseStatementClass extends StatementClass {
    public ElseStatementClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private JavaStatementClass java_statement;
    private BodyFunctionClass body_function;
    private String scope_id;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.java_statement != null) {
            this.java_statement.accept(astVisitor);
        }
        if (this.body_function != null) {
            this.body_function.accept(astVisitor);
        }
    }

    public JavaStatementClass getJava_statement() {
        return java_statement;
    }

    public void setJava_statement(JavaStatementClass java_statement) {
        this.java_statement = java_statement;
    }

    public BodyFunctionClass getBody_function() {
        return body_function;
    }

    public void setBody_function(BodyFunctionClass body_function) {
        this.body_function = body_function;
    }

    public String getScope_id() {
        return scope_id;
    }

    public void setScope_id(String scope_id) {
        this.scope_id = scope_id;
    }
}
