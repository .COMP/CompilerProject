package Classs.Java_Classes.If_Statement_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Full_Condition_Classes.FullConditionClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class HeaderIfClass extends StatementClass {
    public HeaderIfClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private FullConditionClass full_condition;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.full_condition != null) {
            this.full_condition.accept(astVisitor);
        }
    }

    // Getter And Setter

    public FullConditionClass getFull_condition() {
        return full_condition;
    }

    public void setFull_condition(FullConditionClass full_condition) {
        this.full_condition = full_condition;
    }
}
