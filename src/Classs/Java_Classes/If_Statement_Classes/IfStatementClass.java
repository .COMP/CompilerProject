package Classs.Java_Classes.If_Statement_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Global_Classes.BodyFunctionClass;
import Classs.Java_Classes.Global_Classes.JavaStatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class IfStatementClass extends StatementClass {
    public IfStatementClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private HeaderIfClass header_if;
    private JavaStatementClass java_statement;
    private BodyFunctionClass body_function;
    private List<ElseIfStatementClass> else_if_statement;
    private ElseStatementClass else_statement;
    private String scope_id;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.header_if != null) {
            this.header_if.accept(astVisitor);
        }
        if (this.java_statement != null) {
            this.java_statement.accept(astVisitor);
        }
        if (this.body_function != null) {
            this.body_function.accept(astVisitor);
        }
        if (this.else_if_statement != null) {
            for (ElseIfStatementClass elseIfStatementClass : this.else_if_statement) {
                elseIfStatementClass.accept(astVisitor);
            }
        }
        if (this.else_statement != null) {
            this.else_statement.accept(astVisitor);
        }
    }

    // Getter And Setter

    public HeaderIfClass getHeader_if() {
        return header_if;
    }

    public void setHeader_if(HeaderIfClass header_if) {
        this.header_if = header_if;
    }

    public JavaStatementClass getJava_statement() {
        return java_statement;
    }

    public void setJava_statement(JavaStatementClass java_statement) {
        this.java_statement = java_statement;
    }

    public BodyFunctionClass getBody_function() {
        return body_function;
    }

    public void setBody_function(BodyFunctionClass body_function) {
        this.body_function = body_function;
    }

    public List<ElseIfStatementClass> getElse_if_statement() {
        return else_if_statement;
    }

    public void setElse_if_statement(List<ElseIfStatementClass> else_if_statement) {
        this.else_if_statement = else_if_statement;
    }

    public ElseStatementClass getElse_statement() {
        return else_statement;
    }

    public void setElse_statement(ElseStatementClass else_statement) {
        this.else_statement = else_statement;
    }

    public String getScope_id() {
        return scope_id;
    }

    public void setScope_id(String scope_id) {
        this.scope_id = scope_id;
    }
}
