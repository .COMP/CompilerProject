package Classs.Java_Classes.Increase_Type_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Operation_Classes.ContentIncreaseOperation;
import Visitor.AST_Visitor.AstVisitorInterface;

public class AdvancedIncreaseClass extends StatementClass {
    public AdvancedIncreaseClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private ContentIncreaseOperation advanced_increase;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
    }

    public ContentIncreaseOperation getAdvanced_increase() {
        return advanced_increase;
    }

    public void setAdvanced_increase(ContentIncreaseOperation advanced_increase) {
        this.advanced_increase = advanced_increase;
    }
}
