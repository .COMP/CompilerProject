package Classs.Java_Classes.Increase_Type_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Operation_Classes.ContentIncreaseOperation;
import Visitor.AST_Visitor.AstVisitorInterface;

public class DelayedIncreaseClass extends StatementClass {
    public DelayedIncreaseClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private ContentIncreaseOperation delayed_operation;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
    }

    public ContentIncreaseOperation getDelayed_operation() {
        return delayed_operation;
    }

    public void setDelayed_operation(ContentIncreaseOperation delayed_operation) {
        this.delayed_operation = delayed_operation;
    }
}
