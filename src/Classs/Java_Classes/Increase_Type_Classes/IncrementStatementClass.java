package Classs.Java_Classes.Increase_Type_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class IncrementStatementClass extends StatementClass {
    public IncrementStatementClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private IncrementTypeClass increment_type;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.increment_type != null) {
            this.increment_type.accept(astVisitor);
        }
    }

    public IncrementTypeClass getIncrement_type() {
        return increment_type;
    }

    public void setIncrement_type(IncrementTypeClass increment_type) {
        this.increment_type = increment_type;
    }
}
