package Classs.Java_Classes.Increase_Type_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class IncrementTypeClass extends StatementClass {
    public IncrementTypeClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private AdvancedIncreaseClass advanced_increment;
    private DelayedIncreaseClass delayed_increment;
    private SpecificIncreaseClass specific_increment;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.advanced_increment != null) {
            this.advanced_increment.accept(astVisitor);
        }
        if (this.delayed_increment != null) {
            this.delayed_increment.accept(astVisitor);
        }
        if (this.specific_increment != null) {
            this.specific_increment.accept(astVisitor);
        }
    }

    public AdvancedIncreaseClass getAdvanced_increment() {
        return advanced_increment;
    }

    public void setAdvanced_increment(AdvancedIncreaseClass advanced_increment) {
        this.advanced_increment = advanced_increment;
    }

    public DelayedIncreaseClass getDelayed_increment() {
        return delayed_increment;
    }

    public void setDelayed_increment(DelayedIncreaseClass delayed_increment) {
        this.delayed_increment = delayed_increment;
    }

    public SpecificIncreaseClass getSpecific_increment() {
        return specific_increment;
    }

    public void setSpecific_increment(SpecificIncreaseClass specific_increment) {
        this.specific_increment = specific_increment;
    }
}
