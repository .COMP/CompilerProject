package Classs.Java_Classes.Increase_Type_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Expression_Classes.ExpressionJacaClass;
import Classs.Operation_Classes.ContentIncreaseOperation;
import Visitor.AST_Visitor.AstVisitorInterface;

public class SpecificIncreaseClass extends StatementClass {
    public SpecificIncreaseClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private ContentIncreaseOperation operation;
    private ExpressionJacaClass expr_java;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.expr_java != null) {
            this.expr_java.accept(astVisitor);
        }
    }

    public ContentIncreaseOperation getOperation() {
        return operation;
    }

    public void setOperation(ContentIncreaseOperation operation) {
        this.operation = operation;
    }

    public ExpressionJacaClass getExpr_java() {
        return expr_java;
    }

    public void setExpr_java(ExpressionJacaClass expr_java) {
        this.expr_java = expr_java;
    }
}
