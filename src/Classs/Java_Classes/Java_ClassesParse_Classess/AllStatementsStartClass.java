package Classs.Java_Classes.Java_ClassesParse_Classess;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.All_Statement_Start_Classes.JavaStatementsListClass;
import Classs.SQL_Classes.SqlStmtListClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class AllStatementsStartClass extends StatementClass {
    public AllStatementsStartClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private JavaStatementsListClass java_statements_list;
    private SqlStmtListClass sqlStmtListClass;

    public SqlStmtListClass getSqlStmtListClass() {
        return sqlStmtListClass;
    }

    public void setSqlStmtListClass(SqlStmtListClass sqlStmtListClass) {
        this.sqlStmtListClass = sqlStmtListClass;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.java_statements_list != null) {
            this.java_statements_list.accept(astVisitor);
        }else {
            sqlStmtListClass.accept(astVisitor);
        }
        // SQL Statement
    }

    // Getter And Setter
    public JavaStatementsListClass getJava_statements_list() {
        return java_statements_list;
    }

    public void setJava_statements_list(JavaStatementsListClass java_statements_list) {
        this.java_statements_list = java_statements_list;
    }
}
