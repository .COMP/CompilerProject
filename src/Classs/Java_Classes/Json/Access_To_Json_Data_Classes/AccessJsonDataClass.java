package Classs.Java_Classes.Json.Access_To_Json_Data_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Global_Classes.VariableValueClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class AccessJsonDataClass extends StatementClass {
    public AccessJsonDataClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private String variable_name;
    private VariableValueClass variable_value;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.variable_value != null) {
            this.variable_value.accept(astVisitor);
        }
    }

    public String getVariable_name() {
        return variable_name;
    }

    public void setVariable_name(String variable_name) {
        this.variable_name = variable_name;
    }

    public VariableValueClass getVariable_value() {
        return variable_value;
    }

    public void setVariable_value(VariableValueClass variable_value) {
        this.variable_value = variable_value;
    }
}
