package Classs.Java_Classes.Json.Array_Json_Declaration_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Json.Json_Declaration_Classes.JsonDeclarationClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class ArrayJsonDeclarationClass extends StatementClass {
    public ArrayJsonDeclarationClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private List<JsonDeclarationClass> jsons_declaration;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.jsons_declaration != null) {
            for (JsonDeclarationClass jsonDeclarationClass : this.jsons_declaration) {
                jsonDeclarationClass.accept(astVisitor);
            }
        }
    }

    public List<JsonDeclarationClass> getJsons_declaration() {
        return jsons_declaration;
    }

    public void setJsons_declaration(List<JsonDeclarationClass> jsons_declaration) {
        this.jsons_declaration = jsons_declaration;
    }
}
