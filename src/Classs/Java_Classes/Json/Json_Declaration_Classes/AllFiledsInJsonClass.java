package Classs.Java_Classes.Json.Json_Declaration_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class AllFiledsInJsonClass extends StatementClass {
    public AllFiledsInJsonClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private List<FiledInJsonClass> fileds_in_json;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.fileds_in_json != null) {
            for (FiledInJsonClass filedsInJsonClass : this.fileds_in_json) {
                filedsInJsonClass.accept(astVisitor);
            }
        }
    }

    public List<FiledInJsonClass> getFileds_in_json() {
        return fileds_in_json;
    }

    public void setFileds_in_json(List<FiledInJsonClass> fileds_in_json) {
        this.fileds_in_json = fileds_in_json;
    }
}
