package Classs.Java_Classes.Json.Json_Declaration_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Global_Classes.VariableValueClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class FiledInJsonClass extends StatementClass {
    public FiledInJsonClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private KeyFiledClass key_filed;
    private VariableValueClass value_filed;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.key_filed != null) {
            this.key_filed.accept(astVisitor);
        }
        if (value_filed != null) {
            this.value_filed.accept(astVisitor);
        }
    }

    public KeyFiledClass getKey_filed() {
        return key_filed;
    }

    public void setKey_filed(KeyFiledClass key_filed) {
        this.key_filed = key_filed;
    }

    public VariableValueClass getValue_filed() {
        return value_filed;
    }

    public void setValue_filed(VariableValueClass value_filed) {
        this.value_filed = value_filed;
    }
}
