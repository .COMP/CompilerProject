package Classs.Java_Classes.Json.Json_Declaration_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class JsonDeclarationClass extends StatementClass {
    public JsonDeclarationClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private AllFiledsInJsonClass all_filed_in_json;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.all_filed_in_json != null) {
            this.all_filed_in_json.accept(astVisitor);
        }
    }

    public AllFiledsInJsonClass getAll_filed_in_json() {
        return all_filed_in_json;
    }

    public void setAll_filed_in_json(AllFiledsInJsonClass all_filed_in_json) {
        this.all_filed_in_json = all_filed_in_json;
    }
}
