package Classs.Java_Classes.Json.Json_Declaration_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class KeyFiledClass extends StatementClass {
    public KeyFiledClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private String key_name;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
    }

    public String getKey_name() {
        return key_name;
    }

    public void setKey_name(String key_name) {
        this.key_name = key_name;
    }
}
