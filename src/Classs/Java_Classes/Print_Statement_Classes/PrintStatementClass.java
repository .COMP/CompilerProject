package Classs.Java_Classes.Print_Statement_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Global_Classes.VariableValueClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class PrintStatementClass extends StatementClass {
    public PrintStatementClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private List<VariableValueClass> variable_value;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.variable_value != null) {
            for (VariableValueClass variableValueClass : variable_value) {
                variableValueClass.accept(astVisitor);
            }
        }
    }

    public List<VariableValueClass> getVariable_value() {
        return variable_value;
    }

    public void setVariable_value(List<VariableValueClass> variable_value) {
        this.variable_value = variable_value;
    }
}
