package Classs.Java_Classes.Prototype_Function_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class DefaultValueParameterClass extends StatementClass {
    public DefaultValueParameterClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private String number_literal;
    private String value_in_quote;
    private String true_value;
    private String false_value;
    private String null_value;
    private String type_value;
//    private ExprJavaClass expr_java;



    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
//        if (this.expr_java != null) {
//            this.expr_java.accept(astVisitor);
//        }
    }

    public String getNumber_literal() {
        return number_literal;
    }

    public void setNumber_literal(String number_literal) {
        this.number_literal = number_literal;
    }

    public String getValue_in_quote() {
        return value_in_quote;
    }

    public void setValue_in_quote(String value_in_quote) {
        this.value_in_quote = value_in_quote;
    }

    public String getType_value() {
        return type_value;
    }

    public void setType_value(String type_value) {
        this.type_value = type_value;
    }

    public String getTrue_value() {
        return true_value;
    }

    public void setTrue_value(String true_value) {
        this.true_value = true_value;
    }

    public String getFalse_value() {
        return false_value;
    }

    public void setFalse_value(String false_value) {
        this.false_value = false_value;
    }

    public String getNull_value() {
        return null_value;
    }

    public void setNull_value(String null_value) {
        this.null_value = null_value;
    }

    //    public ExprJavaClass getExpr_java() {
//        return expr_java;
//    }
//
//    public void setExpr_java(ExprJavaClass expr_java) {
//        this.expr_java = expr_java;
//    }
}
