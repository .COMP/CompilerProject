package Classs.Java_Classes.Prototype_Function_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Global_Classes.InitialVariableDeclarationClass;
import Visitor.AST_Visitor.AstVisitorInterface;
import org.antlr.v4.runtime.misc.Pair;

import java.util.List;

public class FunctionParameterInPrototypeClass extends StatementClass {
    public FunctionParameterInPrototypeClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private List<InitialVariableDeclarationClass> initial_variables_declaration;
    private FunctionParameterWithDefaultClass function_parameter_with_default;
    private Pair<String , String > symbol_id;
//    private DefaultValueParameterClass default_value_parameter;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.initial_variables_declaration != null) {
            for (InitialVariableDeclarationClass initialVariableDeclarationClass : this.initial_variables_declaration) {
                initialVariableDeclarationClass.accept(astVisitor);
            }
        }
        if (this.function_parameter_with_default != null) {
            this.function_parameter_with_default.accept(astVisitor);
        }
//        if (this.default_value_parameter != null) {
//            this.default_value_parameter.accept(astVisitor);
//        }
    }

    public List<InitialVariableDeclarationClass> getInitial_variables_declaration() {
        return initial_variables_declaration;
    }

    public void setInitial_variables_declaration(List<InitialVariableDeclarationClass> initial_variables_declaration) {
        this.initial_variables_declaration = initial_variables_declaration;
    }

    public FunctionParameterWithDefaultClass getFunction_parameter_with_default() {
        return function_parameter_with_default;
    }

    public void setFunction_parameter_with_default(FunctionParameterWithDefaultClass function_parameter_with_default) {
        this.function_parameter_with_default = function_parameter_with_default;
    }

    public Pair<String, String> getSymbol_id() {
        return symbol_id;
    }

    public void setSymbol_id(Pair<String, String> symbol_id) {
        this.symbol_id = symbol_id;
    }
    //    public DefaultValueParameterClass getDefault_value_parameter() {
//        return default_value_parameter;
//    }
//
//    public void setDefault_value_parameter(DefaultValueParameterClass default_value_parameter) {
//        this.default_value_parameter = default_value_parameter;
//    }
}
