package Classs.Java_Classes.Prototype_Function_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;
import org.antlr.v4.runtime.misc.Pair;

import java.util.List;

public class FunctionParameterWithDefaultClass extends StatementClass {
    public FunctionParameterWithDefaultClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private List<InitialVariableDeclarationWithDefaultValueClass> initial_variable_declaration_with_default_values;
    private Pair<String ,String > symbol_id;
    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.initial_variable_declaration_with_default_values != null) {
            for (InitialVariableDeclarationWithDefaultValueClass initial_variable_declaration_with_default_value : this.initial_variable_declaration_with_default_values) {
                initial_variable_declaration_with_default_value.accept(astVisitor);
            }
        }
    }

    public List<InitialVariableDeclarationWithDefaultValueClass> getInitial_variable_declaration_with_default_values() {
        return initial_variable_declaration_with_default_values;
    }

    public void setInitial_variable_declaration_with_default_values(List<InitialVariableDeclarationWithDefaultValueClass> initial_variable_declaration_with_default_values) {
        this.initial_variable_declaration_with_default_values = initial_variable_declaration_with_default_values;
    }

    public Pair<String, String> getSymbol_id() {
        return symbol_id;
    }

    public void setSymbol_id(Pair<String, String> symbol_id) {
        this.symbol_id = symbol_id;
    }
}
