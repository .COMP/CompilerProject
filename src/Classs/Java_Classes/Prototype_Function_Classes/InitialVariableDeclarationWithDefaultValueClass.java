package Classs.Java_Classes.Prototype_Function_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Global_Classes.InitialVariableDeclarationClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class InitialVariableDeclarationWithDefaultValueClass extends StatementClass {
    public InitialVariableDeclarationWithDefaultValueClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private InitialVariableDeclarationClass initial_variable_declaration;
    private DefaultValueParameterClass default_value_parameter;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.initial_variable_declaration != null) {
            this.initial_variable_declaration.accept(astVisitor);
        }
        if (this.default_value_parameter != null) {
            this.default_value_parameter.accept(astVisitor);
        }
    }

    public InitialVariableDeclarationClass getInitial_variable_declaration() {
        return initial_variable_declaration;
    }

    public void setInitial_variable_declaration(InitialVariableDeclarationClass initial_variable_declaration) {
        this.initial_variable_declaration = initial_variable_declaration;
    }

    public DefaultValueParameterClass getDefault_value_parameter() {
        return default_value_parameter;
    }

    public void setDefault_value_parameter(DefaultValueParameterClass default_value_parameter) {
        this.default_value_parameter = default_value_parameter;
    }
}
