package Classs.Java_Classes.Prototype_Function_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Global_Classes.BodyPrototypeFunctionClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class PrototypeFunctionClass extends StatementClass {
    public PrototypeFunctionClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private String function_name;
    private PrototypeFunctionParameterClass prototype_function_parameter;
    private BodyPrototypeFunctionClass body_prototype_function;
    private String scope_id;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.prototype_function_parameter != null) {
            this.prototype_function_parameter.accept(astVisitor);
        }
        if (this.body_prototype_function != null) {
            this.body_prototype_function.accept(astVisitor);
        }
    }

    public String getFunction_name() {
        return function_name;
    }

    public void setFunction_name(String function_name) {
        this.function_name = function_name;
    }

    public PrototypeFunctionParameterClass getPrototype_function_parameter() {
        return prototype_function_parameter;
    }

    public void setPrototype_function_parameter(PrototypeFunctionParameterClass prototype_function_parameter) {
        this.prototype_function_parameter = prototype_function_parameter;
    }

    public BodyPrototypeFunctionClass getBody_prototype_function() {
        return body_prototype_function;
    }

    public void setBody_prototype_function(BodyPrototypeFunctionClass body_prototype_function) {
        this.body_prototype_function = body_prototype_function;
    }

    public String getScope_id() {
        return scope_id;
    }

    public void setScope_id(String scope_id) {
        this.scope_id = scope_id;
    }
}
