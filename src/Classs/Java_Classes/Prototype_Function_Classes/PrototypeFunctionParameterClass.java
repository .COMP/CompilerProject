package Classs.Java_Classes.Prototype_Function_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class PrototypeFunctionParameterClass extends StatementClass {
    public PrototypeFunctionParameterClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private FunctionParameterInPrototypeClass function_parameter_in_prototype;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.function_parameter_in_prototype != null) {
            this.function_parameter_in_prototype.accept(astVisitor);
        }
    }

    public FunctionParameterInPrototypeClass getFunction_parameter_in_prototype() {
        return function_parameter_in_prototype;
    }

    public void setFunction_parameter_in_prototype(FunctionParameterInPrototypeClass function_parameter_in_prototype) {
        this.function_parameter_in_prototype = function_parameter_in_prototype;
    }
}
