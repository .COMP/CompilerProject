package Classs.Java_Classes.Scope_Statement_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Global_Classes.JavaStatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class ScopeStatementClass extends StatementClass {
    public ScopeStatementClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private List<JavaStatementClass> java_statements;


    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.java_statements != null) {
            for (JavaStatementClass java_statement : this.java_statements) {
                java_statement.accept(astVisitor);
            }
        }
    }

    public List<JavaStatementClass> getJava_statements() {
        return java_statements;
    }


    public void setJava_statements(List<JavaStatementClass> java_statements) {
        this.java_statements = java_statements;
    }
}
