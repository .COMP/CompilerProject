package Classs.Java_Classes.Switch_Statement_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Global_Classes.BodyFunctionClass;
import Classs.Java_Classes.Global_Classes.JavaStatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class BodyForCaseAndDefaultClass extends StatementClass {
    public BodyForCaseAndDefaultClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private List<JavaStatementClass> java_statements;
    private BodyFunctionClass body_function;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.java_statements != null) {
            for (JavaStatementClass javaStatementClass : this.java_statements) {
                javaStatementClass.accept(astVisitor);
            }
        }
        if (this.body_function != null) {
            this.body_function.accept(astVisitor);
        }
    }

    public List<JavaStatementClass> getJava_statements() {
        return java_statements;
    }

    public void setJava_statements(List<JavaStatementClass> java_statements) {
        this.java_statements = java_statements;
    }

    public BodyFunctionClass getBody_function() {
        return body_function;
    }

    public void setBody_function(BodyFunctionClass body_function) {
        this.body_function = body_function;
    }
}
