package Classs.Java_Classes.Switch_Statement_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Global_Classes.VariableValueClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class CaseStatementClass extends StatementClass {
    public CaseStatementClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private CaseValueStatement case_value;
    private BodyForCaseAndDefaultClass body_for_case_and_default;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.case_value != null) {
            this.case_value.accept(astVisitor);
        }
        if (body_for_case_and_default != null) {
            this.body_for_case_and_default.accept(astVisitor);
        }
    }

    public CaseValueStatement getCase_value() {
        return case_value;
    }

    public void setCase_value(CaseValueStatement case_value) {
        this.case_value = case_value;
    }

    public BodyForCaseAndDefaultClass getBody_for_case_and_default() {
        return body_for_case_and_default;
    }

    public void setBody_for_case_and_default(BodyForCaseAndDefaultClass body_for_case_and_default) {
        this.body_for_case_and_default = body_for_case_and_default;
    }
}
