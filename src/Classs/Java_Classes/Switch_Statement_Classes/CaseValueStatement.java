package Classs.Java_Classes.Switch_Statement_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class CaseValueStatement extends StatementClass {
    private String case_value;

    public CaseValueStatement(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {

    }

    public String getCase_value() {
        return case_value;
    }

    public void setCase_value(String case_value) {
        this.case_value = case_value;
    }
}
