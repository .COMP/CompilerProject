package Classs.Java_Classes.Switch_Statement_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class DefaultStatementClass extends StatementClass {
    public DefaultStatementClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private BodyForCaseAndDefaultClass body_for_case_and_default;
    private String scope_id;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.body_for_case_and_default != null) {
            this.body_for_case_and_default.accept(astVisitor);
        }
    }

    public BodyForCaseAndDefaultClass getBody_for_case_and_default() {
        return body_for_case_and_default;
    }

    public void setBody_for_case_and_default(BodyForCaseAndDefaultClass body_for_case_and_default) {
        this.body_for_case_and_default = body_for_case_and_default;
    }

    public String getScope_id() {
        return scope_id;
    }

    public void setScope_id(String scope_id) {
        this.scope_id = scope_id;
    }
}
