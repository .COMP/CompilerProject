package Classs.Java_Classes.Switch_Statement_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.Java_Classes.Global_Classes.VariableValueClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class SwitchStatementClass extends StatementClass {
    public SwitchStatementClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private VariableValueClass variable_value;
    private List<CaseStatementClass> case_statements;
    private DefaultStatementClass default_statement;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if (this.variable_value != null) {
            this.variable_value.accept(astVisitor);
        }
        if (this.case_statements != null) {
            for (CaseStatementClass caseStatementClass : this.case_statements) {
                caseStatementClass.accept(astVisitor);
            }
        }
        if (this.default_statement != null) {
            this.default_statement.accept(astVisitor);
        }
    }

    public VariableValueClass getVariable_value() {
        return variable_value;
    }

    public void setVariable_value(VariableValueClass variable_value) {
        this.variable_value = variable_value;
    }

    public List<CaseStatementClass> getCase_statements() {
        return case_statements;
    }

    public void setCase_statements(List<CaseStatementClass> case_statements) {
        this.case_statements = case_statements;
    }

    public DefaultStatementClass getDefault_statement() {
        return default_statement;
    }

    public void setDefault_statement(DefaultStatementClass default_statement) {
        this.default_statement = default_statement;
    }
}
