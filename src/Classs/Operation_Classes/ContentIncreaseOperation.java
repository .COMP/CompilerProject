package Classs.Operation_Classes;

public class ContentIncreaseOperation extends ContentOperationClass {
    public ContentIncreaseOperation(String operation, String variable_name) {
        super(operation);
        this.variable_name = variable_name;
    }

    private String variable_name;

    public String getVariable_name() {
        return variable_name;
    }

    public void setVariable_name(String variable_name) {
        this.variable_name = variable_name;
    }
}
