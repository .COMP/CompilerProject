package Classs.Operation_Classes;

public class ContentOperationClass {
    private String operation;

    public ContentOperationClass(String operation) {
        this.operation = operation;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }
}
