package Classs.SQL_Classes.Alter_Table_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Classs.SQL_Classes.Create_Table_Statement.TableConstraintClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class AlterTableAddClass extends StatementClass
{
    public AlterTableAddClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private TableConstraintClass tableConstraint;

    public TableConstraintClass getTableConstraint() {
        return tableConstraint;
    }

    public void setTableConstraint(TableConstraintClass tableConstraint) {
        this.tableConstraint = tableConstraint;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
     astVisitor.visit(this);

     tableConstraint.accept(astVisitor);
    }
}
