package Classs.SQL_Classes.Alter_Table_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Classs.SQL_Classes.Create_Table_Statement.ColumnDefClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class AlterTableStmtClass extends StatementClass {
    public AlterTableStmtClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private String newTableName=null;
    private AlterTableAddClass alterTableAdd;
    private AlterTableAddConstraintClass alterTableAddConstraint;
    private ColumnDefClass columnDef;

    private String databaseName=null;
    private String sourceTableName;

    public String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public String getSourceTableName() {
        return sourceTableName;
    }

    public void setSourceTableName(String sourceTableName) {
        this.sourceTableName = sourceTableName;
    }

    public String getNewTableName() {
        return newTableName;
    }

    public void setNewTableName(String newTableName) {
        this.newTableName = newTableName;
    }

    public AlterTableAddClass getAlterTableAdd() {
        return alterTableAdd;
    }

    public void setAlterTableAdd(AlterTableAddClass alterTableAdd) {
        this.alterTableAdd = alterTableAdd;
    }

    public AlterTableAddConstraintClass getAlterTableAddConstraint() {
        return alterTableAddConstraint;
    }

    public void setAlterTableAddConstraint(AlterTableAddConstraintClass alterTableAddConstraint) {
        this.alterTableAddConstraint = alterTableAddConstraint;
    }

    public ColumnDefClass getColumnDef() {
        return columnDef;
    }

    public void setColumnDef(ColumnDefClass columnDef) {
        this.columnDef = columnDef;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
     astVisitor.visit(this);

     if(alterTableAdd!=null)
     {
         alterTableAdd.accept(astVisitor);
     }else if(alterTableAddConstraint!=null)
     {
         alterTableAddConstraint.accept(astVisitor);
     }else if(columnDef!=null)
     {
         columnDef.accept(astVisitor);
     }

    }
}
