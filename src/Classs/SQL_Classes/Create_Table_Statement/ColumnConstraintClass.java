package Classs.SQL_Classes.Create_Table_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Classs.SQL_Classes.Select_Statement.ExprClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class ColumnConstraintClass extends StatementClass {
    public ColumnConstraintClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private String constraintName=null;
    private ColumnConstraintPrimaryKeyClass columnConstraintPrimaryKey;
    private ColumnConstraintForeignKeyClass columnConstraintForeignKey;
    private ColumnConstraintNotNullClass columnConstraintNotNull;
    private ColumnConstraintNullClass columnConstraintNull;
    private ExprClass exprCheck;
    private ColumnDefaultClass columnDefault;
    private String colltionName=null;

    public String getConstraintName() {
        return constraintName;
    }

    public void setConstraintName(String constraintName) {
        this.constraintName = constraintName;
    }

    public ColumnConstraintPrimaryKeyClass getColumnConstraintPrimaryKey() {
        return columnConstraintPrimaryKey;
    }

    public void setColumnConstraintPrimaryKey(ColumnConstraintPrimaryKeyClass columnConstraintPrimaryKey) {
        this.columnConstraintPrimaryKey = columnConstraintPrimaryKey;
    }

    public ColumnConstraintForeignKeyClass getColumnConstraintForeignKey() {
        return columnConstraintForeignKey;
    }

    public void setColumnConstraintForeignKey(ColumnConstraintForeignKeyClass columnConstraintForeignKey) {
        this.columnConstraintForeignKey = columnConstraintForeignKey;
    }

    public ColumnConstraintNotNullClass getColumnConstraintNotNull() {
        return columnConstraintNotNull;
    }

    public void setColumnConstraintNotNull(ColumnConstraintNotNullClass columnConstraintNotNull) {
        this.columnConstraintNotNull = columnConstraintNotNull;
    }

    public ColumnConstraintNullClass getColumnConstraintNull() {
        return columnConstraintNull;
    }

    public void setColumnConstraintNull(ColumnConstraintNullClass columnConstraintNull) {
        this.columnConstraintNull = columnConstraintNull;
    }

    public ExprClass getExprCheck() {
        return exprCheck;
    }

    public void setExprCheck(ExprClass exprCheck) {
        this.exprCheck = exprCheck;
    }

    public ColumnDefaultClass getColumnDefault() {
        return columnDefault;
    }

    public void setColumnDefault(ColumnDefaultClass columnDefault) {
        this.columnDefault = columnDefault;
    }

    public String getColltionName() {
        return colltionName;
    }

    public void setColltionName(String colltionName) {
        this.colltionName = colltionName;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
       astVisitor.visit(this);

       if(columnConstraintForeignKey!=null)
       {
           columnConstraintForeignKey.accept(astVisitor);

       }else if(columnConstraintNotNull!=null)
       {
           columnConstraintNotNull.accept(astVisitor);

       }else if(columnConstraintNull!=null)
       {
           columnConstraintNull.accept(astVisitor);

       }else if(columnConstraintPrimaryKey!=null)
       {
           columnConstraintPrimaryKey.accept(astVisitor);

       }else if(exprCheck!=null){

           exprCheck.accept(astVisitor);

       }else if(columnDefault!=null){

           columnDefault.accept(astVisitor);
       }
    }
}
