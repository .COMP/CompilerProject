package Classs.SQL_Classes.Create_Table_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class ColumnConstraintForeignKeyClass extends StatementClass {
    public ColumnConstraintForeignKeyClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }
    private ForeignKeyClauseClass foreignKeyClause;

    public ForeignKeyClauseClass getForeignKeyClause() {
        return foreignKeyClause;
    }

    public void setForeignKeyClause(ForeignKeyClauseClass foreignKeyClause) {
        this.foreignKeyClause = foreignKeyClause;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
      astVisitor.visit(this);

      foreignKeyClause.accept(astVisitor);
    }
}
