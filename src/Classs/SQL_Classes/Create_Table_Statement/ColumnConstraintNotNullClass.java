package Classs.SQL_Classes.Create_Table_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class ColumnConstraintNotNullClass extends StatementClass {
    public ColumnConstraintNotNullClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }
    private String notNull;

    public String getNotNull() {
        return notNull;
    }

    public void setNotNull(String notNull) {
        this.notNull = notNull;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
    }
}
