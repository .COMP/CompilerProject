package Classs.SQL_Classes.Create_Table_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class ColumnConstraintNullClass extends StatementClass {
    public ColumnConstraintNullClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }


    private String Null;

    public String getNull() {
        return Null;
    }

    public void setNull(String aNull) {
        Null = aNull;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

    }
}
