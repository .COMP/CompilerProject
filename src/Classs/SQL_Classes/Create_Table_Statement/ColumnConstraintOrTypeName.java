package Classs.SQL_Classes.Create_Table_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;


public class ColumnConstraintOrTypeName extends StatementClass {
    public ColumnConstraintOrTypeName(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private ColumnConstraintClass columnConstraint;
    private TypeNameClass typeName;

    public ColumnConstraintClass getColumnConstraint() {
        return columnConstraint;
    }

    public void setColumnConstraint(ColumnConstraintClass columnConstraint) {
        this.columnConstraint = columnConstraint;
    }

    public TypeNameClass getTypeName() {
        return typeName;
    }

    public void setTypeName(TypeNameClass typeName) {
        this.typeName = typeName;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

        if(columnConstraint!=null)
        {
            columnConstraint.accept(astVisitor);

        }else {
            typeName.accept(astVisitor);
        }

    }
}
