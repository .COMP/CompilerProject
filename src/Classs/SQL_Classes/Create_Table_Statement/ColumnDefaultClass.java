package Classs.SQL_Classes.Create_Table_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class ColumnDefaultClass extends StatementClass {
    public ColumnDefaultClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private ColumnDefaultContentClass columnDefaultContent;
    private List<String> anyNames;

    public ColumnDefaultContentClass getColumnDefaultContent() {
        return columnDefaultContent;
    }

    public void setColumnDefaultContent(ColumnDefaultContentClass columnDefaultContent) {
        this.columnDefaultContent = columnDefaultContent;
    }

    public List<String> getAnyNames() {
        return anyNames;
    }

    public void setAnyNames(List<String> anyNames) {
        this.anyNames = anyNames;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        columnDefaultContent.accept(astVisitor);

    }
}
