package Classs.SQL_Classes.Create_Table_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Classs.SQL_Classes.Select_Statement.ExprClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class ColumnDefaultContentClass extends StatementClass {
    public ColumnDefaultContentClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private ColumnDefaultValue columnDefaultValue;
    private ExprClass expr;
    private ExprClass nextvalExpr;
    private String anyName=null;

    public ColumnDefaultValue getColumnDefaultValue() {
        return columnDefaultValue;
    }

    public void setColumnDefaultValue(ColumnDefaultValue columnDefaultValue) {
        this.columnDefaultValue = columnDefaultValue;
    }

    public ExprClass getExpr() {
        return expr;
    }

    public void setExpr(ExprClass expr) {
        this.expr = expr;
    }

    public ExprClass getNextvalExpr() {
        return nextvalExpr;
    }

    public void setNextvalExpr(ExprClass nextvalExpr) {
        this.nextvalExpr = nextvalExpr;
    }

    public String getAnyName() {
        return anyName;
    }

    public void setAnyName(String anyName) {
        this.anyName = anyName;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

        if(columnDefaultValue!=null)
        {
            columnDefaultValue.accept(astVisitor);
        }else if(expr!=null)
        {
            columnDefaultValue.accept(astVisitor);
        }else if(nextvalExpr!=null)
        {
            nextvalExpr.accept(astVisitor);
        }

    }
}
