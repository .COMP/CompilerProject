package Classs.SQL_Classes.Create_Table_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class ColumnDefaultValue extends StatementClass {
    public ColumnDefaultValue(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private String signedNumber=null;
    private String literalValue=null;

    public String getSignedNumber() {
        return signedNumber;
    }

    public void setSignedNumber(String signedNumber) {
        this.signedNumber = signedNumber;
    }

    public String getLiteralValue() {
        return literalValue;
    }

    public void setLiteralValue(String literalValue) {
        this.literalValue = literalValue;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

    }
}
