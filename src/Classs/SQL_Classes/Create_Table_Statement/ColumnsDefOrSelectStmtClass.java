package Classs.SQL_Classes.Create_Table_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Classs.SQL_Classes.Select_Statement.SelectStmtClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class ColumnsDefOrSelectStmtClass extends StatementClass {
    public ColumnsDefOrSelectStmtClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private ColumnsDeftTableConstraintClass columnsDeftTableConstraint;
    private SelectStmtClass selectStmt;

    public ColumnsDeftTableConstraintClass getColumnsDeftTableConstraint() {
        return columnsDeftTableConstraint;
    }

    public void setColumnsDeftTableConstraint(ColumnsDeftTableConstraintClass columnsDeftTableConstraint) {
        this.columnsDeftTableConstraint = columnsDeftTableConstraint;
    }

    public SelectStmtClass getSelectStmt() {
        return selectStmt;
    }

    public void setSelectStmt(SelectStmtClass selectStmt) {
        this.selectStmt = selectStmt;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
      astVisitor.visit(this);

      if(columnsDeftTableConstraint!=null)
      {
          columnsDeftTableConstraint.accept(astVisitor);
      }else {
          selectStmt.accept(astVisitor);
      }
    }
}
