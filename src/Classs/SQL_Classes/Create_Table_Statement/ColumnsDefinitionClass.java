package Classs.SQL_Classes.Create_Table_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class ColumnsDefinitionClass extends StatementClass {
    public ColumnsDefinitionClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }


    private List<ColumnDefinitionClass> columns;

    public List<ColumnDefinitionClass> getColumns() {
        return columns;
    }

    public void setColumns(List<ColumnDefinitionClass> columns) {
        this.columns = columns;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        for(int i=0;i<columns.size();i++)
            columns.get(i).accept(astVisitor);

    }
}
