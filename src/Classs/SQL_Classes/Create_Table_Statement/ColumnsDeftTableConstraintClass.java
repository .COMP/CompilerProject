package Classs.SQL_Classes.Create_Table_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class ColumnsDeftTableConstraintClass extends StatementClass {
    public ColumnsDeftTableConstraintClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }
    private List<ColumnDefClass> columnDefs;
    private List<TableConstraintClass> tableConstraints;

    public List<ColumnDefClass> getColumnDefs() {
        return columnDefs;
    }

    public void setColumnDefs(List<ColumnDefClass> columnDefs) {
        this.columnDefs = columnDefs;
    }

    public List<TableConstraintClass> getTableConstraints() {
        return tableConstraints;
    }

    public void setTableConstraints(List<TableConstraintClass> tableConstraints) {
        this.tableConstraints = tableConstraints;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        for (int i = 0; i <columnDefs.size() ; i++) {
            columnDefs.get(i).accept(astVisitor);
        }
        for (int i = 0; i < tableConstraints.size(); i++) {
            tableConstraints.get(i).accept(astVisitor);
        }
    }
}
