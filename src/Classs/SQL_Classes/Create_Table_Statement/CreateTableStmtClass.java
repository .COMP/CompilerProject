package Classs.SQL_Classes.Create_Table_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Classs.SQL_Classes.Select_Statement.TableNameFaildClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class CreateTableStmtClass extends StatementClass {
    public CreateTableStmtClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private boolean ifNotExist;
    private TableNameFaildClass tableNameFaild;
    private ColumnsDefinitionClass columns;
    private CreateTableStmtTypeClass createTableStmtType;
    private CreateTableStmtPathClass createTableStmtPath;

    public CreateTableStmtTypeClass getCreateTableStmtType() {
        return createTableStmtType;
    }

    public void setCreateTableStmtType(CreateTableStmtTypeClass createTableStmtType) {
        this.createTableStmtType = createTableStmtType;
    }

    public CreateTableStmtPathClass getCreateTableStmtPath() {
        return createTableStmtPath;
    }

    public void setCreateTableStmtPath(CreateTableStmtPathClass createTableStmtPath) {
        this.createTableStmtPath = createTableStmtPath;
    }

    public boolean isIfNotExist() {
        return ifNotExist;
    }

    public void setIfNotExist(boolean ifNotExist) {
        this.ifNotExist = ifNotExist;
    }

    public TableNameFaildClass getTableNameFaild() {
        return tableNameFaild;
    }

    public void setTableNameFaild(TableNameFaildClass tableNameFaild) {
        this.tableNameFaild = tableNameFaild;
    }


    public ColumnsDefinitionClass getColumns() {
        return columns;
    }

    public void setColumns(ColumnsDefinitionClass columns) {
        this.columns = columns;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

        tableNameFaild.accept(astVisitor);

        columns.accept(astVisitor);

        createTableStmtType.accept(astVisitor);

        createTableStmtPath.accept(astVisitor);
    }
}
