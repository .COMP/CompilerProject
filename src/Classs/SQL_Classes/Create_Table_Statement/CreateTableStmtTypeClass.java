package Classs.SQL_Classes.Create_Table_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;
public class CreateTableStmtTypeClass extends StatementClass {


    public CreateTableStmtTypeClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private TypeFaildClass typeFaild;

    public TypeFaildClass getTypeFaild() {
        return typeFaild;
    }

    public void setTypeFaild(TypeFaildClass typeFaild) {
        this.typeFaild = typeFaild;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

        typeFaild.accept(astVisitor);

    }
}
