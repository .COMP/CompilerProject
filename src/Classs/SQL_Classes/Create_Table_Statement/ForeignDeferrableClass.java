package Classs.SQL_Classes.Create_Table_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class ForeignDeferrableClass extends StatementClass {
    public ForeignDeferrableClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private boolean not;
    private boolean initiallyDeferred;
    private boolean initiallyImmediate;
    private boolean enable;

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public boolean isNot() {
        return not;
    }

    public void setNot(boolean not) {
        this.not = not;
    }

    public boolean isInitiallyDeferred() {
        return initiallyDeferred;
    }

    public void setInitiallyDeferred(boolean initiallyDeferred) {
        this.initiallyDeferred = initiallyDeferred;
    }

    public boolean isInitiallyImmediate() {
        return initiallyImmediate;
    }

    public void setInitiallyImmediate(boolean initiallyImmediate) {
        this.initiallyImmediate = initiallyImmediate;
    }
    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
    }
}
