package Classs.SQL_Classes.Create_Table_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class ForeignKeyClauseClass extends StatementClass {
    public ForeignKeyClauseClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private ForeignTableFaildClass foreignTableFaild;
    private List<String> fkTargetColumnName;
    private List<ForeignOnOrMatch> foreignOnOrMatches;
    private ForeignDeferrableClass foreignDeferrable;

    public ForeignDeferrableClass getForeignDeferrable() {
        return foreignDeferrable;
    }

    public void setForeignDeferrable(ForeignDeferrableClass foreignDeferrable) {
        this.foreignDeferrable = foreignDeferrable;
    }

    public List<ForeignOnOrMatch> getForeignOnOrMatches() {
        return foreignOnOrMatches;
    }

    public void setForeignOnOrMatches(List<ForeignOnOrMatch> foreignOnOrMatches) {
        this.foreignOnOrMatches = foreignOnOrMatches;
    }

    public ForeignTableFaildClass getForeignTableFaild() {
        return foreignTableFaild;
    }

    public void setForeignTableFaild(ForeignTableFaildClass foreignTableFaild) {
        this.foreignTableFaild = foreignTableFaild;
    }

    public List<String> getFkTargetColumnName() {
        return fkTargetColumnName;
    }

    public void setFkTargetColumnName(List<String> fkTargetColumnName) {
        this.fkTargetColumnName = fkTargetColumnName;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

        foreignTableFaild.accept(astVisitor);

        for (int i = 0; i <foreignOnOrMatches.size() ; i++) {
            foreignOnOrMatches.get(i).accept(astVisitor);
        }
        if(foreignDeferrable!=null)
        {
            foreignDeferrable.accept(astVisitor);
        }

    }
}
