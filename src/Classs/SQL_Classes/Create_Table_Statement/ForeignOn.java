package Classs.SQL_Classes.Create_Table_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class ForeignOn extends StatementClass {
    public ForeignOn(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }
    private boolean delete;
    private boolean update;
    private boolean setNull;
    private boolean setDefault;
    private boolean casasde;
    private boolean restrict;
    private boolean noAction;

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    public boolean isUpdate() {
        return update;
    }

    public void setUpdate(boolean update) {
        this.update = update;
    }

    public boolean isSetNull() {
        return setNull;
    }

    public void setSetNull(boolean setNull) {
        this.setNull = setNull;
    }

    public boolean isSetDefault() {
        return setDefault;
    }

    public void setSetDefault(boolean setDefault) {
        this.setDefault = setDefault;
    }

    public boolean isCasasde() {
        return casasde;
    }

    public void setCasasde(boolean casasde) {
        this.casasde = casasde;
    }

    public boolean isRestrict() {
        return restrict;
    }

    public void setRestrict(boolean restrict) {
        this.restrict = restrict;
    }

    public boolean isNoAction() {
        return noAction;
    }

    public void setNoAction(boolean noAction) {
        this.noAction = noAction;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

    }
}
