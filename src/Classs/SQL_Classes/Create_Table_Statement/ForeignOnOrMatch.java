package Classs.SQL_Classes.Create_Table_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class ForeignOnOrMatch extends StatementClass {
    public ForeignOnOrMatch(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private ForeignOn foreignOn;
    private String matchName=null;

    public ForeignOn getForeignOn() {
        return foreignOn;
    }

    public void setForeignOn(ForeignOn foreignOn) {
        this.foreignOn = foreignOn;
    }

    public String getMatchName() {
        return matchName;
    }

    public void setMatchName(String matchName) {
        this.matchName = matchName;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
      astVisitor.visit(this);
      if(foreignOn!=null)
      {
          foreignOn.accept(astVisitor);
      }
    }
}
