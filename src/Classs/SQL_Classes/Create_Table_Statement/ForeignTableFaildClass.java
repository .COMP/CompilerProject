package Classs.SQL_Classes.Create_Table_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class ForeignTableFaildClass extends StatementClass {
    public ForeignTableFaildClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private String databaseName=null;
    private String foreignTable;

    public String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public String getForeignTable() {
        return foreignTable;
    }

    public void setForeignTable(String foreignTable) {
        this.foreignTable = foreignTable;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
    }
}
