package Classs.SQL_Classes.Create_Table_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class IndexedColumn extends StatementClass {
    public IndexedColumn(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }
    private String columnName;
    private String collistionName=null;
    private boolean isAsc;
    private boolean isDesc;

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getCollistionName() {
        return collistionName;
    }

    public void setCollistionName(String collistionName) {
        this.collistionName = collistionName;
    }

    public boolean isAsc() {
        return isAsc;
    }

    public void setAsc(boolean asc) {
        isAsc = asc;
    }

    public boolean isDesc() {
        return isDesc;
    }

    public void setDesc(boolean desc) {
        isDesc = desc;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
    }
}
