package Classs.SQL_Classes.Create_Table_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class SignedNumberAnyNameClass extends StatementClass {
    public SignedNumberAnyNameClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private String signedNumber;
    private String anyName=null;

    public String getSignedNumber() {
        return signedNumber;
    }

    public void setSignedNumber(String signedNumber) {
        this.signedNumber = signedNumber;
    }

    public String getAnyName() {
        return anyName;
    }

    public void setAnyName(String anyName) {
        this.anyName = anyName;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
    }
}
