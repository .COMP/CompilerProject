package Classs.SQL_Classes.Create_Table_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Classs.SQL_Classes.Select_Statement.ExprClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class TableConstraintClass extends StatementClass {
    public TableConstraintClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private String constraintName=null;
    private TableConstraintPrimaryKey tableConstraintPrimaryKey;
    private TableConstraintKey tableConstraintKey;
    private TableConstraintUnique tableConstraintUnique;
    private ExprClass exprCheck;
    private TableConstraintForeginKey tableConstraintForeginKey;

    public String getConstraintName() {
        return constraintName;
    }

    public void setConstraintName(String constraintName) {
        this.constraintName = constraintName;
    }

    public TableConstraintPrimaryKey getTableConstraintPrimaryKey() {
        return tableConstraintPrimaryKey;
    }

    public void setTableConstraintPrimaryKey(TableConstraintPrimaryKey tableConstraintPrimaryKey) {
        this.tableConstraintPrimaryKey = tableConstraintPrimaryKey;
    }

    public TableConstraintKey getTableConstraintKey() {
        return tableConstraintKey;
    }

    public void setTableConstraintKey(TableConstraintKey tableConstraintKey) {
        this.tableConstraintKey = tableConstraintKey;
    }

    public TableConstraintUnique getTableConstraintUnique() {
        return tableConstraintUnique;
    }

    public void setTableConstraintUnique(TableConstraintUnique tableConstraintUnique) {
        this.tableConstraintUnique = tableConstraintUnique;
    }

    public ExprClass getExprCheck() {
        return exprCheck;
    }

    public void setExprCheck(ExprClass exprCheck) {
        this.exprCheck = exprCheck;
    }

    public TableConstraintForeginKey getTableConstraintForeginKey() {
        return tableConstraintForeginKey;
    }

    public void setTableConstraintForeginKey(TableConstraintForeginKey tableConstraintForeginKey) {
        this.tableConstraintForeginKey = tableConstraintForeginKey;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

        if(tableConstraintPrimaryKey!=null)
        {
         tableConstraintPrimaryKey.accept(astVisitor);

        }else if(tableConstraintKey!=null){

            tableConstraintKey.accept(astVisitor);
        }else if(tableConstraintUnique!=null)
        {
            tableConstraintUnique.accept(astVisitor);
        }else if(exprCheck!=null){
            exprCheck.accept(astVisitor);
        }else
        {
            tableConstraintForeginKey.accept(astVisitor);
        }

    }
}
