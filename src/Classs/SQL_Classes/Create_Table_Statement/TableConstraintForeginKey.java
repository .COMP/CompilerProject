package Classs.SQL_Classes.Create_Table_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class TableConstraintForeginKey extends StatementClass {
    public TableConstraintForeginKey(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private List<String> fkOriginColumnNames;
    private ForeignKeyClauseClass foreignKeyClause;

    public List<String> getFkOriginColumnNames() {
        return fkOriginColumnNames;
    }

    public void setFkOriginColumnNames(List<String> fkOriginColumnNames) {
        this.fkOriginColumnNames = fkOriginColumnNames;
    }

    public ForeignKeyClauseClass getForeignKeyClause() {
        return foreignKeyClause;
    }

    public void setForeignKeyClause(ForeignKeyClauseClass foreignKeyClause) {
        this.foreignKeyClause = foreignKeyClause;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

        foreignKeyClause.accept(astVisitor);

    }
}
