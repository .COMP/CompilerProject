package Classs.SQL_Classes.Create_Table_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class TableConstraintPrimaryKey extends StatementClass {
    public TableConstraintPrimaryKey(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }
    private List<IndexedColumn> indexedColumns;

    public List<IndexedColumn> getIndexedColumns() {
        return indexedColumns;
    }

    public void setIndexedColumns(List<IndexedColumn> indexedColumns) {
        this.indexedColumns = indexedColumns;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

        for (int i = 0; i <indexedColumns.size() ; i++) {
            indexedColumns.get(i).accept(astVisitor);
        }
    }
}
