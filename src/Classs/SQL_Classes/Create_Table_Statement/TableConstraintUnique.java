package Classs.SQL_Classes.Create_Table_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class TableConstraintUnique extends StatementClass {
    public TableConstraintUnique(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private String name=null;
    private List<IndexedColumn> indexedColumns;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<IndexedColumn> getIndexedColumns() {
        return indexedColumns;
    }

    public void setIndexedColumns(List<IndexedColumn> indexedColumns) {
        this.indexedColumns = indexedColumns;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

        for (int i = 0; i <indexedColumns.size() ; i++) {
            indexedColumns.get(i).accept(astVisitor);
        }
    }
}
