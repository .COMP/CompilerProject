package Classs.SQL_Classes.Create_Table_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class TypeFaildClass extends StatementClass {

    public TypeFaildClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private String typeFaildName;

    public String getTypeFaildName() {
        return typeFaildName;
    }

    public void setTypeFaildName(String typeFaildName) {
        this.typeFaildName = typeFaildName;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

    }
}
