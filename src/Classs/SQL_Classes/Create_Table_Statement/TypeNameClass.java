package Classs.SQL_Classes.Create_Table_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class TypeNameClass extends StatementClass {
    public TypeNameClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private String name;
    private List<SignedNumberAnyNameClass> signedNumberAnyNames;
    private String returnType=null;

    public String getReturnType() {
        return returnType;
    }

    public void setReturnType(String returnType) {
        this.returnType = returnType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SignedNumberAnyNameClass> getSignedNumberAnyNames() {
        return signedNumberAnyNames;
    }

    public void setSignedNumberAnyNames(List<SignedNumberAnyNameClass> signedNumberAnyNames) {
        this.signedNumberAnyNames = signedNumberAnyNames;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if(returnType==null)
        for (int i = 0; i <signedNumberAnyNames.size() ; i++) {
            signedNumberAnyNames.get(i).accept(astVisitor);
        }


    }
}
