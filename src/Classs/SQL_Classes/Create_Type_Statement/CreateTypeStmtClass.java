package Classs.SQL_Classes.Create_Type_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Classs.SQL_Classes.Create_Table_Statement.ColumnsDefOrSelectStmtClass;
import Classs.SQL_Classes.Create_Table_Statement.ColumnsDefinitionClass;
import Classs.SQL_Classes.Select_Statement.TableNameFaildClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class CreateTypeStmtClass extends StatementClass {
    public CreateTypeStmtClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private TableNameFaildClass typeNameFaild;

    private ColumnsDefinitionClass columns;

    private boolean ifNotExist=false;

    public TableNameFaildClass getTypeNameFaild() {
        return typeNameFaild;
    }

    public void setTypeNameFaild(TableNameFaildClass tableNameFaild) {
        this.typeNameFaild = tableNameFaild;
    }


    public ColumnsDefinitionClass getColumns() {
        return columns;
    }

    public void setColumns(ColumnsDefinitionClass columns) {
        this.columns = columns;
    }

    public boolean isIfNotExist() {
        return ifNotExist;
    }

    public void setIfNotExist(boolean ifNotExist) {
        this.ifNotExist = ifNotExist;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
      astVisitor.visit(this);

      typeNameFaild.accept(astVisitor);
columns.accept(astVisitor);
    }
}
