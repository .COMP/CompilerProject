package Classs.SQL_Classes.Create_ِِAggregation_Function;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;


public class CreateAggregationFunctionClass extends StatementClass {
    public CreateAggregationFunctionClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }
    private String aggregation_function_name;
    private CreateAggregationFunctionElementsClass createAggregationFunctionElementsClass;

    public CreateAggregationFunctionElementsClass getCreateAggregationFunctionElementsClass() {
        return createAggregationFunctionElementsClass;
    }

    public void setCreateAggregationFunctionElementsClass(CreateAggregationFunctionElementsClass createAggregationFunctionElementsClass) {
        this.createAggregationFunctionElementsClass = createAggregationFunctionElementsClass;
    }

    public String getAggregation_function_name() {
        return aggregation_function_name;
    }


    public void setAggregation_function_name(String aggregation_function_name) {
        this.aggregation_function_name = aggregation_function_name;
    }


    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        createAggregationFunctionElementsClass.accept(astVisitor);

    }
}
