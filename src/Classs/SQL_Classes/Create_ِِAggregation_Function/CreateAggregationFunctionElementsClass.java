package Classs.SQL_Classes.Create_ِِAggregation_Function;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.ArrayList;

public class CreateAggregationFunctionElementsClass extends StatementClass {
    public CreateAggregationFunctionElementsClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private String jar_path;
    private String class_name;
    private String method_name;
    private String return_type;
    private ArrayList<String> params = new ArrayList<>();
    public String getJar_path() {
        return jar_path;
    }

    public void setJar_path(String jar_path) {
        this.jar_path = jar_path;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getMethod_name() {
        return method_name;
    }

    public void setMethod_name(String method_name) {
        this.method_name = method_name;
    }

    public String getReturn_type() {
        return return_type;
    }

    public void setReturn_type(String return_type) {
        this.return_type = return_type;
    }

    public ArrayList<String> getParams() {
        return params;
    }

    public void setParams(ArrayList<String> params) {
        this.params = params;
    }


    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

    }
}
