package Classs.SQL_Classes.Delete_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Classs.SQL_Classes.Select_Statement.ExprClass;
import Classs.SQL_Classes.Update_Statement.QualifiedTableNameClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class DeleteStmtClass extends StatementClass {
    public DeleteStmtClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private QualifiedTableNameClass qualifiedTableName;
    private ExprClass whereExpr;

    public QualifiedTableNameClass getQualifiedTableName() {
        return qualifiedTableName;
    }

    public void setQualifiedTableName(QualifiedTableNameClass qualifiedTableName) {
        this.qualifiedTableName = qualifiedTableName;
    }

    public ExprClass getWhereExpr() {
        return whereExpr;
    }

    public void setWhereExpr(ExprClass whereExpr) {
        this.whereExpr = whereExpr;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

        qualifiedTableName.accept(astVisitor);

        if(whereExpr!=null)
        {
            whereExpr.accept(astVisitor);
        }

    }
}
