package Classs.SQL_Classes.Drop_Table_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Classs.SQL_Classes.Select_Statement.TableNameFaildClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class DropTableStmtClass extends StatementClass {
    public DropTableStmtClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private TableNameFaildClass tableNameFaild;
    private boolean ifExist;

    public TableNameFaildClass getTableNameFaild() {
        return tableNameFaild;
    }

    public void setTableNameFaild(TableNameFaildClass tableNameFaild) {
        this.tableNameFaild = tableNameFaild;
    }

    public boolean isIfExist() {
        return ifExist;
    }

    public void setIfExist(boolean ifExist) {
        this.ifExist = ifExist;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

        tableNameFaild.accept(astVisitor);

    }
}
