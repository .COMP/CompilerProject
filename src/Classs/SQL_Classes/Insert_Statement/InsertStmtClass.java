package Classs.SQL_Classes.Insert_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Classs.SQL_Classes.Select_Statement.TableNameFaildClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class InsertStmtClass extends StatementClass {
    public InsertStmtClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private TableNameFaildClass tableNameFaild;
    private List<String> columnsName;
    private InsertStmtValuesClass insertStmtValues;

    public InsertStmtValuesClass getInsertStmtValues() {
        return insertStmtValues;
    }

    public void setInsertStmtValues(InsertStmtValuesClass insertStmtValues) {
        this.insertStmtValues = insertStmtValues;
    }

    public TableNameFaildClass getTableNameFaild() {
        return tableNameFaild;
    }

    public void setTableNameFaild(TableNameFaildClass tableNameFaild) {
        this.tableNameFaild = tableNameFaild;
    }

    public List<String> getColumnsName() {
        return columnsName;
    }

    public void setColumnsName(List<String> columnsName) {
        this.columnsName = columnsName;
    }


    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

        tableNameFaild.accept(astVisitor);
        insertStmtValues.accept(astVisitor);

    }
}
