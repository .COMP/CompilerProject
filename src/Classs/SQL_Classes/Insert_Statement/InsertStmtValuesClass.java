package Classs.SQL_Classes.Insert_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Classs.SQL_Classes.Select_Statement.SelectStmtClass;
import Classs.SQL_Classes.Select_Statement.ValuesItemsClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class InsertStmtValuesClass extends StatementClass {
    public InsertStmtValuesClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private ValuesItemsClass valuesItems;
    private SelectStmtClass selectStmt;
    private Boolean defaultValues;

    public ValuesItemsClass getValuesItems() {
        return valuesItems;
    }

    public void setValuesItems(ValuesItemsClass valuesItems) {
        this.valuesItems = valuesItems;
    }

    public SelectStmtClass getSelectStmt() {
        return selectStmt;
    }

    public void setSelectStmt(SelectStmtClass selectStmt) {
        this.selectStmt = selectStmt;
    }

    public Boolean getDefaultValues() {
        return defaultValues;
    }

    public void setDefaultValues(Boolean defaultValues) {
        this.defaultValues = defaultValues;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if(valuesItems!=null)
        {
            valuesItems.accept(astVisitor);
        }else{
            selectStmt.accept(astVisitor);
        }
    }
}
