package Classs.SQL_Classes.Select_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class ColumnNameFaildClass extends StatementClass {


    private String columnName;
    private TableNameFaildClass tableNameFaild;

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String ColumnName) {
        this.columnName = ColumnName;
    }

    public TableNameFaildClass getTableNameFaild() {
        return tableNameFaild;
    }

    public void setTableNameFaild(TableNameFaildClass tableNameFaild) {
        this.tableNameFaild = tableNameFaild;
    }

    public ColumnNameFaildClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }


    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

        if(tableNameFaild!=null)
        {
            tableNameFaild.accept(astVisitor);
        }

    }
}
