package Classs.SQL_Classes.Select_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class ExistsCaseClass extends StatementClass {
    public ExistsCaseClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }
    private boolean exists;
    private boolean kNot;
    private SelectStmtClass selectStmt;

    public boolean isExists() {
        return exists;
    }

    public void setExists(boolean exists) {
        this.exists = exists;
    }

    public boolean iskNot() {
        return kNot;
    }

    public void setkNot(boolean kNot) {
        this.kNot = kNot;
    }

    public SelectStmtClass getSelectStmt() {
        return selectStmt;
    }

    public void setSelectStmt(SelectStmtClass selectStmt) {
        this.selectStmt = selectStmt;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

        selectStmt.accept(astVisitor);

    }
}
