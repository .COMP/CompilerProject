package Classs.SQL_Classes.Select_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class ExprClass extends StatementClass {
    public ExprClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }
    private  String literalValue=null;
    private  ColumnNameFaildClass columnNameFaild;
    private  ExprOperatorExprClass exprOperatorExpr;
    private  FunctionExprClass functionExpr;

    private  ExprClass exprInBracket;
    private  InCaseClass inCase;
    private  ExprClass incaseExpr;

    private ExistsCaseClass existsCase;

    public ExistsCaseClass getExistsCase() {
        return existsCase;
    }

    public void setExistsCase(ExistsCaseClass existsCase) {
        this.existsCase = existsCase;
    }

    public InCaseClass getInCase() {
        return inCase;
    }

    public void setInCase(InCaseClass inCase) {
        this.inCase = inCase;
    }

    public ExprClass getIncaseExpr() {
        return incaseExpr;
    }

    public void setIncaseExpr(ExprClass incaseExpr) {
        this.incaseExpr = incaseExpr;
    }

    public String getLiteralValue() {
        return literalValue;
    }

    public void setLiteralValue(String literalValue) {
        this.literalValue = literalValue;
    }

    public ColumnNameFaildClass getColumnNameFaild() {
        return columnNameFaild;
    }

    public void setColumnNameFaild(ColumnNameFaildClass columnNameFaild) {
        this.columnNameFaild = columnNameFaild;
    }

    public ExprOperatorExprClass getExprOperatorExpr() {
        return exprOperatorExpr;
    }

    public void setExprOperatorExpr(ExprOperatorExprClass exprOperatorExpr) {
        this.exprOperatorExpr = exprOperatorExpr;
    }

    public FunctionExprClass getFunctionExpr() {
        return functionExpr;
    }

    public void setFunctionExpr(FunctionExprClass functionExpr) {
        this.functionExpr = functionExpr;
    }

    public ExprClass getExprInBracket() {
        return exprInBracket;
    }

    public void setExprInBracket(ExprClass exprInBracket) {
        this.exprInBracket = exprInBracket;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

        if(columnNameFaild!=null)
        {
            columnNameFaild.accept(astVisitor);
        }else if(exprOperatorExpr!=null)
        {
            exprOperatorExpr.accept(astVisitor);
        }else if(functionExpr!=null)
        {
            functionExpr.accept(astVisitor);
        }else if(exprInBracket!=null)
        {
            exprInBracket.accept(astVisitor);
        }else if(inCase!=null)
        {
            incaseExpr.accept(astVisitor);
            inCase.accept(astVisitor);
        }else if(existsCase!=null)
        {
            existsCase.accept(astVisitor);
        }

    }
}
