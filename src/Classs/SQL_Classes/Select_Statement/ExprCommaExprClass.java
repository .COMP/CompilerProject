package Classs.SQL_Classes.Select_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class ExprCommaExprClass extends StatementClass {
    public ExprCommaExprClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }
    private List<ExprClass> exprs;

    public List<ExprClass> getExprs() {
        return exprs;
    }

    public void setExprs(List<ExprClass> exprs) {
        this.exprs = exprs;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

        for (int i = 0; i < exprs.size(); i++) {
            exprs.get(i).accept(astVisitor);
        }

    }
}
