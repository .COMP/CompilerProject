package Classs.SQL_Classes.Select_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class ExprOperatorExprClass extends StatementClass {
    public ExprOperatorExprClass(int row, int column, String statement_name) {
        super(row, column,statement_name);
    }
   private ExprClass exp1;
   private String operator;
   private ExprClass exp2;

    public ExprClass getExp1() {
        return exp1;
    }

    public void setExp1(ExprClass exp1) {
        this.exp1 = exp1;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public ExprClass getExp2() {
        return exp2;
    }

    public void setExp2(ExprClass exp2) {
        this.exp2 = exp2;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

        if(exp1!=null)
        {
            exp1.accept(astVisitor);
        }
        if(exp2!=null)
        {
            exp2.accept(astVisitor);
        }

    }
}
