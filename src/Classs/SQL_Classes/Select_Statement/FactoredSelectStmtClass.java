package Classs.SQL_Classes.Select_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class FactoredSelectStmtClass extends StatementClass {
    public FactoredSelectStmtClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }
    private SelectCoreClass selectCoreClass;
    private OrderByClass orderBy;
    private LimitSelectClass limitSelect;

    public OrderByClass getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(OrderByClass orderBy) {
        this.orderBy = orderBy;
    }

    public LimitSelectClass getLimitSelect() {
        return limitSelect;
    }

    public void setLimitSelect(LimitSelectClass limitSelect) {
        this.limitSelect = limitSelect;
    }

    public SelectCoreClass getSelectCoreClass() {
        return selectCoreClass;
    }

    public void setSelectCoreClass(SelectCoreClass selectCoreClass) {
        this.selectCoreClass = selectCoreClass;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

        selectCoreClass.accept(astVisitor);

        if(orderBy!=null)
        {
            orderBy.accept(astVisitor);
        }

        if(limitSelect!=null)
        {
            limitSelect.accept(astVisitor);
        }
    }
}
