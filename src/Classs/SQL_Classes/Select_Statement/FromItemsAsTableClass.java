package Classs.SQL_Classes.Select_Statement;


import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class FromItemsAsTableClass extends StatementClass {
    public FromItemsAsTableClass(int row, int column, String statement_name) {
        super(row, column,statement_name);
    }
    private FromItemsClass fromItems;
    private boolean isAs;
    private String tableAlias=null;

    public FromItemsClass getFromItems() {
        return fromItems;
    }

    public void setFromItems(FromItemsClass fromItems) {
        this.fromItems = fromItems;
    }

    public boolean isAs() {
        return isAs;
    }

    public void setAs(boolean as) {
        isAs = as;
    }

    public String getTableAlias() {
        return tableAlias;
    }

    public void setTableAlias(String tableAlias) {
        this.tableAlias = tableAlias;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

        fromItems.accept(astVisitor);
    }
}
