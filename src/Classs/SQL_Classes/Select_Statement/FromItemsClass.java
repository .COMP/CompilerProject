package Classs.SQL_Classes.Select_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class FromItemsClass extends StatementClass {
    public FromItemsClass(int row, int column, String statement_name) {
        super(row, column,statement_name);
    }

    public List<TableOrSubqueryClass> getTableOrSubqueries() {
        return tableOrSubqueries;
    }

    public void setTableOrSubqueries(List<TableOrSubqueryClass> tableOrSubqueries) {
        this.tableOrSubqueries = tableOrSubqueries;
    }

    public JoinClauseClass getJoinClause() {
        return joinClause;
    }

    public void setJoinClause(JoinClauseClass joinClause) {
        this.joinClause = joinClause;
    }

    private List<TableOrSubqueryClass> tableOrSubqueries;
    private JoinClauseClass joinClause;

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

        if(joinClause!=null)
        {
         joinClause.accept(astVisitor);
        }
        else{
            for (int i = 0; i <tableOrSubqueries.size() ; i++) {
                tableOrSubqueries.get(i).accept(astVisitor);
            }

        }

    }
}
