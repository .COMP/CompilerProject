package Classs.SQL_Classes.Select_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class FunctionExprClass extends StatementClass {
    public FunctionExprClass(int row, int column, String statement_name) {
        super(row, column,statement_name);
    }

    private String functionName;
    private FunctionParameterSqlClass functionParameterSql;

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public FunctionParameterSqlClass getFunctionParameterSql() {
        return functionParameterSql;
    }

    public void setFunctionParameterSql(FunctionParameterSqlClass functionParameterSql) {
        this.functionParameterSql = functionParameterSql;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

        if(functionParameterSql!=null)
        {
            functionParameterSql.accept(astVisitor);
        }

    }
}
