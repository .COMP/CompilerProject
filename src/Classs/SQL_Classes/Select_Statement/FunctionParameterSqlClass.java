package Classs.SQL_Classes.Select_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class FunctionParameterSqlClass extends StatementClass {

    public FunctionParameterSqlClass(int row, int column, String statement_name) {
        super(row, column,statement_name);
    }


    private  boolean isStar;
    private  boolean isDistict;
    private List<ExprClass> exprs;

    public boolean isStar() {
        return isStar;
    }

    public void setStar(boolean star) {
        isStar = star;
    }

    public boolean isDistict() {
        return isDistict;
    }

    public void setDistict(boolean distict) {
        isDistict = distict;
    }

    public List<ExprClass> getExprs() {
        return exprs;
    }

    public void setExprs(List<ExprClass> exprs) {
        this.exprs = exprs;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if(!isStar)
        {
            for (int i = 0; i < exprs.size(); i++) {
                exprs.get(i).accept(astVisitor);
            }
        }

    }
}
