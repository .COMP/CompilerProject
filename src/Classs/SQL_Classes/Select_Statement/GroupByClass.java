package Classs.SQL_Classes.Select_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class GroupByClass extends StatementClass {
    public GroupByClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private List<ExprClass> exprs;
    private ExprClass havingExprs;

    public List<ExprClass> getExprs() {
        return exprs;
    }

    public void setExprs(List<ExprClass> exprs) {
        this.exprs = exprs;
    }

    public ExprClass getHavingExprs() {
        return havingExprs;
    }

    public void setHavingExprs(ExprClass havingExprs) {
        this.havingExprs = havingExprs;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

        for (int i = 0; i < exprs.size(); i++) {
            exprs.get(i).accept(astVisitor);
        }
        if(havingExprs!=null)
        {
            havingExprs.accept(astVisitor);
        }

    }
}
