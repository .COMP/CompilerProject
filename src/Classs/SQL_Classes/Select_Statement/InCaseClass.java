package Classs.SQL_Classes.Select_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class InCaseClass extends StatementClass {
    public InCaseClass(int row, int column, String statement_name) {
        super(row, column,statement_name);
    }

    private boolean kNot;
    private SelectOrExprClass selectOrExpr;
    private TableNameFaildClass tableNameFaild;

    public boolean iskNot() {
        return kNot;
    }

    public void setkNot(boolean kNot) {
        this.kNot = kNot;
    }

    public SelectOrExprClass getSelectOrExpr() {
        return selectOrExpr;
    }

    public void setSelectOrExpr(SelectOrExprClass selectOrExpr) {
        this.selectOrExpr = selectOrExpr;
    }

    public TableNameFaildClass getTableNameFaild() {
        return tableNameFaild;
    }

    public void setTableNameFaild(TableNameFaildClass tableNameFaild) {
        this.tableNameFaild = tableNameFaild;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        if(selectOrExpr!=null)
        {
            selectOrExpr.accept(astVisitor);
        }
        else {
            tableNameFaild.accept(astVisitor);
        }


    }
}
