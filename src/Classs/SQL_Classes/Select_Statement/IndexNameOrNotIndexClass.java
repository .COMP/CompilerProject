package Classs.SQL_Classes.Select_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class IndexNameOrNotIndexClass extends StatementClass {
    public IndexNameOrNotIndexClass(int row, int column, String statement_name) {
        super(row, column,statement_name);
    }

    private  String indexName=null;
    private  boolean notIndex;

    public String getIndexName() {
        return indexName;
    }

    public void setIndexName(String indexName) {
        this.indexName = indexName;
    }

    public boolean isNotIndex() {
        return notIndex;
    }

    public void setNotIndex(boolean notIndex) {
        this.notIndex = notIndex;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
    }
}
