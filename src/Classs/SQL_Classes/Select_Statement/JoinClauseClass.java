package Classs.SQL_Classes.Select_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class JoinClauseClass extends StatementClass {
    public JoinClauseClass(int row, int column, String statement_name) {
        super(row, column,statement_name);
    }

    private TableOrSubqueryClass tableOrSubquery;
    private List<OperatorTableConstraintClass> operatorTableConstraints;

    public TableOrSubqueryClass getTableOrSubquery() {
        return tableOrSubquery;
    }

    public void setTableOrSubquery(TableOrSubqueryClass tableOrSubquery) {
        this.tableOrSubquery = tableOrSubquery;
    }

    public List<OperatorTableConstraintClass> getOperatorTableConstraints() {
        return operatorTableConstraints;
    }

    public void setOperatorTableConstraints(List<OperatorTableConstraintClass> operatorTableConstraints) {
        this.operatorTableConstraints = operatorTableConstraints;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

        tableOrSubquery.accept(astVisitor);
        for (int i = 0; i < operatorTableConstraints.size(); i++) {
            operatorTableConstraints.get(i).accept(astVisitor);
        }

    }
}
