package Classs.SQL_Classes.Select_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class JoinOperatorClass extends StatementClass {
    public JoinOperatorClass(int row, int column, String statement_name) {
        super(row, column,statement_name);
    }
     private boolean isComma;
     private boolean isLeft;
     private boolean isLeftOuter;
     private boolean isInner;

    public boolean isComma() {
        return isComma;
    }

    public void setComma(boolean comma) {
        isComma = comma;
    }

    public boolean isLeft() {
        return isLeft;
    }

    public void setLeft(boolean left) {
        isLeft = left;
    }

    public boolean isLeftOuter() {
        return isLeftOuter;
    }

    public void setLeftOuter(boolean leftOuter) {
        isLeftOuter = leftOuter;
    }

    public boolean isInner() {
        return isInner;
    }

    public void setInner(boolean inner) {
        isInner = inner;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

    }
}
