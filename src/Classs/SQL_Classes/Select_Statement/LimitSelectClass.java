package Classs.SQL_Classes.Select_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class LimitSelectClass extends StatementClass {
    public LimitSelectClass(int row, int column, String statement_name) {
        super(row, column,statement_name);
    }
    private ExprClass limitExpr;
    private ExprClass commaOrOffsetExpr;
    private boolean isComma;
    private boolean isOffset;

    public ExprClass getLimitExpr() {
        return limitExpr;
    }

    public void setLimitExpr(ExprClass limitExpr) {
        this.limitExpr = limitExpr;
    }

    public ExprClass getCommaOrOffsetExpr() {
        return commaOrOffsetExpr;
    }

    public void setCommaOrOffsetExpr(ExprClass commaOrOffsetExpr) {
        this.commaOrOffsetExpr = commaOrOffsetExpr;
    }

    public boolean isComma() {
        return isComma;
    }

    public void setComma(boolean comma) {
        isComma = comma;
    }

    public boolean isOffset() {
        return isOffset;
    }

    public void setOffset(boolean offset) {
        isOffset = offset;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        limitExpr.accept(astVisitor);
        if(commaOrOffsetExpr!=null)
        {
            commaOrOffsetExpr.accept(astVisitor);
        }

    }
}
