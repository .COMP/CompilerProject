package Classs.SQL_Classes.Select_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class OperatorTableConstraintClass extends StatementClass {
    public OperatorTableConstraintClass(int row, int column, String statement_name) {
        super(row, column,statement_name);
    }
    private JoinOperatorClass joinOperator;
    private TableOrSubqueryClass tableOrSubquery;
    private ExprClass joinExpr;

    public TableOrSubqueryClass getTableOrSubquery() {
        return tableOrSubquery;
    }

    public void setTableOrSubquery(TableOrSubqueryClass tableOrSubquery) {
        this.tableOrSubquery = tableOrSubquery;
    }

    public ExprClass getJoinExpr() {
        return joinExpr;
    }

    public void setJoinExpr(ExprClass joinExpr) {
        this.joinExpr = joinExpr;
    }

    public JoinOperatorClass getJoinOperator() {
        return joinOperator;
    }

    public void setJoinOperator(JoinOperatorClass joinOperator) {
        this.joinOperator = joinOperator;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

        joinOperator.accept(astVisitor);
        tableOrSubquery.accept(astVisitor);
        if(joinExpr != null)
            joinExpr.accept(astVisitor);
    }
}
