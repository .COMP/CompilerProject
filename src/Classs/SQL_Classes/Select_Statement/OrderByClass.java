package Classs.SQL_Classes.Select_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class OrderByClass extends StatementClass {
    public OrderByClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }
    private List<OrderingTermClass> orderingTerms;

    public List<OrderingTermClass> getOrderingTerms() {
        return orderingTerms;
    }

    public void setOrderingTerms(List<OrderingTermClass> orderingTerms) {
        this.orderingTerms = orderingTerms;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

        for (int i = 0; i <orderingTerms.size() ; i++) {
            orderingTerms.get(i).accept(astVisitor);
        }

    }
}
