package Classs.SQL_Classes.Select_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class OrderingTermClass extends StatementClass {
    public OrderingTermClass(int row, int column, String statement_name) {
        super(row, column,statement_name);
    }
    private ExprClass expr;
    private String collisionName=null;
    private boolean isAsc;
    private boolean isDesc;

    public ExprClass getExpr() {
        return expr;
    }

    public void setExpr(ExprClass expr) {
        this.expr = expr;
    }

    public String getCollisionName() {
        return collisionName;
    }

    public void setCollisionName(String collisionName) {
        this.collisionName = collisionName;
    }

    public boolean isAsc() {
        return isAsc;
    }

    public void setAsc(boolean asc) {
        isAsc = asc;
    }

    public boolean isDesc() {
        return isDesc;
    }

    public void setDesc(boolean desc) {
        isDesc = desc;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        expr.accept(astVisitor);

    }
}
