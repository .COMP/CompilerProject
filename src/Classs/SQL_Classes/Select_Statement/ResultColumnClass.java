package Classs.SQL_Classes.Select_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class ResultColumnClass extends StatementClass {
    public ResultColumnClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private boolean isStar;
    private String isTableNameWithStar=null;
    private ResultColumnWithExprClass resultColumnWithExpr;

    public boolean isStar() {
        return isStar;
    }

    public void setStar(boolean star) {
        isStar = star;
    }

    public String isTableNameWithStar() {
        return isTableNameWithStar;
    }

    public void setTableNameWithStar(String tableNameWithStar) {
        isTableNameWithStar = tableNameWithStar;
    }

    public ResultColumnWithExprClass getResultColumnWithExpr() {
        return resultColumnWithExpr;
    }

    public void setResultColumnWithExpr(ResultColumnWithExprClass resultColumnWithExpr) {
        this.resultColumnWithExpr = resultColumnWithExpr;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

        if(resultColumnWithExpr!=null)
        {
            resultColumnWithExpr.accept(astVisitor);
        }

    }
}
