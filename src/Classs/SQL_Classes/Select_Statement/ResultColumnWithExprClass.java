package Classs.SQL_Classes.Select_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class ResultColumnWithExprClass extends StatementClass {
    public ResultColumnWithExprClass(int row, int column, String statement_name) {
        super(row, column,statement_name);
    }
    private ExprClass expr;
    private boolean as;
    private String columnAlias;

    public ExprClass getExpr() {
        return expr;
    }

    public void setExpr(ExprClass expr) {
        this.expr = expr;
    }

    public boolean isAs() {
        return as;
    }

    public void setAs(boolean as) {
        this.as = as;
    }

    public String getColumnAlias() {
        return columnAlias;
    }

    public void setColumnAlias(String columnAlias) {
        this.columnAlias = columnAlias;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        expr.accept(astVisitor);
    }
}
