package Classs.SQL_Classes.Select_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class SelectCoreClass extends StatementClass {
    public SelectCoreClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private SelectItemsClass selectItemes;
    private FromItemsClass fromItems;
    private ExprClass whereExpr;
    private GroupByClass groupBy;
    private ValuesItemsClass valuesItems;

    public GroupByClass getGroupBy() {
        return groupBy;
    }

    public void setGroupBy(GroupByClass groupBy) {
        this.groupBy = groupBy;
    }

    public SelectItemsClass getSelectItemes() {
        return selectItemes;
    }

    public void setSelectItemes(SelectItemsClass selectItemes) {
        this.selectItemes = selectItemes;
    }

    public FromItemsClass getFromItems() {
        return fromItems;
    }

    public void setFromItems(FromItemsClass fromItems) {
        this.fromItems = fromItems;
    }

    public ExprClass getWhereExpr() {
        return whereExpr;
    }

    public void setWhereExpr(ExprClass whereExpr) {
        this.whereExpr = whereExpr;
    }

    public ValuesItemsClass getValuesItems() {
        return valuesItems;
    }

    public void setValuesItems(ValuesItemsClass valuesItems) {
        this.valuesItems = valuesItems;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

        if(valuesItems!=null)
        {
            valuesItems.accept(astVisitor);
        }else{
            selectItemes.accept(astVisitor);
            if(fromItems!=null)
            {
                fromItems.accept(astVisitor);
            }
            if(whereExpr!=null)
            {
                whereExpr.accept(astVisitor);
            }
            if(groupBy!=null)
            {
                groupBy.accept(astVisitor);
            }

        }

    }
}
