package Classs.SQL_Classes.Select_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;


public class SelectItemsClass extends StatementClass {
    public SelectItemsClass(int row, int column, String statement_name) {
        super(row, column,statement_name);
    }

    private  boolean isDistinct;
    private  boolean isAll;
    private  List<ResultColumnClass> resultColumns;

    public boolean isDistinct() {
        return isDistinct;
    }

    public void setDistinct(boolean distinct) {
        isDistinct = distinct;
    }

    public boolean isAll() {
        return isAll;
    }

    public void setAll(boolean all) {
        isAll = all;
    }

    public List<ResultColumnClass> getResultColumns() {
        return resultColumns;
    }

    public void setResultColumns(List<ResultColumnClass> resultColumns) {
        this.resultColumns = resultColumns;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        for (int i = 0; i <resultColumns.size() ; i++) {
            resultColumns.get(i).accept(astVisitor);
        }

    }
}
