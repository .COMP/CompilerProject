package Classs.SQL_Classes.Select_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class SelectOrExprClass extends StatementClass {
    public SelectOrExprClass(int row, int column, String statement_name) {
        super(row, column,statement_name);
    }


    private SelectStmtClass selectStmt;
    private List<ExprClass> exprs;

    public SelectStmtClass getSelectStmt() {
        return selectStmt;
    }

    public void setSelectStmt(SelectStmtClass selectStmt) {
        this.selectStmt = selectStmt;
    }

    public List<ExprClass> getExprs() {
        return exprs;
    }

    public void setExprs(List<ExprClass> exprs) {
        this.exprs = exprs;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

        if(selectStmt!=null)
        {
            selectStmt.accept(astVisitor);
        }
        if(exprs != null) {
            for (int i = 0; i < exprs.size(); i++) {
                exprs.get(i).accept(astVisitor);
            }
        }
    }
}
