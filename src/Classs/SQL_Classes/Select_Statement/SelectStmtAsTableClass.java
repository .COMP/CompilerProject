package Classs.SQL_Classes.Select_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class SelectStmtAsTableClass extends StatementClass {
    public SelectStmtAsTableClass(int row, int column, String statement_name) {
        super(row, column,statement_name);
    }
    private SelectStmtClass  selectStmt;
    private boolean isAs;
    private String tableAlias=null;

    public SelectStmtClass getSelectStmt() {
        return selectStmt;
    }

    public void setSelectStmt(SelectStmtClass selectStmt) {
        this.selectStmt = selectStmt;
    }

    public boolean isAs() {
        return isAs;
    }

    public void setAs(boolean as) {
        isAs = as;
    }

    public String getTableAlias() {
        return tableAlias;
    }

    public void setTableAlias(String tableAlias) {
        this.tableAlias = tableAlias;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        selectStmt.accept(astVisitor);
    }
}
