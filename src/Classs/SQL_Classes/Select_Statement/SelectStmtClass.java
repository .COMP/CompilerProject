package Classs.SQL_Classes.Select_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class SelectStmtClass extends StatementClass {


    private FactoredSelectStmtClass factoredSelectStmt;



    public FactoredSelectStmtClass getFactoredSelectStmt() {
        return factoredSelectStmt;
    }

    public void setFactoredSelectStmt(FactoredSelectStmtClass factoredSelectStmt) {
        this.factoredSelectStmt = factoredSelectStmt;
    }

    public SelectStmtClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

        factoredSelectStmt.accept(astVisitor);

    }
}
