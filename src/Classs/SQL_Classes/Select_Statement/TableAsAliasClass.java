package Classs.SQL_Classes.Select_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class TableAsAliasClass extends StatementClass {
    public TableAsAliasClass(int row, int column, String statement_name) {
        super(row, column,statement_name);
    }
    private TableNameFaildClass tableNameFaild;
    private boolean isAs;
    private String  tableAlias;
    private IndexNameOrNotIndexClass indexNameOrNotIndex;

    public TableNameFaildClass getTableNameFaild() {
        return tableNameFaild;
    }

    public void setTableNameFaild(TableNameFaildClass tableNameFaild) {
        this.tableNameFaild = tableNameFaild;
    }

    public boolean isAs() {
        return isAs;
    }

    public void setAs(boolean as) {
        isAs = as;
    }

    public String getTableAlias() {
        return tableAlias;
    }

    public void setTableAlias(String tableAlias) {
        this.tableAlias = tableAlias;
    }

    public IndexNameOrNotIndexClass getIndexNameOrNotIndex() {
        return indexNameOrNotIndex;
    }

    public void setIndexNameOrNotIndex(IndexNameOrNotIndexClass indexNameOrNotIndex) {
        this.indexNameOrNotIndex = indexNameOrNotIndex;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        tableNameFaild.accept(astVisitor);
        if(indexNameOrNotIndex!=null)
        {
            indexNameOrNotIndex.accept(astVisitor);
        }

    }
}
