package Classs.SQL_Classes.Select_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class TableOrSubqueryClass extends StatementClass {
    public TableOrSubqueryClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private  TableAsAliasClass tableAsAlias;
    private  FromItemsAsTableClass fromItemsAsTable;
    private  SelectStmtAsTableClass selectStmtAsTable;

    public TableAsAliasClass getTableAsAlias() {
        return tableAsAlias;
    }

    public void setTableAsAlias(TableAsAliasClass tableAsAlias) {
        this.tableAsAlias = tableAsAlias;
    }

    public FromItemsAsTableClass getFromItemsAsTable() {
        return fromItemsAsTable;
    }

    public void setFromItemsAsTable(FromItemsAsTableClass fromItemsAsTable) {
        this.fromItemsAsTable = fromItemsAsTable;
    }

    public SelectStmtAsTableClass getSelectStmtAsTable() {
        return selectStmtAsTable;
    }

    public void setSelectStmtAsTable(SelectStmtAsTableClass selectStmtAsTable) {
        this.selectStmtAsTable = selectStmtAsTable;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

        if(tableAsAlias!=null)
        {
         tableAsAlias.accept(astVisitor);
        }
        else if(fromItemsAsTable!=null)
        {
            fromItemsAsTable.accept(astVisitor);
        }
        else
        {
         selectStmtAsTable.accept(astVisitor);
        }

    }
}
