package Classs.SQL_Classes.Select_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class ValuesItemsClass extends StatementClass {
    public ValuesItemsClass(int row, int column, String statement_name) {
        super(row, column,statement_name);
    }
    private List<ExprCommaExprClass> exprCommaExprs;

    public List<ExprCommaExprClass> getExprCommaExprs() {
        return exprCommaExprs;
    }

    public void setExprCommaExprs(List<ExprCommaExprClass> exprCommaExprs) {
        this.exprCommaExprs = exprCommaExprs;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);
        for (int i = 0; i < exprCommaExprs.size(); i++) {
            exprCommaExprs.get(i).accept(astVisitor);
        }

    }
}
