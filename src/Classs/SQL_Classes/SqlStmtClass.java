package Classs.SQL_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Classs.SQL_Classes.Alter_Table_Statement.AlterTableStmtClass;
import Classs.SQL_Classes.Create_Table_Statement.CreateTableStmtClass;
import Classs.SQL_Classes.Create_Type_Statement.CreateTypeStmtClass;
import Classs.SQL_Classes.Create_ِِAggregation_Function.CreateAggregationFunctionClass;
import Classs.SQL_Classes.Delete_Statement.DeleteStmtClass;
import Classs.SQL_Classes.Drop_Table_Statement.DropTableStmtClass;
import Classs.SQL_Classes.Insert_Statement.InsertStmtClass;
import Classs.SQL_Classes.Select_Statement.FactoredSelectStmtClass;
import Classs.SQL_Classes.Update_Statement.UpdateStmtClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class SqlStmtClass extends StatementClass  {
    public SqlStmtClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private FactoredSelectStmtClass factoredSelectStmtClass;
    private UpdateStmtClass updateStmt;
    private InsertStmtClass insertStmt;
    private DropTableStmtClass dropTableStmt;
    private DeleteStmtClass deleteStmt;
    private CreateTableStmtClass createTableStmt;
    private AlterTableStmtClass alterTableStmt;
    private CreateTypeStmtClass createTypeStmt;
    private CreateAggregationFunctionClass createAggregationFunction;

    public CreateTypeStmtClass getCreateTypeStmt() {
        return createTypeStmt;
    }

    public void setCreateTypeStmt(CreateTypeStmtClass createTypeStmt) {
        this.createTypeStmt = createTypeStmt;
    }

    public CreateAggregationFunctionClass getCreateAggregationFunction() {
        return createAggregationFunction;
    }

    public void setCreateAggregationFunction(CreateAggregationFunctionClass createAggregationFunction) {
        this.createAggregationFunction = createAggregationFunction;
    }

    public CreateTableStmtClass getCreateTableStmt() {
        return createTableStmt;
    }

    public void setCreateTableStmt(CreateTableStmtClass createTableStmt) {
        this.createTableStmt = createTableStmt;
    }

    public AlterTableStmtClass getAlterTableStmt() {
        return alterTableStmt;
    }

    public void setAlterTableStmt(AlterTableStmtClass alterTableStmt) {
        this.alterTableStmt = alterTableStmt;
    }

    public DeleteStmtClass getDeleteStmt() {
        return deleteStmt;
    }

    public void setDeleteStmt(DeleteStmtClass deleteStmt) {
        this.deleteStmt = deleteStmt;
    }

    public UpdateStmtClass getUpdateStmt() {
        return updateStmt;
    }

    public void setUpdateStmt(UpdateStmtClass updateStmt) {
        this.updateStmt = updateStmt;
    }

    public InsertStmtClass getInsertStmt() {
        return insertStmt;
    }

    public void setInsertStmt(InsertStmtClass insertStmt) {
        this.insertStmt = insertStmt;
    }

    public DropTableStmtClass getDropTableStmt() {
        return dropTableStmt;
    }

    public void setDropTableStmt(DropTableStmtClass dropTableStmt) {
        this.dropTableStmt = dropTableStmt;
    }

    public FactoredSelectStmtClass getFactoredSelectStmtClass() {
        return factoredSelectStmtClass;
    }


    public void setFactoredSelectStmtClass(FactoredSelectStmtClass factoredSelectStmtClass) {
        this.factoredSelectStmtClass = factoredSelectStmtClass;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

        if(factoredSelectStmtClass!=null)
        {
            factoredSelectStmtClass.accept(astVisitor);
        }else if(alterTableStmt!=null)
        {
            alterTableStmt.accept(astVisitor);
        }else if(deleteStmt!=null)
        {
            deleteStmt.accept(astVisitor);
        }else if (dropTableStmt!=null)
        {
            dropTableStmt.accept(astVisitor);
        }else if(createTableStmt!=null)
        {
            createTableStmt.accept(astVisitor);
        }else if(insertStmt!=null)
        {
            insertStmt.accept(astVisitor);
        }else if(updateStmt!=null){
            updateStmt.accept(astVisitor);
        }else if(createTypeStmt!=null){
            createTypeStmt.accept(astVisitor);
        }else {
            createAggregationFunction.accept(astVisitor);
        }


    }
}
