package Classs.SQL_Classes;

import Classs.Abstruction_Classes.StatementClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.ArrayList;
import java.util.List;

public class SqlStmtListClass extends StatementClass {
    public SqlStmtListClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
        this.sqlStmtClasses=new ArrayList<>();
    }
    private List<SqlStmtClass> sqlStmtClasses;

    public List<SqlStmtClass> getSqlStmtClasses() {
        return sqlStmtClasses;
    }

    public void setSqlStmtClasses(List<SqlStmtClass> sqlStmtClasses) {
        this.sqlStmtClasses = sqlStmtClasses;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {
        astVisitor.visit(this);

        for (int i = 0; i < sqlStmtClasses.size(); i++) {
            sqlStmtClasses.get(i).accept(astVisitor);
        }

    }
}
