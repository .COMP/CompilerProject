package Classs.SQL_Classes.Update_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Classs.SQL_Classes.Select_Statement.ExprClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class ColumnNameEqualsExprClass extends StatementClass {
    public ColumnNameEqualsExprClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private String columnName;
    private ExprClass expr;

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public ExprClass getExpr() {
        return expr;
    }

    public void setExpr(ExprClass expr) {
        this.expr = expr;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {

    }
}
