package Classs.SQL_Classes.Update_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Classs.SQL_Classes.Select_Statement.TableNameFaildClass;
import Visitor.AST_Visitor.AstVisitorInterface;

public class QualifiedTableNameClass extends StatementClass {
    public QualifiedTableNameClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private TableNameFaildClass tableNameFaild;
    private String indexName;
    private boolean notIndex;

    public TableNameFaildClass getTableNameFaild() {
        return tableNameFaild;
    }

    public void setTableNameFaild(TableNameFaildClass tableNameFaild) {
        this.tableNameFaild = tableNameFaild;
    }

    public String getIndexName() {
        return indexName;
    }

    public void setIndexName(String indexName) {
        this.indexName = indexName;
    }

    public boolean isNotIndex() {
        return notIndex;
    }

    public void setNotIndex(boolean notIndex) {
        this.notIndex = notIndex;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {

    }
}
