package Classs.SQL_Classes.Update_Statement;

import Classs.Abstruction_Classes.StatementClass;
import Classs.SQL_Classes.Select_Statement.ExprClass;
import Visitor.AST_Visitor.AstVisitorInterface;

import java.util.List;

public class UpdateStmtClass extends StatementClass {
    public UpdateStmtClass(int row, int column, String statement_name) {
        super(row, column, statement_name);
    }

    private QualifiedTableNameClass qualifiedTableName;
    private List<ColumnNameEqualsExprClass> columnNameEqualsExprs;
    private ExprClass whereExpr;

    public ExprClass getWhereExpr() {
        return whereExpr;
    }

    public void setWhereExpr(ExprClass whereExpr) {
        this.whereExpr = whereExpr;
    }

    public QualifiedTableNameClass getQualifiedTableName() {
        return qualifiedTableName;
    }

    public void setQualifiedTableName(QualifiedTableNameClass qualifiedTableName) {
        this.qualifiedTableName = qualifiedTableName;
    }

    public List<ColumnNameEqualsExprClass> getColumnNameEqualsExprs() {
        return columnNameEqualsExprs;
    }

    public void setColumnNameEqualsExprs(List<ColumnNameEqualsExprClass> columnNameEqualsExprs) {
        this.columnNameEqualsExprs = columnNameEqualsExprs;
    }

    @Override
    public void accept(AstVisitorInterface astVisitor) {

    }
}
