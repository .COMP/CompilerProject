package ErrorHandler;

import FileManager.FileManager;

public class AssignAnotherTypeErrorClass extends ErrorHandlerClass {
    private String variable_type;
    private String value_type;

    public AssignAnotherTypeErrorClass(String variable_type, String value_type, String error_message, String error_line, String error_columns) {
        super(error_message, error_line, error_columns);
        this.variable_type = variable_type;
        this.value_type = value_type;
    }

    @Override
    public boolean checkingError() {
        if (this.value_type != null) {
            if (!this.value_type.equals(this.variable_type)) {
                FileManager.getInstance().write("Error " + this.variable_type + ", " + this.value_type + this.error_message + "line(" + this.error_line + "), columns(" + this.error_columns + ") .");
                System.err.println("Error " + this.variable_type + ", " + this.value_type + this.error_message + "line(" + this.error_line + "), columns(" + this.error_columns + ") .");

                return false;
            }
        } else {
            FileManager.getInstance().write("Error " + this.variable_type + ", " + this.value_type + this.error_message + "line(" + this.error_line + "), columns(" + this.error_columns + ") .");
            System.err.println("Error " + this.variable_type + ", " + this.value_type + this.error_message + "line(" + this.error_line + "), columns(" + this.error_columns + ") .");

            return false;
        }
        return true;
    }
}
