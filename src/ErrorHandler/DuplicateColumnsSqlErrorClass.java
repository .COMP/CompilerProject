package ErrorHandler;

import Classs.SQL_Classes.Create_Table_Statement.ColumnDefinitionClass;
import FileManager.FileManager;

import java.util.List;
import java.util.Set;

import static Utils.Utils.findDuplicates;

public class DuplicateColumnsSqlErrorClass extends ErrorHandlerClass {
    private List<ColumnDefinitionClass> column_definition;

    public DuplicateColumnsSqlErrorClass(List<ColumnDefinitionClass> column_definition, String error_message, String error_line, String error_columns) {
        super(error_message, error_line, error_columns);
        this.column_definition = column_definition;
    }

    @Override
    public boolean checkingError() {
        Set<ColumnDefinitionClass> resutl = findDuplicates(this.column_definition);
        if (resutl.size() > 0) {
            String column_name = "";
            for(ColumnDefinitionClass item: resutl){
                column_name = item.getColumnName();
                this.error_line = String.valueOf(item.getRow());
                this.error_columns = String.valueOf(item.getColumn());
                FileManager.getInstance().write("Error " + column_name + this.error_message + "line(" + this.error_line + "), columns(" + this.error_columns + ") .");
                System.err.println("Error " + column_name + this.error_message + "line(" + this.error_line + "), columns(" + this.error_columns + ") .");

            }
            return false;
        }
        return true;
    }


}
