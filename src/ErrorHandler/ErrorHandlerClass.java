package ErrorHandler;

public abstract class ErrorHandlerClass {
    protected String error_line;
    protected String error_columns;
    protected String error_message;

    public ErrorHandlerClass(String error_message, String error_line, String error_columns) {
        this.error_message = error_message;
        this.error_line = error_line;
        this.error_columns = error_columns;
    }

    public abstract boolean  checkingError();

    public String getError_line() {
        return error_line;
    }

    public void setError_line(String error_line) {
        this.error_line = error_line;
    }

    public String getError_columns() {
        return error_columns;
    }

    public void setError_columns(String error_columns) {
        this.error_columns = error_columns;
    }

    public String getError_message() {
        return error_message;
    }

    public void setError_message(String error_message) {
        this.error_message = error_message;
    }
}
