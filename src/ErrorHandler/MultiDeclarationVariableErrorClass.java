package ErrorHandler;

import FileManager.FileManager;
import SymbolTable.ScopeClass;
import SymbolTable.SymbolClass;
import TableManagment.SymbolTableManager;

import java.util.Stack;

public class MultiDeclarationVariableErrorClass extends ErrorHandlerClass {
    private String variable_name;

    public MultiDeclarationVariableErrorClass(String variable_name, String error_message, String error_line, String error_columns) {
        super(error_message, error_line, error_columns);
        this.variable_name = variable_name;
    }

    @Override
    public boolean checkingError() {
        Stack<String> scope_level = (Stack<String>) SymbolTableManager.getInstance().getScopes_level().clone();
        int size = scope_level.size();
        for (int i = 0; i < size; i++) {
            String current_scope_id = scope_level.pop();

            boolean is_duplicated = SymbolTableManager.getInstance().getSymbol_table().getScopes().get(current_scope_id).getSymbols().containsKey(this.variable_name);
            if (is_duplicated) {
                FileManager.getInstance().write("Error " + this.variable_name + this.error_message + "line(" + this.error_line + "), columns(" + this.error_columns + ") .");
                System.err.println("Error " + this.variable_name + this.error_message + "line(" + this.error_line + "), columns(" + this.error_columns + ") .");
                return false;
            }

        }
        return true;
    }

    public String getVariable_name() {
        return variable_name;
    }

    public void setVariable_name(String variable_name) {
        this.variable_name = variable_name;
    }
}
