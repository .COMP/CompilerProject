package ErrorHandler;

import Classs.SQL_Classes.Select_Statement.ExprClass;
import Classs.SQL_Classes.Select_Statement.ResultColumnClass;
import FileManager.FileManager;

import java.util.List;

public class OnClauseErrorClass extends ErrorHandlerClass {
    private final ExprClass exprClass;

    public OnClauseErrorClass(ExprClass exprClass, String error_message, String error_line, String error_columns) {
        super(error_message, error_line, error_columns);
        this.exprClass = exprClass;
    }

    @Override
    public boolean checkingError() {
        FileManager.getInstance().write(
                "Error " + this.error_message + "line(" + this.error_line + "), columns(" + this.error_columns + ") ."
        );
        System.err.println("Error " + this.error_message + "line(" + this.error_line + "), columns(" + this.error_columns + ") .");
        return false;
    }
}

