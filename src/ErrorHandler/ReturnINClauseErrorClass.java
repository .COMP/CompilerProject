package ErrorHandler;

import Classs.SQL_Classes.Select_Statement.ResultColumnClass;
import FileManager.FileManager;
import TableManagment.SymbolTableManager;

import java.util.List;

public class ReturnINClauseErrorClass extends ErrorHandlerClass {
    private final List<ResultColumnClass> resultColumnClasses;

    public ReturnINClauseErrorClass(List<ResultColumnClass> resultColumnClasses, String error_message, String error_line, String error_columns) {
        super(error_message, error_line, error_columns);
        this.resultColumnClasses = resultColumnClasses;
    }

    @Override
    public boolean checkingError() {
        if (resultColumnClasses.size() != 1) {
            FileManager.getInstance().write(
                    "Error " + this.error_message + "line(" + this.error_line + "), columns(" + this.error_columns + ") ."
            );
            System.err.println("Error " + this.error_message + "line(" + this.error_line + "), columns(" + this.error_columns + ") .");
            return false;
        } else {
            if (resultColumnClasses.get(0).getResultColumnWithExpr() == null){
                FileManager.getInstance().write(
                        "Error " + this.error_message + "line(" + this.error_line + "), columns(" + this.error_columns + ") ."
                );
                System.err.println("Error " + this.error_message + "line(" + this.error_line + "), columns(" + this.error_columns + ") ."
                );
                return false;
            }
        return true;
        }
    }
}

