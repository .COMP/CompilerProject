package ErrorHandler;

import FileManager.FileManager;
import SymbolTable.ScopeClass;
import SymbolTable.SymbolClass;
import TableManagment.SymbolTableManager;

import java.util.Stack;

public class UnassignedVariableErrorClass extends ErrorHandlerClass {
    private String variable_value;

    public UnassignedVariableErrorClass(String variable_value, String error_message, String error_line, String error_columns) {
        super(error_message, error_line, error_columns);
        this.variable_value = variable_value;
    }

    @Override
    public boolean checkingError() {
        Stack<String> scope_level = (Stack<String>) SymbolTableManager.getInstance().getScopes_level().clone();
        int size = scope_level.size();
        for (int i = 0; i < size; i++) {
            String current_scope_id = scope_level.pop();
            ScopeClass current_scope = SymbolTableManager.getInstance().getSymbol_table().getScopes().get(current_scope_id);
            SymbolClass symbol = current_scope.getSymbols().get(this.variable_value);
            if (symbol != null) {
                String type = symbol.getSymbol_type();
                if (type != null) {
                    return true;
                } else {
                    FileManager.getInstance().write("Warring " +this.variable_value + this.error_message + "line(" + this.error_line + "), columns(" + this.error_columns + ") .");
                    System.err.println("Warring " +this.variable_value + this.error_message + "line(" + this.error_line + "), columns(" + this.error_columns + ") .");

                    return false;
                }
            }
        }
        return false;
    }
}
