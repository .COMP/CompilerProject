package ErrorHandler;

import FileManager.FileManager;
import TableManagment.SymbolTableManager;

public class UndeclaredFunctionErrorClass extends ErrorHandlerClass {

    private String function_name;

    public UndeclaredFunctionErrorClass(String function_name, String error_message, String error_line, String error_columns) {
        super(error_message, error_line, error_columns);
        this.function_name = function_name;
    }

    @Override
    public boolean checkingError() {
        boolean is_declared = SymbolTableManager.getInstance().getFunction_names().containsKey(this.function_name);
        if (is_declared)
            return true;

        FileManager.getInstance().write("Error " +this.function_name + this.error_message + "line(" + this.error_line + "), columns(" + this.error_columns + ") .");
        System.err.println("Error " +this.function_name + this.error_message + "line(" + this.error_line + "), columns(" + this.error_columns + ") .");

        return false;
    }
}

