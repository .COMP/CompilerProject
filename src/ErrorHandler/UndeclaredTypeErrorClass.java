package ErrorHandler;

import FileManager.FileManager;
import TableManagment.SymbolTableManager;

public class UndeclaredTypeErrorClass extends ErrorHandlerClass {
    private final String tableName;

    public UndeclaredTypeErrorClass(String tableName, String error_message, String error_line, String error_columns) {
        super(error_message, error_line, error_columns);
        this.tableName = tableName;
    }

    @Override
    public boolean checkingError() {
        boolean is_declared = SymbolTableManager.getInstance().getSymbol_table().getDeclaredTypes().containsKey(this.tableName);
        if (!is_declared){
            FileManager.getInstance().write(
                    "Error " +this.tableName + this.error_message + "line(" + this.error_line + "), columns(" + this.error_columns + ") ."
            );
            System.err.println(  "Error " +this.tableName + this.error_message + "line(" + this.error_line + "), columns(" + this.error_columns + ") .");
            return false;
        }
        return true;
    }
}

