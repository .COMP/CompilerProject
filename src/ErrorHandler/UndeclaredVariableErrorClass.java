package ErrorHandler;

import FileManager.FileManager;
import SymbolTable.ScopeClass;
import TableManagment.SymbolTableManager;

public class UndeclaredVariableErrorClass extends ErrorHandlerClass {
    private final String variable_name;

    public UndeclaredVariableErrorClass(String error_message, String variable_name, String error_line, String error_columns) {
        super(error_message, error_line, error_columns);
        this.variable_name = variable_name;
    }

    @Override
    public boolean checkingError() {
        for (String scope_id : SymbolTableManager.getInstance().getScopes_level()) {
            ScopeClass current_scope = SymbolTableManager.getInstance().getSymbol_table().getScopes().get(scope_id);
            if (current_scope.getSymbols().containsKey(this.variable_name)) {
                return true;
            }
        }
        FileManager.getInstance().write("Error " + this.variable_name + this.error_message + "line(" + this.error_line + "), columns(" + this.error_columns + ") .");
        System.err.println("Error " + this.variable_name + this.error_message + "line(" + this.error_line + "), columns(" + this.error_columns + ") .");

        return false;
    }
}
