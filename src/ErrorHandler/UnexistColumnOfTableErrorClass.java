package ErrorHandler;

import Classs.SQL_Classes.Select_Statement.*;
import FileManager.FileManager;
import SymbolTable.SqlTypeClass;
import TableManagment.SymbolTableManager;

import java.util.ArrayList;
import java.util.List;

public class UnexistColumnOfTableErrorClass extends  ErrorHandlerClass {
    private FactoredSelectStmtClass selectStmt;
    public UnexistColumnOfTableErrorClass(String error_message, String error_line, String error_columns,FactoredSelectStmtClass factoredSelectStmt) {
        super(error_message, error_line, error_columns);
        this.selectStmt=factoredSelectStmt;
    }

    private  void getAllColumnsTables(List<String> columnsName, List<String> columsTableName, ExprClass exprClass){
        if(exprClass.getColumnNameFaild()!=null){
            columnsName.add(exprClass.getColumnNameFaild().getColumnName());
            if(exprClass.getColumnNameFaild().getTableNameFaild()!=null){
                columsTableName.add(exprClass.getColumnNameFaild().getTableNameFaild().getTableName());
            }else{
                columsTableName.add(null);
            }
            return;
        }
        if(exprClass.getIncaseExpr()!=null){
            getAllColumnsTables(columnsName,columsTableName,exprClass.getIncaseExpr());
            if(exprClass.getInCase().getSelectOrExpr().getExprs()!=null)
            for (ExprClass exprClassIn:
                 exprClass.getInCase().getSelectOrExpr().getExprs()) {
                getAllColumnsTables(columnsName,columsTableName,exprClassIn);

            }
            return;
        }
        if(exprClass.getExprInBracket()!=null){
            getAllColumnsTables(columnsName,columsTableName,exprClass.getExprInBracket());
            return;
        }
        if(exprClass.getExprOperatorExpr()!=null){

            if(exprClass.getExprOperatorExpr().getExp1()!=null) getAllColumnsTables(columnsName,columsTableName,exprClass.getExprOperatorExpr().getExp1());
            if(exprClass.getExprOperatorExpr().getExp2()!=null) getAllColumnsTables(columnsName,columsTableName,exprClass.getExprOperatorExpr().getExp2());
            return;
        }
        if(exprClass.getFunctionExpr()!=null){
            if(exprClass.getFunctionExpr().getFunctionParameterSql()!=null)
                if(!exprClass.getFunctionExpr().getFunctionParameterSql().isStar())
            for (ExprClass exprClass1:
                 exprClass.getFunctionExpr().getFunctionParameterSql().getExprs()) {
                getAllColumnsTables(columnsName,columsTableName,exprClass1);
            }
        }
    }
    @Override
    public boolean checkingError() {
        boolean isExist=true;
        String errorColumn="";
        //get  from tables string
        List<String> tablesName=new ArrayList<>();
        List<SqlTypeClass> sqlTypes=new ArrayList<>();
        //if not exist join
        if(selectStmt.getSelectCoreClass().getFromItems().getJoinClause()==null) {
            List<TableOrSubqueryClass> tables = selectStmt.getSelectCoreClass().getFromItems().getTableOrSubqueries();
            for (TableOrSubqueryClass table :
                    tables) {
                if(table.getTableAsAlias()!=null) {

                    sqlTypes.add(SymbolTableManager.getInstance().getSymbol_table().getDeclaredTypes().get(table.getTableAsAlias().getTableNameFaild().getTableName()));
                    tablesName.add(table.getTableAsAlias().getTableNameFaild().getTableName());
                }
                else {
                    if(table.getSelectStmtAsTable()!=null){
                        if(!SymbolTableManager.getInstance().getSymbol_table().getDeclaredTypes().containsKey(table.getSelectStmtAsTable().getTableAlias())) {
                            tablesName.add(table.getSelectStmtAsTable().getTableAlias());
                            SymbolTableManager.getInstance().getSymbol_table().getDeclaredTypes().put(table.getSelectStmtAsTable().getTableAlias(), SymbolTableManager.getInstance().flat(table.getSelectStmtAsTable().getSelectStmt().getFactoredSelectStmt(),true));
                            SymbolTableManager.getInstance().getSymbol_table().getDeclaredTypes().get(  table.getSelectStmtAsTable().getTableAlias()).setType_name(  table.getSelectStmtAsTable().getTableAlias());
                            sqlTypes.add(SymbolTableManager.getInstance().getSymbol_table().getDeclaredTypes().get(table.getSelectStmtAsTable().getTableAlias()));

                        }
                    }
                }
            }
        }else{ //if not exist join

            //get fist table in join
            sqlTypes.add(SymbolTableManager.getInstance().getSymbol_table().getDeclaredTypes().get(selectStmt.getSelectCoreClass().getFromItems().getJoinClause().getTableOrSubquery().getTableAsAlias().getTableNameFaild().getTableName()));
            tablesName.add(selectStmt.getSelectCoreClass().getFromItems().getJoinClause().getTableOrSubquery().getTableAsAlias().getTableNameFaild().getTableName());

            //get other tables in join
            List<OperatorTableConstraintClass> operatorTableConstraints = selectStmt.getSelectCoreClass().getFromItems().getJoinClause().getOperatorTableConstraints();
            for (OperatorTableConstraintClass operatorTableConstraint   :
                    operatorTableConstraints) {
                String tableNameInJoin=operatorTableConstraint.getTableOrSubquery().getTableAsAlias().getTableNameFaild().getTableName();
                sqlTypes.add(SymbolTableManager.getInstance().getSymbol_table().getDeclaredTypes().get(tableNameInJoin));
                tablesName.add(tableNameInJoin);
            }

        }
        List<ResultColumnClass> resultColumns= selectStmt.getSelectCoreClass().getSelectItemes().getResultColumns();
        for (ResultColumnClass resultColumn:
                resultColumns) {
            boolean isExistInFor=false;
            //check if not star
            if(!resultColumn.isStar()) {
                //check if table name with star
                if (resultColumn.isTableNameWithStar() != null) {
                    String tableNameWithStar=resultColumn.isTableNameWithStar();
                    if(!tablesName.contains(tableNameWithStar))
                    {
                        isExist = false;
                        errorColumn = tableNameWithStar + ".*";
                        break;
                    }

                }else {  // if not star or table name with star
                    String columnName = resultColumn.getResultColumnWithExpr().getExpr().getColumnNameFaild().getColumnName();
                    //if table name with table name
                    if (resultColumn.getResultColumnWithExpr().getExpr().getColumnNameFaild().getTableNameFaild() != null) {
                        String tableNameColumn = resultColumn.getResultColumnWithExpr().getExpr().getColumnNameFaild().getTableNameFaild().getTableName();
                        if (!tablesName.contains(tableNameColumn)) {
                            isExist = false;
                            errorColumn = tableNameColumn + "." + columnName;
                            break;
                        } else {
                            if (!sqlTypes.get(tablesName.indexOf(tableNameColumn)).getColumns().containsKey(columnName)) {
                                isExist = false;
                                errorColumn = tableNameColumn + "." + columnName;
                                break;
                            }else {
                                isExistInFor=true;
                            }

                        }
                    } else { //if table name with out table name
                        for (SqlTypeClass sqlType :
                                sqlTypes) {
                            if(sqlType!=null)
                            if (sqlType.getColumns().containsKey(columnName)) {
                                isExistInFor = true;
                            }

                        }
                    }
                    //check if column not exist befor
                    if (!isExistInFor) {
                        isExist = false;
                        errorColumn = columnName;
                    }
                }
            }

        }
        if(!isExist){
            FileManager.getInstance().write("Error " + errorColumn + this.error_message + "line(" + this.error_line + "), columns(" + this.error_columns + ") .");
            System.err.println("Error " + errorColumn + this.error_message + "line(" + this.error_line + "), columns(" + this.error_columns + ") .");

        }
        boolean correctWhereColumns=true;
        if(selectStmt.getSelectCoreClass().getWhereExpr()!=null){
            //get columns in where
            List<String> whereColumns=new ArrayList<>();
            List<String> whereColumnsTables=new ArrayList<>();
            getAllColumnsTables(whereColumns,whereColumnsTables,selectStmt.getSelectCoreClass().getWhereExpr());
            for (int i = 0; i < whereColumns.size(); i++) {
                boolean isExistInFor=false;
                if(whereColumnsTables.get(i)!=null){
                    if (!tablesName.contains(whereColumnsTables.get(i))) {
                        isExist = false;
                        FileManager.getInstance().write("Error " + whereColumnsTables.get(i) + "." + whereColumnsTables.get(i)+ "." + whereColumns.get(i) + this.error_message + "line(" + this.error_line + "), columns(" + this.error_columns + ") .");

                        break;
                    } else {
                        if (!sqlTypes.get(tablesName.indexOf(whereColumnsTables.get(i))).getColumns().containsKey(whereColumns.get(i))) {
                            correctWhereColumns = false;
                            FileManager.getInstance().write("Error " + whereColumnsTables.get(i)+ "." + whereColumns.get(i) + this.error_message + "line(" + this.error_line + "), columns(" + this.error_columns + ") .");

                            break;
                        }else {
                            isExistInFor=true;
                        }

                    }
                }else{
                    for (SqlTypeClass sqlType :
                            sqlTypes) {
                        if (sqlType.getColumns().containsKey(whereColumns.get(i))) {
                            isExistInFor = true;
                        }

                    }
                }
                if (!isExistInFor) {
                    correctWhereColumns = false;
                    FileManager.getInstance().write("Error " +whereColumns.get(i) + this.error_message + "line(" + this.error_line + "), columns(" + this.error_columns + ") .");
                    System.err.println("Error " +whereColumns.get(i) + this.error_message + "line(" + this.error_line + "), columns(" + this.error_columns + ") .");

                }

            }
        }

        return isExist && correctWhereColumns;
    }
}
