package ErrorHandler;

import Classs.SQL_Classes.Create_Table_Statement.ColumnDefinitionClass;
import Classs.SQL_Classes.Select_Statement.*;
import FileManager.FileManager;
import SymbolTable.SqlTypeClass;
import TableManagment.SymbolTableManager;
import Utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UnexistColumnOfWhereStatErrorClass extends  ErrorHandlerClass {
    private SelectCoreClass selectStmt;
    private String error_message_in_case;
    public UnexistColumnOfWhereStatErrorClass(String error_message_UnexistColumn, String error_message_in_case, String error_line, String error_columns, SelectCoreClass selectCoreClass) {
        super(error_message_UnexistColumn, error_line, error_columns);
        this.selectStmt = selectCoreClass;
        this.error_message_in_case = error_message_in_case;
    }

    @Override
    public boolean checkingError() {
        if(selectStmt.getWhereExpr() != null){
            String columnType = null;
            if(selectStmt.getWhereExpr().getIncaseExpr()!=null)
            if(selectStmt.getWhereExpr().getIncaseExpr().getColumnNameFaild()!= null){
                Map<String,SqlTypeClass> sqlTypes = new HashMap<>();
                if(selectStmt.getFromItems().getJoinClause()==null) {
                    List<TableOrSubqueryClass> tables = selectStmt.getFromItems().getTableOrSubqueries();
                    for (TableOrSubqueryClass table : tables) {
                       if(SymbolTableManager.getInstance().getSymbol_table().getDeclaredTypes().containsKey(table.getTableAsAlias().getTableNameFaild().getTableName())){
                            String tableName= table.getTableAsAlias().getTableAlias() != null ? table.getTableAsAlias().getTableAlias() :table.getTableAsAlias().getTableNameFaild().getTableName();
                            sqlTypes.put(tableName, SymbolTableManager.getInstance().getSymbol_table().getDeclaredTypes().get(table.getTableAsAlias().getTableNameFaild().getTableName()));
                        }
                    }
                }else{
                    if(SymbolTableManager.getInstance().getSymbol_table().getDeclaredTypes().containsKey(selectStmt.getFromItems().getJoinClause().getTableOrSubquery().getTableAsAlias().getTableNameFaild().getTableName())){
                        String tableName= selectStmt.getFromItems().getJoinClause().getTableOrSubquery().getTableAsAlias().getTableAlias() != null ? selectStmt.getFromItems().getJoinClause().getTableOrSubquery().getTableAsAlias().getTableAlias() :selectStmt.getFromItems().getJoinClause().getTableOrSubquery().getTableAsAlias().getTableNameFaild().getTableName();
                        sqlTypes.put(tableName, SymbolTableManager.getInstance().getSymbol_table().getDeclaredTypes().get(selectStmt.getFromItems().getJoinClause().getTableOrSubquery().getTableAsAlias().getTableNameFaild().getTableName()));
                    }

                    List<OperatorTableConstraintClass> operatorTableConstraints = selectStmt.getFromItems().getJoinClause().getOperatorTableConstraints();
                    for (OperatorTableConstraintClass operatorTableConstraint   : operatorTableConstraints) {
                        if(SymbolTableManager.getInstance().getSymbol_table().getDeclaredTypes().containsKey(operatorTableConstraint.getTableOrSubquery().getTableAsAlias().getTableNameFaild().getTableName())){
                            String tableName= operatorTableConstraint.getTableOrSubquery().getTableAsAlias().getTableAlias() != null ? operatorTableConstraint.getTableOrSubquery().getTableAsAlias().getTableAlias() : operatorTableConstraint.getTableOrSubquery().getTableAsAlias().getTableNameFaild().getTableName();
                            sqlTypes.put(tableName, SymbolTableManager.getInstance().getSymbol_table().getDeclaredTypes().get(operatorTableConstraint.getTableOrSubquery().getTableAsAlias().getTableNameFaild().getTableName()));
                        }
                    }

                }
                String columnName = selectStmt.getWhereExpr().getIncaseExpr().getColumnNameFaild().getColumnName();
                if( sqlTypes.size() == 1 ){
                    SqlTypeClass sqlType = sqlTypes.entrySet().iterator().next().getValue();
                    if(!sqlType.getColumns().containsKey(columnName)){
                        String row = String.valueOf(selectStmt.getWhereExpr().getIncaseExpr().getColumnNameFaild().getRow());
                        String column  = String.valueOf(selectStmt.getWhereExpr().getIncaseExpr().getColumnNameFaild().getColumn());
                        FileManager.getInstance().write("Error " + columnName + this.error_message + "line(" + row + "), columns(" + column + ") .");
                        System.err.println("Error " + columnName + this.error_message + "line(" + row + "), columns(" + column + ") .");

                        return false;
                    }
                    columnType = sqlType.getColumns().get(columnName).getTypeName();
                }else if( sqlTypes.size() > 1 ){
                    if(selectStmt.getWhereExpr().getIncaseExpr().getColumnNameFaild().getTableNameFaild() != null){
                        SqlTypeClass sqlType = sqlTypes.get(selectStmt.getWhereExpr().getIncaseExpr().getColumnNameFaild().getTableNameFaild().getTableName());
                        if(sqlType != null){
                            if(!sqlType.getColumns().containsKey(columnName)){
                                String row = String.valueOf(selectStmt.getWhereExpr().getIncaseExpr().getColumnNameFaild().getRow());
                                String column  = String.valueOf(selectStmt.getWhereExpr().getIncaseExpr().getColumnNameFaild().getColumn());
                                FileManager.getInstance().write("Error " + columnName + this.error_message + "line(" + row + "), columns(" + column + ") .");
                                System.err.println("Error " + columnName + this.error_message + "line(" + row + "), columns(" + column + ") .");

                                return false;
                            }
                            columnType = sqlType.getColumns().get(columnName).getTypeName();
                        }else {
                            String row = String.valueOf(selectStmt.getWhereExpr().getIncaseExpr().getColumnNameFaild().getRow());
                            String column  = String.valueOf(selectStmt.getWhereExpr().getIncaseExpr().getColumnNameFaild().getColumn());
                            FileManager.getInstance().write("Error " + columnName + this.error_message + "line(" + row + "), columns(" + column + ") .");
                            System.err.println("Error " + columnName + this.error_message + "line(" + row + "), columns(" + column + ") .");

                            return false;
                        }
                    }else {
                        FileManager.getInstance().write("Error " + columnName + Utils.ERROR_MESSAGE_UNDEFINED_ALIAS_TABLE + "line(" + this.error_line + "), columns(" + this.error_columns + ") .");
                        System.err.println("Error " + columnName + Utils.ERROR_MESSAGE_UNDEFINED_ALIAS_TABLE + "line(" + this.error_line + "), columns(" + this.error_columns + ") .");
                        return false;
                    }
                }else{
                    FileManager.getInstance().write("Error " + columnName + this.error_message + "line(" + this.error_line + "), columns(" + this.error_columns + ") .");
                    System.err.println("Error " + columnName + this.error_message + "line(" + this.error_line + "), columns(" + this.error_columns + ") .");

                    return false;
                }
            }


            if (selectStmt.getWhereExpr().getInCase() != null){
                SelectItemsClass selectItemsClass =selectStmt.getWhereExpr().getInCase().getSelectOrExpr().getSelectStmt().getFactoredSelectStmt().getSelectCoreClass().getSelectItemes();
                ReturnINClauseErrorClass returnINClauseErrorClass = new ReturnINClauseErrorClass(
                        selectItemsClass.getResultColumns(),
                        Utils.ERROR_MESSAGE_RETURN_IN_CLAUSE,
                        String.valueOf(selectItemsClass.getRow()),
                        String.valueOf(selectItemsClass.getColumn())
                );
                if(returnINClauseErrorClass.checkingError()){
                    String columnName = selectStmt.getWhereExpr().getIncaseExpr().getColumnNameFaild().getColumnName();
                    String columnNameInStmt = selectStmt.getWhereExpr().getInCase().getSelectOrExpr().getSelectStmt().getFactoredSelectStmt().getSelectCoreClass().getSelectItemes().getResultColumns().get(0).getResultColumnWithExpr().getExpr().getColumnNameFaild().getColumnName();
                    String tableNameInStmt = selectStmt.getWhereExpr().getInCase().getSelectOrExpr().getSelectStmt().getFactoredSelectStmt().getSelectCoreClass().getFromItems().getTableOrSubqueries().get(0).getTableAsAlias().getTableNameFaild().getTableName();
                    SqlTypeClass sqlType = SymbolTableManager.getInstance().getSymbol_table().getDeclaredTypes().get(tableNameInStmt);
                    if (sqlType == null){
                        FileManager.getInstance().write("Error " + tableNameInStmt + Utils.ERROR_MESSAGE_UNDECLARED_TYPE + "line(" + this.error_line + "), columns(" + this.error_columns + ") .");
                        return false;
                    }else {
                        ColumnDefinitionClass columnDefinition = sqlType.getColumns().get(columnNameInStmt);
                        if(columnDefinition != null) {
                            String columnTypeInStmt = columnDefinition.getTypeName();
                            if (!columnTypeInStmt.equals(columnType)) {
                                String row = String.valueOf(selectStmt.getWhereExpr().getInCase().getSelectOrExpr().getSelectStmt().getFactoredSelectStmt().getSelectCoreClass().getSelectItemes().getResultColumns().get(0).getResultColumnWithExpr().getExpr().getColumnNameFaild().getRow());
                                String column = String.valueOf(selectStmt.getWhereExpr().getInCase().getSelectOrExpr().getSelectStmt().getFactoredSelectStmt().getSelectCoreClass().getSelectItemes().getResultColumns().get(0).getResultColumnWithExpr().getExpr().getColumnNameFaild().getColumn());
                                FileManager.getInstance().write("Error " + columnNameInStmt + " AND " + columnName + this.error_message_in_case + "line(" + row + "), columns(" + column + ") .");
                                System.err.println("Error " + columnNameInStmt + " AND " + columnName + this.error_message_in_case + "line(" + row + "), columns(" + column + ") .");

                                return false;
                            }
                        }else{
                            String row = String.valueOf(selectStmt.getWhereExpr().getInCase().getSelectOrExpr().getSelectStmt().getFactoredSelectStmt().getSelectCoreClass().getSelectItemes().getResultColumns().get(0).getResultColumnWithExpr().getExpr().getColumnNameFaild().getRow());
                            String column = String.valueOf(selectStmt.getWhereExpr().getInCase().getSelectOrExpr().getSelectStmt().getFactoredSelectStmt().getSelectCoreClass().getSelectItemes().getResultColumns().get(0).getResultColumnWithExpr().getExpr().getColumnNameFaild().getColumn());
                            FileManager.getInstance().write("Error " + columnNameInStmt+ Utils.ERROR_USING_UN_EXISTED_COLUMN_OF_TABLE + "line(" + row + "), columns(" + column + ") .");
                            System.err.println("Error " + columnNameInStmt+ Utils.ERROR_USING_UN_EXISTED_COLUMN_OF_TABLE + "line(" + row + "), columns(" + column + ") .");
                        }
                    }
                }
            }
        }
        return true;
    }
}
