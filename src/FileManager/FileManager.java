package FileManager;

import java.io.*;

public class FileManager implements FileManagerMethod {

    private BufferedWriter buffered_writer;
    private BufferedReader buffered_reader;
    public static FileManager instance;


    private FileManager() {
        try {
            buffered_writer = new BufferedWriter(new FileWriter("error.txt",false));
            buffered_reader = new BufferedReader(new FileReader("error.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void write(String message) {
        try {
            this.buffered_writer.append(message);
            this.buffered_writer.newLine();
            this.buffered_writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String read() {
        return null;
    }

    public static FileManager getInstance() {
        if (instance == null)
            instance = new FileManager();

        return instance;
    }

    public BufferedWriter getBuffered_writer() {
        return buffered_writer;
    }

    public void setBuffered_writer(BufferedWriter buffered_writer) {
        this.buffered_writer = buffered_writer;
    }

    public BufferedReader getBuffered_reader() {
        return buffered_reader;
    }

    public void setBuffered_reader(BufferedReader buffered_reader) {
        this.buffered_reader = buffered_reader;
    }


}
