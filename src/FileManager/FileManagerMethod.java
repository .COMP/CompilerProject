package FileManager;

public interface FileManagerMethod {
    public void write(String message);

    public String read();
}
