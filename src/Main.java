import SymbolTable.ScopeClass;
import TableManagment.SymbolTableManager;
import Visitor.AST_Visitor.BaseAstVisitor;
import Visitor.BaseVisitor.BaseVisitor;
import Visitor.gen.RulesLexer;
import Visitor.gen.RulesParser;
import Classs.Abstruction_Classes.ParserClass;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.IOException;

import static org.antlr.v4.runtime.CharStreams.fromFileName;

public class Main {

    public static void main(String[] args) {
        try {
            SymbolTableManager.getInstance().getScopes_level().push("global");
            ScopeClass scope = new ScopeClass();
            scope.setParent_scope_id(null);
            scope.setScope_id("global");
            SymbolTableManager.getInstance().getSymbol_table().getScopes().put("global", scope);

            String source = "samples//samples.txt";
            CharStream cs = fromFileName(source);
            RulesLexer lexer = new RulesLexer(cs);
            CommonTokenStream token = new CommonTokenStream(lexer);
            RulesParser parser = new RulesParser(token);
            ParseTree tree = parser.parse();
            ParserClass p = (ParserClass) new BaseVisitor().visit(tree);
            p.accept(new BaseAstVisitor());
//            String d = 1 + 2+ true;
            System.out.println(p.toString());
            SymbolTableManager.getInstance().getScopes_level().push("global");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

