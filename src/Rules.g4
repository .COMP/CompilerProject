/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 by Bart Kiers
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * Project      : sqlite-parser; an ANTLR4 grammar for SQLite
 *                https://github.com/bkiers/sqlite-parser
 * Developed by : Bart Kiers, bart@big-o.nl
 * Test
 */
grammar Rules;
// Parse Node
parse
 : all_statement_starts* EOF
 ;

// Parse All Statement Node
 all_statement_starts
:   ( sql_stmt_list | java_stmt_list | error )
;

// Parse Java Statement List
 java_stmt_list
:   java_stmt |prototype_function
;

//---------------------------------------------------Variable---------------------------------------------------//

// Parse Create variable
create_variable_stmt
:   K_VAR
    (assign_variable_in_creation)
    SCOL
;

// Parse Post Square
post_square:
    variable_name OPEN_SQUARE CLOSE_SQUARE
;

// Parse Previous Square
previous_square
:   OPEN_SQUARE CLOSE_SQUARE variable_name
;

// Parse Assign Variable In Creation
assign_variable_in_creation
:   assign_array_variable (COMMA assign_array_variable )*
;

// Parse Assign Variable
assign_variable
:   variable_name (ASSIGN variable_value)?
;

// Parse Assign Array
assign_array
:   (post_square | previous_square)
    ASSIGN array_value
;

// Parse Assign Array Value
assign_array_variable
:   (assign_array | assign_variable)
;

// Parse Assign Variable Without SCOL
assign_variable_without_var
:   (variable_name | var_name_item_in_array) ASSIGN variable_value SCOL
;

// Prase Direct Array Values
direct_array_values
:   OPEN_BRACKET variable_value (COMMA variable_value)* CLOSE_BRACKET
;

// Parse Variable Name Item In Array ex : arr[1]
var_name_item_in_array
:   variable_name value_of_one_item_array
;

// Parse Value Of One Item Array ex : [1]
value_of_one_item_array
:   OPEN_SQUARE (variable_value) CLOSE_SQUARE
;

// Parse Variable Name
variable_name
:   VALID_NAME
;

// Parse Array Value
array_value
:   (
    K_NEW K_VAR
    ( OPEN_SQUARE (number_value_java) CLOSE_SQUARE )|
    ( OPEN_SQUARE CLOSE_SQUARE direct_array_values )
    )
    |direct_array_values
    |select_stmt
    |callback_function_without_scol
;

// Parse Variable Value
variable_value
:   (select_stmt
    | K_TRUE
    | K_FALSE
    | K_NULL
    | NUMERIC_LITERAL
    | QUOTE value_in_quote QUOTE
    | variable_name
    | var_name_item_in_array
    |callback_function_without_scol
    |access_to_data_in_json
    |array_value
    |expr_java
    |number_condition
    |logical_condition
    |increment_type
    |full_condition_java
    |line_condition__expression_java
    |json_declaration
    |array_json_declaration

    )
;

// Parse Value In Quote
value_in_quote
:   ( ~'*' ~'"' | ( '*'+ ~'"' ) )* '*'* ~'"'*;

// Parse Default Value Parameter
default_value_parameter
:   NUMERIC_LITERAL
    |QUOTE value_in_quote QUOTE
//    |expr_java
    | K_TRUE
    | K_FALSE
    | K_NULL
;

// Parse Initial Variable Declaration ex L var number
initial_variable_declaration
:   K_VAR  variable_name
;

//---------------------------------------------------Function---------------------------------------------------//

// Parse Prototype Function
prototype_function
:   variable_name OPEN_PAR (prototype_function_parameter) CLOSE_PAR
    body_prototype_function
;

// Parse Prototype Function Parameter
prototype_function_parameter
:   function_parameter_in_prototype?
;

// Parse Function Parameter In Prototype
function_parameter_in_prototype
:   ( initial_variable_declaration   (COMMA initial_variable_declaration)* )
    /*( ASSIGN default_value_parameter */(COMMA function_parameter_with_default)? /*)*/?
;

// Parse Function Parameter With Default
function_parameter_with_default
:   ( initial_variable_declaration_with_default_value
    (COMMA initial_variable_declaration_with_default_value)* )
;

// Parse Initial Variable Declaration With Default Value ex : var num = 1
initial_variable_declaration_with_default_value
:   initial_variable_declaration ASSIGN default_value_parameter
;

//---------------------------------------------------Body's and statement's---------------------------------------------------//

// Parse Body Function
body_function
:   OPEN_BRACKET
   (java_stmt)*
   CLOSE_BRACKET
;

// Parse Body Function
body_prototype_function
:   OPEN_BRACKET
   (java_stmt)*
   return_stmt?
   CLOSE_BRACKET
;

//scope_function
//:
//    (OPEN_BRACKET
//    java_stmt*
//
//    CLOSE_BRACKET)
//;

// Parse Javas Statement
java_stmt
:   for_stmt
    | if_statement
    | callback_function
    | while_statement
    | do_while_statement
    | create_variable_stmt
    | switch_statement
    | foreach_statement
    | print_statement
    | assign_variable_without_var
    | break_java
    | continue_java
    | increment_stmt
//    | scope_function
;

//---------------------------------------------------Condition's---------------------------------------------------//

// Parse Full Condition Java (Contain's Any Condition)
full_condition_java
:   one_condition_in_java multi_condition_java*
    |( OPEN_PAR full_condition_java CLOSE_PAR )
;

// Parse Multi Condition Java ex : (1>2) || (d<3) && True
multi_condition_java:
logical_in_java one_condition_in_java
;

// Parse Header If
header_if
:   OPEN_PAR
    full_condition_java
    CLOSE_PAR
;

// Parse One Conditino In Java
one_condition_in_java
:   not_var_name
    |not_one_condition
    | K_TRUE
    | K_FALSE
    |number_condition
    |logical_condition
    |callback_function_without_scol
 ;

// Parse Not Variable Name ex : !number_value
 not_var_name
:   (NOT)? variable_name
 ;

// Parse Not One Condititon
 not_one_condition
:   (NOT)? OPEN_PAR one_condition_in_java CLOSE_PAR
;

// Parse Number Condition ex : (1+(2+3)) > 4
number_condition
:   expr_java
    comparison_in_java
    expr_java
;

// Parse Logical Condition ex : number_value || age && city
logical_condition
:   logic_operation_statement_in_java
    logical_in_java
    logic_operation_statement_in_java
;

// Parse Expression Java ex : (1+(2+3) + (4+5)
expr_java
:   OPEN_PAR expr_java operation_in_java expr_java CLOSE_PAR|
    expr_java operation_in_java expr_java|
    number_value_java
;

// Parse Number Value Java
number_value_java
:   NUMERIC_LITERAL
    |variable_name
    | '"' variable_value '"'
    |var_name_item_in_array
    | access_to_data_in_json
    | callback_function_without_scol
    | advanced_increase
    | delayed_increase
;

// Parse Logic Operation Statement Java ex : (number_value || age) && (city || (grade && number))
logic_operation_statement_in_java
  :variable_name
  | '"' variable_value '"'
  |logic_operation_statement_in_java logical_in_java logic_operation_statement_in_java
  |OPEN_PAR logic_operation_statement_in_java logical_in_java logic_operation_statement_in_java CLOSE_PAR
  ;

// Parse Operation In Java
operation_in_java
:   (PLUS|MINUS|STAR|DIV|MOD)
 ;

// Parse Comparison In Java
comparison_in_java
:   (LT|LT_EQ|GT|GT_EQ|EQ|NOT_EQ1)
;

// Parse Logical In Java
logical_in_java
:   (OR|AND)
;

// Parse Line Condition Expression Java
line_condition__expression_java
:   full_condition_java     QUSTION_MAP     line_condition_result   DUAL_DOT     line_condition_result
;

// Parse Line Condition Resu;t
line_condition_result
:   ( variable_value | OPEN_PAR line_condition__expression_java CLOSE_PAR )
;

//---------------------------------------------------If, else if, else statement---------------------------------------------------//

// Parse If Statement
if_statement
:   K_IF
    header_if
    (java_stmt | body_function)
    (else_if_statement)*
    else_statement?
 ;

// Parse Else If Statement
else_if_statement
:   K_ELSE_IF
    header_if
    body_function
;

// Parse Else Statement
else_statement
:   K_ELSE (java_stmt|body_function)
;

//---------------------------------------------------Switch Statement-----------------------------------------------//
// Parse Switch Statement
switch_statement
:   K_SWITCH  OPEN_PAR  variable_value CLOSE_PAR
    OPEN_BRACKET (case_statenemt* default_statement? ) CLOSE_BRACKET
;

// Parse Case Statement
case_statenemt
:   K_CASE case_value DUAL_DOT
    body_for_case_and_default
;

case_value:
    NUMERIC_LITERAL
    |QUOTE value_in_quote QUOTE
;

// Parse Default Statement
default_statement
:   K_DEFAULT  DUAL_DOT
    body_for_case_and_default
;

// Parse Body For Case And default Statement
body_for_case_and_default
:   java_stmt* | body_function
;

//---------------------------------------------------For statement---------------------------------------------------//

// Parse For Statement
for_stmt
:   K_FOR OPEN_PAR for_parameter CLOSE_PAR (java_stmt|body_function)
;

// Parse For Parameter
for_parameter
:   initial_start_count_in_for      SCOL     condition_for      SCOL     increment_count_for
;

// Parse Initial start Count In For Statement
initial_start_count_in_for
:   initial_variable_declaration (ASSIGN number_value_java )? (COMMA initial_start_count_in_for)*
;

// Parse Condition For Statement
condition_for
:   full_condition_java ( COMMA full_condition_java )*
;

// Advanced Increase ex : age++
advanced_increase
:   variable_name (DOUBLEPLUS | DOUBLEMINUS)
;

// Parse Delayed Increase ex : --age
delayed_increase
:   (DOUBLEPLUS | DOUBLEMINUS) variable_name
;

// Parse Specific Increase
specific_increase
:   variable_name (PLUS | MINUS) ASSIGN (expr_java)
;

// Parse Increment Type
increment_type
:   advanced_increase
    | delayed_increase
    | specific_increase
;

// Parse Increment Count In For Statement
increment_count_for
:   increment_type ( COMMA increment_type )*
;

// Parse Increment Statement
increment_stmt
:   increment_type SCOL
;

//---------------------------------------------------While statement---------------------------------------------------//

// Parse While Statement
while_statement
:   K_WHILE
    OPEN_PAR
    full_condition_java
    CLOSE_PAR
    (java_stmt|body_function)
;

//---------------------------------------------------Do While statement---------------------------------------------------//

// Parse Do While Statement
do_while_statement
:   K_DO
    (java_stmt|body_function)
    K_WHILE
    OPEN_PAR full_condition_java CLOSE_PAR SCOL
;

//---------------------------------------------------Callback function---------------------------------------------------//

// Parse Callback Function
callback_function
:   callback_function_without_scol SCOL
;

// Parse Vallback Function Without SCOL
callback_function_without_scol
:   variable_name callback_function_heder
;

// Parse Callback Function Header
callback_function_heder
:   OPEN_PAR
    (callback_function_parameter_in_header)*
    CLOSE_PAR
;

// Parse Callback Function Parameter
callback_function_parameter
:   (variable_name|NUMERIC_LITERAL)
    |function_parameter
    | callback_function_without_scol
    | expr_java

;

// Parse Callback Function Parameter In Header
callback_function_parameter_in_header
:   callback_function_parameter
    (COMMA callback_function_parameter)*
;

// Parse Function Parameter
function_parameter
:   K_FUNCTION
    OPEN_PAR
    var_name_in_parameter
    CLOSE_PAR
    body_function
;

// Parse Variable Name In Parameter
var_name_in_parameter
:   variable_name
    (COMMA variable_name)*
;

// Parse Return Statement
return_stmt
:   K_RETURN
    (variable_value)? SCOL

;

//---------------------------------------------------JSON---------------------------------------------------//

// Parse JSON Declaration
json_declaration
:   OPEN_BRACKET
    all_fileds_in_json
    CLOSE_BRACKET
;

// Parse All Filed's In JSON
all_fileds_in_json
:   filed_in_json
    (COMMA filed_in_json)*
;

// Parse Filesd In JSON
filed_in_json
:   key_filed    DUAL_DOT    variable_value
;

// Parse Key Filed
key_filed
:   (QUOTE variable_name QUOTE) | variable_name
;

// Parse Array JSON Declaration
array_json_declaration
:   OPEN_SQUARE
    json_declaration
    (COMMA json_declaration)*
    CLOSE_SQUARE

;

// Parse Number Literal
 NUMERIC_LITERAL
:   DIGIT+ ( '.' DIGIT* )? ( E [-+]? DIGIT+ )?
    | '.' DIGIT+ ( E [-+]? DIGIT+ )?
;
 INT: [0-9]+;

// Parse Acess To Data In JSON
 access_to_data_in_json:
 variable_name DOT variable_value
 ;

//--------------------------------------------------ForEach Statement------------------------------------------------//

// Parse Foreach Statement
foreach_statement
:   variable_in_foreach DOT K_FOREACH OPEN_PAR
    (OPEN_PAR variable_in_foreach CLOSE_PAR | variable_in_foreach) MINUS GT (java_stmt | body_function)
    CLOSE_PAR SCOL
;

// Parse Variable In Foreach
variable_in_foreach
:variable_name | var_name_item_in_array;

//--------------------------------------------------Print Statement------------------------------------------------//

// Parse Print Statement
print_statement :
   K_PRINT OPEN_PAR variable_value (PLUS variable_value)* CLOSE_PAR SCOL
;
// ---------------------------------------------------single comment---------------------------------------------------//

SINGLE_LINE_COMMENT_JAVA
 : '//' ~[\r\n]* -> channel(HIDDEN)
 ;
// ---------------------------------------------------SQL Statement---------------------------------------------------//

error
 : UNEXPECTED_CHAR
   {
     throw new RuntimeException("UNEXPECTED_CHAR=" + $UNEXPECTED_CHAR.text);
   }
 ;

sql_stmt_list
 : ';'* sql_stmt ( ';'+ sql_stmt )* ';'*
 ;

sql_stmt
 :                                     ( alter_table_stmt
                                      | create_table_stmt
                                      | delete_stmt
                                      | drop_table_stmt
                                      | factored_select_stmt
                                      | insert_stmt
                                      | update_stmt
                                      |create_type_stmt
                                      |create_aggregation_function

                                       )
 ;

alter_table_stmt

 :  K_ALTER K_TABLE  ( database_name '.' )? source_table_name
    ( K_RENAME K_TO new_table_name
   | alter_table_add
   | alter_table_add_constraint
   | K_ADD K_COLUMN? column_def
   )
 ;

alter_table_add_constraint
 : K_ADD K_CONSTRAINT any_name table_constraint
 ;

alter_table_add
 : K_ADD table_constraint
 ;


create_table_stmt
 : K_CREATE  K_TABLE ( K_IF K_NOT K_EXISTS )?
   table_name_faild
   ( columns_definition )
   create_table_stmt_type

   create_table_stmt_path
 ;

columns_definition:
 '(' column_definition (',' column_definition )* ')'
;
column_definition: column_name type_name;


create_table_stmt_type:
 K_TYPE ASSIGN QUOTE type_faild QUOTE
;

type_faild:
'json' | K_CSV | K_XML
;

create_table_stmt_path:
K_PATH ASSIGN path_string
;
create_type_stmt
 : K_CREATE  K_TYPE ( K_IF K_NOT K_EXISTS )?
   table_name_faild
   ( columns_definition  )
 ;

create_aggregation_function:
K_CREATE K_AGGREGATION_FUNCTION any_name create_aggregation_function_elements
;

create_aggregation_function_elements:
OPEN_PAR path_string COMMA class_name COMMA method_name COMMA return_type COMMA return_type_array CLOSE_PAR
;

return_type_array:
OPEN_SQUARE (return_type (COMMA return_type)*) CLOSE_SQUARE
;

return_type:K_NUMBER | K_STRING | K_BOOLEAN;


class_name:any_name;

method_name:any_name;

path_string: any_name_with_quote;

any_name_with_quote:
QUOTE value_in_quote QUOTE
;

//delted
columns_def_tabels_constraint:
column_def ( ',' table_constraint | ',' column_def )*
;
//deleted
columns_def_or_select_stmt:
'(' columns_def_tabels_constraint ')' | K_AS select_stmt
;


delete_stmt
    :  K_DELETE K_FROM qualified_table_name
    ( K_WHERE expr )?
 ;

drop_table_stmt
 : K_DROP K_TABLE ( K_IF K_EXISTS )? table_name_faild
 ;

factored_select_stmt
   : select_core
   ( order_by )?
   ( limit_select )?
 ;

 order_by:
  K_ORDER K_BY ordering_term ( ',' ordering_term )*
 ;

 limit_select:
 K_LIMIT expr ( ( K_OFFSET | ',' ) expr )?
 ;

insert_stmt
:   K_INSERT  K_INTO
   table_name_faild ( '(' column_name ( ',' column_name )* ')' )?
   (insert_stmt_values
   )
 ;
insert_stmt_values
:values_items
 |select_stmt
 |K_DEFAULT K_VALUES
;

select_stmt
 :  factored_select_stmt
 ;

/*
select_or_values
 : K_SELECT ( K_DISTINCT | K_ALL )? result_column ( ',' result_column )*
   ( K_FROM ( table_or_subquery ( ',' table_or_subquery )* | join_clause ) )?
   ( K_WHERE expr )?
   ( K_GROUP K_BY expr ( ',' expr )* ( K_HAVING expr )? )?
 | K_VALUES '(' expr ( ',' expr )* ')' ( ',' '(' expr ( ',' expr )* ')' )*
 ;
 */

update_stmt
:  K_UPDATE  qualified_table_name
   K_SET  column_name_equals_expr ( ','  column_name_equals_expr )* ( K_WHERE expr )?
 ;

 column_name_equals_expr:
 column_name '=' expr
 ;

column_def
 : column_name ( column_constraint_or_type_name )*
 ;
column_constraint_or_type_name:
column_constraint | type_name
;

type_name
 : name ( '(' signed_number_any_name')'
         | '(' signed_number_any_name ',' signed_number_any_name ')' )? |return_type
 ;
signed_number_any_name:
signed_number (any_name)?
;
column_constraint
 : ( K_CONSTRAINT name )?
   ( column_constraint_primary_key
   | column_constraint_foreign_key
   | column_constraint_not_null
   | column_constraint_null
   | K_CHECK '(' expr ')'
   | column_default
   | K_COLLATE collation_name
   )
 ;

column_constraint_primary_key
 : K_PRIMARY K_KEY ( K_ASC | K_DESC )?  K_AUTOINCREMENT?
 ;

column_constraint_foreign_key
 : foreign_key_clause
 ;

column_constraint_not_null
 : K_NOT K_NULL
 ;

column_constraint_null
 : K_NULL
 ;

column_default
 : K_DEFAULT ( column_default_content )  ( '::' any_name+ )?
 ;

 column_default_content:
 column_default_value | '(' expr ')' | K_NEXTVAL '(' expr ')' | any_name
 ;

column_default_value
 : ( signed_number | literal_value )
 ;

expr
 : literal_value
 | expr in_case
 | column_name_faild
 | unary_operator expr
 | expr '||' expr
 | expr ( '*' | '/' | '%' ) expr
 | expr ( '+' | '-' ) expr
 | expr ( '<<' | '>>' | '&' | '|' ) expr
 | expr ( '<' | '<=' | '>' | '>=' ) expr
 | expr ( '=' | '==' | '!=' | '<>' | K_IS | K_IS K_NOT  | K_LIKE | K_GLOB | K_MATCH | K_REGEXP ) expr
 | expr K_AND expr
 | expr K_OR expr
 | function_expr
 | '(' expr ')'
 | exists_case

 ;

exists_case
:( ( K_NOT )? K_EXISTS )? '(' select_stmt ')'
;

function_expr
:function_name '(' ( K_DISTINCT? expr ( ',' expr )* | '*' )? ')'
;

column_name_faild
:( table_name_faild '.' )? column_name
;

table_name_faild
:/*( database_name '.' )?*/ table_name
;

select_or_expr
:( select_stmt | expr ( ',' expr )* )?
;

in_case:
 K_NOT? K_IN ( '(' select_or_expr ')' | table_name_faild )
;



foreign_key_clause
 : K_REFERENCES foreign_table_faild ( '(' fk_target_column_name ( ',' fk_target_column_name )* ')' )?
   ( foreign_on_or_match)*
   ( foreign_deferrable )?
 ;


foreign_deferrable:
K_NOT? K_DEFERRABLE ( K_INITIALLY K_DEFERRED | K_INITIALLY K_IMMEDIATE )? K_ENABLE?
;

foreign_on:
 K_ON ( K_DELETE | K_UPDATE ) ( K_SET K_NULL
                                    | K_SET K_DEFAULT
                                    | K_CASCADE
                                    | K_RESTRICT
                                    | K_NO K_ACTION )
;

foreign_on_or_match:
foreign_on
     | K_MATCH name

;
foreign_table_faild:
( database_name '.' )? foreign_table
;

fk_target_column_name
 : name
 ;

indexed_column
 : column_name ( K_COLLATE collation_name )? ( K_ASC | K_DESC )?
 ;

table_constraint
 : ( K_CONSTRAINT name )?
   ( table_constraint_primary_key
   | table_constraint_key
   | table_constraint_unique
   | K_CHECK '(' expr ')'
   | table_constraint_foreign_key
   )
 ;

table_constraint_primary_key
 : K_PRIMARY K_KEY '(' indexed_column ( ',' indexed_column )* ')'
 ;

table_constraint_foreign_key
 : K_FOREIGN K_KEY '(' fk_origin_column_name ( ',' fk_origin_column_name )* ')' foreign_key_clause
 ;

table_constraint_unique
 : K_UNIQUE K_KEY? name? '(' indexed_column ( ',' indexed_column )* ')'
 ;

table_constraint_key
 : K_KEY name? '(' indexed_column ( ',' indexed_column )* ')'
 ;

fk_origin_column_name
 : column_name
 ;

qualified_table_name
 : table_name_faild ( K_INDEXED K_BY index_name
                                     | K_NOT K_INDEXED )?
 ;

ordering_term
 : expr ( K_COLLATE collation_name )? ( K_ASC | K_DESC )?
 ;

//pragma_value
// : signed_number
// | name
// | STRING_LITERAL
// ;

//common_table_expression
// : table_name ( '(' column_name ( ',' column_name )* ')' )? K_AS '(' select_stmt ')'
// ;

result_column
 : '*'
 | table_name '.' '*'
 | result_column_with_expr
 ;

result_column_with_expr:
expr ( K_AS? column_alias )?
;
table_or_subquery
 : table_as_alias
 | from_itmes_as_table
 | select_stmt_as_table
 ;

from_itmes_as_table:
'('
        from_itmes
   ')' ( K_AS? table_alias )?
;
select_stmt_as_table:
'(' select_stmt ')' ( K_AS? table_alias )?
;


table_as_alias:
table_name_faild ( K_AS? table_alias )? ( index_name_or_not_index )?
;

index_name_or_not_index:
K_INDEXED K_BY index_name  | K_NOT K_INDEXED
;

join_clause
 : table_or_subquery ( operator_table_constraint )*
 ;

operator_table_constraint:
join_operator table_or_subquery join_constraint
;

join_operator
 : ','
 |  ( K_LEFT K_OUTER? | K_INNER  )? K_JOIN
 ;

join_constraint
: ( K_ON expr)?
 ;

select_core
 : select_items
   (K_FROM from_itmes)?
   ( K_WHERE expr )?
   ( group_by )?
 | values_items
 ;

values_items:
K_VALUES '(' expr_comma_expr ')' ( ',' '(' expr_comma_expr ')' )*
;

expr_comma_expr:
expr ( ',' expr )*
;

group_by:
 K_GROUP K_BY expr ( ',' expr )* ( K_HAVING expr )?
;

select_items:
K_SELECT ( K_DISTINCT | K_ALL )? result_column ( ',' result_column )*
;

from_itmes:
  ( table_or_subquery ( ',' table_or_subquery )* | join_clause )
;




//cte_table_name
// : table_name ( '(' column_name ( ',' column_name )* ')' )?
// ;

signed_number
 : ( ( '+' | '-' )? NUMERIC_LITERAL | '*' )
 ;

literal_value
 : NUMERIC_LITERAL
 | STRING_LITERAL
 | BLOB_LITERAL
 | K_NULL
 | K_CURRENT_TIME
 | K_CURRENT_DATE
 | K_CURRENT_TIMESTAMP
 ;

unary_operator
 : '-'
 | '+'
 | '~'
 | K_NOT
 ;

//error_message
// : STRING_LITERAL
// ;

//module_argument // TODO check what exactly is permitted here
// : expr
// | column_def
// ;

column_alias
 : (IDENTIFIER |VALID_NAME)
 | STRING_LITERAL
 ;

keyword
 : K_ABORT
 | K_ACTION
 | K_ADD
 | K_AFTER
 | K_AGGREGATION_FUNCTION
 | K_ALL
 | K_ALTER
 | K_ANALYZE
 | K_AND
 | K_AS
 | K_ASC
 | K_ATTACH
 | K_AUTOINCREMENT
 | K_BEFORE
 | K_BEGIN
 | K_BETWEEN
 | K_BY
 | K_CASCADE
 | K_CASE
 | K_CAST
 | K_CHECK
 | K_COLLATE
 | K_COLUMN
 | K_COMMIT
 | K_CONFLICT
 | K_CONSTRAINT
 | K_CREATE
 | K_CROSS
 | K_CSV
 | K_CURRENT_DATE
 | K_CURRENT_TIME
 | K_CURRENT_TIMESTAMP
 | K_DATABASE
 | K_DEFAULT
 | K_DEFERRABLE
 | K_DEFERRED
 | K_DELETE
 | K_DESC
 | K_DETACH
 | K_DISTINCT
 | K_DROP
 | K_EACH
 | K_ELSE
 | K_END
 | K_ENABLE
 | K_ESCAPE
 | K_EXCEPT
 | K_EXCLUSIVE
 | K_EXISTS
 | K_EXPLAIN
 | K_FAIL
 | K_FOR
 | K_FOREIGN
 | K_FROM
 | K_FULL
 | K_GLOB
 | K_GROUP
 | K_HAVING
 | K_IF
 | K_IGNORE
 | K_IMMEDIATE
 | K_IN
 | K_INDEX
 | K_INDEXED
 | K_INITIALLY
 | K_INNER
 | K_INSERT
 | K_INSTEAD
 | K_INTERSECT
 | K_INTO
 | K_IS
 | K_ISNULL
 | K_JOIN
 | K_KEY
 | K_LEFT
 | K_LIKE
 | K_LIMIT
 | K_MATCH
 | K_NATURAL
 | K_NO
 | K_NOT
 | K_NOTNULL
 | K_NULL
 | K_OF
 | K_OFFSET
 | K_ON
 | K_OR
 | K_ORDER
 | K_OUTER
 | K_PATH
 | K_PLAN
 | K_PRAGMA
 | K_PRIMARY
 | K_QUERY
 | K_RAISE
 | K_RECURSIVE
 | K_REFERENCES
 | K_REGEXP
 | K_REINDEX
 | K_RELEASE
 | K_RENAME
 | K_REPLACE
 | K_RESTRICT
 | K_RIGHT
 | K_ROLLBACK
 | K_ROW
 | K_SAVEPOINT
 | K_SELECT
 | K_SET
 | K_SWITCH
 | K_TABLE
 | K_TYPE
 | K_TEMP
 | K_TEMPORARY
 | K_THEN
 | K_TO
 | K_TRANSACTION
 | K_TRIGGER
 | K_UNION
 | K_UNIQUE
 | K_UPDATE
 | K_USING
 | K_VACUUM
 | K_VALUES
 | K_VIEW
 | K_VIRTUAL
 | K_WHEN
 | K_WHERE
 | K_WITH
 | K_WITHOUT
 | K_NEXTVAL
 | K_XML
 ;

//unknown
// : .+?
// ;

//number_value
// : any_name
// ;

name
 : any_name
;

function_name
 : any_name
 ;

database_name
 : any_name
 ;

source_table_name
 : any_name
 ;

table_name
 : any_name
 ;

//table_or_index_name
// : any_name
// ;

new_table_name
 : any_name
 ;

column_name
 : any_name
 ;

collation_name
 : any_name
 ;

foreign_table
 : any_name
 ;

index_name
 : any_name
 ;

//trigger_name
// : any_name
// ;
//
//view_name
// : any_name
// ;
//
//module_name
// : any_name
// ;
//
//pragma_name
// : any_name
// ;
//
//savepoint_name
// : any_name
// ;

table_alias
 : any_name
 ;

//transaction_name
// : any_name
// ;

any_name
 : (VALID_NAME |IDENTIFIER)
 | STRING_LITERAL
 | '(' any_name ')'
 ;

break_java:
K_BREAK SCOL
;

continue_java:
K_CONTINUE SCOL
;


SCOL : ';';
DUAL_DOT : ':';
DOT : '.';
OPEN_PAR : '(';
CLOSE_PAR : ')';
COMMA : ',';
ASSIGN : '=';
STAR : '*';
PLUS : '+';
DOUBLEPLUS : '++';
DOUBLEMINUS : '--';
MINUS : '-';
TILDE : '~';
OR : '||';
AND : '&&';
DIV : '/';
MOD : '%';
LT2 : '<<';
GT2 : '>>';
AMP : '&';
PIPE : '|';
LT : '<';
LT_EQ : '<=';
GT : '>';
GT_EQ : '>=';
EQ : '==';
NOT_EQ1 : '!=';
NOT_EQ2 : '<>';
NOT : '!';
OPEN_BRACKET : '{';
CLOSE_BRACKET : '}';
OPEN_SQUARE : '[';
CLOSE_SQUARE : ']';
QUOTE : '"';
QUSTION_MAP:'?';
UNDER_SCORE:'_';

// http://www.sqlite.org/lang_keywords.html
K_FOREACH : F O R E A C H ;
K_VAR : V A R;
K_PRINT : P R I N T ;
K_ABORT : A B O R T;
K_ACTION : A C T I O N;
K_ADD : A D D;
K_AFTER : A F T E R;
K_AGGREGATION_FUNCTION: A G G R E G A T I O N UNDER_SCORE F U N C T I O N;
K_ALL : A L L;
K_ALTER : A L T E R;
K_ANALYZE : A N A L Y Z E;
K_AND : A N D;
K_AS : A S;
K_ASC : A S C;
K_ATTACH : A T T A C H;
K_AUTOINCREMENT : A U T O I N C R E M E N T;
K_BEFORE : B E F O R E;
K_BEGIN : B E G I N;
K_BETWEEN : B E T W E E N;
K_BREAK : B R E A K ;
K_BY : B Y;
K_BOOLEAN: B O O L E A N;
K_CASCADE : C A S C A D E;
K_CASE : C A S E;
K_CAST : C A S T;
K_CHECK : C H E C K;
K_COLLATE : C O L L A T E;
K_COLUMN : C O L U M N;
K_COMMIT : C O M M I T;
K_CONFLICT : C O N F L I C T;
K_CONSTRAINT : C O N S T R A I N T;
K_CONTINUE : C O N T I N U E ;
K_CREATE : C R E A T E;
K_CROSS : C R O S S;
K_CSV : C S V;
K_CURRENT_DATE : C U R R E N T '_' D A T E;
K_CURRENT_TIME : C U R R E N T '_' T I M E;
K_CURRENT_TIMESTAMP : C U R R E N T '_' T I M E S T A M P;
K_DATABASE : D A T A B A S E;
K_DEFAULT : D E F A U L T;
K_DEFERRABLE : D E F E R R A B L E;
K_DEFERRED : D E F E R R E D;
K_DELETE : D E L E T E;
K_DESC : D E S C;
K_DETACH : D E T A C H;
K_DISTINCT : D I S T I N C T;
K_DO: D O ;
K_DROP : D R O P;
K_EACH : E A C H;
K_ELSE : E L S E;
K_ELSE_IF : E L S E I F;
K_END : E N D;
K_ENABLE : E N A B L E;
K_ESCAPE : E S C A P E;
K_EXCEPT : E X C E P T;
K_EXCLUSIVE : E X C L U S I V E;
K_EXISTS : E X I S T S;
K_EXPLAIN : E X P L A I N;
K_FAIL : F A I L;
K_FALSE: F A L S E;
K_FOR : F O R;
K_FOREIGN : F O R E I G N;
K_FROM : F R O M;
K_FUNCTION:F U N C T I O N;
K_FULL : F U L L;
K_GLOB : G L O B;
K_GROUP : G R O U P;
K_HAVING : H A V I N G;
K_IF : I F;
K_IGNORE : I G N O R E;
K_IMMEDIATE : I M M E D I A T E;
K_IN : I N;
K_INDEX : I N D E X;
K_INDEXED : I N D E X E D;
K_INITIALLY : I N I T I A L L Y;
K_INNER : I N N E R;
K_INSERT : I N S E R T;
K_INSTEAD : I N S T E A D;
K_INTERSECT : I N T E R S E C T;
K_INTO : I N T O;
K_IS : I S;
K_ISNULL : I S N U L L;
K_JOIN : J O I N;
K_KEY : K E Y;
K_LEFT : L E F T;
K_LIKE : L I K E;
K_LIMIT : L I M I T;
K_MATCH : M A T C H;
K_NATURAL : N A T U R A L;
K_NEXTVAL : N E X T V A L;
K_NEW : N E W;
K_NO : N O;
K_NOT : N O T;
K_NOTNULL : N O T N U L L;
K_NUMBER : N U M B E R;
K_NULL : N U L L;
K_OF : O F;
K_OFFSET : O F F S E T;
K_ON : O N;
K_ONLY : O N L Y;
K_OR : O R;
K_ORDER : O R D E R;
K_OUTER : O U T E R;
K_PATH : P A T H;
K_PLAN : P L A N;
K_PRAGMA : P R A G M A;
K_PRIMARY : P R I M A R Y;
K_QUERY : Q U E R Y;
K_RAISE : R A I S E;
K_RECURSIVE : R E C U R S I V E;
K_REFERENCES : R E F E R E N C E S;
K_REGEXP : R E G E X P;
K_REINDEX : R E I N D E X;
K_RELEASE : R E L E A S E;
K_RENAME : R E N A M E;
K_REPLACE : R E P L A C E;
K_RESTRICT : R E S T R I C T;
K_RETURN: R E T U R N;
K_RIGHT : R I G H T;
K_ROLLBACK : R O L L B A C K;
K_ROW : R O W;
K_SAVEPOINT : S A V E P O I N T;
K_SELECT : S E L E C T;
K_SET : S E T;
K_STRING: S T R I N G;
K_SWITCH : S W I T C H ;
K_TABLE : T A B L E;
K_TYPE: T Y P E;
K_TEMP : T E M P;
K_TEMPORARY : T E M P O R A R Y;
K_THEN : T H E N;
K_TO : T O;
K_TRANSACTION : T R A N S A C T I O N;
K_TRIGGER : T R I G G E R;
K_TRUE:T R U E;
K_UNION : U N I O N;
K_UNIQUE : U N I Q U E;
K_UPDATE : U P D A T E;
K_USING : U S I N G;
K_VACUUM : V A C U U M;
K_VALUES : V A L U E S;
K_VIEW : V I E W;
K_VIRTUAL : V I R T U A L;
K_WHEN : W H E N;
K_WHERE : W H E R E;
K_WHILE : W H I L E;
K_WITH : W I T H;
K_WITHOUT : W I T H O U T;
K_XML: X M L;

 VALID_NAME
   : [a-zA-Z$_] [a-zA-Z0-9$_]*
   ;



IDENTIFIER
//  '"' (~'"' | '""')* '"'
 : '`' (~'`' | '``')* '`'
// | '[' ~']'* ']'
 ;

BIND_PARAMETER
 : '?' DIGIT*
 | [@$] (IDENTIFIER | VALID_NAME)
 ;

STRING_LITERAL
 : '\'' ( ~'\'' | '\'\'' )* '\''
 ;

BLOB_LITERAL
 : X STRING_LITERAL
 ;

MULTILINE_COMMENT
 : '/*' .*? ( '*/' | EOF ) -> channel(HIDDEN)
 ;

SPACES
 : [ \u000B\t\r\n] -> channel(HIDDEN)
 ;

UNEXPECTED_CHAR
 : .
 ;



fragment DIGIT : [0-9];
fragment A : [aA];
fragment B : [bB];
fragment C : [cC];
fragment D : [dD];
fragment E : [eE];
fragment F : [fF];
fragment G : [gG];
fragment H : [hH];
fragment I : [iI];
fragment J : [jJ];
fragment K : [kK];
fragment L : [lL];
fragment M : [mM];
fragment N : [nN];
fragment O : [oO];
fragment P : [pP];
fragment Q : [qQ];
fragment R : [rR];
fragment S : [sS];
fragment T : [tT];
fragment U : [uU];
fragment V : [vV];
fragment W : [wW];
fragment X : [xX];
fragment Y : [yY];
fragment Z : [zZ];
