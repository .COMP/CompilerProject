package SymbolTable;

import java.util.ArrayList;

public class AggregationFunctionClass {
    protected String aggregation_function_name;
    protected String jar_path;
    protected String class_name;
    protected String method_name;
    protected String return_type;
    private ArrayList<Object> params = new ArrayList<Object>();


    public String getAggregation_function_name() {
        return aggregation_function_name;
    }

    public void setAggregation_function_name(String aggregation_function_name) {
        this.aggregation_function_name = aggregation_function_name;
    }

    public String getJar_path() {
        return jar_path;
    }

    public void setJar_path(String jar_path) {
        this.jar_path = jar_path;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getMethod_name() {
        return method_name;
    }

    public void setMethod_name(String method_name) {
        this.method_name = method_name;
    }

    public String getReturn_type() {
        return return_type;
    }

    public void setReturn_type(String return_type) {
        this.return_type = return_type;
    }

    public ArrayList<Object> getParams() {
        return params;
    }

    public void setParams(ArrayList<Object> params) {
        this.params = params;
    }
}
