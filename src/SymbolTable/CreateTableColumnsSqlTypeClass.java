package SymbolTable;

public class CreateTableColumnsSqlTypeClass extends SqlTypeClass {
    private String path;
    private String fileType;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }
}
