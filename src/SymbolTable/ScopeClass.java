package SymbolTable;

import java.util.LinkedHashMap;
import java.util.Map;

public class ScopeClass {
    private String scope_id;
    private String parent_scope_id;
    private String return_type;
    private Map<String, SymbolClass> symbols = new LinkedHashMap<String, SymbolClass>();


    public String getScope_id() {
        return scope_id;
    }

    public void setScope_id(String scope_id) {
        this.scope_id = scope_id;
    }

    public String getParent_scope_id() {
        return parent_scope_id;
    }

    public void setParent_scope_id(String parent_scope_id) {
        this.parent_scope_id = parent_scope_id;
    }

    public Map<String, SymbolClass> getSymbols() {
        return symbols;
    }

    public void setSymbols(Map<String, SymbolClass> symbols) {
        this.symbols = symbols;
    }

    public String getReturn_type() {
        return return_type;
    }

    public void setReturn_type(String return_type) {
        this.return_type = return_type;
    }
}
