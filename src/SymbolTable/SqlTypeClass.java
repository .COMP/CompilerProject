package SymbolTable;

import Classs.SQL_Classes.Create_Table_Statement.ColumnDefinitionClass;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SqlTypeClass extends JavaTypeClass {



    protected Map<String,ColumnDefinitionClass> columns = new HashMap<>();

    public Map<String,ColumnDefinitionClass> getColumns() {
        return columns;
    }

    public void setColumns(Map<String,ColumnDefinitionClass> columns) {
        this.columns = columns;
    }
}
