package SymbolTable;

public class SymbolClass {
    private String scope_id;
    private String symbol_name;
    private String symbol_type;
    private boolean is_param;


    public String getSymbol_name() {
        return symbol_name;
    }

    public void setSymbol_name(String symbol_name) {
        this.symbol_name = symbol_name;
    }

    public String getSymbol_type() {
        return symbol_type;
    }

    public void setSymbol_type(String symbol_type) {
        this.symbol_type = symbol_type;
    }

    public String getScope_id() {
        return scope_id;
    }

    public void setScope_id(String scope_id) {
        this.scope_id = scope_id;
    }

    public boolean isIs_param() {
        return is_param;
    }

    public void setIs_param(boolean is_param) {
        this.is_param = is_param;
    }
}
