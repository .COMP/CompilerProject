package SymbolTable;


import java.util.HashMap;
import java.util.Map;

public class SymbolTableClass {
    private Map<String, ScopeClass> scopes;

    private Map<String, SqlTypeClass> declaredTypes;

    private Map<String, AggregationFunctionClass> declaredAggregationFunction;

    public SymbolTableClass() {
        scopes = new HashMap<>();
        declaredTypes = new HashMap<>();
        declaredAggregationFunction = new HashMap<>();
    }

    public Map<String, ScopeClass> getScopes() {
        return scopes;
    }

    public void setScopes(Map<String, ScopeClass> scopes) {
        this.scopes = scopes;
    }

    public Map<String, SqlTypeClass> getDeclaredTypes() {
        return declaredTypes;
    }

    public void setDeclaredTypes(Map<String, SqlTypeClass> declaredTypes) {
        this.declaredTypes = declaredTypes;
    }

    public Map<String, AggregationFunctionClass> getDeclaredAggregationFunction() {
        return declaredAggregationFunction;
    }

    public void setDeclaredAggregationFunction(Map<String, AggregationFunctionClass> declaredAggregationFunction) {
        this.declaredAggregationFunction = declaredAggregationFunction;
    }
}
