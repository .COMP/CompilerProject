package TableManagment;

import Classs.SQL_Classes.Create_Table_Statement.ColumnDefinitionClass;
import Classs.SQL_Classes.Create_Table_Statement.ColumnsDefinitionClass;
import Classs.SQL_Classes.Select_Statement.FactoredSelectStmtClass;
import Classs.SQL_Classes.Select_Statement.OperatorTableConstraintClass;
import Classs.SQL_Classes.Select_Statement.ResultColumnClass;
import Classs.SQL_Classes.Select_Statement.TableOrSubqueryClass;
import SymbolTable.*;

import java.util.*;

public class SymbolTableManager implements SymbolTableMethod {
    // static variable single_instance of type Singleton
    private static SymbolTableManager instance = null;

    private SymbolTableClass symbol_table;

    private Stack<String> scopes_level;

    private int random_number = 0;

    Map<String, String> function_names;

    private SymbolTableManager() {
        this.symbol_table = new SymbolTableClass();
        this.scopes_level = new Stack<String>();
        this.function_names = new HashMap<>();
    }

    @Override
    public SqlTypeClass flat(FactoredSelectStmtClass selectStmt, boolean isSubQuary) {
        SqlTypeClass selctType = new SqlTypeClass();

        //get  from tables string
        List<String> tablesName = new ArrayList<>();
        if (selectStmt.getSelectCoreClass().getFromItems().getJoinClause() == null) {

            List<TableOrSubqueryClass> tables = selectStmt.getSelectCoreClass().getFromItems().getTableOrSubqueries();
            for (TableOrSubqueryClass table :
                    tables) {
                if (table.getTableAsAlias() != null) {
                    tablesName.add(table.getTableAsAlias().getTableNameFaild().getTableName());
                } else {
                    if (table.getSelectStmtAsTable() != null) {

                        tablesName.add(table.getSelectStmtAsTable().getTableAlias());
                        this.symbol_table.getDeclaredTypes().put(table.getSelectStmtAsTable().getTableAlias(), this.flat(table.getSelectStmtAsTable().getSelectStmt().getFactoredSelectStmt(), true));
                        this.symbol_table.getDeclaredTypes().get(table.getSelectStmtAsTable().getTableAlias()).setType_name(table.getSelectStmtAsTable().getTableAlias());
                    }
                }
            }
        } else {
            tablesName.add(selectStmt.getSelectCoreClass().getFromItems().getJoinClause().getTableOrSubquery().getTableAsAlias().getTableNameFaild().getTableName());

            //get other tables in join
            List<OperatorTableConstraintClass> operatorTableConstraints = selectStmt.getSelectCoreClass().getFromItems().getJoinClause().getOperatorTableConstraints();
            for (OperatorTableConstraintClass operatorTableConstraint :
                    operatorTableConstraints) {
                String tableNameInJoin = operatorTableConstraint.getTableOrSubquery().getTableAsAlias().getTableNameFaild().getTableName();
                tablesName.add(tableNameInJoin);
            }
        }


        //get select columns name string
        //get select columns table string
        List<String> columnsName = new ArrayList<>();
        List<ResultColumnClass> resultColumns = selectStmt.getSelectCoreClass().getSelectItemes().getResultColumns();
        List<String> columnsTable = new ArrayList<>();
        boolean isStar = false;
        List<String> StarWithTableNameList = new ArrayList<>();
        for (ResultColumnClass resultColumn :
                resultColumns) {
            //check if star or Table name with star
            if (resultColumn.isStar()) {
                isStar = true;
                break;
            }
            if (resultColumn.isTableNameWithStar() != null) {
                StarWithTableNameList.add(resultColumn.isTableNameWithStar());
                continue;
            }
            columnsName.add(resultColumn.getResultColumnWithExpr().getExpr().getColumnNameFaild().getColumnName());
            columnsTable.add(resultColumn.getResultColumnWithExpr().getExpr().getColumnNameFaild().getTableNameFaild() != null ? resultColumn.getResultColumnWithExpr().getExpr().getColumnNameFaild().getTableNameFaild().getTableName() : null);

        }

        if (isStar) {
            columnsName = new ArrayList<>();
            columnsTable = new ArrayList<>();
            addStarType(columnsName, columnsTable, tablesName);
        }
        if (StarWithTableNameList.size() != 0) {
            addStarType(columnsName, columnsTable, StarWithTableNameList);
            //getAllTypeStar
        }


        //get type each column
        getAllTypeInOneLevel(selctType, columnsName, columnsTable, tablesName, isSubQuary);

        //set sql  select type name
        String typeSelectSql = "";
        StringBuilder sb = new StringBuilder();
        for (String keyColumn :
                selctType.getColumns().keySet()) {
            sb.append(keyColumn);
        }
        typeSelectSql = sb.toString();
        selctType.setType_name(typeSelectSql);


        return selctType;
    }

    private void addStarType(List<String> columnsName, List<String> columnsTable, List<String> starWithTableNameList) {
        for (String tableInFrom :
                starWithTableNameList) {
            SqlTypeClass newSqlType = this.symbol_table.getDeclaredTypes().get(tableInFrom);
            if (newSqlType != null)
                for (String key :
                        newSqlType.getColumns().keySet()) {
                    columnsName.add(newSqlType.getColumns().get(key).getColumnName());
                    columnsTable.add(tableInFrom);
                }
        }
    }

    private void addColumnsToSqlType(ColumnsDefinitionClass columnsDefinition, String typeName, SqlTypeClass sqlType) {
        sqlType.setType_name(typeName);

        for (int i = 0; i < columnsDefinition.getColumns().size(); i++) {
            sqlType.getColumns().put(columnsDefinition.getColumns().get(i).getColumnName(), columnsDefinition.getColumns().get(i));
        }
    }

    private boolean isProtoType(String type) {
        if (type.equals("string") || type.equals("number") || type.equals("boolean")) {
            return true;
        }
        return false;
    }

    private void getAllTypeInOneLevel(SqlTypeClass selectType, List<String> columnsName, List<String> columnsTable, List<String> tablesName, boolean isSubQuary) {

        SqlTypeClass sqlType = null;
        //check if one table in from
        if (tablesName.size() == 1) {
            sqlType = this.symbol_table.getDeclaredTypes().get(tablesName.get(0));
        }


        for (int i = 0; i < columnsName.size(); i++) {
            boolean sqlTypeEdited = false;
            String currentTableColumnName = tablesName.get(0);
            //check if more one table if from
            if (sqlType == null) {
                sqlTypeEdited = true;
                if (columnsTable.get(i) == null) {
                    for (String tableStr :
                            tablesName) {
                        if (this.symbol_table.getDeclaredTypes().get(tableStr).getColumns().containsKey(columnsName.get(i))) {
                            sqlType = this.symbol_table.getDeclaredTypes().get(tableStr);
                            currentTableColumnName = tableStr;
                        }
                    }
                } else {
                    sqlType = this.symbol_table.getDeclaredTypes().get(columnsTable.get(i));
                    currentTableColumnName = columnsTable.get(i);
                }
            }

            ColumnDefinitionClass columnDefinition;
            columnDefinition = sqlType.getColumns().get(columnsName.get(i));

            //get column type name
            String columnType = columnDefinition.getTypeName();
            //check if proto type
            if (isProtoType(columnType)) {
                //store it in select type
                if (!isSubQuary) {
                    selectType.getColumns().put(currentTableColumnName + "." + columnsName.get(i), columnDefinition);

                } else {
                    selectType.getColumns().put(columnsName.get(i), columnDefinition);

                }
            } else {
                //go to get all proto type in nonProto type
                List<String> newColumnsName = new ArrayList<>();
                List<String> newTablesName = new ArrayList<>();

                SqlTypeClass newSqlType = this.symbol_table.getDeclaredTypes().get(columnDefinition.getTypeName());
                if (newSqlType != null) {
                    for (String key :
                            newSqlType.getColumns().keySet()) {
                        newColumnsName.add(newSqlType.getColumns().get(key).getColumnName());
                    }
                    newTablesName.add(columnDefinition.getTypeName());
                    getAllTypeInOneLevel(selectType, newColumnsName, null, newTablesName, false);
                }
            }
            if (sqlTypeEdited) {
                sqlType = null;
            }
        }
    }

    @Override
    public void addType(ColumnsDefinitionClass columnsDefinition, String typeName) {
        SqlTypeClass sqlType = new SqlTypeClass();
        addColumnsToSqlType(columnsDefinition, typeName, sqlType);
        this.symbol_table.getDeclaredTypes().put(typeName, sqlType);
        System.out.println("");
    }

    @Override
    public void addTableType(ColumnsDefinitionClass columnsDefinition, String typeName, String fileType, String path) {
        CreateTableColumnsSqlTypeClass sqlType = new CreateTableColumnsSqlTypeClass();
        addColumnsToSqlType(columnsDefinition, typeName, sqlType);
        sqlType.setFileType(fileType);
        sqlType.setPath(path);
        this.symbol_table.getDeclaredTypes().put(typeName, sqlType);


    }

    @Override
    public ScopeClass addScope(String scope_id, String parent_scope_id) {
        ScopeClass scope = new ScopeClass();
        scope.setScope_id(scope_id);
        scope.setParent_scope_id(parent_scope_id);
        this.symbol_table.getScopes().put(scope_id, scope);
        this.random_number++;
        return scope;
    }

    @Override
    public void removeScopeFromScopeLevelStack() {
        this.scopes_level.pop();
    }

    @Override
    public boolean getScopeIsExist(String scope_id) {
        return this.symbol_table.getScopes().get(scope_id) != null;
    }

    @Override
    public void addSymbol(String scope_id, String symbol_name, String symbol_type, boolean is_params) {
        SymbolClass symbol = new SymbolClass();
        symbol.setScope_id(scope_id);
        symbol.setSymbol_name(symbol_name);
        symbol.setSymbol_type(symbol_type);
        symbol.setIs_param(is_params);
        this.symbol_table.getScopes().get(scope_id).getSymbols().put(symbol_name, symbol);
    }

    @Override
    public void addAggregationFunction(String aggregation_function_name, String jar_path, String class_name, String method_name, String return_type, ArrayList params) {
        AggregationFunctionClass aggregationFunction = new AggregationFunctionClass();
        aggregationFunction.setAggregation_function_name(aggregation_function_name);
        aggregationFunction.setJar_path(jar_path);
        aggregationFunction.setClass_name(class_name);
        aggregationFunction.setMethod_name(method_name);
        aggregationFunction.setReturn_type(return_type);
        aggregationFunction.setParams(params);
        this.symbol_table.getDeclaredAggregationFunction().put(aggregation_function_name, aggregationFunction);
    }

    @Override
    public void updateTypeSymbol(String scope_id, String symbol_name, String symbol_type) {
        SymbolClass symbol = this.getSymbolIsExist(symbol_name);
        this.symbol_table.getScopes().get(scope_id).getSymbols().get(symbol_name).setSymbol_type(symbol_type);
    }


    @Override
    public boolean checkTypeIsExist(SymbolClass symbol) {
        return symbol.getSymbol_type() != null;
    }

    @Override
    public SymbolClass getSymbolIsExist(String symbol_name) {
        for (String parent_scope_id : this.scopes_level) {
            SymbolClass symbol = this.symbol_table.getScopes().get(parent_scope_id).getSymbols().get(symbol_name);
            if (symbol != null) {
                return symbol;
            }
        }
        return null;
    }


    @Override
    public String getParentOfSymbol(String symbol_name) {
        String symbol_type = null;
        for (String parent_scope_id : this.scopes_level) {
            SymbolClass symbol = this.symbol_table.getScopes().get(parent_scope_id).getSymbols().get(symbol_name);
            if (symbol != null) {
                symbol_type = symbol.getSymbol_type();
                break;
            }
        }
        return symbol_type;
    }

    @Override
    public String getReturnTypeOfScope(String scope_id) {
        return this.function_names.getOrDefault(scope_id, null);
    }

    @Override
    public String getTypeOfSymbol(String symbol_name) {
        SymbolClass symbol = this.symbol_table.getScopes().get(this.scopes_level.lastElement()).getSymbols().get(symbol_name);
        return symbol != null ? symbol.getSymbol_type() : null;
    }


    public static SymbolTableManager getInstance() {
        if (instance == null)
            instance = new SymbolTableManager();

        return instance;
    }

    public SymbolTableClass getSymbol_table() {
        return symbol_table;
    }

    public Stack<String> getScopes_level() {
        return scopes_level;
    }

    public void setScopes_level(Stack<String> scopes_level) {
        this.scopes_level = scopes_level;
    }

    public int getRandom_number() {
        return random_number;
    }

    public void setRandom_number(int random_number) {
        this.random_number = random_number;
    }

    public Map<String, String> getFunction_names() {
        return function_names;
    }

    public void setFunction_names(Map<String, String> function_names) {
        this.function_names = function_names;
    }
}
