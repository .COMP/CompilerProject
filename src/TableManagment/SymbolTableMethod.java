package TableManagment;

import Classs.SQL_Classes.Create_Table_Statement.ColumnsDefinitionClass;
import Classs.SQL_Classes.Select_Statement.FactoredSelectStmtClass;
import Classs.SQL_Classes.Select_Statement.SelectStmtClass;
import SymbolTable.CreateTableColumnsSqlTypeClass;
import SymbolTable.ScopeClass;
import SymbolTable.SqlTypeClass;
import SymbolTable.SymbolClass;

import java.util.ArrayList;

public interface SymbolTableMethod {
    public ScopeClass addScope(String scope_id, String parent_scope_id);

    public void removeScopeFromScopeLevelStack();

    public boolean getScopeIsExist(String scope_id);

    public void addSymbol(String scope_id, String symbol_name, String symbol_type, boolean is_params);

    public void addAggregationFunction(String aggregation_function_name, String jar_path, String class_name, String method_name, String return_type, ArrayList params);

    public void updateTypeSymbol(String scope_id, String symbol_name, String symbol_type);

    public boolean checkTypeIsExist(SymbolClass symbol);

    public SymbolClass getSymbolIsExist(String symbol_name);

    public String  getParentOfSymbol(String symbol_name);

    public String getReturnTypeOfScope(String scope_id);

    public String getTypeOfSymbol(String symbol_name);


    public void addType(ColumnsDefinitionClass columnsDefinition,String typeName);
    public void addTableType(ColumnsDefinitionClass columnsDefinition, String typeName,String fileType,String path);
    public SqlTypeClass flat(FactoredSelectStmtClass selectStmt,boolean isSubQuary);

}
