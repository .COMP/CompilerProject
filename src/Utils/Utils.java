package Utils;

import Classs.Java_Classes.Global_Classes.VariableValueClass;
import Classs.SQL_Classes.Create_Table_Statement.ColumnDefinitionClass;
import Classs.SQL_Classes.Select_Statement.FactoredSelectStmtClass;
import ErrorHandler.UnassignedVariableErrorClass;
import ErrorHandler.UndeclaredVariableErrorClass;
import ErrorHandler.UnexistColumnOfTableErrorClass;
import ErrorHandler.UnexistColumnOfWhereStatErrorClass;
import SymbolTable.SqlTypeClass;
import SymbolTable.SymbolClass;
import SymbolTable.TypeEnum;
import TableManagment.SymbolTableManager;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public  class Utils {
    static boolean is_found = false;

    public static final String ERROR_MESSAGE_UNDECLARED_VARIABLE = " dose not decelerated at : ";
    public static final String ERROR_MESSAGE_MULTI_DECLARATION_VARIABLE = " decelerated more than once in this scope at : ";
    public static final String ERROR_MESSAGE_UNASSIGNED__VARIABLE = "  unassigned variable at : ";
    public static final String ERROR_MESSAGE_UNDECLARED_FUNCTION = " function dose not decelerated at : ";
    public static final String ERROR_MESSAGE_ASSIGN_INCOMPATIBLE_TYPE = " incompatible type at : ";
    public static final String ERROR_USING_UN_EXISTED_COLUMN_OF_TABLE = " un existed column in : ";
    public static final String ERROR_MESSAGE_DUPLICATE_COLUMNS_IN_CREATE = " duplicated in same table at : ";
    public static final String ERROR_MESSAGE_UNDECLARED_TYPE = " dose not decelerated at :  ";
    public static final String ERROR_MESSAGE_RETURN_IN_CLAUSE = " Return IN Clause more than one column : ";
    public static final String ERROR_MESSAGE_UNDEFINED_ALIAS_TABLE = " Undefined alias table in : ";
    public static final String ERROR_MESSAGE_COLUMNS_NOT_SAME_TYPE = " not same type : ";
    public static final String ERROR_MESSAGE_JOIN_HAVE_ON = " Join have On Statement in : ";


//    public static String getTypeOFVariable(VariableValueClass variable_value) {
//        String symbol_type = null;
//        if (variable_value.getVariable_name() != null) {
//            SymbolClass symbol = SymbolTableManager.getInstance().getSymbolIsExist(variable_value.getVariable_name());
//            if (symbol != null) {
//                symbol_type = variable_value.getType_value();
//            }
//        } else if (variable_value.getCallback_function_without_scol() != null) {
//            boolean scope_is_exist = SymbolTableManager.getInstance().getScopeIsExist(variable_value.getCallback_function_without_scol().getFunction_name());
//            if (scope_is_exist) {
//                symbol_type = SymbolTableManager.getInstance().getSymbol_table().getScopes().get(variable_value.getCallback_function_without_scol().getFunction_name()).getReturn_type();
//            }
//        } else if (variable_value.getExpression_java() != null) {
//            symbol_type = variable_value.getType_value();
//        } else {
//            symbol_type = variable_value != null ? variable_value.getType_value() : null;
//        }
//        return symbol_type;
//    }





    public static Set<ColumnDefinitionClass> findDuplicates(List<ColumnDefinitionClass> listContainingDuplicates)
    {
        final Set<ColumnDefinitionClass > setToReturn = new HashSet<>();
        final Set<String > set1 = new HashSet<>();

        for (ColumnDefinitionClass item : listContainingDuplicates)
        {
            if (!set1.add(item.getColumnName()))
            {
                setToReturn.add(item);
            }
        }
        return setToReturn;
    }
}
