package Visitor.AST_Visitor;

import Classs.Abstruction_Classes.ParserClass;
import Classs.Java_Classes.All_Statement_Start_Classes.JavaStatementsListClass;
import Classs.Java_Classes.Break_Continue_Classes.BreakStatementClass;
import Classs.Java_Classes.Break_Continue_Classes.ContinueStatementClass;
import Classs.Java_Classes.Callbakc_Function_Classes.*;
import Classs.Java_Classes.Create_Variable_Statement_Classes.*;
import Classs.Java_Classes.Do_While_Statement_Classes.DoWhileStatementClass;
import Classs.Java_Classes.Expression_Classes.*;
import Classs.Java_Classes.For_Statement_Classes.*;
import Classs.Java_Classes.Foreach_Statement_Classess.ForeachStatementClass;
import Classs.Java_Classes.Foreach_Statement_Classess.VariableInForeachClass;
import Classs.Java_Classes.Full_Condition_Classes.*;
import Classs.Java_Classes.Global_Classes.*;
import Classs.Java_Classes.If_Statement_Classes.ElseIfStatementClass;
import Classs.Java_Classes.If_Statement_Classes.ElseStatementClass;
import Classs.Java_Classes.If_Statement_Classes.HeaderIfClass;
import Classs.Java_Classes.If_Statement_Classes.IfStatementClass;
import Classs.Java_Classes.Increase_Type_Classes.*;
import Classs.Java_Classes.Java_ClassesParse_Classess.AllStatementsStartClass;
import Classs.Java_Classes.Json.Access_To_Json_Data_Classes.AccessJsonDataClass;
import Classs.Java_Classes.Json.Array_Json_Declaration_Classes.ArrayJsonDeclarationClass;
import Classs.Java_Classes.Json.Json_Declaration_Classes.AllFiledsInJsonClass;
import Classs.Java_Classes.Json.Json_Declaration_Classes.FiledInJsonClass;
import Classs.Java_Classes.Json.Json_Declaration_Classes.JsonDeclarationClass;
import Classs.Java_Classes.Json.Json_Declaration_Classes.KeyFiledClass;
import Classs.Java_Classes.Print_Statement_Classes.PrintStatementClass;
import Classs.Java_Classes.Prototype_Function_Classes.*;
import Classs.Java_Classes.Return_Classes.ReturnStatementClass;
import Classs.Java_Classes.Scope_Statement_Classes.ScopeStatementClass;
import Classs.Java_Classes.Switch_Statement_Classes.BodyForCaseAndDefaultClass;
import Classs.Java_Classes.Switch_Statement_Classes.CaseStatementClass;
import Classs.Java_Classes.Switch_Statement_Classes.DefaultStatementClass;
import Classs.Java_Classes.Switch_Statement_Classes.SwitchStatementClass;
import Classs.Java_Classes.While_Statement_Classes.WhileStatementClass;
import Classs.SQL_Classes.Alter_Table_Statement.AlterTableAddClass;
import Classs.SQL_Classes.Alter_Table_Statement.AlterTableAddConstraintClass;
import Classs.SQL_Classes.Alter_Table_Statement.AlterTableStmtClass;
import Classs.SQL_Classes.Create_Table_Statement.*;
import Classs.SQL_Classes.Create_Type_Statement.CreateTypeStmtClass;
import Classs.SQL_Classes.Create_ِِAggregation_Function.CreateAggregationFunctionClass;
import Classs.SQL_Classes.Create_ِِAggregation_Function.CreateAggregationFunctionElementsClass;
import Classs.SQL_Classes.Delete_Statement.DeleteStmtClass;
import Classs.SQL_Classes.Drop_Table_Statement.DropTableStmtClass;
import Classs.SQL_Classes.Insert_Statement.InsertStmtClass;
import Classs.SQL_Classes.Insert_Statement.InsertStmtValuesClass;
import Classs.SQL_Classes.Select_Statement.*;
import Classs.SQL_Classes.SqlStmtClass;
import Classs.SQL_Classes.SqlStmtListClass;

public interface AstVisitorInterface {
    public void visit(ParserClass parser);

    public void visit(AllStatementsStartClass allStatements_Start);

    public void visit(JavaStatementsListClass java_statements_listClass);

    public void visit(JavaStatementClass java_statement);

    public void visit(ScopeStatementClass scope_statement);

    public void visit(PrototypeFunctionClass prototype_function);

    public void visit(PrototypeFunctionParameterClass prototype_function_parameter);

    public void visit(BodyPrototypeFunctionClass body_prototype_function);

    public void visit(FunctionParameterInPrototypeClass function_parameter_in_prototype);

    public void visit(CreateVariableStatementClass create_variable_statement);

    public void visit(AssignVariableInCreationClass assign_variable_in_creation);

    public void visit(DefaultValueParameterClass defaultValueParameterClass);

    public void visit(InitialVariableDeclarationClass initial_variable_declaration);

    public void visit(InitialVariableDeclarationWithDefaultValueClass initial_variable_declaration_with_default_value);

    public void visit(FunctionParameterWithDefaultClass function_parameter_with_default);

    public void visit(BodyFunctionClass bodyFunctionClass);

    public void visit(CallbackFunctionParameterClass callbackFunctionParameterClass);

    public void visit(CallbackFunctionParameterInHeaderClass callbackFunctionParameterInHeaderClass);

    public void visit(CallbackFunctionWithOutScolClass callbackFunctionWithOutScolClass);

    public void visit(FunctionParameterClass functionParameterClass);

    public void visit(VarNameInParameterClass varNameInParameterClass);

    public void visit(ArrayValueClass arrayValueClass);

    public void visit(AssignArrayClass assignArrayClass);

    public void visit(AssignArrayVariableClass assignArrayVariableClass);

    public void visit(AssignVariableClass assignVariableClass);

    public void visit(DirectArrayValueClass directArrayValueClass);

    public void visit(PostSquareClass postSquareClass);

    public void visit(PreviousSquareClass previousSquareClass);

    public void visit(ExpressionJacaClass expressionJacaClass);

    public void visit(LogicalConditionClass logicalConditionClass);

    public void visit(NumberConditionClass numberConditionClass);

    public void visit(LineConditionExpressionJavaClass lineConditionExpressionJavaClass);

    public void visit(ValueOfOneItemArrayClass valueOfOneItemArrayClass);

    public void visit(VariableNameItemInArrayClass variableNameItemInArrayClass);

    public void visit(VariableValueClass variableValueClass);

    public void visit(IncrementTypeClass incrementTypeClass);

    public void visit(ArrayJsonDeclarationClass arrayJsonDeclarationClass);

    public void visit(AllFiledsInJsonClass allFiledsInJsonClass);

    public void visit(FiledInJsonClass filedInJsonClass);

    public void visit(JsonDeclarationClass jsonDeclarationClass);

    public void visit(KeyFiledClass keyFiledClass);

    public void visit(ForStatementClass forStatementClass);

    public void visit(ForParameterClass forParameterClass);

    public void visit(InitialStartCountInForClass initialStartCountInForClass);

    public void visit(NumberValueJavaClass numberValueJavaClass);

    public void visit(ConditionForClass conditionForClass);

    public void visit(FullConditionClass fullConditionClass);

    public void visit(OneConditionJavaClass oneConditionJavaClass);

    public void visit(NotVarNameClass notVarNameClass);

    public void visit(NotOneConditionClass notOneConditionClass);

    public void visit(MultiConditionJavaClass multiConditionJavaClass);

    public void visit(IncrementCountForClass incrementCountForClass);

    public void visit(AdvancedIncreaseClass advancedIncreaseClass);

    public void visit(DelayedIncreaseClass delayedIncreaseClass);

    public void visit(SpecificIncreaseClass specificIncreaseClass);

    public void visit(ElseIfStatementClass elseIfStatementClass);

    public void visit(ElseStatementClass elseStatementClass);

    public void visit(HeaderIfClass headerIfClass);

    public void visit(IfStatementClass ifStatementClass);

    public void visit(LogicalOperationStatement logicalOperationStatement);

    public void visit(OperationInJava operationInJava);

    public void visit(ComparisonInJava comparisonInJava);

    public void visit(LogicalInJava logicalInJava);

    public void visit(CallbackFunctionClass callbackFunctionClass);

    public void visit(CallbackFunctionHeaderClass callbackFunctionHeaderClass);

    public void visit(WhileStatementClass whileStatementClass);

    public void visit(DoWhileStatementClass doWhileStatementClass);

    public void visit(SwitchStatementClass switchStatementClass);

    public void visit(CaseStatementClass caseStatementClass);

    public void visit(BodyForCaseAndDefaultClass bodyForCaseAndDefaultClass);

    public void visit(DefaultStatementClass defaultStatementClass);

    public void visit(ForeachStatementClass foreachStatementClass);

    public void visit(VariableInForeachClass variableInForeachClass);

    public void visit(PrintStatementClass printStatementClass);

    public void visit(BreakStatementClass breakStatementClass);

    public void visit(ContinueStatementClass continueStatementClass);

    public void visit(IncrementStatementClass incrementStatementClass);

    public void visit(ReturnStatementClass returnStatementClass);

    public void visit(LineConditionResultClass lineConditionResultClass);

    public void visit(AssignVariableWithoutVarClass assignVariableWithoutVarClass);

    void visit(SqlStmtListClass sqlStmtListClass);

    void visit(SqlStmtClass sqlStmtClass);

    void visit(AlterTableStmtClass alterTableStmtClass);

    void visit(AlterTableAddClass alterTableAddClass);

    void visit(AlterTableAddConstraintClass alterTableAddConstraintClass);

    void visit(ColumnConstraintClass columnConstraintClass);

    void visit(ColumnConstraintForeignKeyClass columnConstraintForeignKeyClass);

    void visit(ColumnConstraintNotNullClass columnConstraintNotNullClass);

    void visit(ColumnConstraintNullClass columnConstraintNullClass);

    void visit(ColumnConstraintOrTypeName columnConstraintOrTypeName);

    void visit(ColumnConstraintPrimaryKeyClass columnConstraintPrimaryKeyClass);

    void visit(ColumnDefaultClass columnDefaultClass);

    void visit(ColumnDefaultContentClass columnDefaultContentClass);

    void visit(ColumnDefaultValue columnDefaultValue);

    void visit(ColumnDefClass columnDefClass);

    void visit(ColumnsDefOrSelectStmtClass columnsDefOrSelectStmtClass);

    void visit(ColumnsDeftTableConstraintClass columnsDeftTableConstraintClass);

    void visit(CreateTableStmtClass createTableStmtClass);

    void visit(ForeignDeferrableClass foreignDeferrableClass);

    void visit(ForeignKeyClauseClass foreignKeyClauseClass);

    void visit(ForeignOn foreignOn);

    void visit(ForeignOnOrMatch foreignOnOrMatch);

    void visit(ForeignTableFaildClass foreignTableFaildClass);

    void visit(IndexedColumn indexedColumn);

    void visit(SignedNumberAnyNameClass signedNumberAnyNameClass);

    void visit(TableConstraintClass tableConstraintClass);

    void visit(TableConstraintForeginKey tableConstraintForeginKey);

    void visit(TableConstraintKey tableConstraintKey);

    void visit(TableConstraintPrimaryKey tableConstraintPrimaryKey);

    void visit(TableConstraintUnique tableConstraintUnique);

    void visit(TypeNameClass typeNameClass);

    void visit(DeleteStmtClass deleteStmtClass);

    void visit(DropTableStmtClass dropTableStmtClass);

    void visit(InsertStmtClass insertStmtClass);

    void visit(InsertStmtValuesClass insertStmtValuesClass);

    void visit(ColumnNameFaildClass columnNameFaildClass);

    void visit(ExistsCaseClass existsCaseClass);

    void visit(ExprClass exprClass);

    void visit(ExprCommaExprClass exprCommaExprClass);

    void visit(ExprOperatorExprClass exprOperatorExprClass);

    void visit(FactoredSelectStmtClass factoredSelectStmtClass);

    void visit(FromItemsAsTableClass fromItemsAsTableClass);

    void visit(FromItemsClass fromItemsClass);

    void visit(FunctionExprClass functionExprClass);

    void visit(FunctionParameterSqlClass functionParameterSqlClass);

    void visit(GroupByClass groupByClass);

    void visit(InCaseClass inCaseClass);

    void visit(IndexNameOrNotIndexClass indexNameOrNotIndexClass);

    void visit(JoinClauseClass joinClauseClass);

    void visit(JoinOperatorClass joinOperatorClass);

    void visit(LimitSelectClass limitSelectClass);

    void visit(OperatorTableConstraintClass operatorTableConstraintClass);

    void visit(OrderByClass orderByClass);

    void visit(OrderingTermClass orderingTermClass);

    void visit(ResultColumnClass resultColumnClass);

    void visit(ResultColumnWithExprClass resultColumnWithExprClass);

    void visit(SelectCoreClass selectCoreClass);

    void visit(SelectItemsClass selectItemsClass);

    void visit(SelectOrExprClass selectOrExprClass);

    void visit(SelectStmtAsTableClass selectStmtAsTableClass);

    void visit(SelectStmtClass selectStmtClass);

    void visit(TableAsAliasClass tableAsAliasClass);

    void visit(TableNameFaildClass tableNameFaildClass);

    void visit(TableOrSubqueryClass tableOrSubqueryClass);

    void visit(ValuesItemsClass valuesItemsClass);

    void visit(AccessJsonDataClass accessJsonDataClass);

    void visit(CreateTableStmtTypeClass createTableStmtTypeClass);

    void visit(TypeFaildClass typeFaildClass);

    void visit(CreateTableStmtPathClass createTableStmtPathClass);

    void visit(CreateTypeStmtClass createTypeStmtClass);

    void visit(CreateAggregationFunctionClass createAggregationFunctionClass);

    void visit(CreateAggregationFunctionElementsClass createAggregationFunctionElementsClass);

    void visit(ColumnsDefinitionClass columnsDefinitionClass);
    void visit(ColumnDefinitionClass columnDefinitionClass);


}
