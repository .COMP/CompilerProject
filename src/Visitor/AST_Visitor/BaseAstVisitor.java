package Visitor.AST_Visitor;


import Classs.Abstruction_Classes.ParserClass;
import Classs.Java_Classes.All_Statement_Start_Classes.JavaStatementsListClass;
import Classs.Java_Classes.Break_Continue_Classes.BreakStatementClass;
import Classs.Java_Classes.Break_Continue_Classes.ContinueStatementClass;
import Classs.Java_Classes.Callbakc_Function_Classes.*;
import Classs.Java_Classes.Create_Variable_Statement_Classes.*;
import Classs.Java_Classes.Do_While_Statement_Classes.DoWhileStatementClass;
import Classs.Java_Classes.Expression_Classes.*;
import Classs.Java_Classes.For_Statement_Classes.*;
import Classs.Java_Classes.Foreach_Statement_Classess.ForeachStatementClass;
import Classs.Java_Classes.Foreach_Statement_Classess.VariableInForeachClass;
import Classs.Java_Classes.Full_Condition_Classes.*;
import Classs.Java_Classes.Global_Classes.*;
import Classs.Java_Classes.If_Statement_Classes.ElseIfStatementClass;
import Classs.Java_Classes.If_Statement_Classes.ElseStatementClass;
import Classs.Java_Classes.If_Statement_Classes.HeaderIfClass;
import Classs.Java_Classes.If_Statement_Classes.IfStatementClass;
import Classs.Java_Classes.Increase_Type_Classes.*;
import Classs.Java_Classes.Java_ClassesParse_Classess.AllStatementsStartClass;
import Classs.Java_Classes.Json.Access_To_Json_Data_Classes.AccessJsonDataClass;
import Classs.Java_Classes.Json.Array_Json_Declaration_Classes.ArrayJsonDeclarationClass;
import Classs.Java_Classes.Json.Json_Declaration_Classes.AllFiledsInJsonClass;
import Classs.Java_Classes.Json.Json_Declaration_Classes.FiledInJsonClass;
import Classs.Java_Classes.Json.Json_Declaration_Classes.JsonDeclarationClass;
import Classs.Java_Classes.Json.Json_Declaration_Classes.KeyFiledClass;
import Classs.Java_Classes.Print_Statement_Classes.PrintStatementClass;
import Classs.Java_Classes.Prototype_Function_Classes.*;
import Classs.Java_Classes.Return_Classes.ReturnStatementClass;
import Classs.Java_Classes.Scope_Statement_Classes.ScopeStatementClass;
import Classs.Java_Classes.Switch_Statement_Classes.BodyForCaseAndDefaultClass;
import Classs.Java_Classes.Switch_Statement_Classes.CaseStatementClass;
import Classs.Java_Classes.Switch_Statement_Classes.DefaultStatementClass;
import Classs.Java_Classes.Switch_Statement_Classes.SwitchStatementClass;
import Classs.Java_Classes.While_Statement_Classes.WhileStatementClass;
import Classs.SQL_Classes.Alter_Table_Statement.AlterTableAddClass;
import Classs.SQL_Classes.Alter_Table_Statement.AlterTableAddConstraintClass;
import Classs.SQL_Classes.Alter_Table_Statement.AlterTableStmtClass;
import Classs.SQL_Classes.Create_Table_Statement.*;
import Classs.SQL_Classes.Create_Type_Statement.CreateTypeStmtClass;
import Classs.SQL_Classes.Create_ِِAggregation_Function.CreateAggregationFunctionClass;
import Classs.SQL_Classes.Create_ِِAggregation_Function.CreateAggregationFunctionElementsClass;
import Classs.SQL_Classes.Delete_Statement.DeleteStmtClass;
import Classs.SQL_Classes.Drop_Table_Statement.DropTableStmtClass;
import Classs.SQL_Classes.Insert_Statement.InsertStmtClass;
import Classs.SQL_Classes.Insert_Statement.InsertStmtValuesClass;
import Classs.SQL_Classes.Select_Statement.*;
import Classs.SQL_Classes.SqlStmtClass;
import Classs.SQL_Classes.SqlStmtListClass;

public class BaseAstVisitor implements AstVisitorInterface {



    @Override
    public void visit(ParserClass parser) {
        System.out.println("♠ Visit Parser");
    }

    @Override
    public void visit(AllStatementsStartClass allStatements_Start) {
        System.out.println("♠ Visit All Statement");
    }

    @Override
    public void visit(JavaStatementsListClass java_statements_listClass) {
        System.out.println("♠ Visit Java Statement List");
    }

    @Override
    public void visit(JavaStatementClass java_statement) {
        System.out.println("♠ Visit Java Statement");
    }

    @Override
    public void visit(ScopeStatementClass scope_statement) {
        System.out.println("♠ Visit Scope Statement");
    }

    @Override
    public void visit(PrototypeFunctionClass prototype_function) {
        System.out.println("♠ Visit Prototype Function");
    }

    @Override
    public void visit(PrototypeFunctionParameterClass prototype_function_parameter) {
        System.out.println("♠ Visit Prototype Function Parameter");
    }

    @Override
    public void visit(BodyPrototypeFunctionClass body_prototype_function) {
        System.out.println("♠ Visit Body Prototype Function");
    }

    @Override
    public void visit(FunctionParameterInPrototypeClass function_parameter_in_prototype) {
        System.out.println("♠ Visit Function Parameter In Prototype");
    }

    @Override
    public void visit(CreateVariableStatementClass create_variable_statement) {
        System.out.println("♠ Visit Create Variable Statement");
    }

    @Override
    public void visit(AssignVariableInCreationClass assign_variable_in_creation) {
        System.out.println("♠ Visit Assign Variable In Creation");
    }

    @Override
    public void visit(DefaultValueParameterClass defaultValueParameterClass) {
        System.out.println("♠ Visit Default Value Parameter");
    }

    @Override
    public void visit(InitialVariableDeclarationClass initial_variable_declaration) {
        System.out.println("♠ Visit Initial Variable Declaration");
    }

    @Override
    public void visit(InitialVariableDeclarationWithDefaultValueClass initial_variable_declaration_with_default_value) {
        System.out.println("♠ Visit Initial Variable Declaration With Default Value");
    }

    @Override
    public void visit(FunctionParameterWithDefaultClass function_parameter_with_default) {
        System.out.println("♠ Visit Function Parameter With Default");
    }

    @Override
    public void visit(BodyFunctionClass bodyFunctionClass) {
        System.out.println("♠ Visit Body Function");
    }

    @Override
    public void visit(CallbackFunctionParameterClass callbackFunctionParameterClass) {
        System.out.println("♠ Visit Callback Function Parameter");
        if (callbackFunctionParameterClass.getPrimitive_parameter() != null) {
            System.out.println("\t\tVariable : " + callbackFunctionParameterClass.getPrimitive_parameter());
        }
    }

    @Override
    public void visit(CallbackFunctionParameterInHeaderClass callbackFunctionParameterInHeaderClass) {
        System.out.println("♠ Visit Callback Function Parameter In Header");
    }

    @Override
    public void visit(CallbackFunctionWithOutScolClass callbackFunctionWithOutScolClass) {
        System.out.println("♠ Visit Callback Function Without SCOL");
        if (callbackFunctionWithOutScolClass.getFunction_name() != null) {
            System.out.println("\t\tFunction Name : " + callbackFunctionWithOutScolClass.getFunction_name());
        }
    }

    @Override
    public void visit(FunctionParameterClass functionParameterClass) {
        System.out.println("♠ Visit Function Parameter");
        if (functionParameterClass.getVar_name_in_parameter() != null) {
            for (String variable_name : functionParameterClass.getVar_name_in_parameter().getVariable_parameters()) {
                System.out.println("\t\tVariable Parameter : " + variable_name);
            }
        }
    }

    @Override
    public void visit(VarNameInParameterClass varNameInParameterClass) {
        System.out.println("♠ Visit Var Name In Parameter");
    }

    @Override
    public void visit(ArrayValueClass arrayValueClass) {
        System.out.println("♠ Visit Array Value");
        if (arrayValueClass.getNumber_value() != null) {
            System.out.println("Size Array : " + arrayValueClass.getNumber_value().getNumber_value());
        }
    }

    @Override
    public void visit(AssignArrayClass assignArrayClass) {
        System.out.println("♠ Visit Assign Array");
        if (assignArrayClass.getPost_square() != null) {
            System.out.println("\t\tVariable Name : " + assignArrayClass.getPost_square().getVariable_name());
        }
        if (assignArrayClass.getPrevious_square() != null) {
            System.out.println("\t\tVariable Name : " + assignArrayClass.getPrevious_square().getVariable_name());
        }
    }

    @Override
    public void visit(AssignArrayVariableClass assignArrayVariableClass) {
        System.out.println("♠ Visit Assign Array Variable");
    }

    @Override
    public void visit(AssignVariableClass assignVariableClass) {
        System.out.println("♠ Visit Assign Variable");
        if (assignVariableClass.getVariable_name() != null) {
            System.out.println("\t\tVariable Name : " + assignVariableClass.getVariable_name());
        }
    }

    @Override
    public void visit(DirectArrayValueClass directArrayValueClass) {
        System.out.println("♠ Visit Direct Array Value");
    }

    @Override
    public void visit(PostSquareClass postSquareClass) {
        System.out.println("♠ Visit Post Square");
    }

    @Override
    public void visit(PreviousSquareClass previousSquareClass) {
        System.out.println("♠ Visit Previous Square");
    }

    @Override
    public void visit(ExpressionJacaClass expressionJacaClass) {
        System.out.println("♠ Visit Expression Java");
//        if (exprJavaClass.getNumber_value() != null) {
//            System.out.println("\t\tNumber Value : " + exprJavaClass.getNumber_value().getNumber_value());
//        }
//        if (exprJavaClass.getOperation_in_java() != null) {
//            System.out.println("\t\tOperation : " + exprJavaClass.getOperation_in_java().getOperation());
//        }
    }

    @Override
    public void visit(LogicalConditionClass logicalConditionClass) {
        System.out.println("♠ Visit Logical Condition");
//        if (logicalConditionClass.getLogical_in_java() != null) {
//            System.out.println("\t\tOperation : " + logicalConditionClass.getLogical_in_java().getOperation());
//        }
    }

    @Override
    public void visit(NumberConditionClass numberConditionClass) {
        System.out.println("♠ Visit Number Condition");
//        if (numberConditionClass.getComparison_in_java() != null) {
//            System.out.println("\t\t Operation : " + numberConditionClass.getComparison_in_java().getOperation());
//        }
    }

    @Override
    public void visit(LineConditionExpressionJavaClass lineConditionExpressionJavaClass) {
        System.out.println("♠ Visit Line Condition Expression Java");
    }

    @Override
    public void visit(ValueOfOneItemArrayClass valueOfOneItemArrayClass) {
        System.out.println("♠ Visit Value Of One Item Array");
    }

    @Override
    public void visit(VariableNameItemInArrayClass variableNameItemInArrayClass) {
        System.out.println("♠ Visit Variable Name Item In Array");
    }

    @Override
    public void visit(VariableValueClass variableValueClass) {
        System.out.println("♠ Visit Variable Value");
        if (variableValueClass.getNumber_literal() != null) {
            System.out.println("\t\tNumber Literal : " + variableValueClass.getNumber_literal());
        }
        if (variableValueClass.getValue_in_quote() != null) {
            System.out.println("\t\tValue In Quote : " + variableValueClass.getValue_in_quote());
        }
        if (variableValueClass.getVariable_name() != null) {
            System.out.println("\t\tVariable Name : " + variableValueClass.getVariable_name());
        }
        if (variableValueClass.getVariable_name_item_in_array() != null) {
            System.out.println("Variable Name Item : " + variableValueClass.getVariable_name_item_in_array().getVariable_item_name());
//            System.out.println("Variable Index Item : " + variableValueClass.getVariable_name_item_in_array().getValue_of_one_item_array().getNumber_value_java());
        }
        if (variableValueClass.getTrue_false_null_value() != null) {
            System.out.println("Variable Value : " + variableValueClass.getTrue_false_null_value());
        }
    }

    @Override
    public void visit(IncrementTypeClass incrementTypeClass) {
        System.out.println("♠ Visit Increment Type");
    }

    @Override
    public void visit(ArrayJsonDeclarationClass arrayJsonDeclarationClass) {
        System.out.println("♠ Visit Array Json Declaration");
    }

    @Override
    public void visit(AllFiledsInJsonClass allFiledsInJsonClass) {
        System.out.println("♠ Visit All Filed's In Json");
    }

    @Override
    public void visit(FiledInJsonClass filedInJsonClass) {
        System.out.println("♠ Visit Filed In Json");
        if (filedInJsonClass.getKey_filed() != null) {
            System.out.println("\t\tKey Name : " + filedInJsonClass.getKey_filed().getKey_name());
        }
    }

    @Override
    public void visit(JsonDeclarationClass jsonDeclarationClass) {
        System.out.println("♠ Visit Json Declaration");
    }

    @Override
    public void visit(AccessJsonDataClass accessJsonDataClass) {
        System.out.println("♠ Visit Access Json Data");
        System.out.println("♠ Object Name : " + accessJsonDataClass.getVariable_name());


    }

    @Override
    public void visit(KeyFiledClass keyFiledClass) {
        System.out.println("♠ Visit Key Filed");
    }

    @Override
    public void visit(ForStatementClass forStatementClass) {
        System.out.println("♠ Visit For Statement");
    }

    @Override
    public void visit(ForParameterClass forParameterClass) {
        System.out.println("♠ Visit For Parameter");
    }

    @Override
    public void visit(InitialStartCountInForClass initialStartCountInForClass) {
        System.out.println("♠ Visit Initial Start Count In For");
        if (initialStartCountInForClass.getInitial_variable_declaration() != null)
            System.out.println("\t\tVariable Name : " + initialStartCountInForClass.getInitial_variable_declaration().getVariable_name());
        if (initialStartCountInForClass.getNumber_value() != null)
            System.out.println("\t\tVariable Value : " + initialStartCountInForClass.getNumber_value().getNumber_value());
    }

    @Override
    public void visit(NumberValueJavaClass numberValueJavaClass) {
        System.out.println("♠ Visit Number Value");
        if (numberValueJavaClass.getVariable_value_in_quotes() != null) {
            System.out.println("\t\t♠ Value In Quote's: " + numberValueJavaClass.getVariable_value_in_quotes());
        }
        if (numberValueJavaClass.getNumber_value() != null)
            System.out.println("\t\t♠ Number Value : " + numberValueJavaClass.getNumber_value());
    }

    @Override
    public void visit(ConditionForClass conditionForClass) {
        System.out.println("♠ Visit Condition For");
    }

    @Override
    public void visit(FullConditionClass fullConditionClass) {
        System.out.println("♠ Visit Full Condition");
    }

    @Override
    public void visit(OneConditionJavaClass oneConditionJavaClass) {
        System.out.println("♠ Visit One Condition Java");
        if (oneConditionJavaClass.getOperand() != null) {
            System.out.println("\t\tOperand : " + oneConditionJavaClass.getOperand());
        }
    }

    @Override
    public void visit(NotVarNameClass notVarNameClass) {
        System.out.println("♠ Visit Not Var Name");
        if (notVarNameClass.getOperation() != null)
            System.out.println("\t\tOperation : " + notVarNameClass.getOperation());
        if (notVarNameClass.getOperand() != null)
            System.out.println("\t\tOperand : " + notVarNameClass.getOperand());
    }

    @Override
    public void visit(NotOneConditionClass notOneConditionClass) {
        System.out.println("♠ Visit Not One Condition");
        if (notOneConditionClass.getOperation() != null) {
            System.out.println("Operation : " + notOneConditionClass.getOperation());
        }
    }

    @Override
    public void visit(MultiConditionJavaClass multiConditionJavaClass) {
        System.out.println("♠ Visit Multi Condition Java");
    }

    @Override
    public void visit(IncrementCountForClass incrementCountForClass) {
        System.out.println("♠ Visit Increment Count For");
    }

    @Override
    public void visit(AdvancedIncreaseClass advancedIncreaseClass) {
        System.out.println("♠ Visit Advanced Increase");
        if (advancedIncreaseClass.getAdvanced_increase() != null) {
            System.out.println("\t\tVariable Name : " + advancedIncreaseClass.getAdvanced_increase().getVariable_name());
            System.out.println("\t\tOperation : " + advancedIncreaseClass.getAdvanced_increase().getOperation());
        }
    }

    @Override
    public void visit(DelayedIncreaseClass delayedIncreaseClass) {
        System.out.println("♠ Visit Delayed Increase");
        if (delayedIncreaseClass.getDelayed_operation() != null) {
            System.out.println("\t\tVariable Name : " + delayedIncreaseClass.getDelayed_operation().getVariable_name());
            System.out.println("\t\tOperation : " + delayedIncreaseClass.getDelayed_operation().getOperation());
        }
    }

    @Override
    public void visit(SpecificIncreaseClass specificIncreaseClass) {
        System.out.println("♠ Visit Specific Increase");
        if (specificIncreaseClass.getOperation() != null) {
            System.out.println("Operation : " + specificIncreaseClass.getOperation().getOperation());
            System.out.println("Variable Name : " + specificIncreaseClass.getOperation().getVariable_name());
        }
    }

    @Override
    public void visit(ElseIfStatementClass elseIfStatementClass) {
        System.out.println("♠ Visit Else If Statement");
    }

    @Override
    public void visit(ElseStatementClass elseStatementClass) {
        System.out.println("♠ Visit Else Statement");
    }

    @Override
    public void visit(HeaderIfClass headerIfClass) {
        System.out.println("♠ Visit Header If");
    }

    @Override
    public void visit(IfStatementClass ifStatementClass) {
        System.out.println("♠ Visit If Statement");
    }

    @Override
    public void visit(LogicalOperationStatement logicalOperationStatement) {
        System.out.println("♠ Visit Logical Operation");
        if (logicalOperationStatement.getVariable_value_in_quotes() != null) {
            System.out.println("\t\tVariable Value In Quote : " + logicalOperationStatement.getVariable_value_in_quotes());
        }

        if (logicalOperationStatement.getVariable_name() != null) {
            System.out.println("\t\tVariable Name : " + logicalOperationStatement.getVariable_name());
        }

//        if (logicalOperationStatement.getLogical_in_java() != null) {
//            System.out.println("\t\tOperation : " + logicalOperationStatement.getLogical_in_java().getOperation());
//        }
    }

    @Override
    public void visit(OperationInJava operationInJava) {
        System.out.println("♠ Visit Operation In Java");
        if (operationInJava.getOperation() != null) {
            System.out.println("\t\tOperation : " + operationInJava.getOperation());
        }
    }

    @Override
    public void visit(ComparisonInJava comparisonInJava) {
        System.out.println("♠ Visit Comparision In Java");
        if (comparisonInJava.getOperation() != null) {
            System.out.println("\t\t Operation : " + comparisonInJava.getOperation());
        }
    }

    @Override
    public void visit(LogicalInJava logicalInJava) {
        System.out.println("♠ Visit Logical In Java");
        if (logicalInJava.getOperation() != null) {
            System.out.println("\t\tOperation : " + logicalInJava.getOperation());
        }
    }

    @Override
    public void visit(CallbackFunctionClass callbackFunctionClass) {
        System.out.println("♠ Visit Callback Function");
    }

    @Override
    public void visit(CallbackFunctionHeaderClass callbackFunctionHeaderClass) {
        System.out.println("♠ Visit Callback Function Header");
    }

    @Override
    public void visit(WhileStatementClass whileStatementClass) {
        System.out.println("♠ Visit While Statement");
    }

    @Override
    public void visit(DoWhileStatementClass doWhileStatementClass) {
        System.out.println("♠ Visit Do While Function");
    }

    @Override
    public void visit(SwitchStatementClass switchStatementClass) {
        System.out.println("♠ Visit Switch Statement");
    }

    @Override
    public void visit(CaseStatementClass caseStatementClass) {
        System.out.println("♠ Visit Case Statement");
    }

    @Override
    public void visit(BodyForCaseAndDefaultClass bodyForCaseAndDefaultClass) {
        System.out.println("♠ Visit Body For Case And Default Statement");
    }

    @Override
    public void visit(DefaultStatementClass defaultStatementClass) {
        System.out.println("♠ Visit Default Statement");
    }

    @Override
    public void visit(ForeachStatementClass foreachStatementClass) {
        System.out.println("♠ Visit Foreach Statement");
        if (foreachStatementClass.getVariables_in_foreach() != null) {
            for (VariableInForeachClass variableInForeachClass : foreachStatementClass.getVariables_in_foreach()) {
                System.out.println("\t\tVariable Name In Foreach: " + variableInForeachClass.getVariable_name());
            }
        }
    }

    @Override
    public void visit(VariableInForeachClass variableInForeachClass) {
        System.out.println("♠ Visit Variable In Foreach");
    }

    @Override
    public void visit(PrintStatementClass printStatementClass) {
        System.out.println("♠ Visit Print Statement");
    }

    @Override
    public void visit(BreakStatementClass breakStatementClass) {
        System.out.println("♠ Visit Break Statement");
    }

    @Override
    public void visit(ContinueStatementClass continueStatementClass) {
        System.out.println("♠ Visit Continue Statement");
    }

    @Override
    public void visit(IncrementStatementClass incrementStatementClass) {
        System.out.println("♠ Visit Increment Statement");
    }

    @Override
    public void visit(ReturnStatementClass returnStatementClass) {
        System.out.println("♠ Visit Return Statement");
        if (returnStatementClass.getVariable_value() != null) {
            System.out.println("\t\tVariable Value : " + returnStatementClass.getVariable_value());
        }
    }

    @Override
    public void visit(LineConditionResultClass lineConditionResultClass) {
        System.out.println("♠ Visit Line Condition Result");
    }

    @Override
    public void visit(AssignVariableWithoutVarClass assignVariableWithoutVarClass) {
        System.out.println("♠ Visit Assign Variable Without Var");
        if(assignVariableWithoutVarClass.getVariable_name() != null){
            System.out.println("\t\tVariable Name : " + assignVariableWithoutVarClass.getVariable_name());
        }

    }


    @Override
    public void visit(SqlStmtListClass sqlStmtListClass) {
        System.out.println("♠ Visit Sql Stmt List");
    }

    @Override
    public void visit(SqlStmtClass sqlStmtClass) {
        System.out.println("♠ Visit Sql Stmt");
    }

    @Override
    public void visit(AlterTableStmtClass alterTableStmtClass) {
        System.out.print("♠ Visit Alter Table Stmt : ");

        if(alterTableStmtClass.getDatabaseName()!=null)
        {
            System.out.print("data base name : "+alterTableStmtClass.getDatabaseName());
        }
        System.out.print("source table name : "+alterTableStmtClass.getSourceTableName());
        if(alterTableStmtClass.getNewTableName()!=null)
        {
            System.out.print("new table name : "+alterTableStmtClass.getNewTableName());
        }
        System.out.println();
    }

    @Override
    public void visit(AlterTableAddClass alterTableAddClass) {
        System.out.println("♠ Visit Alter Table Add");
    }

    @Override
    public void visit(AlterTableAddConstraintClass alterTableAddConstraintClass) {
        System.out.print("♠ Visit Alter Table Add Constraint : ");
        System.out.print("table name"+alterTableAddConstraintClass.getAnyName());
        System.out.println();

    }

    @Override
    public void visit(ColumnConstraintClass columnConstraintClass) {
        System.out.print("♠ Visit Column Constrain : ");
        if(columnConstraintClass.getConstraintName()!=null)
        {
            System.out.print(" constraint name : "+columnConstraintClass.getConstraintName());
        }
        if(columnConstraintClass.getColltionName()!=null)
        {
            System.out.println(" collation name : "+columnConstraintClass.getColltionName());
        }
        System.out.println();
    }

    @Override
    public void visit(ColumnConstraintForeignKeyClass columnConstraintForeignKeyClass) {
        System.out.println("♠ Visit Column Constraint Foreign Key");
    }

    @Override
    public void visit(ColumnConstraintNotNullClass columnConstraintNotNullClass) {
        System.out.print("♠ Visit Column Constraint Not Null : ");
        System.out.print(columnConstraintNotNullClass.getNotNull());
        System.out.println();
    }

    @Override
    public void visit(ColumnConstraintNullClass columnConstraintNullClass) {
        System.out.print("♠ Visit Column Constraint Null : ");
        System.out.print(columnConstraintNullClass.getNull());
        System.out.println();
    }

    @Override
    public void visit(ColumnConstraintOrTypeName columnConstraintOrTypeName) {
        System.out.println("♠ Visit Column Constraint Or TypeName");
    }

    @Override
    public void visit(ColumnConstraintPrimaryKeyClass columnConstraintPrimaryKeyClass) {
        System.out.print("♠ Visit Column Constraint Primary Key :");
        System.out.print("Asc : "+columnConstraintPrimaryKeyClass.isAsc());
        System.out.print(" Desc : "+columnConstraintPrimaryKeyClass.isDesc());
        System.out.print(" auto Increment : "+columnConstraintPrimaryKeyClass.isAutoIncrement());
        System.out.println();

    }

    @Override
    public void visit(ColumnDefaultClass columnDefaultClass) {
        System.out.print("♠ Visit Column Default :");
        for (int i = 0; i <columnDefaultClass.getAnyNames().size() ; i++) {
            System.out.print(" any name :: "+columnDefaultClass.getAnyNames().get(i));
        }
        System.out.println();
    }

    @Override
    public void visit(ColumnDefaultContentClass columnDefaultContentClass) {
        System.out.print("♠ Visit Column Default Content : ");
        if(columnDefaultContentClass.getAnyName()!=null)
        {
            System.out.print("name : "+columnDefaultContentClass.getAnyName());
        }
        System.out.println();
    }

    @Override
    public void visit(ColumnDefaultValue columnDefaultValue) {
        System.out.print("♠ Visit Column Default Value : ");
        if(columnDefaultValue.getLiteralValue()!=null)
        {
            System.out.print("literal Value : "+columnDefaultValue.getLiteralValue());
        }else {
            System.out.print("signed number : "+columnDefaultValue.getLiteralValue());
        }
        System.out.println();
    }

    @Override
    public void visit(ColumnDefClass columnDefClass) {
        System.out.print("♠ Visit Column Def : ");
        System.out.print("column name : "+columnDefClass.getColumnName());
        System.out.println();
    }

    @Override
    public void visit(ColumnsDefOrSelectStmtClass columnsDefOrSelectStmtClass) {
        System.out.println("♠ Visit Columns Def Or SelectS tmt");
    }

    @Override
    public void visit(ColumnsDeftTableConstraintClass columnsDeftTableConstraintClass) {
        System.out.println("♠ Visit Columns Deft Table Constraint");
    }

    @Override
    public void visit(CreateTableStmtClass createTableStmtClass) {
        System.out.print("♠ Visit Create Table Stmt");
        System.out.print("if not exist : "+createTableStmtClass.isIfNotExist());
        System.out.println();
    }

    @Override
    public void visit(ForeignDeferrableClass foreignDeferrableClass) {
        System.out.print("♠ Visit Foreign Deferrable");
        System.out.print("not : "+foreignDeferrableClass.isNot());
        System.out.print(" initially Deferred : "+foreignDeferrableClass.isInitiallyDeferred());
        System.out.print(" initially Immediate : "+foreignDeferrableClass.isInitiallyImmediate());
        System.out.print(" enable : "+foreignDeferrableClass.isEnable());
        System.out.println();

    }

    @Override
    public void visit(ForeignKeyClauseClass foreignKeyClauseClass) {
        System.out.print("♠ Visit Foreign Key Clause :");
        for (int i = 0; i <foreignKeyClauseClass.getFkTargetColumnName().size() ; i++) {
            System.out.print(" Fk Target Column Name :: "+foreignKeyClauseClass.getFkTargetColumnName().get(i));
        }
        System.out.println();

    }

    @Override
    public void visit(ForeignOn foreignOn) {
        System.out.print("♠ Visit Foreign On :");
        System.out.print(" delete : "+foreignOn.isDelete());
        System.out.print(" update : "+foreignOn.isUpdate());
        System.out.print(" set Null : "+foreignOn.isSetNull());
        System.out.print(" set Default : "+foreignOn.isSetDefault());
        System.out.print(" casasde : "+foreignOn.isCasasde());
        System.out.print(" restrict : "+foreignOn.isRestrict());
        System.out.print(" no Action : "+foreignOn.isNoAction());
        System.out.println();
    }

    @Override
    public void visit(ForeignOnOrMatch foreignOnOrMatch) {
        System.out.print("♠ Visit Foreign On Or Match : ");
        if(foreignOnOrMatch.getMatchName()!=null)
        {
            System.out.print("match Name : "+foreignOnOrMatch.getMatchName());
        }
        System.out.println();
    }

    @Override
    public void visit(ForeignTableFaildClass foreignTableFaildClass) {
        System.out.print("♠ Visit Foreign Table Field : ");
        if(foreignTableFaildClass.getDatabaseName()!=null)
        {
            System.out.print("Database Name : " + foreignTableFaildClass.getDatabaseName());
        }
        System.out.print("Foreign Table : " + foreignTableFaildClass.getForeignTable());
        System.out.println();
    }

    @Override
    public void visit(IndexedColumn indexedColumn) {
        System.out.print("♠ Visit Indexed Column");
        System.out.print("column Name : "+indexedColumn.getColumnName());
        if(indexedColumn.getCollistionName()!=null)
        {
            System.out.print(" collation name :" + indexedColumn.getCollistionName());
        }
        System.out.print(" Asc : "+indexedColumn.isAsc());
        System.out.print(" Desc : "+indexedColumn.isDesc());
        System.out.println();
    }

    @Override
    public void visit(SignedNumberAnyNameClass signedNumberAnyNameClass) {
        System.out.print("♠ Visit Signed Number Any Name : ");
        System.out.print("signed Number : "+signedNumberAnyNameClass.getSignedNumber());
        if(signedNumberAnyNameClass.getAnyName()!=null)
        {
            System.out.print(" any name : "+signedNumberAnyNameClass.getAnyName());
        }
        System.out.println();

    }

    @Override
    public void visit(TableConstraintClass tableConstraintClass) {
        System.out.print("♠ Visit Table Constraint : ");
        if(tableConstraintClass.getConstraintName()!=null)
        {
            System.out.print("Constraint name : "+tableConstraintClass.getConstraintName());
        }
        System.out.println();
    }

    @Override
    public void visit(TableConstraintForeginKey tableConstraintForeginKey) {
        System.out.print("♠ Visit Table Constraint Foregin Key :");
        for (int i = 0; i <tableConstraintForeginKey.getFkOriginColumnNames().size() ; i++) {
            System.out.print(" Fk Origin Column Name :: "+tableConstraintForeginKey.getFkOriginColumnNames().get(i));
        }
        System.out.println();
    }

    @Override
    public void visit(TableConstraintKey tableConstraintKey) {
        System.out.print("♠ Visit Table Constraint Key : ");
        if(tableConstraintKey.getName()!=null)
        {
            System.out.print("name : "+tableConstraintKey.getName());
        }
        System.out.println();
    }

    @Override
    public void visit(TableConstraintPrimaryKey tableConstraintPrimaryKey) {
        System.out.println("♠ Visit Table Constraint Primary Key");
    }

    @Override
    public void visit(TableConstraintUnique tableConstraintUnique) {
        System.out.print("♠ Visit Table Constraint Unique : ");
        if(tableConstraintUnique.getName()!=null)
        {
            System.out.print("name : "+tableConstraintUnique.getName());
        }
        System.out.println();
    }

    @Override
    public void visit(TypeNameClass typeNameClass) {
        System.out.print("♠ Visit Type Name : ");
        System.out.print(" type name : "+typeNameClass.getName());
        System.out.println();
    }

    @Override
    public void visit(DeleteStmtClass deleteStmtClass) {
        System.out.println("♠ Visit Delete Stmt");
    }

    @Override
    public void visit(DropTableStmtClass dropTableStmtClass) {
        System.out.print("♠ Visit Drop Table Stmt : ");
        System.out.print("if exist : "+dropTableStmtClass.isIfExist());
        System.out.println();
    }

    @Override
    public void visit(InsertStmtClass insertStmtClass) {
        System.out.print("♠ Visit Insert Stmt :");
        for (int i = 0; i <insertStmtClass.getColumnsName().size() ; i++) {
            System.out.println(" column name :: "+insertStmtClass.getColumnsName().get(i));
        }
        System.out.println();
    }

    @Override
    public void visit(InsertStmtValuesClass insertStmtValuesClass) {
        System.out.print("♠ Visit Insert Stmt Values : ");
        System.out.print("Default Values : "+insertStmtValuesClass.getDefaultValues());
        System.out.println();
    }

    @Override
    public void visit(ColumnNameFaildClass columnNameFaildClass) {
        System.out.print("♠ Visit Column Name Faild : ");
        System.out.print("column name : "+columnNameFaildClass.getColumnName());
        System.out.println();
    }

    @Override
    public void visit(ExistsCaseClass existsCaseClass) {
        System.out.print("♠ Visit Exists Case :");
        System.out.print(" exists : "+existsCaseClass.isExists());
        System.out.println();
    }

    @Override
    public void visit(ExprClass exprClass) {
        System.out.print("♠ Visit Expr : ");
        if(exprClass.getLiteralValue()!=null)
        {
            System.out.print("Literal Value : "+exprClass.getLiteralValue());
        }
        System.out.println();

    }

    @Override
    public void visit(ExprCommaExprClass exprCommaExprClass) {
        System.out.println("♠ Visit Insert Expr Comma Expr");
    }

    @Override
    public void visit(ExprOperatorExprClass exprOperatorExprClass) {
        System.out.print("♠ Visit Expr Operator Expr : ");
        System.out.print("operator : "+exprOperatorExprClass.getOperator());
        System.out.println();
    }

    @Override
    public void visit(FactoredSelectStmtClass factoredSelectStmtClass) {
        System.out.println("♠ Visit Factored Select Stmt");
    }

    @Override
    public void visit(FromItemsAsTableClass fromItemsAsTableClass) {
        System.out.print("♠ Visit From Items As Table : ");
        if(fromItemsAsTableClass.getTableAlias()!=null)
        {
            System.out.print("Table Alias : "+fromItemsAsTableClass.getTableAlias());
        }
        System.out.print(" as : "+fromItemsAsTableClass.isAs());
        System.out.println();
    }

    @Override
    public void visit(FromItemsClass fromItemsClass) {
        System.out.println("♠ Visit From Items");
    }

    @Override
    public void visit(FunctionExprClass functionExprClass) {
        System.out.print("♠ Visit Function Expr : ");
        System.out.print("function name : "+functionExprClass.getFunctionName());
        System.out.println();

    }

    @Override
    public void visit(FunctionParameterSqlClass functionParameterSqlClass) {
        System.out.print("♠ Visit Function Parameter Sql : ");
        System.out.print("Distict : "+functionParameterSqlClass.isDistict());
        System.out.println(" Star : "+functionParameterSqlClass.isStar());
        System.out.println();
    }

    @Override
    public void visit(GroupByClass groupByClass) {
        System.out.println("♠ Visit Group By");
    }

    @Override
    public void visit(InCaseClass inCaseClass) {
        System.out.print("♠ Visit InCase : ");
        System.out.print("not : "+inCaseClass.iskNot());
        System.out.println();
    }

    @Override
    public void visit(IndexNameOrNotIndexClass indexNameOrNotIndexClass) {
        System.out.print("♠ Visit Index Name Or Not Index : ");
        if(indexNameOrNotIndexClass.getIndexName()!=null)
        {
            System.out.print("Index name : "+indexNameOrNotIndexClass.getIndexName());
        }
        System.out.print(" Not Index : "+indexNameOrNotIndexClass.isNotIndex());
        System.out.println();
    }

    @Override
    public void visit(JoinClauseClass joinClauseClass) {
        System.out.println("♠ Visit Join Clause");
    }

    @Override
    public void visit(JoinOperatorClass joinOperatorClass) {
        System.out.print("♠ Visit Join Operator : ");
        System.out.print("comma : "+joinOperatorClass.isComma());
        System.out.print(" inner : "+joinOperatorClass.isInner());
        System.out.print(" left : "+joinOperatorClass.isLeft());
        System.out.print(" left outer"+joinOperatorClass.isLeftOuter());
        System.out.println();
    }

    @Override
    public void visit(LimitSelectClass limitSelectClass) {
        System.out.print("♠ Visit Limit Select : ");
        System.out.print("comma : "+limitSelectClass.isComma());
        System.out.print(" Offset : "+limitSelectClass.isOffset());
        System.out.println();
    }

    @Override
    public void visit(OperatorTableConstraintClass operatorTableConstraintClass) {
        System.out.println("♠ Visit Operator Table Constraint");
    }

    @Override
    public void visit(OrderByClass orderByClass) {
        System.out.println("♠ Visit Order By");
    }

    @Override
    public void visit(OrderingTermClass orderingTermClass) {
        System.out.print("♠ Visit Ordering Term : ");
        if(orderingTermClass.getCollisionName()!=null)
        {
            System.out.print("collation name : "+orderingTermClass.getCollisionName());
        }
        System.out.print(" Asc : "+orderingTermClass.isAsc());
        System.out.print(" Desc : "+orderingTermClass.isDesc());
        System.out.println();
    }

    @Override
    public void visit(ResultColumnClass resultColumnClass) {
        System.out.print("♠ Visit Result Column :");
        System.out.print(" isStar : "+resultColumnClass.isStar());
        if(resultColumnClass.isTableNameWithStar()!=null)
        {
            System.out.print(" table Name with star : "+resultColumnClass.isTableNameWithStar());
        }
        System.out.println();
    }

    @Override
    public void visit(ResultColumnWithExprClass resultColumnWithExprClass) {
        System.out.print("♠ Visit Result Column With Expr : ");
        if(resultColumnWithExprClass.getColumnAlias()!=null)
        {
            System.out.print("Column Alias : "+resultColumnWithExprClass.getColumnAlias());
        }
        System.out.print(" As : "+resultColumnWithExprClass.isAs());
        System.out.println();
    }

    @Override
    public void visit(SelectCoreClass selectCoreClass) {
        System.out.println("♠ Visit Select Core");
    }

    @Override
    public void visit(SelectItemsClass selectItemsClass) {
        System.out.print("♠ Visit Select Items : ");
        System.out.print("all : "+selectItemsClass.isAll());
        System.out.print(" distinct : "+selectItemsClass.isDistinct());
        System.out.println();
    }

    @Override
    public void visit(SelectOrExprClass selectOrExprClass) {
        System.out.println("♠ Visit Select Or Expr");
    }

    @Override
    public void visit(SelectStmtAsTableClass selectStmtAsTableClass) {
        System.out.print("♠ Visit Select Stmt As Table : ");
        if(selectStmtAsTableClass.getTableAlias()!=null)
        {
            System.out.print("table Alias : "+selectStmtAsTableClass.getTableAlias());
        }
        System.out.print(" As : "+selectStmtAsTableClass.isAs());
        System.out.println();
    }

    @Override
    public void visit(SelectStmtClass selectStmtClass) {
        System.out.println("♠ Visit Select Stmt");
    }

    @Override
    public void visit(TableAsAliasClass tableAsAliasClass) {
        System.out.print("♠ Visit Table As Alias");
        if(tableAsAliasClass.getTableAlias()!=null)
        {
            System.out.print("table Alias : "+tableAsAliasClass.getTableAlias());
        }
        System.out.print(" As : "+tableAsAliasClass.isAs());
        System.out.println();
    }

    @Override
    public void visit(TableNameFaildClass tableNameFaildClass) {
        System.out.print("♠ Visit Table Name Faild :");
       /* if(tableNameFaildClass.getDatabaseName()!=null)
        {
            System.out.print(" database name : "+tableNameFaildClass.getDatabaseName());
        }*/
        System.out.print(" table name : "+tableNameFaildClass.getTableName());
        System.out.println();
    }

    @Override
    public void visit(TableOrSubqueryClass tableOrSubqueryClass) {
        System.out.println("♠ Visit Table Or Sub query");
    }

    @Override
    public void visit(ValuesItemsClass valuesItemsClass) {
        System.out.println("♠ Visit Values Items");
    }


    @Override
    public void visit(CreateTableStmtTypeClass createTableStmtTypeClass) {

            System.out.println("♠ Visit Create Table Stmt Type");
    }

    @Override
    public void visit(TypeFaildClass typeFaildClass) {
        System.out.println("♠ Visit Type Faild");
    }

    @Override
    public void visit(CreateTableStmtPathClass createTableStmtPathClass) {
        System.out.println("♠ Visit Create Table Stmt Path");

    }

    @Override
    public void visit(CreateTypeStmtClass createTypeStmtClass) {
        System.out.println("♠ Visit Create Type Stmt");
    }

    @Override
    public void visit(CreateAggregationFunctionClass createAggregationFunctionClass) {
        System.out.println("♠ Visit Create Aggregation Function");

    }

    @Override
    public void visit(CreateAggregationFunctionElementsClass createAggregationFunctionElementsClass) {
        System.out.println("♠ Visit Create Aggregation  Function");
    }

    @Override
    public void visit(ColumnsDefinitionClass columnsDefinitionClass) {
        System.out.println("♠ Visit Columns Definition");

    }

    @Override
    public void visit(ColumnDefinitionClass columnDefinitionClass) {
        System.out.println("♠ Visit Column Definition");

    }
}
