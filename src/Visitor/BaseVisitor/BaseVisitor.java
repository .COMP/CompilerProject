package Visitor.BaseVisitor;

import Classs.Abstruction_Classes.ParserClass;
import Classs.Java_Classes.All_Statement_Start_Classes.JavaStatementsListClass;
import Classs.Java_Classes.Break_Continue_Classes.BreakStatementClass;
import Classs.Java_Classes.Break_Continue_Classes.ContinueStatementClass;
import Classs.Java_Classes.Callbakc_Function_Classes.*;
import Classs.Java_Classes.Create_Variable_Statement_Classes.*;
import Classs.Java_Classes.Do_While_Statement_Classes.DoWhileStatementClass;
import Classs.Java_Classes.Expression_Classes.*;
import Classs.Java_Classes.For_Statement_Classes.*;
import Classs.Java_Classes.Foreach_Statement_Classess.ForeachStatementClass;
import Classs.Java_Classes.Foreach_Statement_Classess.VariableInForeachClass;
import Classs.Java_Classes.Full_Condition_Classes.*;
import Classs.Java_Classes.Global_Classes.*;
import Classs.Java_Classes.If_Statement_Classes.ElseIfStatementClass;
import Classs.Java_Classes.If_Statement_Classes.ElseStatementClass;
import Classs.Java_Classes.If_Statement_Classes.HeaderIfClass;
import Classs.Java_Classes.If_Statement_Classes.IfStatementClass;
import Classs.Java_Classes.Increase_Type_Classes.*;
import Classs.Java_Classes.Java_ClassesParse_Classess.AllStatementsStartClass;
import Classs.Java_Classes.Json.Access_To_Json_Data_Classes.AccessJsonDataClass;
import Classs.Java_Classes.Json.Array_Json_Declaration_Classes.ArrayJsonDeclarationClass;
import Classs.Java_Classes.Json.Json_Declaration_Classes.AllFiledsInJsonClass;
import Classs.Java_Classes.Json.Json_Declaration_Classes.FiledInJsonClass;
import Classs.Java_Classes.Json.Json_Declaration_Classes.JsonDeclarationClass;
import Classs.Java_Classes.Json.Json_Declaration_Classes.KeyFiledClass;
import Classs.Java_Classes.Print_Statement_Classes.PrintStatementClass;
import Classs.Java_Classes.Prototype_Function_Classes.*;
import Classs.Java_Classes.Return_Classes.ReturnStatementClass;
import Classs.Java_Classes.Switch_Statement_Classes.*;
import Classs.Java_Classes.While_Statement_Classes.WhileStatementClass;
import Classs.Operation_Classes.ContentIncreaseOperation;
import Classs.SQL_Classes.Alter_Table_Statement.AlterTableAddClass;
import Classs.SQL_Classes.Alter_Table_Statement.AlterTableAddConstraintClass;
import Classs.SQL_Classes.Alter_Table_Statement.AlterTableStmtClass;
import Classs.SQL_Classes.Create_Table_Statement.*;
import Classs.SQL_Classes.Create_Type_Statement.CreateTypeStmtClass;
import Classs.SQL_Classes.Create_ِِAggregation_Function.CreateAggregationFunctionClass;
import Classs.SQL_Classes.Create_ِِAggregation_Function.CreateAggregationFunctionElementsClass;
import Classs.SQL_Classes.Delete_Statement.DeleteStmtClass;
import Classs.SQL_Classes.Drop_Table_Statement.DropTableStmtClass;
import Classs.SQL_Classes.Insert_Statement.InsertStmtClass;
import Classs.SQL_Classes.Insert_Statement.InsertStmtValuesClass;
import Classs.SQL_Classes.Select_Statement.*;
import Classs.SQL_Classes.SqlStmtClass;
import Classs.SQL_Classes.SqlStmtListClass;
import Classs.SQL_Classes.Update_Statement.ColumnNameEqualsExprClass;
import Classs.SQL_Classes.Update_Statement.QualifiedTableNameClass;
import Classs.SQL_Classes.Update_Statement.UpdateStmtClass;
import ErrorHandler.*;
import SymbolTable.ScopeClass;
import SymbolTable.SqlTypeClass;
import SymbolTable.SymbolClass;
import SymbolTable.TypeEnum;
import TableManagment.SymbolTableManager;
import Utils.Utils;
import Visitor.gen.RulesBaseVisitor;
import Visitor.gen.RulesParser;
import org.antlr.v4.runtime.misc.Pair;

import java.util.ArrayList;
import java.util.List;

public class BaseVisitor extends RulesBaseVisitor {


    @Override
    public ColumnsDefinitionClass visitColumns_definition(RulesParser.Columns_definitionContext ctx) {
        ColumnsDefinitionClass columns_definition = new ColumnsDefinitionClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Columns Definition"
        );
        List<ColumnDefinitionClass> columnDefinitionClasses = new ArrayList<>();
        for (int i = 0; i < ctx.column_definition().size(); i++) {
            columnDefinitionClasses.add(visitColumn_definition(ctx.column_definition(i)));
        }
        columns_definition.setColumns(columnDefinitionClasses);
        return columns_definition;
    }

    @Override
    public ColumnDefinitionClass visitColumn_definition(RulesParser.Column_definitionContext ctx) {
        ColumnDefinitionClass column_definition = new ColumnDefinitionClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Column Definition"
        );
        column_definition.setColumnName(ctx.column_name().getText());
        column_definition.setTypeName(ctx.type_name().getText());
        String type = ctx.type_name().getText().toLowerCase();
        if (!type.equals("number") && !type.equals("string") && !type.equals("boolean")) {

            UndeclaredTypeErrorClass undeclaredTypeErrorClass = new UndeclaredTypeErrorClass(
                    column_definition.getTypeName(),
                    Utils.ERROR_MESSAGE_UNDECLARED_TYPE,
                    String.valueOf(column_definition.getRow()),
                    String.valueOf(column_definition.getColumn())
            );
            undeclaredTypeErrorClass.checkingError();
        }
        return column_definition;
    }

    @Override
    public CreateTableStmtTypeClass visitCreate_table_stmt_type(RulesParser.Create_table_stmt_typeContext ctx) {
        CreateTableStmtTypeClass createTableStmtType = new CreateTableStmtTypeClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Create Table Stmt Type Class"
        );
        createTableStmtType.setTypeFaild(visitType_faild(ctx.type_faild()));
        return createTableStmtType;
    }

    @Override
    public TypeFaildClass visitType_faild(RulesParser.Type_faildContext ctx) {
        TypeFaildClass typeFaild = new TypeFaildClass(ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Type Faild");
        typeFaild.setTypeFaildName(ctx.getText());
        return typeFaild;
    }

    @Override
    public CreateTableStmtPathClass visitCreate_table_stmt_path(RulesParser.Create_table_stmt_pathContext ctx) {
        CreateTableStmtPathClass createTableStmtPathClass = new CreateTableStmtPathClass(ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Create Table Stmt Path");
        createTableStmtPathClass.setPathString(ctx.path_string().getText());


        return createTableStmtPathClass;
    }

    @Override
    public CreateTypeStmtClass visitCreate_type_stmt(RulesParser.Create_type_stmtContext ctx) {
        CreateTypeStmtClass createTypeStmtClass = new CreateTypeStmtClass(ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Create Type Stmt");
        createTypeStmtClass.setIfNotExist(ctx.K_IF() != null);
        createTypeStmtClass.setTypeNameFaild(visitTable_name_faild(ctx.table_name_faild()));
        createTypeStmtClass.setColumns(visitColumns_definition(ctx.columns_definition()));
        DuplicateColumnsSqlErrorClass duplicate_column_sql_error = new DuplicateColumnsSqlErrorClass(
                createTypeStmtClass.getColumns().getColumns(),
                Utils.ERROR_MESSAGE_DUPLICATE_COLUMNS_IN_CREATE,
                String.valueOf(createTypeStmtClass.getRow()),
                String.valueOf(createTypeStmtClass.getColumn())
        );

        duplicate_column_sql_error.checkingError();
        SymbolTableManager.getInstance().addType(createTypeStmtClass.getColumns(), createTypeStmtClass.getTypeNameFaild().getTableName());

        return createTypeStmtClass;
    }

    @Override
    public CreateAggregationFunctionClass visitCreate_aggregation_function(RulesParser.Create_aggregation_functionContext ctx) {
        CreateAggregationFunctionClass createAggregationFunction = new CreateAggregationFunctionClass(ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Create Aggregation Function");

        String aggregation_function_name = ctx.any_name().getText();
        createAggregationFunction.setAggregation_function_name(aggregation_function_name);

        createAggregationFunction.setCreateAggregationFunctionElementsClass(visitCreate_aggregation_function_elements(ctx.create_aggregation_function_elements()));
        CreateAggregationFunctionElementsClass aggregationFunctionElements = createAggregationFunction.getCreateAggregationFunctionElementsClass();

        String class_name = aggregationFunctionElements.getClass_name();
        String return_type = aggregationFunctionElements.getReturn_type();
        String method_name = aggregationFunctionElements.getMethod_name();
        String jar_string = aggregationFunctionElements.getJar_path();
        ArrayList<String> params = aggregationFunctionElements.getParams();

        SymbolTableManager.getInstance().addAggregationFunction(aggregation_function_name, jar_string, class_name, method_name, return_type, params);

        return createAggregationFunction;
    }

    @Override
    public CreateAggregationFunctionElementsClass visitCreate_aggregation_function_elements(RulesParser.Create_aggregation_function_elementsContext ctx) {
        CreateAggregationFunctionElementsClass createAggregationFunctionElements = new CreateAggregationFunctionElementsClass(ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Create Aggregation Function Elements");
        createAggregationFunctionElements.setClass_name(ctx.class_name().getText());
        createAggregationFunctionElements.setJar_path(ctx.path_string().getText());
        createAggregationFunctionElements.setMethod_name(ctx.method_name().getText());
        createAggregationFunctionElements.setReturn_type(ctx.return_type().getText());
        ArrayList<String> params = new ArrayList<>();
        for (int i = 0; i < ctx.return_type_array().return_type().size(); i++)
            params.add(ctx.return_type_array().return_type().get(i).getText());
        createAggregationFunctionElements.setParams(params);
        return createAggregationFunctionElements;
    }

    @Override
    public Object visitReturn_type_array(RulesParser.Return_type_arrayContext ctx) {
        return super.visitReturn_type_array(ctx);
    }

    @Override
    public Object visitReturn_type(RulesParser.Return_typeContext ctx) {
        return super.visitReturn_type(ctx);
    }

    @Override
    public Object visitClass_name(RulesParser.Class_nameContext ctx) {
        return super.visitClass_name(ctx);
    }

    @Override
    public Object visitMethod_name(RulesParser.Method_nameContext ctx) {
        return super.visitMethod_name(ctx);
    }

    @Override
    public Object visitPath_string(RulesParser.Path_stringContext ctx) {
        return super.visitPath_string(ctx);
    }

    @Override
    public Object visitAny_name_with_quote(RulesParser.Any_name_with_quoteContext ctx) {
        return super.visitAny_name_with_quote(ctx);
    }

    @Override
    public Object visitTable_alias(RulesParser.Table_aliasContext ctx) {
        return super.visitTable_alias(ctx);
    }


    @Override
    public ParserClass visitParse(RulesParser.ParseContext ctx) {
        System.out.println(">> Parse Parser");
        ParserClass parserClass = new ParserClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Parse Node"
        );

        if (ctx.all_statement_starts() != null) {
            for (int i = 0; i < ctx.all_statement_starts().size(); i++) {
                String function_name = "";
                String function_type = null;
                RulesParser.Java_stmt_listContext java_statement = ctx.all_statement_starts().get(i).java_stmt_list();
                if (java_statement != null) {
                    RulesParser.Prototype_functionContext prototype_function = java_statement.prototype_function();
                    if (prototype_function != null) {
                        function_name = prototype_function.variable_name().getText();
                        RulesParser.Return_stmtContext return_type = prototype_function.body_prototype_function().return_stmt();
                        if (return_type != null) {
                            if (return_type.variable_value() != null) {
                                String symbol_type = null;
                                RulesParser.Variable_valueContext variable_value = return_type.variable_value();
                                if (variable_value.K_TRUE() != null || variable_value.K_FALSE() != null ||
                                        variable_value.number_condition() != null || variable_value.logical_condition() != null || variable_value.line_condition__expression_java() != null) {
                                    symbol_type = TypeEnum.Boolean.toString();
                                }
                                if (variable_value.K_NULL() != null) {
                                    symbol_type = TypeEnum.Null.toString();
                                }
                                if (variable_value.NUMERIC_LITERAL() != null || variable_value.increment_type() != null) {
                                    symbol_type = TypeEnum.Number.toString();
                                }
                                if (variable_value.value_in_quote() != null) {
                                    symbol_type = TypeEnum.String.toString();
                                }
                                if (variable_value.variable_name() != null) {
                                    symbol_type = SymbolTableManager.getInstance().getParentOfSymbol(variable_value.variable_name().getText());

                                }
                                if (variable_value.callback_function_without_scol() != null) {
                                    symbol_type = SymbolTableManager.getInstance().getReturnTypeOfScope(variable_value.callback_function_without_scol().variable_name().getText());
                                }
                                if (variable_value.select_stmt() != null) {
                                    SelectStmtClass select_statement = visitSelect_stmt(variable_value.select_stmt());
                                    FactoredSelectStmtClass factoredSelectStmt = select_statement.getFactoredSelectStmt();

                                }

                                function_type = symbol_type;
                            }

                        } else {
                            function_type = TypeEnum.Void.toString();
                        }
                        SymbolTableManager.getInstance().getFunction_names().put(function_name, function_type);
                    }

                }
            }
        }


        if (ctx.all_statement_starts() != null) {
            List<AllStatementsStartClass> all_statements_start_class = new ArrayList<>();
            for (int i = 0; i < ctx.all_statement_starts().size(); i++) {
                all_statements_start_class.add(visitAll_statement_starts(ctx.all_statement_starts(i)));
            }
            parserClass.setAllStatementClasses(all_statements_start_class);
        }
        return parserClass;
    }

    @Override
    public AllStatementsStartClass visitAll_statement_starts(RulesParser.All_statement_startsContext ctx) {
        System.out.println(">> Parse All Statement Starts");
        AllStatementsStartClass all_statement_start_class = new AllStatementsStartClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "All Statement Start Node"
        );


        if (ctx.java_stmt_list() != null) {
            all_statement_start_class.setJava_statements_list(visitJava_stmt_list(ctx.java_stmt_list()));
        }
        if (ctx.sql_stmt_list() != null) {
            all_statement_start_class.setSqlStmtListClass(visitSql_stmt_list(ctx.sql_stmt_list()));
        }
        return all_statement_start_class;
    }

    @Override
    public JavaStatementsListClass visitJava_stmt_list(RulesParser.Java_stmt_listContext ctx) {
        System.out.println(">> Parse Java Statement List");
        JavaStatementsListClass java_statements_list = new JavaStatementsListClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Java Statements List Node"
        );
        if (ctx.java_stmt() != null) {
            java_statements_list.setJava_statement_class(visitJava_stmt(ctx.java_stmt()));
        }
        if (ctx.prototype_function() != null) {
            java_statements_list.setPrototype_function(visitPrototype_function(ctx.prototype_function()));
        }
        // Enter code Here
        return java_statements_list;
    }

    @Override
    public CreateVariableStatementClass visitCreate_variable_stmt(RulesParser.Create_variable_stmtContext ctx) {
        System.out.println(">> Parse Create Variable Statement");
        CreateVariableStatementClass create_variable_statement = new CreateVariableStatementClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Create Variable Statement Node"
        );

        create_variable_statement.setAssign_variable_class(visitAssign_variable_in_creation(ctx.assign_variable_in_creation()));


        for (AssignArrayVariableClass assign_array_variable_class : create_variable_statement.getAssign_variable_class().getAssign_array_variable()) {
            // Add symbol
            String scope_id = SymbolTableManager.getInstance().getScopes_level().lastElement();
            String symbol_name = assign_array_variable_class.getAssign_variable().getVariable_name();
            MultiDeclarationVariableErrorClass multi_declaration_variable_error = new MultiDeclarationVariableErrorClass(
                    symbol_name,
                    Utils.ERROR_MESSAGE_MULTI_DECLARATION_VARIABLE,
                    String.valueOf(assign_array_variable_class.getAssign_variable().getRow()),
                    String.valueOf(assign_array_variable_class.getAssign_variable().getColumn())
            );
            multi_declaration_variable_error.checkingError();

            String symbol_type = null;
            boolean is_params = false;
            VariableValueClass variable_value = assign_array_variable_class.getAssign_variable().getVariable_value();
            if (variable_value != null)
                symbol_type = variable_value.getType_value();


            SymbolTableManager.getInstance().addSymbol(scope_id, symbol_name, symbol_type, is_params);
            create_variable_statement.setSymbol_id(new Pair<>(scope_id, symbol_name));
        }


        return create_variable_statement;
    }

    @Override
    public AssignVariableInCreationClass visitAssign_variable_in_creation(RulesParser.Assign_variable_in_creationContext ctx) {
        System.out.println(">> Parse Assign Variable In Creation Node");
        AssignVariableInCreationClass assign_variable_in_creation = new AssignVariableInCreationClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Assign Variable In Creation Node"
        );
        List<AssignArrayVariableClass> assign_array_variable_list = new ArrayList<>();
        if (ctx.assign_array_variable() != null) {
            for (int i = 0; i < ctx.assign_array_variable().size(); i++) {
                if (ctx.assign_array_variable(i) != null) {
                    assign_array_variable_list.add(visitAssign_array_variable(ctx.assign_array_variable(i)));
                }
            }
            assign_variable_in_creation.setAssign_array_variable(assign_array_variable_list);
        }
        return assign_variable_in_creation;
    }

    @Override
    public AssignVariableClass visitAssign_variable(RulesParser.Assign_variableContext ctx) {
        System.out.println(">> Parse Assign Variable Node");
        AssignVariableClass assign_variable = new AssignVariableClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Assign Variable Node"
        );
        assign_variable.setVariable_name(ctx.variable_name().getText());

        if (ctx.variable_value() != null) {
            assign_variable.setVariable_value(visitVariable_value(ctx.variable_value()));
        }

        return assign_variable;
    }

    @Override
    public AssignVariableWithoutVarClass visitAssign_variable_without_var(RulesParser.Assign_variable_without_varContext ctx) {
        System.out.println(">> Parse Assign Variable With Out Var");
        AssignVariableWithoutVarClass assign_variable_with_out_var = new AssignVariableWithoutVarClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Assign Variable With Out Var"
        );
        if (ctx.variable_name() != null) {
            assign_variable_with_out_var.setVariable_name(ctx.variable_name().getText());
        }
        if (ctx.var_name_item_in_array() != null) {
            assign_variable_with_out_var.setVariable_name(ctx.var_name_item_in_array().getText());
        }


        assign_variable_with_out_var.setVariable_value(visitVariable_value(ctx.variable_value()));

        // Add symbol
        String scope_id = SymbolTableManager.getInstance().getScopes_level().lastElement();
        String symbol_name = assign_variable_with_out_var.getVariable_name();
        String symbol_type = assign_variable_with_out_var.getVariable_value().getType_value();
        SymbolClass symbol_is_exist = SymbolTableManager.getInstance().getSymbolIsExist(symbol_name);
        if (symbol_is_exist != null) {
            boolean type_is_exist = SymbolTableManager.getInstance().checkTypeIsExist(symbol_is_exist);
            if (!type_is_exist) {
                SymbolTableManager.getInstance().updateTypeSymbol(scope_id, symbol_name, symbol_type);
            } else {
                AssignAnotherTypeErrorClass assign_another_type_error = new AssignAnotherTypeErrorClass(
                        symbol_is_exist.getSymbol_type(),
                        symbol_type,
                        Utils.ERROR_MESSAGE_ASSIGN_INCOMPATIBLE_TYPE,
                        String.valueOf(assign_variable_with_out_var.getRow()),
                        String.valueOf(assign_variable_with_out_var.getColumn())
                );
                assign_another_type_error.checkingError();

            }
        } else {
            System.err.println("Variable used but not declaration");
            UndeclaredVariableErrorClass undeclared_variable_error = new UndeclaredVariableErrorClass(
                    Utils.ERROR_MESSAGE_UNDECLARED_VARIABLE,
                    symbol_name,
                    String.valueOf(assign_variable_with_out_var.getRow()),
                    String.valueOf(assign_variable_with_out_var.getColumn())
            );
            undeclared_variable_error.checkingError();

        }

        return assign_variable_with_out_var;
    }

    @Override
    public PostSquareClass visitPost_square(RulesParser.Post_squareContext ctx) {
        System.out.println(">> Parse Post Square");
        PostSquareClass post_square = new PostSquareClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Post Square"
        );
        post_square.setVariable_name(ctx.variable_name().getText());
        return post_square;
    }

    @Override
    public PreviousSquareClass visitPrevious_square(RulesParser.Previous_squareContext ctx) {
        System.out.println(">> Parse Previous Square");
        PreviousSquareClass previous_square = new PreviousSquareClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Previous Square"
        );
        previous_square.setVariable_name(ctx.variable_name().getText());
        return previous_square;
    }

    @Override
    public AssignArrayClass visitAssign_array(RulesParser.Assign_arrayContext ctx) {
        System.out.println(">> Parse Assign Array");
        AssignArrayClass assign_array = new AssignArrayClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Assign Array"
        );
        if (ctx.post_square() != null) {
            assign_array.setPost_square(visitPost_square(ctx.post_square()));
        }
        if (ctx.previous_square() != null) {
            assign_array.setPrevious_square(visitPrevious_square(ctx.previous_square()));
        }
        assign_array.setArray_value(visitArray_value(ctx.array_value()));
        return assign_array;
    }

    @Override
    public AssignArrayVariableClass visitAssign_array_variable(RulesParser.Assign_array_variableContext ctx) {
        System.out.println(">> Parse Assign Array Variable");
        AssignArrayVariableClass assign_array_variable = new AssignArrayVariableClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Assign Array Variable"
        );
        if (ctx.assign_variable() != null) {
            assign_array_variable.setAssign_variable(visitAssign_variable(ctx.assign_variable()));
        }
        if (ctx.assign_array() != null) {
            assign_array_variable.setAssign_array(visitAssign_array(ctx.assign_array()));
        }
        return assign_array_variable;
    }

    @Override
    public DirectArrayValueClass visitDirect_array_values(RulesParser.Direct_array_valuesContext ctx) {
        System.out.println(">> Parse Direct Array Value");
        DirectArrayValueClass direct_array_value = new DirectArrayValueClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Direct Array Value"
        );
        if (ctx.variable_value() != null) {
            List<VariableValueClass> variables_value = new ArrayList<>();
            for (int i = 0; i < ctx.variable_value().size(); i++) {
                variables_value.add(visitVariable_value(ctx.variable_value().get(i)));
            }
            direct_array_value.setVariables_value(variables_value);
        }
        return direct_array_value;
    }

    @Override
    public ValueOfOneItemArrayClass visitValue_of_one_item_array(RulesParser.Value_of_one_item_arrayContext ctx) {
        System.out.println(">> Parse Value Of One Item Array");
        ValueOfOneItemArrayClass value_of_one_item = new ValueOfOneItemArrayClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Value Of One Item Array"
        );
        value_of_one_item.setVariable_value(visitVariable_value(ctx.variable_value()));
        return value_of_one_item;
    }

    @Override
    public ArrayValueClass visitArray_value(RulesParser.Array_valueContext ctx) {
        System.out.println("Parse Array Value");
        ArrayValueClass array_value = new ArrayValueClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Array Value"
        );
        if (ctx.number_value_java() != null) {
            array_value.setNumber_value(visitNumber_value_java(ctx.number_value_java()));
        }
        if (ctx.direct_array_values() != null) {
            array_value.setDirect_array_value(visitDirect_array_values(ctx.direct_array_values()));
        }

        if (ctx.callback_function_without_scol() != null) {
            array_value.setCallback_function_without_scol(visitCallback_function_without_scol(ctx.callback_function_without_scol()));
        }

        // Select Statement
        return array_value;
    }

    @Override
    public VariableValueClass visitVariable_value(RulesParser.Variable_valueContext ctx) {
        System.out.println(">> Parse Variable Value");
        VariableValueClass variable_value = new VariableValueClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Variable Value"
        );
        variable_value.setType_value(null);

        if (ctx.NUMERIC_LITERAL() != null) {
            variable_value.setNumber_literal(ctx.NUMERIC_LITERAL().getText());
            variable_value.setType_value(TypeEnum.Number.toString());
        }

        if (ctx.K_TRUE() != null || ctx.K_FALSE() != null) {
            variable_value.setTrue_false_null_value(ctx.getText());
            variable_value.setType_value(TypeEnum.Boolean.toString());

        }

        if (ctx.K_NULL() != null) {
            variable_value.setType_value(TypeEnum.Null.toString());

        }

        if (ctx.value_in_quote() != null) {
            variable_value.setValue_in_quote(ctx.value_in_quote().getText());
            variable_value.setType_value(TypeEnum.String.toString());

        }

        if (ctx.variable_name() != null) {
            variable_value.setVariable_name(ctx.variable_name().getText());
            variable_value.setType_value(SymbolTableManager.getInstance().getParentOfSymbol(ctx.variable_name().getText()));
            UndeclaredVariableErrorClass undeclared_variable_error = new UndeclaredVariableErrorClass(
                    Utils.ERROR_MESSAGE_UNDECLARED_VARIABLE,
                    variable_value.getVariable_name(),
                    String.valueOf(variable_value.getRow()),
                    String.valueOf(variable_value.getColumn()));

            if (undeclared_variable_error.checkingError()) {
                UnassignedVariableErrorClass unassigned_variable_error = new UnassignedVariableErrorClass(
                        variable_value.getVariable_name(),
                        Utils.ERROR_MESSAGE_UNASSIGNED__VARIABLE,
                        String.valueOf(variable_value.getRow()),
                        String.valueOf(variable_value.getColumn())
                );
                unassigned_variable_error.checkingError();

            }
        }
        // Deleted
        if (ctx.var_name_item_in_array() != null) {
            variable_value.setVariable_name_item_in_array(visitVar_name_item_in_array(ctx.var_name_item_in_array()));
        }

        if (ctx.callback_function_without_scol() != null) {
            variable_value.setCallback_function_without_scol(visitCallback_function_without_scol(ctx.callback_function_without_scol()));
            variable_value.setType_value(SymbolTableManager.getInstance().getReturnTypeOfScope(ctx.callback_function_without_scol().variable_name().getText()));
        }
        // Deleted
        if (ctx.array_value() != null) {
            variable_value.setArray_value(visitArray_value(ctx.array_value()));
        }
        if (ctx.expr_java() != null) {
            variable_value.setExpression_java(visitExpr_java(ctx.expr_java()));
            variable_value.setType_value(variable_value.getExpression_java().getTypeOfExpression(variable_value.getExpression_java()));
        }
        if (ctx.number_condition() != null) {
            variable_value.setNumber_condition(visitNumber_condition(ctx.number_condition()));
            variable_value.setType_value(TypeEnum.Boolean.toString());
        }
        if (ctx.logical_condition() != null) {
            variable_value.setLogical_condition(visitLogical_condition(ctx.logical_condition()));
            variable_value.setType_value(TypeEnum.Boolean.toString());
        }
        if (ctx.increment_type() != null) {
            variable_value.setIncrement_type(visitIncrement_type(ctx.increment_type()));
            variable_value.setType_value(TypeEnum.Number.toString());
        }
        if (ctx.line_condition__expression_java() != null) {
            variable_value.setLine_condition_expression(visitLine_condition__expression_java(ctx.line_condition__expression_java()));
            variable_value.setType_value(TypeEnum.Boolean.toString());
        }
        if (ctx.select_stmt() != null) {
            variable_value.setSelect_statement(visitSelect_stmt(ctx.select_stmt()));
            FactoredSelectStmtClass factoredSelectStmt = variable_value.getSelect_statement().getFactoredSelectStmt();
            //checking error
            UnexistColumnOfWhereStatErrorClass unexistColumnOfWhereStatErrorClass = new UnexistColumnOfWhereStatErrorClass(
                    Utils.ERROR_USING_UN_EXISTED_COLUMN_OF_TABLE,
                    Utils.ERROR_MESSAGE_COLUMNS_NOT_SAME_TYPE,
                    String.valueOf(factoredSelectStmt.getRow()),
                    String.valueOf(factoredSelectStmt.getColumn()),
                    factoredSelectStmt.getSelectCoreClass()
            );
            UnexistColumnOfTableErrorClass unexistColumnOfTableError = new UnexistColumnOfTableErrorClass(Utils.ERROR_USING_UN_EXISTED_COLUMN_OF_TABLE, String.valueOf(factoredSelectStmt.getRow()), String.valueOf(factoredSelectStmt.getColumn()), factoredSelectStmt);


            if (unexistColumnOfTableError.checkingError() && unexistColumnOfWhereStatErrorClass.checkingError()) {
                SqlTypeClass sql_type = SymbolTableManager.getInstance().flat(factoredSelectStmt, false);
                variable_value.setType_value(sql_type.getType_name());
            }
        }
        // Deleted
        if (ctx.json_declaration() != null) {
            variable_value.setJson_declaration(visitJson_declaration(ctx.json_declaration()));
        }
        // Deleted
        if (ctx.array_json_declaration() != null) {
            variable_value.setArray_json_declaration(visitArray_json_declaration(ctx.array_json_declaration()));
        }
        // Deleted
        if (ctx.access_to_data_in_json() != null) {
            variable_value.setAccess_json_data(visitAccess_to_data_in_json(ctx.access_to_data_in_json()));
        }
        return variable_value;
    }

    @Override
    public Object visitValue_in_quote(RulesParser.Value_in_quoteContext ctx) {
//        System.out.println("Parse Value In Quote");
//        ValueInQuote value_in_quote = new ValueInQuote(
//          ctx.getStart().getLine(),
//          ctx.getStart().getCharPositionInLine(),
//          "Value In Quote"
//        );
//        value_in_quote.setAny_value_in_quote(ctx.ANY_VALUE_IN_QUOTE().getText());
        return null;
    }

    @Override
    public DefaultValueParameterClass visitDefault_value_parameter(RulesParser.Default_value_parameterContext ctx) {
        System.out.println(">> Parse Default Value Parameter");
        DefaultValueParameterClass default_value_parameter = new DefaultValueParameterClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Default Value Parameter"
        );
        if (ctx.NUMERIC_LITERAL() != null) {
            default_value_parameter.setNumber_literal(ctx.NUMERIC_LITERAL().getText());
            default_value_parameter.setType_value(TypeEnum.Number.toString());
        }
        if (ctx.value_in_quote() != null) {
            default_value_parameter.setValue_in_quote(ctx.value_in_quote().getText());
            default_value_parameter.setType_value(TypeEnum.String.toString());
        }
        if (ctx.K_TRUE() != null) {
            default_value_parameter.setTrue_value(ctx.K_TRUE().getText());
            default_value_parameter.setType_value(TypeEnum.Boolean.toString());
        }
        if (ctx.K_FALSE() != null) {
            default_value_parameter.setFalse_value(ctx.K_FALSE().getText());
            default_value_parameter.setType_value(TypeEnum.Boolean.toString());
        }
        if (ctx.K_NULL() != null) {
            default_value_parameter.setNull_value(ctx.K_NULL().getText());
            default_value_parameter.setType_value(TypeEnum.Null.toString());
        }
//        if (ctx.expr_java() != null) {
//            default_value_parameter.setExpr_java(visitExpr_java(ctx.expr_java()));
//        }
        return default_value_parameter;
    }

    @Override
    public InitialVariableDeclarationClass visitInitial_variable_declaration(RulesParser.Initial_variable_declarationContext ctx) {
        System.out.println(">> Parse Initial Variable Declaration");
        InitialVariableDeclarationClass initial_variable_declaration = new InitialVariableDeclarationClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Initial Variable Declaration"
        );
        initial_variable_declaration.setVariable_name(ctx.variable_name().getText());

        return initial_variable_declaration;
    }

    @Override
    public InitialVariableDeclarationWithDefaultValueClass visitInitial_variable_declaration_with_default_value(RulesParser.Initial_variable_declaration_with_default_valueContext ctx) {
        System.out.println(">> Parse Initial Variable Declaration With Default Valuer");
        InitialVariableDeclarationWithDefaultValueClass initial_variable_declaration_with_default_value = new InitialVariableDeclarationWithDefaultValueClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Initial Variable Declaration With Default Valuer"
        );
        initial_variable_declaration_with_default_value.setInitial_variable_declaration(visitInitial_variable_declaration(ctx.initial_variable_declaration()));


        initial_variable_declaration_with_default_value.setDefault_value_parameter(visitDefault_value_parameter(ctx.default_value_parameter()));
        return initial_variable_declaration_with_default_value;
    }

    @Override
    public Object visitVariable_name(RulesParser.Variable_nameContext ctx) {
        return super.visitVariable_name(ctx);
    }

    @Override
    public VariableNameItemInArrayClass visitVar_name_item_in_array(RulesParser.Var_name_item_in_arrayContext ctx) {
        System.out.println(">> Parse Variable Name Item In Array");
        VariableNameItemInArrayClass variable_name_item_in_array = new VariableNameItemInArrayClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Variable Name Item In Array"
        );
        variable_name_item_in_array.setVariable_item_name(ctx.variable_name().getText());

        variable_name_item_in_array.setValue_of_one_item_array(visitValue_of_one_item_array(ctx.value_of_one_item_array()));
        return variable_name_item_in_array;
    }

    @Override
    public PrototypeFunctionClass visitPrototype_function(RulesParser.Prototype_functionContext ctx) {
        System.out.println(">> Parse Prototype Function");
        PrototypeFunctionClass prototype_function = new PrototypeFunctionClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Prototype Function"
        );
        prototype_function.setFunction_name(ctx.variable_name().getText());

        // Add Scope
        String scope_id = ctx.variable_name().getText();
        String parent_scope_id = null;
        ScopeClass scope = SymbolTableManager.getInstance().addScope(scope_id, parent_scope_id);
        SymbolTableManager.getInstance().getScopes_level().push(scope.getScope_id());
        prototype_function.setScope_id(scope_id);

        prototype_function.setPrototype_function_parameter(visitPrototype_function_parameter(ctx.prototype_function_parameter()));

        // Add symbol for non default parameters
        FunctionParameterInPrototypeClass function_parameter_in_prototype = prototype_function.getPrototype_function_parameter().getFunction_parameter_in_prototype();
        if (function_parameter_in_prototype != null) {
            List<InitialVariableDeclarationClass> initial_variable_declaration = function_parameter_in_prototype.getInitial_variables_declaration();
            for (InitialVariableDeclarationClass variable_item : initial_variable_declaration) {
                // Add Symbol
                String symbol_name = variable_item.getVariable_name();
                String symbol_type = null;
                boolean is_params = true;
                SymbolTableManager.getInstance().addSymbol(scope_id, symbol_name, symbol_type, is_params);
                function_parameter_in_prototype.setSymbol_id(new Pair<>(scope_id, symbol_name));
            }


            // Add symbol for default parameter
            FunctionParameterWithDefaultClass function_parameter_with_default = function_parameter_in_prototype.getFunction_parameter_with_default();
            if (function_parameter_with_default != null) {
                List<InitialVariableDeclarationWithDefaultValueClass> initial_variable_declaration_with_default_value = function_parameter_with_default.getInitial_variable_declaration_with_default_values();
                for (InitialVariableDeclarationWithDefaultValueClass variable_item : initial_variable_declaration_with_default_value) {
                    String symbol_name = variable_item.getInitial_variable_declaration().getVariable_name();
                    String symbol_type = variable_item.getDefault_value_parameter().getType_value();
                    boolean is_params = true;
                    SymbolTableManager.getInstance().addSymbol(scope_id, symbol_name, symbol_type, is_params);
                    function_parameter_with_default.setSymbol_id(new Pair<>(scope_id, symbol_name));
                }

            }
        }


        prototype_function.setBody_prototype_function(visitBody_prototype_function(ctx.body_prototype_function()));

        SymbolTableManager.getInstance().removeScopeFromScopeLevelStack();
        return prototype_function;
    }


    @Override
    public PrototypeFunctionParameterClass visitPrototype_function_parameter(RulesParser.Prototype_function_parameterContext ctx) {
        System.out.println(">> Parse Prototype Function Parameter");
        PrototypeFunctionParameterClass prototype_function_parameter = new PrototypeFunctionParameterClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Prototype Function Parameter"
        );
        if (ctx.function_parameter_in_prototype() != null) {
            prototype_function_parameter.setFunction_parameter_in_prototype(visitFunction_parameter_in_prototype(ctx.function_parameter_in_prototype()));
        }
        return prototype_function_parameter;
    }

    @Override
    public BodyPrototypeFunctionClass visitBody_prototype_function(RulesParser.Body_prototype_functionContext ctx) {
        System.out.println(">> Parse Body Prototype Function");
        BodyPrototypeFunctionClass body_prototype_function = new BodyPrototypeFunctionClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Body Prototype Function"
        );
        if (ctx.java_stmt() != null) {
            List<JavaStatementClass> java_statements = new ArrayList<>();
            for (int i = 0; i < ctx.java_stmt().size(); i++) {
                java_statements.add(visitJava_stmt(ctx.java_stmt().get(i)));
            }
            body_prototype_function.setJava_statements(java_statements);
        }
        if (ctx.return_stmt() != null) {
            body_prototype_function.setReturn_statement(visitReturn_stmt(ctx.return_stmt()));
        }
        return body_prototype_function;
    }

    @Override
    public FunctionParameterInPrototypeClass visitFunction_parameter_in_prototype(RulesParser.Function_parameter_in_prototypeContext ctx) {
        System.out.println(">> Parse Function Parameter In Prototype");
        FunctionParameterInPrototypeClass function_parameter_in_prototype = new FunctionParameterInPrototypeClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Function Parameter In Prototype"
        );
        if (ctx.initial_variable_declaration() != null) {
            List<InitialVariableDeclarationClass> initial_variables_declaration = new ArrayList<>();
            for (int i = 0; i < ctx.initial_variable_declaration().size(); i++) {
                initial_variables_declaration.add(visitInitial_variable_declaration(ctx.initial_variable_declaration().get(i)));
            }
            function_parameter_in_prototype.setInitial_variables_declaration(initial_variables_declaration);
        }
//        if (ctx.default_value_parameter() != null) {
//            function_parameter_in_prototype.setDefault_value_parameter(visitDefault_value_parameter(ctx.default_value_parameter()));
//        }
        if (ctx.function_parameter_with_default() != null) {
            function_parameter_in_prototype.setFunction_parameter_with_default(visitFunction_parameter_with_default(ctx.function_parameter_with_default()));
        }
        return function_parameter_in_prototype;
    }

    @Override
    public FunctionParameterWithDefaultClass visitFunction_parameter_with_default(RulesParser.Function_parameter_with_defaultContext ctx) {
        System.out.println(">> Parse Function Parameter With Default");
        FunctionParameterWithDefaultClass function_parameter_with_default = new FunctionParameterWithDefaultClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Function Parameter With Default"
        );
        List<InitialVariableDeclarationWithDefaultValueClass> initial_variables_declaration_with_default_value = new ArrayList<>();
        for (int i = 0; i < ctx.initial_variable_declaration_with_default_value().size(); i++) {
            initial_variables_declaration_with_default_value.add(visitInitial_variable_declaration_with_default_value(ctx.initial_variable_declaration_with_default_value().get(i)));
        }
        function_parameter_with_default.setInitial_variable_declaration_with_default_values(initial_variables_declaration_with_default_value);
        return function_parameter_with_default;
    }

    @Override
    public BodyFunctionClass visitBody_function(RulesParser.Body_functionContext ctx) {
        System.out.println(">> Parse Body Function");
        BodyFunctionClass body_function = new BodyFunctionClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Body Function"
        );
        if (ctx.java_stmt() != null) {
            List<JavaStatementClass> java_statements = new ArrayList<>();
            for (int i = 0; i < ctx.java_stmt().size(); i++) {
                java_statements.add(visitJava_stmt(ctx.java_stmt().get(i)));
            }
            body_function.setJava_statements(java_statements);
        }

        return body_function;
    }

    @Override
    public JavaStatementClass visitJava_stmt(RulesParser.Java_stmtContext ctx) {
        System.out.println(">> Parse Java Statement");
        JavaStatementClass java_statement = new JavaStatementClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Java Statement Node"
        );
        if (ctx.create_variable_stmt() != null) {
            java_statement.setCreate_variable_statement(visitCreate_variable_stmt(ctx.create_variable_stmt()));
        }
        if (ctx.assign_variable_without_var() != null) {
            java_statement.setAssign_variable_with_out_var(visitAssign_variable_without_var(ctx.assign_variable_without_var()));
        }
        if (ctx.if_statement() != null) {
            java_statement.setIf_statement(visitIf_statement(ctx.if_statement()));
        }
        if (ctx.for_stmt() != null) {
            java_statement.setFor_statement(visitFor_stmt(ctx.for_stmt()));
        }
        if (ctx.while_statement() != null) {
            java_statement.setWhile_statement(visitWhile_statement(ctx.while_statement()));
        }
        if (ctx.do_while_statement() != null) {
            java_statement.setDo_while_statement(visitDo_while_statement(ctx.do_while_statement()));
        }
        if (ctx.callback_function() != null) {
            java_statement.setCallback_function(visitCallback_function(ctx.callback_function()));
        }
        if (ctx.switch_statement() != null) {
            java_statement.setSwitch_statement(visitSwitch_statement(ctx.switch_statement()));
        }
        if (ctx.foreach_statement() != null) {
            java_statement.setForeach_statement(visitForeach_statement(ctx.foreach_statement()));
        }
        if (ctx.print_statement() != null) {
            java_statement.setPrint_statement(visitPrint_statement(ctx.print_statement()));
        }
        if (ctx.break_java() != null) {
            java_statement.setBreak_statement(visitBreak_java(ctx.break_java()));
        }
        if (ctx.continue_java() != null) {
            java_statement.setContinue_statement(visitContinue_java(ctx.continue_java()));
        }
        if (ctx.increment_stmt() != null) {
            java_statement.setIncrement_statement(visitIncrement_stmt(ctx.increment_stmt()));
        }
//        if (ctx.scope_function() != null) {
//            java_statement.setScope_statement(visitScope_function(ctx.scope_function()));
//        }
        // Enter Code Here
        return java_statement;
    }

//    @Override
//    public ScopeStatementClass visitScope_function(RulesParser.Scope_functionContext ctx) {
//        System.out.println(">> Parse Scope Statement");
//        ScopeStatementClass scope_statement = new ScopeStatementClass(
//                ctx.getStart().getLine(),
//                ctx.getStart().getCharPositionInLine(),
//                "Scope Statement"
//        );
//        if (ctx.java_stmt() != null) {
//            List<JavaStatementClass> java_statements = new ArrayList<>();
//            for (int i = 0; i < ctx.java_stmt().size(); i++) {
//                java_statements.add(visitJava_stmt(ctx.java_stmt().get(i)));
//            }
//            scope_statement.setJava_statements(java_statements);
//        }
//        return scope_statement;
//    }

    @Override
    public FullConditionClass visitFull_condition_java(RulesParser.Full_condition_javaContext ctx) {
        System.out.println(">> Parse Full Condition");
        FullConditionClass full_condition = new FullConditionClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Full Condition"
        );
        full_condition.setOne_condition_java(visitOne_condition_in_java(ctx.one_condition_in_java()));

        if (ctx.multi_condition_java() != null) {
            List<MultiConditionJavaClass> multi_condition_java = new ArrayList<>();
            for (int i = 0; i < ctx.multi_condition_java().size(); i++) {
                multi_condition_java.add(visitMulti_condition_java(ctx.multi_condition_java(i)));
            }
            full_condition.setMulti_condition_java(multi_condition_java);
        }
        if (ctx.full_condition_java() != null) {
            full_condition.setFull_condition(visitFull_condition_java(ctx.full_condition_java()));
        }
        return full_condition;
    }

    @Override
    public MultiConditionJavaClass visitMulti_condition_java(RulesParser.Multi_condition_javaContext ctx) {
        System.out.println(">> Parse Multi Condition Java");
        MultiConditionJavaClass multi_condition_java = new MultiConditionJavaClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Multi Condition Java"
        );
        multi_condition_java.setLogical_in_java(visitLogical_in_java(ctx.logical_in_java()));
        multi_condition_java.setOnd_condition_java(visitOne_condition_in_java(ctx.one_condition_in_java()));
        return multi_condition_java;
    }

    @Override
    public HeaderIfClass visitHeader_if(RulesParser.Header_ifContext ctx) {
        System.out.println(">> Parse Header If");
        HeaderIfClass header_if = new HeaderIfClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Header If"
        );
        header_if.setFull_condition(visitFull_condition_java(ctx.full_condition_java()));
        return header_if;
    }

    @Override
    public OneConditionJavaClass visitOne_condition_in_java(RulesParser.One_condition_in_javaContext ctx) {
        System.out.println(">> Parse One Condition");
        OneConditionJavaClass one_condition = new OneConditionJavaClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "One Condition"
        );

        if (ctx.not_var_name() != null) {
            one_condition.setNot_var_name(visitNot_var_name(ctx.not_var_name()));
        }
        if (ctx.not_one_condition() != null) {
            one_condition.setNot_one_condition(visitNot_one_condition(ctx.not_one_condition()));
        }
        if (ctx.K_TRUE() != null || ctx.K_FALSE() != null) {
            one_condition.setOperand(ctx.getText());
        }
        if (ctx.number_condition() != null) {
            one_condition.setNumber_condition(visitNumber_condition(ctx.number_condition()));
        }
        if (ctx.logical_condition() != null) {
            one_condition.setLogical_condition(visitLogical_condition(ctx.logical_condition()));
        }
        if (ctx.callback_function_without_scol() != null) {
            one_condition.setCallback_with_out_scol(visitCallback_function_without_scol(ctx.callback_function_without_scol()));
        }
        return one_condition;
    }

    @Override
    public NotVarNameClass visitNot_var_name(RulesParser.Not_var_nameContext ctx) {
        System.out.println(">> Parse Not Var Name");
        NotVarNameClass not_var_name = new NotVarNameClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Not Var Name"
        );
        if (ctx.NOT() != null) {
            not_var_name.setOperation(ctx.NOT().getText());
        }

        not_var_name.setOperand(ctx.variable_name().getText());
        UndeclaredVariableErrorClass undeclared_variable_java = new UndeclaredVariableErrorClass(
                Utils.ERROR_MESSAGE_UNDECLARED_VARIABLE,
                not_var_name.getOperand(),
                String.valueOf(not_var_name.getRow()),
                String.valueOf(not_var_name.getColumn()));

        if (undeclared_variable_java.checkingError()) {
            UnassignedVariableErrorClass unassigned_variable_error = new UnassignedVariableErrorClass(
                    not_var_name.getOperand(),
                    Utils.ERROR_MESSAGE_UNASSIGNED__VARIABLE,
                    String.valueOf(not_var_name.getRow()),
                    String.valueOf(not_var_name.getColumn())
            );
            unassigned_variable_error.checkingError();
        }

        return not_var_name;
    }

    @Override
    public NotOneConditionClass visitNot_one_condition(RulesParser.Not_one_conditionContext ctx) {
        System.out.println(">> Parse Not One Condition");
        NotOneConditionClass not_one_condition = new NotOneConditionClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Not One Condition"
        );
        if (ctx.NOT() != null) {
            not_one_condition.setOperation(ctx.NOT().getText());
        }

        not_one_condition.setOne_condition_java(visitOne_condition_in_java(ctx.one_condition_in_java()));
        return not_one_condition;
    }

    @Override
    public NumberConditionClass visitNumber_condition(RulesParser.Number_conditionContext ctx) {
        System.out.println(">> Parse Number Condition");
        NumberConditionClass number_condition = new NumberConditionClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Number Condition"
        );
        if (ctx.expr_java() != null) {
            List<ExpressionJacaClass> exprs_java = new ArrayList<>();
            for (int i = 0; i < ctx.expr_java().size(); i++) {
                exprs_java.add(visitExpr_java(ctx.expr_java(i)));

            }
            number_condition.setExprs_java(exprs_java);
        }
        number_condition.setComparison_in_java(visitComparison_in_java(ctx.comparison_in_java()));
        return number_condition;
    }

    @Override
    public LogicalConditionClass visitLogical_condition(RulesParser.Logical_conditionContext ctx) {
        System.out.println(">> Parse Logical Condition");
        LogicalConditionClass logical_condition = new LogicalConditionClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Logical Condition"
        );
        if (ctx.logic_operation_statement_in_java() != null) {
            List<LogicalOperationStatement> logical_operation_statement = new ArrayList<>();
            for (int i = 0; i < ctx.logic_operation_statement_in_java().size(); i++) {
                logical_operation_statement.add(visitLogic_operation_statement_in_java(ctx.logic_operation_statement_in_java(i)));
            }
            logical_condition.setLogical_operation(logical_operation_statement);
        }
        logical_condition.setLogical_in_java(visitLogical_in_java(ctx.logical_in_java()));
        return logical_condition;
    }

    @Override
    public ExpressionJacaClass visitExpr_java(RulesParser.Expr_javaContext ctx) {
        System.out.println(">> Parse Expr Java");
        ExpressionJacaClass expr_java = new ExpressionJacaClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Expr Java"
        );
        List<ExpressionJacaClass> exprs_java = new ArrayList<>();
        if (ctx.expr_java() != null) {
            for (int i = 0; i < ctx.expr_java().size(); i++) {
                exprs_java.add(visitExpr_java(ctx.expr_java(i)));
            }
            expr_java.setExpression_java(exprs_java);
        }
        if (ctx.operation_in_java() != null) {
            expr_java.setOperation_in_java(visitOperation_in_java(ctx.operation_in_java()));
        }
        if (ctx.number_value_java() != null) {
            expr_java.setNumber_value(visitNumber_value_java(ctx.number_value_java()));
        }
        return expr_java;
    }

    @Override
    public NumberValueJavaClass visitNumber_value_java(RulesParser.Number_value_javaContext ctx) {
        System.out.println(">> Parse Number Value Java");
        NumberValueJavaClass number_value_java = new NumberValueJavaClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Number Value Java"
        );
        if (ctx.variable_name() != null) {
            number_value_java.setVariable_name(ctx.variable_name().getText());
            UndeclaredVariableErrorClass undeclared_variable_java = new UndeclaredVariableErrorClass(
                    Utils.ERROR_MESSAGE_UNDECLARED_VARIABLE,
                    number_value_java.getVariable_name(),
                    String.valueOf(number_value_java.getRow()),
                    String.valueOf(number_value_java.getColumn()));
            if (undeclared_variable_java.checkingError()) {
                UnassignedVariableErrorClass unassigned_variable_error = new UnassignedVariableErrorClass(
                        number_value_java.getVariable_name(),
                        Utils.ERROR_MESSAGE_UNASSIGNED__VARIABLE,
                        String.valueOf(number_value_java.getRow()),
                        String.valueOf(number_value_java.getColumn())
                );
                unassigned_variable_error.checkingError();
            }
        }
        if (ctx.NUMERIC_LITERAL() != null) {
            number_value_java.setNumber_value(ctx.NUMERIC_LITERAL().getText());
        }
        if (ctx.variable_value() != null) {
            number_value_java.setVariable_value_in_quotes(ctx.getText());
        }
        if (ctx.var_name_item_in_array() != null) {
            number_value_java.setVar_name_item_in_array(visitVar_name_item_in_array(ctx.var_name_item_in_array()));
        }
        if (ctx.access_to_data_in_json() != null) {
            number_value_java.setAccess_json_data(visitAccess_to_data_in_json(ctx.access_to_data_in_json()));
        }
        if (ctx.callback_function_without_scol() != null) {
            number_value_java.setCallback_function_without_scol(visitCallback_function_without_scol(ctx.callback_function_without_scol()));
        }
        if (ctx.advanced_increase() != null) {
            number_value_java.setAdvanced_increase(visitAdvanced_increase(ctx.advanced_increase()));
        }
        if (ctx.delayed_increase() != null) {
            number_value_java.setDelayed_increase(visitDelayed_increase(ctx.delayed_increase()));
        }
        return number_value_java;
    }

    @Override
    public LogicalOperationStatement visitLogic_operation_statement_in_java(RulesParser.Logic_operation_statement_in_javaContext ctx) {
        System.out.println(">> Parse Logical Operation Statement");

        LogicalOperationStatement logical_operation_statement = new LogicalOperationStatement(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Logical Condition Operation"
        );

        if (ctx.logic_operation_statement_in_java() != null) {
            List<LogicalOperationStatement> logical_operation_statements = new ArrayList<>();
            for (int i = 0; i < ctx.logic_operation_statement_in_java().size(); i++) {
                logical_operation_statements.add(visitLogic_operation_statement_in_java(ctx.logic_operation_statement_in_java(i)));
            }
        }
        if (ctx.logical_in_java() != null) {
            logical_operation_statement.setLogical_in_java(visitLogical_in_java(ctx.logical_in_java()));
        }
        if (ctx.variable_name() != null) {
            logical_operation_statement.setVariable_name(ctx.variable_name().getText());
            UndeclaredVariableErrorClass undeclared_variable_error = new UndeclaredVariableErrorClass(
                    Utils.ERROR_MESSAGE_UNDECLARED_VARIABLE,
                    logical_operation_statement.getVariable_name(),
                    String.valueOf(logical_operation_statement.getRow()),
                    String.valueOf(logical_operation_statement.getColumn()));
            if (undeclared_variable_error.checkingError()) {
                UnassignedVariableErrorClass unassigned_variable_error = new UnassignedVariableErrorClass(
                        logical_operation_statement.getVariable_name(),
                        Utils.ERROR_MESSAGE_UNASSIGNED__VARIABLE,
                        String.valueOf(logical_operation_statement.getRow()),
                        String.valueOf(logical_operation_statement.getColumn())
                );
                unassigned_variable_error.checkingError();
            }
        }
        if (ctx.variable_value() != null) {
            logical_operation_statement.setVariable_value_in_quotes(ctx.getText());
        }
        return logical_operation_statement;
    }

    @Override
    public OperationInJava visitOperation_in_java(RulesParser.Operation_in_javaContext ctx) {
        System.out.println(">> Parse Operation In Java");
        OperationInJava operation_in_java = new OperationInJava(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Operation In Java"
        );
        operation_in_java.setOperation(ctx.getText());
        return operation_in_java;
    }

    @Override
    public ComparisonInJava visitComparison_in_java(RulesParser.Comparison_in_javaContext ctx) {
        System.out.println(">> Parse Comparison In Java");
        ComparisonInJava comparison_in_java = new ComparisonInJava(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Comparison In Java"
        );
        comparison_in_java.setOperation(ctx.getText());
        return comparison_in_java;
    }

    @Override
    public LogicalInJava visitLogical_in_java(RulesParser.Logical_in_javaContext ctx) {
        System.out.println(">> Parse Logical In Java");
        LogicalInJava logical_in_java = new LogicalInJava(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Logical In Java"
        );
        logical_in_java.setOperation(ctx.getText());

        return logical_in_java;
    }

    @Override
    public LineConditionExpressionJavaClass visitLine_condition__expression_java(RulesParser.Line_condition__expression_javaContext ctx) {
        System.out.println(">> Parse Line Condition Expression");
        LineConditionExpressionJavaClass line_condition_expression = new LineConditionExpressionJavaClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Line Condition Expression"
        );
        line_condition_expression.setFull_condition(visitFull_condition_java(ctx.full_condition_java()));
        if (ctx.line_condition_result() != null) {
            List<LineConditionResultClass> lines_condition_result = new ArrayList<>();
            for (int i = 0; i < ctx.line_condition_result().size(); i++) {
                lines_condition_result.add(visitLine_condition_result(ctx.line_condition_result().get(i)));
            }
            line_condition_expression.setLines_condition_result(lines_condition_result);
        }
        return line_condition_expression;
    }

    @Override
    public LineConditionResultClass visitLine_condition_result(RulesParser.Line_condition_resultContext ctx) {
        System.out.println(">> Parse Line Condition Result");
        LineConditionResultClass line_condition_result = new LineConditionResultClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Line Condition result"
        );
        if (ctx.variable_value() != null) {
            line_condition_result.setVariable_value(visitVariable_value(ctx.variable_value()));
        }
        if (ctx.line_condition__expression_java() != null) {
            line_condition_result.setLine_condition_expression(visitLine_condition__expression_java(ctx.line_condition__expression_java()));
        }
        return line_condition_result;
    }

    @Override
    public IfStatementClass visitIf_statement(RulesParser.If_statementContext ctx) {
        System.out.println(">> Parse If Statement");
        IfStatementClass if_statement = new IfStatementClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "If Statement"
        );
        if_statement.setHeader_if(visitHeader_if(ctx.header_if()));

        // Add scope
        String parent_scope_id = SymbolTableManager.getInstance().getScopes_level().lastElement();
        int number_of_scope = SymbolTableManager.getInstance().getRandom_number();
        String scope_id = ctx.K_IF().getText() + number_of_scope;
        ScopeClass scope = SymbolTableManager.getInstance().addScope(scope_id, parent_scope_id);
        SymbolTableManager.getInstance().getScopes_level().push(scope.getScope_id());
        if_statement.setScope_id(scope_id);

        if (ctx.java_stmt() != null) {
            if_statement.setJava_statement(visitJava_stmt(ctx.java_stmt()));
        }

        if (ctx.body_function() != null) {
            if_statement.setBody_function(visitBody_function(ctx.body_function()));
        }

        SymbolTableManager.getInstance().removeScopeFromScopeLevelStack();
        if (ctx.else_if_statement() != null) {
            List<ElseIfStatementClass> else_if_statements = new ArrayList<>();
            for (int i = 0; i < ctx.else_if_statement().size(); i++) {
                else_if_statements.add(visitElse_if_statement(ctx.else_if_statement(i)));
            }
            if_statement.setElse_if_statement(else_if_statements);
        }
        if (ctx.else_statement() != null) {
            if_statement.setElse_statement(visitElse_statement(ctx.else_statement()));
        }

        return if_statement;
    }

    @Override
    public ElseIfStatementClass visitElse_if_statement(RulesParser.Else_if_statementContext ctx) {
        System.out.println(">> Parse Else If Statement");
        ElseIfStatementClass else_if_statement = new ElseIfStatementClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Else If Statement"
        );
        else_if_statement.setHeader_if(visitHeader_if(ctx.header_if()));

        // Add scope
        String parent_scope_id = SymbolTableManager.getInstance().getScopes_level().lastElement();
        int number_of_scope = SymbolTableManager.getInstance().getRandom_number();
        String scope_id = ctx.K_ELSE_IF().getText() + number_of_scope;

        ScopeClass scope = SymbolTableManager.getInstance().addScope(scope_id, parent_scope_id);

        SymbolTableManager.getInstance().getScopes_level().push(scope.getScope_id());
        else_if_statement.setScope_id(scope_id);
        else_if_statement.setBody_function(visitBody_function(ctx.body_function()));

        //remove scope
        SymbolTableManager.getInstance().removeScopeFromScopeLevelStack();

        return else_if_statement;
    }

    @Override
    public ElseStatementClass visitElse_statement(RulesParser.Else_statementContext ctx) {
        System.out.println(">> Parse Else Statement");
        ElseStatementClass else_statement = new ElseStatementClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Else Statement"
        );

        // Add scope
        String parent_scope_id = SymbolTableManager.getInstance().getScopes_level().lastElement();
        int number_of_scope = SymbolTableManager.getInstance().getRandom_number();
        String scope_id = ctx.K_ELSE().getText() + number_of_scope;
        ScopeClass scope = SymbolTableManager.getInstance().addScope(scope_id, parent_scope_id);
        SymbolTableManager.getInstance().getScopes_level().push(scope.getScope_id());
        else_statement.setScope_id(scope_id);
        if (ctx.java_stmt() != null) {
            else_statement.setJava_statement(visitJava_stmt(ctx.java_stmt()));
        }
        if (ctx.body_function() != null) {
            else_statement.setBody_function(visitBody_function(ctx.body_function()));
        }

        //remove scope
        SymbolTableManager.getInstance().removeScopeFromScopeLevelStack();

        return else_statement;
    }

    @Override
    public SwitchStatementClass visitSwitch_statement(RulesParser.Switch_statementContext ctx) {
        System.out.println(">> Parse Switch Statement");
        SwitchStatementClass switch_statement = new SwitchStatementClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Switch Statement"
        );
        switch_statement.setVariable_value(visitVariable_value(ctx.variable_value()));

        if (ctx.case_statenemt() != null) {
            List<CaseStatementClass> case_statements = new ArrayList<>();
            for (int i = 0; i < ctx.case_statenemt().size(); i++) {
                case_statements.add(visitCase_statenemt(ctx.case_statenemt().get(i)));
            }
            switch_statement.setCase_statements(case_statements);
        }
        if (ctx.default_statement() != null) {
            switch_statement.setDefault_statement(visitDefault_statement(ctx.default_statement()));
        }
        return switch_statement;
    }

    @Override
    public CaseStatementClass visitCase_statenemt(RulesParser.Case_statenemtContext ctx) {
        System.out.println(">> Parse Case Statement");
        CaseStatementClass case_statement = new CaseStatementClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Case Statement"
        );
        // Add scope
        String parent_scope_id = SymbolTableManager.getInstance().getScopes_level().lastElement();
        int number_of_scope = SymbolTableManager.getInstance().getRandom_number();
        String scope_id = ctx.K_CASE().getText() + number_of_scope;

        ScopeClass scope = SymbolTableManager.getInstance().addScope(scope_id, parent_scope_id);
        SymbolTableManager.getInstance().getScopes_level().push(scope.getScope_id());

        case_statement.setCase_value(visitCase_value(ctx.case_value()));

        case_statement.setBody_for_case_and_default(visitBody_for_case_and_default(ctx.body_for_case_and_default()));

        //remove scope
        SymbolTableManager.getInstance().removeScopeFromScopeLevelStack();

        return case_statement;
    }

    @Override
    public CaseValueStatement visitCase_value(RulesParser.Case_valueContext ctx) {
        CaseValueStatement case_value = new CaseValueStatement(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Case Value Statement"
        );
        case_value.setCase_value(ctx.getText());
        return case_value;
    }


    @Override
    public DefaultStatementClass visitDefault_statement(RulesParser.Default_statementContext ctx) {
        System.out.println(">> Parse Default Statement");
        DefaultStatementClass default_statement = new DefaultStatementClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Default Statement"
        );
        // Add scope
        String parent_scope_id = SymbolTableManager.getInstance().getScopes_level().lastElement();
        String scope_id = ctx.K_DEFAULT().getText();

        ScopeClass scope = SymbolTableManager.getInstance().addScope(scope_id, parent_scope_id);
        SymbolTableManager.getInstance().getScopes_level().push(scope.getScope_id());
        default_statement.setScope_id(scope_id);
        default_statement.setBody_for_case_and_default(visitBody_for_case_and_default(ctx.body_for_case_and_default()));

        //remove scope
        SymbolTableManager.getInstance().removeScopeFromScopeLevelStack();

        return default_statement;
    }

    @Override
    public BodyForCaseAndDefaultClass visitBody_for_case_and_default(RulesParser.Body_for_case_and_defaultContext ctx) {
        System.out.println(">> Parse Body For Case And Default");
        BodyForCaseAndDefaultClass body_for_case_and_default = new BodyForCaseAndDefaultClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Body For Case and Default"
        );

        if (ctx.java_stmt() != null) {
            List<JavaStatementClass> java_statements = new ArrayList<>();
            for (int i = 0; i < ctx.java_stmt().size(); i++) {
                java_statements.add(visitJava_stmt(ctx.java_stmt().get(i)));
            }
            body_for_case_and_default.setJava_statements(java_statements);
        }
        if (ctx.body_function() != null) {
            body_for_case_and_default.setBody_function(visitBody_function(ctx.body_function()));
        }
        return body_for_case_and_default;
    }


    @Override
    public ForStatementClass visitFor_stmt(RulesParser.For_stmtContext ctx) {
        System.out.println(">> Parse For Statement");
        ForStatementClass for_statement = new ForStatementClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "For Statement"
        );
        // Add scope
        String parent_scope_id = SymbolTableManager.getInstance().getScopes_level().lastElement();
        int number_of_scope = SymbolTableManager.getInstance().getRandom_number();
        String scope_id = ctx.K_FOR().getText() + number_of_scope;

        ScopeClass scope = SymbolTableManager.getInstance().addScope(scope_id, parent_scope_id);
        SymbolTableManager.getInstance().getScopes_level().push(scope.getScope_id());
        for_statement.setScope_id(scope_id);

        for_statement.setFor_parameter(visitFor_parameter(ctx.for_parameter()));

        if (ctx.java_stmt() != null) {
            for_statement.setJava_statement(visitJava_stmt(ctx.java_stmt()));
        }
        if (ctx.body_function() != null) {
            for_statement.setBody_function(visitBody_function(ctx.body_function()));
        }
        //remove scope
        SymbolTableManager.getInstance().removeScopeFromScopeLevelStack();

        return for_statement;
    }

    @Override
    public ForParameterClass visitFor_parameter(RulesParser.For_parameterContext ctx) {
        System.out.println(">> Parse For Parameter");
        ForParameterClass for_parameter = new ForParameterClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "For Parameter"
        );
        for_parameter.setInitial_start_count_in_for(visitInitial_start_count_in_for(ctx.initial_start_count_in_for()));

        for_parameter.setCondition_for(visitCondition_for(ctx.condition_for()));

        for_parameter.setIncrement_count_for(visitIncrement_count_for(ctx.increment_count_for()));
        return for_parameter;
    }

    @Override
    public ConditionForClass visitCondition_for(RulesParser.Condition_forContext ctx) {
        System.out.println(">> Parse Condition for");
        ConditionForClass condition_for = new ConditionForClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Condition For"
        );
        if (ctx.full_condition_java() != null) {
            List<FullConditionClass> full_conditions = new ArrayList<>();
            for (int i = 0; i < ctx.full_condition_java().size(); i++) {
                full_conditions.add(visitFull_condition_java(ctx.full_condition_java().get(i)));
            }
            condition_for.setFull_conditions(full_conditions);
        }
        return condition_for;
    }

    @Override
    public InitialStartCountInForClass visitInitial_start_count_in_for(RulesParser.Initial_start_count_in_forContext ctx) {
        System.out.println(">> Parse Initial Start Count In For");
        InitialStartCountInForClass initial_start_count_in_for = new InitialStartCountInForClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Initial Start Count In For"
        );
        initial_start_count_in_for.setInitial_variable_declaration(visitInitial_variable_declaration(ctx.initial_variable_declaration()));
        if (ctx.number_value_java() != null) {
            initial_start_count_in_for.setNumber_value(visitNumber_value_java(ctx.number_value_java()));
        }
        // Add symbol
        String scope_id = SymbolTableManager.getInstance().getScopes_level().lastElement();
        String symbol_name = initial_start_count_in_for.getInitial_variable_declaration().getVariable_name();
        String symbol_type = null;
        boolean is_params = false;
        if (initial_start_count_in_for.getNumber_value() != null) {
            symbol_type = TypeEnum.Number.toString();
        }
        SymbolTableManager.getInstance().addSymbol(scope_id, symbol_name, symbol_type, is_params);
        initial_start_count_in_for.setSymbol_id(new Pair<>(scope_id, symbol_name));

        List<InitialStartCountInForClass> initial_start_count_in_for_list = new ArrayList<>();
        if (ctx.initial_start_count_in_for() != null) {

            for (int i = 0; i < ctx.initial_start_count_in_for().size(); i++) {
                initial_start_count_in_for_list.add(visitInitial_start_count_in_for(ctx.initial_start_count_in_for().get(i)));
            }
            initial_start_count_in_for.setInitial_start_counts_in_for(initial_start_count_in_for_list);
        }

        return initial_start_count_in_for;
    }

    @Override
    public AdvancedIncreaseClass visitAdvanced_increase(RulesParser.Advanced_increaseContext ctx) {
        System.out.println(">> Parse Advanced Increase");
        AdvancedIncreaseClass advanced_increase = new AdvancedIncreaseClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Advanced Increase"
        );
        String operation = "";
        if (ctx.DOUBLEPLUS() != null) {
            operation = ctx.DOUBLEPLUS().getText();
        }
        if (ctx.DOUBLEMINUS() != null) {
            operation = ctx.DOUBLEMINUS().getText();
        }
        advanced_increase.setAdvanced_increase(new ContentIncreaseOperation(operation, ctx.variable_name().getText()));
        UndeclaredVariableErrorClass undeclared_variable_error = new UndeclaredVariableErrorClass(
                Utils.ERROR_MESSAGE_UNDECLARED_VARIABLE,
                advanced_increase.getAdvanced_increase().getVariable_name(),
                String.valueOf(advanced_increase.getRow()),
                String.valueOf(advanced_increase.getColumn()));
        if (undeclared_variable_error.checkingError()) {
            UnassignedVariableErrorClass unassigned_variable_error = new UnassignedVariableErrorClass(
                    advanced_increase.getAdvanced_increase().getVariable_name(),
                    Utils.ERROR_MESSAGE_UNASSIGNED__VARIABLE,
                    String.valueOf(advanced_increase.getRow()),
                    String.valueOf(advanced_increase.getColumn())
            );
            unassigned_variable_error.checkingError();
        }
        return advanced_increase;
    }

    @Override
    public DelayedIncreaseClass visitDelayed_increase(RulesParser.Delayed_increaseContext ctx) {
        System.out.println(">> Parse Delayed Increase");
        DelayedIncreaseClass delayed_increase = new DelayedIncreaseClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Delayed Increase"
        );
        String operation = "";
        if (ctx.DOUBLEPLUS() != null) {
            operation = ctx.DOUBLEPLUS().getText();
        }
        if (ctx.DOUBLEMINUS() != null) {
            operation = ctx.DOUBLEMINUS().getText();
        }
        delayed_increase.setDelayed_operation(new ContentIncreaseOperation(operation, ctx.variable_name().getText()));
        UndeclaredVariableErrorClass undeclared_variable_error = new UndeclaredVariableErrorClass(
                Utils.ERROR_MESSAGE_UNDECLARED_VARIABLE,
                delayed_increase.getDelayed_operation().getVariable_name(),
                String.valueOf(delayed_increase.getRow()),
                String.valueOf(delayed_increase.getColumn()));
        if (undeclared_variable_error.checkingError()) {
            UnassignedVariableErrorClass unassigned_variable_error = new UnassignedVariableErrorClass(
                    delayed_increase.getDelayed_operation().getVariable_name(),
                    Utils.ERROR_MESSAGE_UNASSIGNED__VARIABLE,
                    String.valueOf(delayed_increase.getRow()),
                    String.valueOf(delayed_increase.getColumn())
            );
            unassigned_variable_error.checkingError();
        }
        return delayed_increase;
    }

    @Override
    public SpecificIncreaseClass visitSpecific_increase(RulesParser.Specific_increaseContext ctx) {
        System.out.println(">> Parse Spesific Increase");
        SpecificIncreaseClass specific_increase = new SpecificIncreaseClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Specific Increase"
        );
        String operation = "";
        if (ctx.PLUS() != null) {
            operation = ctx.PLUS().getText() + ctx.ASSIGN().getText();
        }
        if (ctx.MINUS() != null) {
            operation = ctx.MINUS().getText() + ctx.ASSIGN().getText();
        }
        specific_increase.setOperation(new ContentIncreaseOperation(operation, ctx.variable_name().getText()));

        specific_increase.setExpr_java(visitExpr_java(ctx.expr_java()));
        return specific_increase;
    }

    @Override
    public IncrementTypeClass visitIncrement_type(RulesParser.Increment_typeContext ctx) {
        System.out.println(">> Parse Increment Type");
        IncrementTypeClass increment_type = new IncrementTypeClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Increment Type"
        );
        if (ctx.advanced_increase() != null) {
            increment_type.setAdvanced_increment(visitAdvanced_increase(ctx.advanced_increase()));
        }
        if (ctx.delayed_increase() != null) {
            increment_type.setDelayed_increment(visitDelayed_increase(ctx.delayed_increase()));
        }
        if (ctx.specific_increase() != null) {
            increment_type.setSpecific_increment(visitSpecific_increase(ctx.specific_increase()));
        }
        return increment_type;
    }

    @Override
    public IncrementCountForClass visitIncrement_count_for(RulesParser.Increment_count_forContext ctx) {
        System.out.println(">> Parse Increment Count For");
        IncrementCountForClass increment_count_for = new IncrementCountForClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Increment Count For"
        );
        if (ctx.increment_type() != null) {
            List<IncrementTypeClass> increment_types = new ArrayList<>();
            for (int i = 0; i < ctx.increment_type().size(); i++) {
                increment_types.add(visitIncrement_type(ctx.increment_type().get(i)));
            }
            increment_count_for.setIncrements_type(increment_types);
        }
        return increment_count_for;
    }

    @Override
    public IncrementStatementClass visitIncrement_stmt(RulesParser.Increment_stmtContext ctx) {
        IncrementStatementClass increment_statement = new IncrementStatementClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Increment Statement"
        );
        increment_statement.setIncrement_type(visitIncrement_type(ctx.increment_type()));
        return increment_statement;
    }

    @Override
    public WhileStatementClass visitWhile_statement(RulesParser.While_statementContext ctx) {
        System.out.println(">> Parse While Statement");
        WhileStatementClass while_statement = new WhileStatementClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "While Statement"
        );
        // Add scope
        String parent_scope_id = SymbolTableManager.getInstance().getScopes_level().lastElement();
        int number_of_scope = SymbolTableManager.getInstance().getRandom_number();
        String scope_id = ctx.K_WHILE().getText() + number_of_scope;

        ScopeClass scope = SymbolTableManager.getInstance().addScope(scope_id, parent_scope_id);
        SymbolTableManager.getInstance().getScopes_level().push(scope.getScope_id());
        while_statement.setScope_id(scope_id);

        while_statement.setFullConditionClass(visitFull_condition_java(ctx.full_condition_java()));
        if (ctx.java_stmt() != null) {
            while_statement.setJava_statement(visitJava_stmt(ctx.java_stmt()));
        }
        if (ctx.body_function() != null) {
            while_statement.setBody_function(visitBody_function(ctx.body_function()));
        }

        //remove scope
        SymbolTableManager.getInstance().removeScopeFromScopeLevelStack();

        return while_statement;
    }

    @Override
    public DoWhileStatementClass visitDo_while_statement(RulesParser.Do_while_statementContext ctx) {
        System.out.println(">> Parse Do While Statement");
        DoWhileStatementClass do_while_statement = new DoWhileStatementClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Do While Statement"
        );
        // Add scope
        String parent_scope_id = SymbolTableManager.getInstance().getScopes_level().lastElement();
        int number_of_scope = SymbolTableManager.getInstance().getRandom_number();
        String scope_id = ctx.K_DO().getText() + number_of_scope;

        ScopeClass scope = SymbolTableManager.getInstance().addScope(scope_id, parent_scope_id);
        SymbolTableManager.getInstance().getScopes_level().push(scope.getScope_id());
        do_while_statement.setScope_id(scope_id);

        if (ctx.java_stmt() != null) {
            do_while_statement.setJava_statement(visitJava_stmt(ctx.java_stmt()));
        }
        if (ctx.body_function() != null) {
            do_while_statement.setBody_function(visitBody_function(ctx.body_function()));
        }
        do_while_statement.setFull_condition(visitFull_condition_java(ctx.full_condition_java()));

        //remove scope
        SymbolTableManager.getInstance().removeScopeFromScopeLevelStack();

        return do_while_statement;
    }

    @Override
    public CallbackFunctionClass visitCallback_function(RulesParser.Callback_functionContext ctx) {
        System.out.println(">> Parse Callback Function");
        CallbackFunctionClass callback_function = new CallbackFunctionClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Callback Function"
        );
        callback_function.setCallback_function_without_scol(visitCallback_function_without_scol(ctx.callback_function_without_scol()));
        return callback_function;
    }

    @Override
    public CallbackFunctionWithOutScolClass visitCallback_function_without_scol(RulesParser.Callback_function_without_scolContext ctx) {
        System.out.println(">> Parse Callback Function Without Scol");
        CallbackFunctionWithOutScolClass callback_function_without_scol = new CallbackFunctionWithOutScolClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Callback Function Without Scol"
        );
        callback_function_without_scol.setFunction_name(ctx.variable_name().getText());

        UndeclaredFunctionErrorClass undeclared_function_error = new UndeclaredFunctionErrorClass(
                callback_function_without_scol.getFunction_name(),
                Utils.ERROR_MESSAGE_UNDECLARED_FUNCTION,
                String.valueOf(callback_function_without_scol.getRow()),
                String.valueOf(callback_function_without_scol.getColumn())
        );
        undeclared_function_error.checkingError();

        callback_function_without_scol.setCallback_function_header(visitCallback_function_heder(ctx.callback_function_heder()));
        return callback_function_without_scol;
    }

    @Override
    public CallbackFunctionHeaderClass visitCallback_function_heder(RulesParser.Callback_function_hederContext ctx) {
        System.out.println(">> Parse Callback Function Header Class");
        CallbackFunctionHeaderClass callback_function_header = new CallbackFunctionHeaderClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Callback Function Header Class"
        );
        if (ctx.callback_function_parameter_in_header() != null) {
            List<CallbackFunctionParameterInHeaderClass> callback_function_parameters_in_header = new ArrayList<>();
            for (int i = 0; i < ctx.callback_function_parameter_in_header().size(); i++) {
                callback_function_parameters_in_header.add(visitCallback_function_parameter_in_header(ctx.callback_function_parameter_in_header().get(i)));
            }
            callback_function_header.setCallback_function_parameter_in_header(callback_function_parameters_in_header);
        }
        return callback_function_header;
    }

    @Override
    public CallbackFunctionParameterClass visitCallback_function_parameter(RulesParser.Callback_function_parameterContext ctx) {
        System.out.println(">> Parse Callback Function Parameter");
        CallbackFunctionParameterClass callback_function_parameter = new CallbackFunctionParameterClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Callback Function Parameter"
        );

        if (ctx.variable_name() != null) {
            callback_function_parameter.setPrimitive_parameter(ctx.variable_name().getText());
            UndeclaredVariableErrorClass undeclared_variable_error = new UndeclaredVariableErrorClass(
                    Utils.ERROR_MESSAGE_UNDECLARED_VARIABLE,
                    callback_function_parameter.getPrimitive_parameter(),
                    String.valueOf(callback_function_parameter.getRow()),
                    String.valueOf(callback_function_parameter.getColumn()));
            if (undeclared_variable_error.checkingError()) {
                UnassignedVariableErrorClass unassigned_variable_error = new UnassignedVariableErrorClass(
                        callback_function_parameter.getPrimitive_parameter(),
                        Utils.ERROR_MESSAGE_UNASSIGNED__VARIABLE,
                        String.valueOf(callback_function_parameter.getRow()),
                        String.valueOf(callback_function_parameter.getColumn())
                );
                unassigned_variable_error.checkingError();
            }
        }
        if (ctx.NUMERIC_LITERAL() != null) {
            callback_function_parameter.setPrimitive_parameter(ctx.NUMERIC_LITERAL().getText());
        }
        if (ctx.function_parameter() != null) {
            callback_function_parameter.setFunction_parameter(visitFunction_parameter(ctx.function_parameter()));
        }
        if (ctx.callback_function_without_scol() != null) {
            callback_function_parameter.setCallback_function_without_scol(visitCallback_function_without_scol(ctx.callback_function_without_scol()));
        }
        if (ctx.expr_java() != null) {
            callback_function_parameter.setExpr_java(visitExpr_java(ctx.expr_java()));
        }
        return callback_function_parameter;
    }

    @Override
    public CallbackFunctionParameterInHeaderClass visitCallback_function_parameter_in_header(RulesParser.Callback_function_parameter_in_headerContext ctx) {
        System.out.println(">> Parse Callback Function Parameter In Header");
        CallbackFunctionParameterInHeaderClass callback_function_parameter_in_header = new CallbackFunctionParameterInHeaderClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Callback Function Parameter In Header"
        );
        if (ctx.callback_function_parameter() != null) {
            List<CallbackFunctionParameterClass> callback_function_parameters = new ArrayList<>();
            for (int i = 0; i < ctx.callback_function_parameter().size(); i++) {
                callback_function_parameters.add(visitCallback_function_parameter(ctx.callback_function_parameter().get(i)));
            }
            callback_function_parameter_in_header.setCallback_function_parameter(callback_function_parameters);
        }
        return callback_function_parameter_in_header;
    }

    @Override
    public FunctionParameterClass visitFunction_parameter(RulesParser.Function_parameterContext ctx) {
        System.out.println(">> Parse Function Parameter Class");
        FunctionParameterClass function_parameter = new FunctionParameterClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Function Parameter Class"
        );
        function_parameter.setVar_name_in_parameter(visitVar_name_in_parameter(ctx.var_name_in_parameter()));

        // Add Scope
        String parent_scope_id = SymbolTableManager.getInstance().getScopes_level().lastElement();
        int number_of_scope = SymbolTableManager.getInstance().getRandom_number();
        String scope_id = ctx.K_FUNCTION().getText() + number_of_scope;
        SymbolTableManager.getInstance().addScope(scope_id, parent_scope_id);
        SymbolTableManager.getInstance().getScopes_level().push(scope_id);
        function_parameter.setScope_id(scope_id);

        function_parameter.setBody_function(visitBody_function(ctx.body_function()));

        SymbolTableManager.getInstance().removeScopeFromScopeLevelStack();
        return function_parameter;
    }

    @Override
    public VarNameInParameterClass visitVar_name_in_parameter(RulesParser.Var_name_in_parameterContext ctx) {
        System.out.println(">> Parse Var Name In Parameter");
        VarNameInParameterClass var_name_in_parameter = new VarNameInParameterClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Var Name In Parameter"
        );
        if (ctx.variable_name() != null) {
            List<String> var_names_in_parameter = new ArrayList<>();
            for (int i = 0; i < ctx.variable_name().size(); i++) {
                var_names_in_parameter.add(ctx.variable_name().get(i).getText());
            }
            var_name_in_parameter.setVariable_parameters(var_names_in_parameter);
        }
        return var_name_in_parameter;
    }

    @Override
    public ReturnStatementClass visitReturn_stmt(RulesParser.Return_stmtContext ctx) {
        System.out.println(">> Parse return Statement");
        ReturnStatementClass return_statement = new ReturnStatementClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Return Statement"
        );
        String return_type = TypeEnum.Void.toString();
        if (ctx.variable_value() != null) {
            return_statement.setVariable_value(visitVariable_value(ctx.variable_value()));
            return_type = return_statement.getVariable_value().getType_value();
        }
        String scope_id = SymbolTableManager.getInstance().getScopes_level().lastElement();
        if (return_type == null) {
            return_type = TypeEnum.Void.toString();
        }
        SymbolTableManager.getInstance().getSymbol_table().getScopes().get(scope_id).setReturn_type(return_type);
        return return_statement;
    }

    @Override
    public BreakStatementClass visitBreak_java(RulesParser.Break_javaContext ctx) {
        System.out.println(">> Parse Break Statement");
        return new BreakStatementClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Break Statement"
        );
    }

    @Override
    public ContinueStatementClass visitContinue_java(RulesParser.Continue_javaContext ctx) {
        System.out.println(">> Parse Continue Statement");
        return new ContinueStatementClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Continue Statement"
        );
    }

    @Override
    public JsonDeclarationClass visitJson_declaration(RulesParser.Json_declarationContext ctx) {
        System.out.println(">> Parse Json Declaration");
        JsonDeclarationClass json_declaration = new JsonDeclarationClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Json Declaration"
        );
        json_declaration.setAll_filed_in_json(visitAll_fileds_in_json(ctx.all_fileds_in_json()));
        return json_declaration;
    }

    @Override
    public AccessJsonDataClass visitAccess_to_data_in_json(RulesParser.Access_to_data_in_jsonContext ctx) {
        System.out.println(">> Parse Access To Data In Json");
        AccessJsonDataClass access_json_data = new AccessJsonDataClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Access To Data In Json"
        );
        access_json_data.setVariable_name(ctx.variable_name().getText());
        access_json_data.setVariable_value(visitVariable_value(ctx.variable_value()));
        return access_json_data;
    }

    @Override
    public AllFiledsInJsonClass visitAll_fileds_in_json(RulesParser.All_fileds_in_jsonContext ctx) {
        System.out.println(">> Parse All Filed's In Json");
        AllFiledsInJsonClass all_fileds_in_json = new AllFiledsInJsonClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "All Filed's In Json"
        );
        if (ctx.filed_in_json() != null) {
            List<FiledInJsonClass> fileds_in_json = new ArrayList<>();
            for (int i = 0; i < ctx.filed_in_json().size(); i++) {
                fileds_in_json.add(visitFiled_in_json(ctx.filed_in_json().get(i)));
            }
            all_fileds_in_json.setFileds_in_json(fileds_in_json);
        }
        return all_fileds_in_json;
    }

    @Override
    public FiledInJsonClass visitFiled_in_json(RulesParser.Filed_in_jsonContext ctx) {
        System.out.println(">> Parse Filed In Json");
        FiledInJsonClass filed_in_json = new FiledInJsonClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Filed In Json"
        );
        filed_in_json.setKey_filed(visitKey_filed(ctx.key_filed()));

        filed_in_json.setValue_filed(visitVariable_value(ctx.variable_value()));
        return filed_in_json;
    }

    @Override
    public KeyFiledClass visitKey_filed(RulesParser.Key_filedContext ctx) {
        System.out.println(">> Parse Key Filed");
        KeyFiledClass key_filed = new KeyFiledClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Key Filed"
        );
        if (ctx.variable_name() != null) {
            key_filed.setKey_name(ctx.variable_name().getText());
        }
        return key_filed;
    }

    @Override
    public ArrayJsonDeclarationClass visitArray_json_declaration(RulesParser.Array_json_declarationContext ctx) {
        System.out.println(">> Parse Array Json Declaration");
        ArrayJsonDeclarationClass array_json_declaration = new ArrayJsonDeclarationClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Array Json Declaration"
        );
        if (ctx.json_declaration() != null) {
            List<JsonDeclarationClass> jsons_declaration = new ArrayList<>();
            for (int i = 0; i < ctx.json_declaration().size(); i++) {
                jsons_declaration.add(visitJson_declaration(ctx.json_declaration().get(i)));
            }
            array_json_declaration.setJsons_declaration(jsons_declaration);
        }
        return array_json_declaration;
    }

    @Override
    public ForeachStatementClass visitForeach_statement(RulesParser.Foreach_statementContext ctx) {
        System.out.println(">> Parse For Each Statement");
        ForeachStatementClass foreach_statement = new ForeachStatementClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Foreach statement"
        );
        // Add scope
        String parent_scope_id = SymbolTableManager.getInstance().getScopes_level().lastElement();
        int number_of_scope = SymbolTableManager.getInstance().getRandom_number();
        String scope_id = ctx.K_FOREACH().getText() + number_of_scope;

        ScopeClass scope = SymbolTableManager.getInstance().addScope(scope_id, parent_scope_id);
        SymbolTableManager.getInstance().getScopes_level().push(scope.getScope_id());
        foreach_statement.setScope_id(scope_id);

        if (ctx.variable_in_foreach() != null) {
            List<VariableInForeachClass> variables_in_foreach = new ArrayList<>();
            for (int i = 0; i < ctx.variable_in_foreach().size(); i++) {
                variables_in_foreach.add(visitVariable_in_foreach(ctx.variable_in_foreach().get(i)));
            }
            foreach_statement.setVariables_in_foreach(variables_in_foreach);
        }
        if (ctx.java_stmt() != null) {
            foreach_statement.setJava_statement(visitJava_stmt(ctx.java_stmt()));
        }
        if (ctx.body_function() != null) {
            foreach_statement.setBody_function(visitBody_function(ctx.body_function()));
        }
        //remove scope
        SymbolTableManager.getInstance().removeScopeFromScopeLevelStack();

        return foreach_statement;
    }

    @Override
    public VariableInForeachClass visitVariable_in_foreach(RulesParser.Variable_in_foreachContext ctx) {
        System.out.println(">> Parse Variable In Foreach");
        VariableInForeachClass variable_in_foreach = new VariableInForeachClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Variable In Foreach"
        );
        if (ctx.variable_name() != null) {
            variable_in_foreach.setVariable_name(ctx.variable_name().getText());
        }
        if (ctx.var_name_item_in_array() != null) {
            variable_in_foreach.setVariable_name(ctx.var_name_item_in_array().getText());
        }
        return variable_in_foreach;
    }

    @Override
    public PrintStatementClass visitPrint_statement(RulesParser.Print_statementContext ctx) {
        System.out.println(">> Parse Print Statement");
        PrintStatementClass print_statement = new PrintStatementClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Print Statement"
        );
        if (ctx.variable_value() != null) {
            List<VariableValueClass> variable_values = new ArrayList<>();
            for (int i = 0; i < ctx.variable_value().size(); i++) {
                variable_values.add(visitVariable_value(ctx.variable_value().get(i)));
            }
            print_statement.setVariable_value(variable_values);
        }
        return print_statement;
    }

    @Override
    public Object visitError(RulesParser.ErrorContext ctx) {
        return super.visitError(ctx);
    }

    @Override
    public SqlStmtListClass visitSql_stmt_list(RulesParser.Sql_stmt_listContext ctx) {
        System.out.println("Sql Statement List");
        SqlStmtListClass sqlStatment = new SqlStmtListClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Sql Statement List"

        );
        for (int i = 0; i < ctx.sql_stmt().size(); i++) {
            sqlStatment.getSqlStmtClasses().add(visitSql_stmt(ctx.sql_stmt(i)));
        }


        return sqlStatment;
    }

    @Override
    public SqlStmtClass visitSql_stmt(RulesParser.Sql_stmtContext ctx) {
        System.out.println("Sql Statement");
        SqlStmtClass sqlStmtClass = new SqlStmtClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Sql Statement"

        );
        if (ctx.factored_select_stmt() != null) {
            sqlStmtClass.setFactoredSelectStmtClass(visitFactored_select_stmt(ctx.factored_select_stmt()));
        } else if (ctx.alter_table_stmt() != null) {
            sqlStmtClass.setAlterTableStmt(visitAlter_table_stmt(ctx.alter_table_stmt()));
        } else if (ctx.delete_stmt() != null) {
            sqlStmtClass.setDeleteStmt(visitDelete_stmt(ctx.delete_stmt()));
        } else if (ctx.drop_table_stmt() != null) {
            sqlStmtClass.setDropTableStmt(visitDrop_table_stmt(ctx.drop_table_stmt()));
        } else if (ctx.insert_stmt() != null) {
            sqlStmtClass.setInsertStmt(visitInsert_stmt(ctx.insert_stmt()));
        } else if (ctx.create_table_stmt() != null) {
            sqlStmtClass.setCreateTableStmt(visitCreate_table_stmt(ctx.create_table_stmt()));
        } else if (ctx.update_stmt() != null) {
            sqlStmtClass.setUpdateStmt(visitUpdate_stmt(ctx.update_stmt()));
        } else if (ctx.create_type_stmt() != null) {
            sqlStmtClass.setCreateTypeStmt(visitCreate_type_stmt(ctx.create_type_stmt()));
        } else {
            sqlStmtClass.setCreateAggregationFunction(visitCreate_aggregation_function(ctx.create_aggregation_function()));
        }
        return sqlStmtClass;
    }

    @Override
    public AlterTableStmtClass visitAlter_table_stmt(RulesParser.Alter_table_stmtContext ctx) {
        System.out.println("Alter Table Stmt");
        AlterTableStmtClass alterTableStmt = new AlterTableStmtClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Alter Table Stmt"
        );
        alterTableStmt.setSourceTableName(ctx.source_table_name().any_name().getText());

        if (ctx.database_name() != null) {
            alterTableStmt.setDatabaseName(ctx.database_name().any_name().getText());
        }
        if (ctx.new_table_name() != null) {
            alterTableStmt.setNewTableName(ctx.new_table_name().any_name().getText());
        } else if (ctx.alter_table_add() != null) {
            alterTableStmt.setAlterTableAdd(visitAlter_table_add(ctx.alter_table_add()));
        } else if (ctx.alter_table_add_constraint() != null) {
            alterTableStmt.setAlterTableAddConstraint(visitAlter_table_add_constraint(ctx.alter_table_add_constraint()));
        } else {
            alterTableStmt.setColumnDef(visitColumn_def(ctx.column_def()));
        }
        return alterTableStmt;
    }

    @Override
    public AlterTableAddConstraintClass visitAlter_table_add_constraint(RulesParser.Alter_table_add_constraintContext ctx) {
        System.out.println("Alter Table Add");
        AlterTableAddConstraintClass alterTableAddConstraint = new AlterTableAddConstraintClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Alter Table Add Constraint"
        );
        alterTableAddConstraint.setAnyName(ctx.any_name().getText());
        alterTableAddConstraint.setTableConstraint(visitTable_constraint(ctx.table_constraint()));
        return alterTableAddConstraint;
    }

    @Override
    public AlterTableAddClass visitAlter_table_add(RulesParser.Alter_table_addContext ctx) {
        System.out.println("Alter Table Add");
        AlterTableAddClass alterTableAdd = new AlterTableAddClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Alter Table Add"
        );
        alterTableAdd.setTableConstraint(visitTable_constraint(ctx.table_constraint()));


        return alterTableAdd;
    }

    @Override
    public CreateTableStmtClass visitCreate_table_stmt(RulesParser.Create_table_stmtContext ctx) {
        System.out.println("Create Table Stmt");
        CreateTableStmtClass createTableStmt = new CreateTableStmtClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Create Table Stmt"
        );


        if (ctx.K_IF() != null) {
            createTableStmt.setIfNotExist(true);
        }
        createTableStmt.setTableNameFaild(visitTable_name_faild(ctx.table_name_faild()));
        createTableStmt.setColumns(visitColumns_definition(ctx.columns_definition()));
        createTableStmt.setCreateTableStmtType(visitCreate_table_stmt_type(ctx.create_table_stmt_type()));
        createTableStmt.setCreateTableStmtPath(visitCreate_table_stmt_path(ctx.create_table_stmt_path()));

        DuplicateColumnsSqlErrorClass duplicate_column_sql_error = new DuplicateColumnsSqlErrorClass(
                createTableStmt.getColumns().getColumns(),
                Utils.ERROR_MESSAGE_DUPLICATE_COLUMNS_IN_CREATE,
                String.valueOf(createTableStmt.getRow()),
                String.valueOf(createTableStmt.getColumn())
        );
        duplicate_column_sql_error.checkingError();

        SymbolTableManager.getInstance().addTableType(createTableStmt.getColumns(), createTableStmt.getTableNameFaild().getTableName(), createTableStmt.getCreateTableStmtType().getTypeFaild().getTypeFaildName(), createTableStmt.getCreateTableStmtPath().getPathString());


        return createTableStmt;
    }

    @Override
    public DeleteStmtClass visitDelete_stmt(RulesParser.Delete_stmtContext ctx) {
        System.out.println("Delete Stmt");
        DeleteStmtClass deleteStmt = new DeleteStmtClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Delete Stmt"
        );
        deleteStmt.setQualifiedTableName(visitQualified_table_name(ctx.qualified_table_name()));
        if (ctx.expr() != null) {
            deleteStmt.setWhereExpr(visitExpr(ctx.expr()));
        }
        return deleteStmt;
    }

    @Override
    public DropTableStmtClass visitDrop_table_stmt(RulesParser.Drop_table_stmtContext ctx) {
        System.out.println("Drop Table Stmt");
        DropTableStmtClass dropTableStmt = new DropTableStmtClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Drop Table Stmt"
        );
        if (ctx.K_EXISTS() != null) {
            dropTableStmt.setIfExist(true);
        }
        dropTableStmt.setTableNameFaild(visitTable_name_faild(ctx.table_name_faild()));
        return dropTableStmt;
    }

    @Override
    public FactoredSelectStmtClass visitFactored_select_stmt(RulesParser.Factored_select_stmtContext ctx) {
        FactoredSelectStmtClass factoredSelectStmt = new FactoredSelectStmtClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "factored select stmt"
        );
        factoredSelectStmt.setSelectCoreClass(visitSelect_core(ctx.select_core()));


        if (ctx.order_by() != null) {
            factoredSelectStmt.setOrderBy(visitOrder_by(ctx.order_by()));
        }
        if (ctx.limit_select() != null) {
            factoredSelectStmt.setLimitSelect(visitLimit_select(ctx.limit_select()));
        }

        //
//       if(unexistColumnOfTableError.checkingError())
//       {
//           SymbolTableManager.getInstance().flat(factoredSelectStmt);
//
//       }

        return factoredSelectStmt;
    }

    @Override
    public InsertStmtClass visitInsert_stmt(RulesParser.Insert_stmtContext ctx) {
        System.out.println("Insert stmt");
        InsertStmtClass insertStmtClass = new InsertStmtClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Insert stmt"
        );
        insertStmtClass.setTableNameFaild(visitTable_name_faild(ctx.table_name_faild()));
        List<String> columnsNames = new ArrayList<>();
        for (int i = 0; i < ctx.column_name().size(); i++) {
            columnsNames.add(ctx.column_name(i).any_name().getText());
        }
        insertStmtClass.setColumnsName(columnsNames);
        insertStmtClass.setInsertStmtValues(visitInsert_stmt_values(ctx.insert_stmt_values()));

        return insertStmtClass;
    }

    @Override
    public SelectStmtClass visitSelect_stmt(RulesParser.Select_stmtContext ctx) {
        System.out.println("select stmt");
        SelectStmtClass selectStmt = new SelectStmtClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Column Def"
        );
        selectStmt.setFactoredSelectStmt(visitFactored_select_stmt(ctx.factored_select_stmt()));

        return selectStmt;
    }


    @Override
    public UpdateStmtClass visitUpdate_stmt(RulesParser.Update_stmtContext ctx) {
        System.out.println("Update stmt");
        UpdateStmtClass updateStmt = new UpdateStmtClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Column Def"
        );
        updateStmt.setQualifiedTableName(visitQualified_table_name(ctx.qualified_table_name()));
        List<ColumnNameEqualsExprClass> columnNameEqualsExprs = new ArrayList<>();
        for (int i = 0; i < ctx.column_name_equals_expr().size(); i++) {
            columnNameEqualsExprs.add(visitColumn_name_equals_expr(ctx.column_name_equals_expr(i)));
        }
        updateStmt.setColumnNameEqualsExprs(columnNameEqualsExprs);
        if (ctx.expr() != null) {
            updateStmt.setWhereExpr(visitExpr(ctx.expr()));
        }
        return updateStmt;
    }

    @Override
    public ColumnDefClass visitColumn_def(RulesParser.Column_defContext ctx) {
        System.out.println("Column Def");
        ColumnDefClass columnDef = new ColumnDefClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Column Def"
        );
        columnDef.setColumnName(ctx.column_name().any_name().getText());
        List<ColumnConstraintOrTypeName> columnConstraintOrTypeNames = new ArrayList<>();
        for (int i = 0; i < ctx.column_constraint_or_type_name().size(); i++) {
            columnConstraintOrTypeNames.add(visitColumn_constraint_or_type_name(ctx.column_constraint_or_type_name(i)));
        }
        columnDef.setColumnConstraintOrTypeNames(columnConstraintOrTypeNames);
        return columnDef;
    }

    @Override
    public ColumnConstraintOrTypeName visitColumn_constraint_or_type_name(RulesParser.Column_constraint_or_type_nameContext ctx) {
        System.out.println("Column Constraint Or TypeName");
        ColumnConstraintOrTypeName columnConstraintOrTypeName = new ColumnConstraintOrTypeName(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Column Constraint Or TypeName"
        );
        if (ctx.column_constraint() != null) {
            columnConstraintOrTypeName.setColumnConstraint(visitColumn_constraint(ctx.column_constraint()));
        } else {
            columnConstraintOrTypeName.setTypeName(visitType_name(ctx.type_name()));
        }
        return columnConstraintOrTypeName;
    }

    @Override
    public TypeNameClass visitType_name(RulesParser.Type_nameContext ctx) {
        TypeNameClass typeName = new TypeNameClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Type Name"
        );
        if (ctx.return_type() == null) {
            typeName.setName(ctx.name().any_name().getText());
            List<SignedNumberAnyNameClass> signedNumberAnyNames = new ArrayList<>();
            for (int i = 0; i < ctx.signed_number_any_name().size(); i++) {
                signedNumberAnyNames.add(visitSigned_number_any_name(ctx.signed_number_any_name(i)));
            }
            typeName.setSignedNumberAnyNames(signedNumberAnyNames);
        } else {
            typeName.setReturnType(ctx.return_type().getText());
        }

        return typeName;
    }


    @Override
    public ColumnConstraintClass visitColumn_constraint(RulesParser.Column_constraintContext ctx) {
        System.out.println("Column Constraint");
        ColumnConstraintClass columnConstraintClass = new ColumnConstraintClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Column Constraint"
        );
        if (ctx.K_CONSTRAINT() != null) {
            columnConstraintClass.setConstraintName(ctx.name().any_name().getText());
        } else if (ctx.column_constraint_primary_key() != null) {
            columnConstraintClass.setColumnConstraintPrimaryKey(visitColumn_constraint_primary_key(ctx.column_constraint_primary_key()));
        } else if (ctx.column_constraint_foreign_key() != null) {
            columnConstraintClass.setColumnConstraintForeignKey(visitColumn_constraint_foreign_key(ctx.column_constraint_foreign_key()));
        } else if (ctx.column_constraint_not_null() != null) {
            columnConstraintClass.setColumnConstraintNotNull(visitColumn_constraint_not_null(ctx.column_constraint_not_null()));
        } else if (ctx.column_constraint_null() != null) {
            columnConstraintClass.setColumnConstraintNull(visitColumn_constraint_null(ctx.column_constraint_null()));
        } else if (ctx.expr() != null) {
            columnConstraintClass.setExprCheck(visitExpr(ctx.expr()));
        } else if (ctx.column_default() != null) {
            columnConstraintClass.setColumnDefault(visitColumn_default(ctx.column_default()));
        } else {
            columnConstraintClass.setColltionName(ctx.collation_name().any_name().getText());
        }
        return columnConstraintClass;
    }

    @Override
    public ColumnConstraintPrimaryKeyClass visitColumn_constraint_primary_key(RulesParser.Column_constraint_primary_keyContext ctx) {
        System.out.println("Column Constraint Primary Key");
        ColumnConstraintPrimaryKeyClass columnConstraintPrimaryKey = new ColumnConstraintPrimaryKeyClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Column Constraint Primary Key"
        );
        if (ctx.K_ASC() != null) {
            columnConstraintPrimaryKey.setAsc(true);
        } else if (ctx.K_DESC() != null) {
            columnConstraintPrimaryKey.setDesc(true);
        }
        if (ctx.K_AUTOINCREMENT() != null) {
            columnConstraintPrimaryKey.setAutoIncrement(true);
        }
        return columnConstraintPrimaryKey;
    }

    @Override
    public ColumnConstraintForeignKeyClass visitColumn_constraint_foreign_key(RulesParser.Column_constraint_foreign_keyContext ctx) {
        System.out.println("Column Constraint Foreign Key");
        ColumnConstraintForeignKeyClass columnConstraintForeignKey = new ColumnConstraintForeignKeyClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Column Constraint Foreign Key"
        );
        columnConstraintForeignKey.setForeignKeyClause(visitForeign_key_clause(ctx.foreign_key_clause()));
        return columnConstraintForeignKey;
    }

    @Override
    public ColumnConstraintNotNullClass visitColumn_constraint_not_null(RulesParser.Column_constraint_not_nullContext ctx) {
        System.out.println("Column Constraint Not Null");
        ColumnConstraintNotNullClass columnConstraintNotNull = new ColumnConstraintNotNullClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Column Constraint Not Null"
        );
        columnConstraintNotNull.setNotNull("not Null");
        return columnConstraintNotNull;
    }

    @Override
    public ColumnConstraintNullClass visitColumn_constraint_null(RulesParser.Column_constraint_nullContext ctx) {
        System.out.println("Column Constraint Null");
        ColumnConstraintNullClass columnConstraintNull = new ColumnConstraintNullClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Column Constraint Null"
        );
        columnConstraintNull.setNull("Null");
        return columnConstraintNull;

    }

    @Override
    public ColumnDefaultClass visitColumn_default(RulesParser.Column_defaultContext ctx) {
        System.out.println("Column Default");
        ColumnDefaultClass columnDefault = new ColumnDefaultClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Column Default"
        );
        columnDefault.setColumnDefaultContent(visitColumn_default_content(ctx.column_default_content()));
        if (ctx.any_name() != null) {
            List<String> anyNames = new ArrayList<>();
            for (int i = 0; i < ctx.any_name().size(); i++) {
                anyNames.add(ctx.any_name(i).any_name().getText());
            }
            columnDefault.setAnyNames(anyNames);
        }
        return columnDefault;
    }

    @Override
    public ColumnDefaultValue visitColumn_default_value(RulesParser.Column_default_valueContext ctx) {
        System.out.println("Column Default");
        ColumnDefaultValue columnDefaultValue = new ColumnDefaultValue(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Column Default"
        );
        if (ctx.signed_number() != null) {
            columnDefaultValue.setSignedNumber(ctx.signed_number().getText());
        } else {
            columnDefaultValue.setLiteralValue(ctx.literal_value().getText());
        }
        return columnDefaultValue;
    }

    @Override
    public ExprClass visitExpr(RulesParser.ExprContext ctx) {
        System.out.println("Expr");
        ExprClass expr = new ExprClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Expr"
        );
        if (ctx.literal_value() != null) {
            expr.setLiteralValue(ctx.literal_value().getText());
        } else if (ctx.in_case() != null) {
            expr.setIncaseExpr(visitExpr(ctx.expr(0)));
            expr.setInCase(visitIn_case(ctx.in_case()));
        } else if (ctx.column_name_faild() != null) {
            expr.setColumnNameFaild(visitColumn_name_faild(ctx.column_name_faild()));
        } else if (ctx.unary_operator() != null) {
            ExprOperatorExprClass exprOperatorExpr = new ExprOperatorExprClass(
                    ctx.getStart().getLine(),
                    ctx.getStart().getCharPositionInLine(),
                    "Expr Operation Expr"
            );
            exprOperatorExpr.setExp1(null);
            exprOperatorExpr.setExp2(visitExpr(ctx.expr(0)));
            exprOperatorExpr.setOperator(ctx.unary_operator().getText());
            expr.setExprOperatorExpr(exprOperatorExpr);
        } else if (ctx.function_expr() != null) {
            expr.setFunctionExpr(visitFunction_expr(ctx.function_expr()));
        } else if (ctx.OPEN_PAR() != null) {
            expr.setExprInBracket(visitExpr(ctx.expr(0)));
        } else if (ctx.exists_case() != null) {
            expr.setExistsCase(visitExists_case(ctx.exists_case()));
        } else {
            ExprOperatorExprClass exprOperatorExpr = new ExprOperatorExprClass(
                    ctx.getStart().getLine(),
                    ctx.getStart().getCharPositionInLine(),
                    "Expr Operation Expr"
            );
            exprOperatorExpr.setExp1(visitExpr(ctx.expr(0)));
            exprOperatorExpr.setExp2(visitExpr(ctx.expr(1)));
            if (ctx.OR() != null) exprOperatorExpr.setOperator("||");
            else if (ctx.STAR() != null) exprOperatorExpr.setOperator("*");
            else if (ctx.DIV() != null) exprOperatorExpr.setOperator("/");
            else if (ctx.MOD() != null) exprOperatorExpr.setOperator("%");
            else if (ctx.PLUS() != null) exprOperatorExpr.setOperator("+");
            else if (ctx.MINUS() != null) exprOperatorExpr.setOperator("-");
            else if (ctx.LT2() != null) exprOperatorExpr.setOperator("<<");
            else if (ctx.GT2() != null) exprOperatorExpr.setOperator(">>");
            else if (ctx.AMP() != null) exprOperatorExpr.setOperator("&");
            else if (ctx.PIPE() != null) exprOperatorExpr.setOperator("|");
            else if (ctx.LT() != null) exprOperatorExpr.setOperator("<");
            else if (ctx.LT_EQ() != null) exprOperatorExpr.setOperator("<=");
            else if (ctx.GT() != null) exprOperatorExpr.setOperator(">");
            else if (ctx.GT_EQ() != null) exprOperatorExpr.setOperator(">=");
            else if (ctx.ASSIGN() != null) exprOperatorExpr.setOperator("=");
            else if (ctx.EQ() != null) exprOperatorExpr.setOperator("==");
            else if (ctx.NOT_EQ1() != null) exprOperatorExpr.setOperator("!=");
            else if (ctx.NOT_EQ2() != null) exprOperatorExpr.setOperator("<>");
            else if (ctx.K_IS() != null) exprOperatorExpr.setOperator("is");
            else if (ctx.K_NOT() != null) exprOperatorExpr.setOperator("is not");
            else if (ctx.K_LIKE() != null) exprOperatorExpr.setOperator("like");
            else if (ctx.K_GLOB() != null) exprOperatorExpr.setOperator("glob");
            else if (ctx.K_MATCH() != null) exprOperatorExpr.setOperator("match");
            else if (ctx.K_REGEXP() != null) exprOperatorExpr.setOperator("regexp");
            else if (ctx.K_AND() != null) exprOperatorExpr.setOperator("and");
            else if (ctx.K_OR() != null) exprOperatorExpr.setOperator("or");

            expr.setExprOperatorExpr(exprOperatorExpr);
        }

        return expr;
    }

    @Override
    public ForeignKeyClauseClass visitForeign_key_clause(RulesParser.Foreign_key_clauseContext ctx) {
        System.out.println("foreign Key Clause");
        ForeignKeyClauseClass foreignKeyClause = new ForeignKeyClauseClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "foreign Key Clause"
        );
        foreignKeyClause.setForeignTableFaild(visitForeign_table_faild(ctx.foreign_table_faild()));
        List<String> fkTargetColumnNames = new ArrayList<>();
        for (int i = 0; i < ctx.fk_target_column_name().size(); i++) {
            fkTargetColumnNames.add(ctx.fk_target_column_name(i).name().any_name().getText());
        }
        foreignKeyClause.setFkTargetColumnName(fkTargetColumnNames);

        List<ForeignOnOrMatch> foreignOnOrMatches = new ArrayList<>();
        for (int i = 0; i < ctx.foreign_on_or_match().size(); i++) {
            foreignOnOrMatches.add(visitForeign_on_or_match(ctx.foreign_on_or_match(i)));
        }
        foreignKeyClause.setForeignOnOrMatches(foreignOnOrMatches);
        if (ctx.foreign_deferrable() != null) {
            foreignKeyClause.setForeignDeferrable(visitForeign_deferrable(ctx.foreign_deferrable()));
        }
        return foreignKeyClause;
    }

    @Override
    public Object visitFk_target_column_name(RulesParser.Fk_target_column_nameContext ctx) {
        return super.visitFk_target_column_name(ctx);
    }

    @Override
    public IndexedColumn visitIndexed_column(RulesParser.Indexed_columnContext ctx) {
        return null;
    }

    @Override
    public TableConstraintClass visitTable_constraint(RulesParser.Table_constraintContext ctx) {
        System.out.println("Table Constraint");
        TableConstraintClass tableConstraint = new TableConstraintClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Table Constraint"
        );
        if (ctx.K_CONSTRAINT() != null) {
            tableConstraint.setConstraintName(ctx.name().any_name().getText());
        }
        if (ctx.table_constraint_primary_key() != null) {
            tableConstraint.setTableConstraintPrimaryKey(visitTable_constraint_primary_key(ctx.table_constraint_primary_key()));
        } else if (ctx.table_constraint_key() != null) {

            tableConstraint.setTableConstraintKey(visitTable_constraint_key(ctx.table_constraint_key()));
        } else if (ctx.table_constraint_unique() != null) {
            tableConstraint.setTableConstraintUnique(visitTable_constraint_unique(ctx.table_constraint_unique()));
        } else if (ctx.K_CHECK() != null) {
            tableConstraint.setExprCheck(visitExpr(ctx.expr()));
        } else {
            tableConstraint.setTableConstraintForeginKey(visitTable_constraint_foreign_key(ctx.table_constraint_foreign_key()));
        }
        return tableConstraint;
    }

    @Override
    public TableConstraintPrimaryKey visitTable_constraint_primary_key(RulesParser.Table_constraint_primary_keyContext ctx) {
        System.out.println("Table Constraint Primary Key");
        TableConstraintPrimaryKey tableConstraintPrimaryKey = new TableConstraintPrimaryKey(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Table Constraint Primary Key"
        );

        List<IndexedColumn> indexedColumns = new ArrayList<>();
        for (int i = 0; i < ctx.indexed_column().size(); i++) {
            indexedColumns.add(visitIndexed_column(ctx.indexed_column(i)));
        }
        tableConstraintPrimaryKey.setIndexedColumns(indexedColumns);

        return tableConstraintPrimaryKey;
    }

    @Override
    public TableConstraintForeginKey visitTable_constraint_foreign_key(RulesParser.Table_constraint_foreign_keyContext ctx) {
        System.out.println("Table Constraint Foregin Key");
        TableConstraintForeginKey tableConstraintForeginKey = new TableConstraintForeginKey(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Table Constraint Foregin Key"
        );
        List<String> fkOriginColumnNames = new ArrayList<>();
        for (int i = 0; i < ctx.fk_origin_column_name().size(); i++) {
            fkOriginColumnNames.add(ctx.fk_origin_column_name(i).column_name().any_name().getText());
        }
        tableConstraintForeginKey.setFkOriginColumnNames(fkOriginColumnNames);
        tableConstraintForeginKey.setForeignKeyClause(visitForeign_key_clause(ctx.foreign_key_clause()));
        return tableConstraintForeginKey;
    }

    @Override
    public TableConstraintUnique visitTable_constraint_unique(RulesParser.Table_constraint_uniqueContext ctx) {
        System.out.println("Table Constraint Unique");
        TableConstraintUnique tableConstraintUnique = new TableConstraintUnique(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Table Constraint Unique"
        );
        if (ctx.name() != null) {
            tableConstraintUnique.setName(ctx.name().any_name().getText());
        }
        List<IndexedColumn> indexedColumns = new ArrayList<>();
        for (int i = 0; i < ctx.indexed_column().size(); i++) {
            indexedColumns.add(visitIndexed_column(ctx.indexed_column(i)));
        }
        tableConstraintUnique.setIndexedColumns(indexedColumns);
        return tableConstraintUnique;
    }

    @Override
    public TableConstraintKey visitTable_constraint_key(RulesParser.Table_constraint_keyContext ctx) {
        System.out.println("Table Constraint Key");
        TableConstraintKey tableConstraintKey = new TableConstraintKey(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Table Constraint Key"
        );
        if (ctx.name() != null) {
            tableConstraintKey.setName(ctx.name().any_name().getText());
        }
        List<IndexedColumn> indexedColumns = new ArrayList<>();
        for (int i = 0; i < ctx.indexed_column().size(); i++) {
            indexedColumns.add(visitIndexed_column(ctx.indexed_column(i)));
        }
        tableConstraintKey.setIndexedColumns(indexedColumns);

        return tableConstraintKey;
    }

    @Override
    public Object visitFk_origin_column_name(RulesParser.Fk_origin_column_nameContext ctx) {
        return super.visitFk_origin_column_name(ctx);
    }

    @Override
    public QualifiedTableNameClass visitQualified_table_name(RulesParser.Qualified_table_nameContext ctx) {
        System.out.println("Qualified Table Name");
        QualifiedTableNameClass qualifiedTableName = new QualifiedTableNameClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Qualified Table Name"
        );
        qualifiedTableName.setTableNameFaild(visitTable_name_faild(ctx.table_name_faild()));
        if (ctx.index_name() != null)
            qualifiedTableName.setIndexName(ctx.index_name().any_name().getText());
        else qualifiedTableName.setNotIndex(true);
        return qualifiedTableName;
    }

    @Override
    public OrderingTermClass visitOrdering_term(RulesParser.Ordering_termContext ctx) {
        System.out.println("Ordering Term");
        OrderingTermClass orderingTerm = new OrderingTermClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Ordering Term"
        );
        orderingTerm.setExpr(visitExpr(ctx.expr()));
        if (ctx.collation_name() != null) {
            orderingTerm.setCollisionName(ctx.collation_name().getText());
        }
        if (ctx.K_ASC() != null) {
            orderingTerm.setAsc(true);
        }
        if (ctx.K_DESC() != null) {
            orderingTerm.setDesc(true);
        }
        return orderingTerm;
    }


    @Override
    public ResultColumnClass visitResult_column(RulesParser.Result_columnContext ctx) {
        System.out.println("Result column");
        ResultColumnClass resultColumn = new ResultColumnClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Result column"
        );
        if (ctx.result_column_with_expr() != null) {
            resultColumn.setResultColumnWithExpr(visitResult_column_with_expr(ctx.result_column_with_expr()));
        } else if (ctx.table_name() != null) {
            resultColumn.setTableNameWithStar(ctx.table_name().getText());
        } else {
            resultColumn.setStar(true);
        }
        return resultColumn;
    }

    @Override
    public TableOrSubqueryClass visitTable_or_subquery(RulesParser.Table_or_subqueryContext ctx) {
        System.out.println("table or subquery");
        TableOrSubqueryClass tableOrSubquery = new TableOrSubqueryClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "table or subquery"
        );
        if (ctx.table_as_alias() != null) {
            tableOrSubquery.setTableAsAlias(visitTable_as_alias(ctx.table_as_alias()));
        } else if (ctx.from_itmes_as_table() != null) {
            tableOrSubquery.setFromItemsAsTable(visitFrom_itmes_as_table(ctx.from_itmes_as_table()));
        } else {
            tableOrSubquery.setSelectStmtAsTable(visitSelect_stmt_as_table(ctx.select_stmt_as_table()));
        }
        return tableOrSubquery;
    }

    @Override
    public JoinClauseClass visitJoin_clause(RulesParser.Join_clauseContext ctx) {
        System.out.println("join clause");
        JoinClauseClass joinClauseClass = new JoinClauseClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "join clause"
        );
        joinClauseClass.setTableOrSubquery(visitTable_or_subquery(ctx.table_or_subquery()));
        List<OperatorTableConstraintClass> operatorTableConstraints = new ArrayList<>();
        for (int i = 0; i < ctx.operator_table_constraint().size(); i++) {
            operatorTableConstraints.add(visitOperator_table_constraint(ctx.operator_table_constraint(i)));
        }
        joinClauseClass.setOperatorTableConstraints(operatorTableConstraints);

        return joinClauseClass;
    }

    @Override
    public JoinOperatorClass visitJoin_operator(RulesParser.Join_operatorContext ctx) {
        System.out.println("Join Operator");
        JoinOperatorClass joinOperator = new JoinOperatorClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Join Operator"
        );
        if (ctx.COMMA() != null) {
            joinOperator.setComma(true);
        } else {
            if (ctx.K_INNER() != null) {
                joinOperator.setInner(true);
            } else {
                if (ctx.K_LEFT() != null) {
                    if (ctx.K_OUTER() != null) {
                        joinOperator.setLeftOuter(true);
                    } else {
                        joinOperator.setLeft(true);
                    }
                }
            }
        }
        return joinOperator;
    }

    @Override
    public Object visitJoin_constraint(RulesParser.Join_constraintContext ctx) {
        return super.visitJoin_constraint(ctx);
    }

    @Override
    public SelectCoreClass visitSelect_core(RulesParser.Select_coreContext ctx) {
        System.out.println("select core");
        SelectCoreClass selectCore = new SelectCoreClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "select core"
        );
        if (ctx.select_items() != null) {
            selectCore.setSelectItemes(visitSelect_items(ctx.select_items()));
            if (ctx.K_FROM() != null)
                selectCore.setFromItems(visitFrom_itmes(ctx.from_itmes()));
            if (ctx.K_WHERE() != null) {
                System.out.println("Where item");
                selectCore.setWhereExpr(visitExpr(ctx.expr()));
            }
            if (ctx.group_by() != null)
                selectCore.setGroupBy(visitGroup_by(ctx.group_by()));
        } else {
            selectCore.setValuesItems(visitValues_items(ctx.values_items()));
        }

        return selectCore;
    }

    @Override
    public Object visitSigned_number(RulesParser.Signed_numberContext ctx) {
        return super.visitSigned_number(ctx);
    }

    @Override
    public Object visitLiteral_value(RulesParser.Literal_valueContext ctx) {
        return super.visitLiteral_value(ctx);
    }

    @Override
    public Object visitUnary_operator(RulesParser.Unary_operatorContext ctx) {
        return super.visitUnary_operator(ctx);
    }


    @Override
    public Object visitColumn_alias(RulesParser.Column_aliasContext ctx) {
        return super.visitColumn_alias(ctx);
    }

    @Override
    public Object visitKeyword(RulesParser.KeywordContext ctx) {
        return super.visitKeyword(ctx);
    }


    @Override
    public Object visitName(RulesParser.NameContext ctx) {
        return super.visitName(ctx);
    }

    @Override
    public Object visitFunction_name(RulesParser.Function_nameContext ctx) {
        return super.visitFunction_name(ctx);
    }

    @Override
    public Object visitDatabase_name(RulesParser.Database_nameContext ctx) {
        return super.visitDatabase_name(ctx);
    }

    @Override
    public Object visitSource_table_name(RulesParser.Source_table_nameContext ctx) {
        return super.visitSource_table_name(ctx);
    }

    @Override
    public Object visitTable_name(RulesParser.Table_nameContext ctx) {
        return super.visitTable_name(ctx);
    }

    @Override
    public Object visitNew_table_name(RulesParser.New_table_nameContext ctx) {
        return super.visitNew_table_name(ctx);
    }

    @Override
    public Object visitColumn_name(RulesParser.Column_nameContext ctx) {
        return super.visitColumn_name(ctx);
    }

    @Override
    public Object visitCollation_name(RulesParser.Collation_nameContext ctx) {
        return super.visitCollation_name(ctx);
    }

    @Override
    public Object visitForeign_table(RulesParser.Foreign_tableContext ctx) {
        return super.visitForeign_table(ctx);
    }

    @Override
    public Object visitIndex_name(RulesParser.Index_nameContext ctx) {
        return super.visitIndex_name(ctx);
    }


    @Override
    public Object visitAny_name(RulesParser.Any_nameContext ctx) {
        return super.visitAny_name(ctx);
    }


    @Override
    public OrderByClass visitOrder_by(RulesParser.Order_byContext ctx) {
        System.out.println("Order by");
        OrderByClass orderBy = new OrderByClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Order by"
        );
        List<OrderingTermClass> orderingTerms = new ArrayList<>();
        for (int i = 0; i < ctx.ordering_term().size(); i++) {
            orderingTerms.add(visitOrdering_term(ctx.ordering_term(i)));
        }
        orderBy.setOrderingTerms(orderingTerms);
        return orderBy;
    }

    @Override
    public LimitSelectClass visitLimit_select(RulesParser.Limit_selectContext ctx) {
        System.out.println("Limit select");
        LimitSelectClass limitSelect = new LimitSelectClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Limit select"
        );
        limitSelect.setLimitExpr(visitExpr(ctx.expr(0)));
        if (ctx.K_OFFSET() != null || ctx.COMMA() != null) {
            limitSelect.setCommaOrOffsetExpr(visitExpr(ctx.expr(1)));
            if (ctx.K_OFFSET() != null) {
                limitSelect.setOffset(true);
            } else {
                limitSelect.setComma(true);
            }
        }
        return limitSelect;
    }

    @Override
    public ExistsCaseClass visitExists_case(RulesParser.Exists_caseContext ctx) {
        System.out.println("Exists Case");
        ExistsCaseClass existsCase = new ExistsCaseClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Exists Case"
        );
        existsCase.setSelectStmt(visitSelect_stmt(ctx.select_stmt()));
        if (ctx.K_EXISTS() != null) {
            existsCase.setExists(true);
            if (ctx.K_NOT() != null) {
                existsCase.setkNot(true);
            }
        }

        return existsCase;
    }

    @Override
    public FunctionExprClass visitFunction_expr(RulesParser.Function_exprContext ctx) {
        System.out.println("Function Expr");
        FunctionExprClass functionExprClass = new FunctionExprClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Function Expr"
        );
        functionExprClass.setFunctionName(ctx.function_name().getText());
        functionExprClass.setFunctionParameterSql(new FunctionParameterSqlClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Function Parameter Sql"
        ));
        if (ctx.STAR() != null) {
            functionExprClass.getFunctionParameterSql().setStar(true);
        } else {
            if (ctx.expr().size() > 0) {
                List<ExprClass> exprs = new ArrayList<>();
                for (int i = 0; i < ctx.expr().size(); i++) {
                    exprs.add(visitExpr(ctx.expr(i)));
                }
                functionExprClass.getFunctionParameterSql().setExprs(exprs);
                if (ctx.K_DISTINCT() != null) {
                    functionExprClass.getFunctionParameterSql().setDistict(true);
                }
            }
        }
        return functionExprClass;
    }

    @Override
    public ColumnNameFaildClass visitColumn_name_faild(RulesParser.Column_name_faildContext ctx) {
        System.out.println("Column Name Faild");
        ColumnNameFaildClass columnNameFaild = new ColumnNameFaildClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Column Name Faild"
        );
        columnNameFaild.setColumnName(ctx.column_name().getText());
        if (ctx.table_name_faild() != null) {
            columnNameFaild.setTableNameFaild(visitTable_name_faild(ctx.table_name_faild()));
        }
        return columnNameFaild;
    }

    @Override
    public TableNameFaildClass visitTable_name_faild(RulesParser.Table_name_faildContext ctx) {
        System.out.println("Table Name Faild");
        TableNameFaildClass tableNameFaildClass = new TableNameFaildClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Table Name Faild"
        );
        tableNameFaildClass.setTableName(ctx.table_name().getText());

        return tableNameFaildClass;
    }

    @Override
    public SelectOrExprClass visitSelect_or_expr(RulesParser.Select_or_exprContext ctx) {
        System.out.println("Select Or Exp");
        SelectOrExprClass selectOrExpr = new SelectOrExprClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Select Or Exp"
        );
        if (ctx.select_stmt() != null) {
            selectOrExpr.setSelectStmt(visitSelect_stmt(ctx.select_stmt()));
        } else {
            if (ctx.expr().size() > 0) {
                List<ExprClass> exprClasses = new ArrayList<>();
                for (int i = 0; i < ctx.expr().size(); i++) {
                    exprClasses.add(visitExpr(ctx.expr(i)));
                }
                selectOrExpr.setExprs(exprClasses);
            }
        }
        return selectOrExpr;
    }

    @Override
    public InCaseClass visitIn_case(RulesParser.In_caseContext ctx) {
        System.out.println("In case");
        InCaseClass inCase = new InCaseClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "In case"
        );
        if (ctx.K_NOT() != null) {
            inCase.setkNot(true);
        }
        if (ctx.select_or_expr() != null) {
            inCase.setSelectOrExpr(visitSelect_or_expr(ctx.select_or_expr()));
        } else {
            inCase.setTableNameFaild(visitTable_name_faild(ctx.table_name_faild()));
        }
        return inCase;
    }

    @Override
    public ResultColumnWithExprClass visitResult_column_with_expr(RulesParser.Result_column_with_exprContext ctx) {
        System.out.println("Result Column with Expr");
        ResultColumnWithExprClass resultColumnWithExpr = new ResultColumnWithExprClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Result Column with Expr"
        );
        resultColumnWithExpr.setExpr(visitExpr(ctx.expr()));
        if (ctx.column_alias() != null) {
            resultColumnWithExpr.setColumnAlias(ctx.column_alias().getText());
            if (ctx.K_AS() != null) {
                resultColumnWithExpr.setAs(true);
            }
        }
        return resultColumnWithExpr;
    }

    @Override
    public FromItemsAsTableClass visitFrom_itmes_as_table(RulesParser.From_itmes_as_tableContext ctx) {
        System.out.println("From Items as Table");
        FromItemsAsTableClass fromItemsAsTable = new FromItemsAsTableClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "From Items as Table"
        );
        fromItemsAsTable.setFromItems(visitFrom_itmes(ctx.from_itmes()));
        if (ctx.table_alias() != null) {
            fromItemsAsTable.setTableAlias(ctx.table_alias().toString());
            if (ctx.K_AS() != null) {
                fromItemsAsTable.setAs(true);
            }
        }
        return fromItemsAsTable;
    }


    @Override
    public SelectStmtAsTableClass visitSelect_stmt_as_table(RulesParser.Select_stmt_as_tableContext ctx) {
        System.out.println("Select Stmt As Table");
        SelectStmtAsTableClass selectStmtAsTable = new SelectStmtAsTableClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Select Stmt As Table"
        );
        selectStmtAsTable.setSelectStmt(visitSelect_stmt(ctx.select_stmt()));
        if (ctx.table_alias() != null) {
            selectStmtAsTable.setTableAlias(ctx.table_alias().getText());
            if (ctx.K_AS() != null) {
                selectStmtAsTable.setAs(true);
            }
        }

        return selectStmtAsTable;
    }

    @Override
    public TableAsAliasClass visitTable_as_alias(RulesParser.Table_as_aliasContext ctx) {
        System.out.println("Table Alias");
        TableAsAliasClass tableAsAlias = new TableAsAliasClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Table Alias"
        );
        tableAsAlias.setTableNameFaild(visitTable_name_faild(ctx.table_name_faild()));
        UndeclaredTypeErrorClass undeclaredTypeErrorClass = new UndeclaredTypeErrorClass(
                tableAsAlias.getTableNameFaild().getTableName(),
                Utils.ERROR_MESSAGE_UNDECLARED_TYPE,
                String.valueOf(tableAsAlias.getRow()),
                String.valueOf(tableAsAlias.getColumn())
        );
        undeclaredTypeErrorClass.checkingError();

        if (ctx.table_alias() != null) {
            tableAsAlias.setTableAlias(ctx.table_alias().toString());
            if (ctx.K_AS() != null) {
                tableAsAlias.setAs(true);
            }
        }
        if (ctx.index_name_or_not_index() != null) {
            tableAsAlias.setIndexNameOrNotIndex(visitIndex_name_or_not_index(ctx.index_name_or_not_index()));
        }
        return tableAsAlias;
    }

    @Override
    public IndexNameOrNotIndexClass visitIndex_name_or_not_index(RulesParser.Index_name_or_not_indexContext ctx) {
        System.out.println("Index Name Or Not Index");
        IndexNameOrNotIndexClass indexNameOrNotIndex = new IndexNameOrNotIndexClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Index Name Or Not Index"
        );
        if (ctx.index_name() != null) {
            indexNameOrNotIndex.setIndexName(ctx.index_name().getText());
        } else {
            indexNameOrNotIndex.setNotIndex(true);
        }
        return indexNameOrNotIndex;
    }

    @Override
    public OperatorTableConstraintClass visitOperator_table_constraint(RulesParser.Operator_table_constraintContext ctx) {
        System.out.println("Operator Table Constraint");
        OperatorTableConstraintClass operatorTableConstraint = new OperatorTableConstraintClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Operator Table Constraint"
        );
        operatorTableConstraint.setJoinOperator(visitJoin_operator(ctx.join_operator()));
        operatorTableConstraint.setTableOrSubquery(visitTable_or_subquery(ctx.table_or_subquery()));
        if (ctx.join_constraint().expr() != null) {
            operatorTableConstraint.setJoinExpr(visitExpr(ctx.join_constraint().expr()));
            OnClauseErrorClass onClauseErrorClass = new OnClauseErrorClass(
                    operatorTableConstraint.getJoinExpr(),
                    Utils.ERROR_MESSAGE_JOIN_HAVE_ON,
                    String.valueOf(operatorTableConstraint.getJoinExpr().getRow()),
                    String.valueOf(operatorTableConstraint.getJoinExpr().getColumn())
            );
            onClauseErrorClass.checkingError();
        }
        return operatorTableConstraint;
    }

    @Override
    public ValuesItemsClass visitValues_items(RulesParser.Values_itemsContext ctx) {
        System.out.println("Value Items");
        ValuesItemsClass valuesItems = new ValuesItemsClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Value Items"
        );
        List<ExprCommaExprClass> exprCommaExprs = new ArrayList<>();
        for (int i = 0; i < ctx.expr_comma_expr().size(); i++) {
            exprCommaExprs.add(visitExpr_comma_expr(ctx.expr_comma_expr(i)));
        }
        valuesItems.setExprCommaExprs(exprCommaExprs);
        return valuesItems;
    }

    @Override
    public ExprCommaExprClass visitExpr_comma_expr(RulesParser.Expr_comma_exprContext ctx) {
        System.out.println("Expr comma Expr");
        ExprCommaExprClass exprCommaExpr = new ExprCommaExprClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Expr comma Expr"
        );
        List<ExprClass> exprs = new ArrayList<>();
        for (int i = 0; i < ctx.expr().size(); i++) {
            exprs.add(visitExpr(ctx.expr(i)));
        }
        exprCommaExpr.setExprs(exprs);
        return exprCommaExpr;
    }

    @Override
    public GroupByClass visitGroup_by(RulesParser.Group_byContext ctx) {
        System.out.println("Group By");
        GroupByClass groupBy = new GroupByClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Group By"
        );
        int countExpr = ctx.expr().size();
        if (ctx.K_HAVING() != null) {
            countExpr--;
            groupBy.setHavingExprs(visitExpr(ctx.expr(countExpr)));
        }
        List<ExprClass> exprs = new ArrayList<>();
        for (int i = 0; i < countExpr; i++) {
            exprs.add(visitExpr(ctx.expr(i)));
        }
        groupBy.setExprs(exprs);

        return groupBy;
    }

    @Override
    public SelectItemsClass visitSelect_items(RulesParser.Select_itemsContext ctx) {
        System.out.println("Select Items");
        SelectItemsClass selectItems = new SelectItemsClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Select Items"
        );
        List<ResultColumnClass> resultColumns = new ArrayList<>();
        for (int i = 0; i < ctx.result_column().size(); i++) {
            resultColumns.add(visitResult_column(ctx.result_column(i)));
        }
        selectItems.setResultColumns(resultColumns);
        if (ctx.K_DISTINCT() != null) {
            selectItems.setDistinct(true);
        }
        if (ctx.K_ALL() != null) {
            selectItems.setAll(true);
        }
        return selectItems;
    }

    @Override
    public FromItemsClass visitFrom_itmes(RulesParser.From_itmesContext ctx) {
        System.out.println("from items");
        FromItemsClass fromItems = new FromItemsClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "from items"
        );
        if (ctx.join_clause() != null) {
            fromItems.setJoinClause(visitJoin_clause(ctx.join_clause()));
        } else {
            List<TableOrSubqueryClass> tableOrSubqueries = new ArrayList<>();
            for (int i = 0; i < ctx.table_or_subquery().size(); i++) {
                tableOrSubqueries.add(visitTable_or_subquery(ctx.table_or_subquery(i)));
            }
            fromItems.setTableOrSubqueries(tableOrSubqueries);
        }
        return fromItems;
    }

    @Override
    public ColumnsDeftTableConstraintClass visitColumns_def_tabels_constraint(RulesParser.Columns_def_tabels_constraintContext ctx) {
        System.out.println("Columns Def Table Constraint");
        ColumnsDeftTableConstraintClass columnsDeftTableConstraint = new ColumnsDeftTableConstraintClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Columns Def Table Constraint"
        );
        List<ColumnDefClass> columnDefs = new ArrayList<>();
        List<TableConstraintClass> tableConstraints = new ArrayList<>();
        for (int i = 0; i < ctx.column_def().size(); i++) {
            columnDefs.add(visitColumn_def(ctx.column_def(i)));
        }
        for (int i = 0; i < ctx.table_constraint().size(); i++) {
            tableConstraints.add(visitTable_constraint(ctx.table_constraint(i)));
        }
        columnsDeftTableConstraint.setColumnDefs(columnDefs);
        columnsDeftTableConstraint.setTableConstraints(tableConstraints);
        return columnsDeftTableConstraint;
    }

    @Override
    public ColumnsDefOrSelectStmtClass visitColumns_def_or_select_stmt(RulesParser.Columns_def_or_select_stmtContext ctx) {
        System.out.println("Columns Def Or Select Stmt");
        ColumnsDefOrSelectStmtClass columnsDefOrSelectStmt = new ColumnsDefOrSelectStmtClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Columns Def Or Select Stmt"
        );
        if (ctx.select_stmt() != null) {
            columnsDefOrSelectStmt.setSelectStmt(visitSelect_stmt(ctx.select_stmt()));
        } else {
            columnsDefOrSelectStmt.setColumnsDeftTableConstraint(visitColumns_def_tabels_constraint(ctx.columns_def_tabels_constraint()));
        }
        return columnsDefOrSelectStmt;
    }

    @Override
    public InsertStmtValuesClass visitInsert_stmt_values(RulesParser.Insert_stmt_valuesContext ctx) {
        System.out.println("Insert Stmt Values");
        InsertStmtValuesClass insertStmtValues = new InsertStmtValuesClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Insert Stmt Values"
        );
        if (ctx.values_items() != null) {
            insertStmtValues.setValuesItems(visitValues_items(ctx.values_items()));
        } else if (ctx.select_stmt() != null) {
            insertStmtValues.setSelectStmt(visitSelect_stmt(ctx.select_stmt()));
        } else {
            insertStmtValues.setDefaultValues(true);
        }

        return insertStmtValues;
    }

    @Override
    public ColumnNameEqualsExprClass visitColumn_name_equals_expr(RulesParser.Column_name_equals_exprContext ctx) {
        System.out.println("Column Name Equals Expr");
        ColumnNameEqualsExprClass columnNameEqualsExpr = new ColumnNameEqualsExprClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Column Name Equals Expr"

        );
        columnNameEqualsExpr.setColumnName(ctx.column_name().any_name().getText());
        columnNameEqualsExpr.setExpr(visitExpr(ctx.expr()));
        return columnNameEqualsExpr;
    }

    @Override
    public SignedNumberAnyNameClass visitSigned_number_any_name(RulesParser.Signed_number_any_nameContext ctx) {
        System.out.println("Signed Number Any Name");
        SignedNumberAnyNameClass signedNumberAnyName = new SignedNumberAnyNameClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Signed Number Any Name"
        );
        signedNumberAnyName.setSignedNumber(ctx.signed_number().getText());
        if (ctx.any_name() != null) {
            signedNumberAnyName.setAnyName(ctx.any_name().getText());
        }
        return signedNumberAnyName;
    }

    @Override
    public ColumnDefaultContentClass visitColumn_default_content(RulesParser.Column_default_contentContext ctx) {
        System.out.println("Column Default Content");
        ColumnDefaultContentClass columnDefaultContent = new ColumnDefaultContentClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Column Default Content"
        );
        if (ctx.column_default_value() != null) {
            columnDefaultContent.setColumnDefaultValue(visitColumn_default_value(ctx.column_default_value()));
        } else if (ctx.K_NEXTVAL() != null) {
            columnDefaultContent.setNextvalExpr(visitExpr(ctx.expr()));
        } else if (ctx.any_name() != null) {
            columnDefaultContent.setAnyName(ctx.any_name().getText());
        } else {
            columnDefaultContent.setExpr(visitExpr(ctx.expr()));
        }

        return columnDefaultContent;
    }

    @Override
    public ForeignDeferrableClass visitForeign_deferrable(RulesParser.Foreign_deferrableContext ctx) {
        System.out.println("Foreign Deferrable");
        ForeignDeferrableClass foreignDeferrable = new ForeignDeferrableClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Foreign Deferrable"
        );
        if (ctx.K_NOT() != null) {
            foreignDeferrable.setNot(true);
        }
        if (ctx.K_DEFERRABLE() != null) {
            foreignDeferrable.setInitiallyDeferred(true);
        } else if (ctx.K_IMMEDIATE() != null) {
            foreignDeferrable.setInitiallyImmediate(true);
        }
        if (ctx.K_ENABLE() != null) {
            foreignDeferrable.setEnable(true);
        }
        return foreignDeferrable;
    }

    @Override
    public ForeignOn visitForeign_on(RulesParser.Foreign_onContext ctx) {
        System.out.println("Foreign On");
        ForeignOn foreignOn = new ForeignOn(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Foreign On"

        );
        if (ctx.K_DELETE() != null) {
            foreignOn.setDelete(true);
        } else {
            foreignOn.setUpdate(true);
        }
        if (ctx.K_NULL() != null) {
            foreignOn.setSetNull(true);
        } else if (ctx.K_DEFAULT() != null) {
            foreignOn.setSetDefault(true);
        } else if (ctx.K_CASCADE() != null) {
            foreignOn.setCasasde(true);
        } else if (ctx.K_RESTRICT() != null) {
            foreignOn.setRestrict(true);
        } else {
            foreignOn.setNoAction(true);
        }

        return foreignOn;
    }

    @Override
    public ForeignOnOrMatch visitForeign_on_or_match(RulesParser.Foreign_on_or_matchContext ctx) {
        System.out.println("Foreign On Or Match");
        ForeignOnOrMatch foreignOnOrMatch = new ForeignOnOrMatch(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Foreign On Or Match"

        );
        if (ctx.foreign_on() != null) {
            foreignOnOrMatch.setForeignOn(visitForeign_on(ctx.foreign_on()));
        } else {
            foreignOnOrMatch.setMatchName(ctx.name().any_name().getText());
        }
        return foreignOnOrMatch;
    }

    @Override
    public ForeignTableFaildClass visitForeign_table_faild(RulesParser.Foreign_table_faildContext ctx) {
        System.out.println("Foreign Table Field");
        ForeignTableFaildClass foreignTableFaild = new ForeignTableFaildClass(
                ctx.getStart().getLine(),
                ctx.getStart().getCharPositionInLine(),
                "Foreign Table Field"
        );
        if (ctx.database_name() != null) {
            foreignTableFaild.setDatabaseName(ctx.database_name().any_name().getText());
        }
        foreignTableFaild.setForeignTable(ctx.foreign_table().any_name().getText());
        return foreignTableFaild;
    }
}
