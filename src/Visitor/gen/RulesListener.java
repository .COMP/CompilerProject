package Visitor.gen;// Generated from C:/Users/mhmas/Desktop/My File/CompilerProject/src\Rules.g4 by ANTLR 4.8
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link RulesParser}.
 */
public interface RulesListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link RulesParser#parse}.
	 * @param ctx the parse tree
	 */
	void enterParse(RulesParser.ParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#parse}.
	 * @param ctx the parse tree
	 */
	void exitParse(RulesParser.ParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#all_statement_starts}.
	 * @param ctx the parse tree
	 */
	void enterAll_statement_starts(RulesParser.All_statement_startsContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#all_statement_starts}.
	 * @param ctx the parse tree
	 */
	void exitAll_statement_starts(RulesParser.All_statement_startsContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#java_stmt_list}.
	 * @param ctx the parse tree
	 */
	void enterJava_stmt_list(RulesParser.Java_stmt_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#java_stmt_list}.
	 * @param ctx the parse tree
	 */
	void exitJava_stmt_list(RulesParser.Java_stmt_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#create_variable_stmt}.
	 * @param ctx the parse tree
	 */
	void enterCreate_variable_stmt(RulesParser.Create_variable_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#create_variable_stmt}.
	 * @param ctx the parse tree
	 */
	void exitCreate_variable_stmt(RulesParser.Create_variable_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#post_square}.
	 * @param ctx the parse tree
	 */
	void enterPost_square(RulesParser.Post_squareContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#post_square}.
	 * @param ctx the parse tree
	 */
	void exitPost_square(RulesParser.Post_squareContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#previous_square}.
	 * @param ctx the parse tree
	 */
	void enterPrevious_square(RulesParser.Previous_squareContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#previous_square}.
	 * @param ctx the parse tree
	 */
	void exitPrevious_square(RulesParser.Previous_squareContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#assign_variable_in_creation}.
	 * @param ctx the parse tree
	 */
	void enterAssign_variable_in_creation(RulesParser.Assign_variable_in_creationContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#assign_variable_in_creation}.
	 * @param ctx the parse tree
	 */
	void exitAssign_variable_in_creation(RulesParser.Assign_variable_in_creationContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#assign_variable}.
	 * @param ctx the parse tree
	 */
	void enterAssign_variable(RulesParser.Assign_variableContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#assign_variable}.
	 * @param ctx the parse tree
	 */
	void exitAssign_variable(RulesParser.Assign_variableContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#assign_array}.
	 * @param ctx the parse tree
	 */
	void enterAssign_array(RulesParser.Assign_arrayContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#assign_array}.
	 * @param ctx the parse tree
	 */
	void exitAssign_array(RulesParser.Assign_arrayContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#assign_array_variable}.
	 * @param ctx the parse tree
	 */
	void enterAssign_array_variable(RulesParser.Assign_array_variableContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#assign_array_variable}.
	 * @param ctx the parse tree
	 */
	void exitAssign_array_variable(RulesParser.Assign_array_variableContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#assign_variable_without_var}.
	 * @param ctx the parse tree
	 */
	void enterAssign_variable_without_var(RulesParser.Assign_variable_without_varContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#assign_variable_without_var}.
	 * @param ctx the parse tree
	 */
	void exitAssign_variable_without_var(RulesParser.Assign_variable_without_varContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#direct_array_values}.
	 * @param ctx the parse tree
	 */
	void enterDirect_array_values(RulesParser.Direct_array_valuesContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#direct_array_values}.
	 * @param ctx the parse tree
	 */
	void exitDirect_array_values(RulesParser.Direct_array_valuesContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#var_name_item_in_array}.
	 * @param ctx the parse tree
	 */
	void enterVar_name_item_in_array(RulesParser.Var_name_item_in_arrayContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#var_name_item_in_array}.
	 * @param ctx the parse tree
	 */
	void exitVar_name_item_in_array(RulesParser.Var_name_item_in_arrayContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#value_of_one_item_array}.
	 * @param ctx the parse tree
	 */
	void enterValue_of_one_item_array(RulesParser.Value_of_one_item_arrayContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#value_of_one_item_array}.
	 * @param ctx the parse tree
	 */
	void exitValue_of_one_item_array(RulesParser.Value_of_one_item_arrayContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#variable_name}.
	 * @param ctx the parse tree
	 */
	void enterVariable_name(RulesParser.Variable_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#variable_name}.
	 * @param ctx the parse tree
	 */
	void exitVariable_name(RulesParser.Variable_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#array_value}.
	 * @param ctx the parse tree
	 */
	void enterArray_value(RulesParser.Array_valueContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#array_value}.
	 * @param ctx the parse tree
	 */
	void exitArray_value(RulesParser.Array_valueContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#variable_value}.
	 * @param ctx the parse tree
	 */
	void enterVariable_value(RulesParser.Variable_valueContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#variable_value}.
	 * @param ctx the parse tree
	 */
	void exitVariable_value(RulesParser.Variable_valueContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#value_in_quote}.
	 * @param ctx the parse tree
	 */
	void enterValue_in_quote(RulesParser.Value_in_quoteContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#value_in_quote}.
	 * @param ctx the parse tree
	 */
	void exitValue_in_quote(RulesParser.Value_in_quoteContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#default_value_parameter}.
	 * @param ctx the parse tree
	 */
	void enterDefault_value_parameter(RulesParser.Default_value_parameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#default_value_parameter}.
	 * @param ctx the parse tree
	 */
	void exitDefault_value_parameter(RulesParser.Default_value_parameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#initial_variable_declaration}.
	 * @param ctx the parse tree
	 */
	void enterInitial_variable_declaration(RulesParser.Initial_variable_declarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#initial_variable_declaration}.
	 * @param ctx the parse tree
	 */
	void exitInitial_variable_declaration(RulesParser.Initial_variable_declarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#prototype_function}.
	 * @param ctx the parse tree
	 */
	void enterPrototype_function(RulesParser.Prototype_functionContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#prototype_function}.
	 * @param ctx the parse tree
	 */
	void exitPrototype_function(RulesParser.Prototype_functionContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#prototype_function_parameter}.
	 * @param ctx the parse tree
	 */
	void enterPrototype_function_parameter(RulesParser.Prototype_function_parameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#prototype_function_parameter}.
	 * @param ctx the parse tree
	 */
	void exitPrototype_function_parameter(RulesParser.Prototype_function_parameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#function_parameter_in_prototype}.
	 * @param ctx the parse tree
	 */
	void enterFunction_parameter_in_prototype(RulesParser.Function_parameter_in_prototypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#function_parameter_in_prototype}.
	 * @param ctx the parse tree
	 */
	void exitFunction_parameter_in_prototype(RulesParser.Function_parameter_in_prototypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#function_parameter_with_default}.
	 * @param ctx the parse tree
	 */
	void enterFunction_parameter_with_default(RulesParser.Function_parameter_with_defaultContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#function_parameter_with_default}.
	 * @param ctx the parse tree
	 */
	void exitFunction_parameter_with_default(RulesParser.Function_parameter_with_defaultContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#initial_variable_declaration_with_default_value}.
	 * @param ctx the parse tree
	 */
	void enterInitial_variable_declaration_with_default_value(RulesParser.Initial_variable_declaration_with_default_valueContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#initial_variable_declaration_with_default_value}.
	 * @param ctx the parse tree
	 */
	void exitInitial_variable_declaration_with_default_value(RulesParser.Initial_variable_declaration_with_default_valueContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#body_function}.
	 * @param ctx the parse tree
	 */
	void enterBody_function(RulesParser.Body_functionContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#body_function}.
	 * @param ctx the parse tree
	 */
	void exitBody_function(RulesParser.Body_functionContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#body_prototype_function}.
	 * @param ctx the parse tree
	 */
	void enterBody_prototype_function(RulesParser.Body_prototype_functionContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#body_prototype_function}.
	 * @param ctx the parse tree
	 */
	void exitBody_prototype_function(RulesParser.Body_prototype_functionContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#java_stmt}.
	 * @param ctx the parse tree
	 */
	void enterJava_stmt(RulesParser.Java_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#java_stmt}.
	 * @param ctx the parse tree
	 */
	void exitJava_stmt(RulesParser.Java_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#full_condition_java}.
	 * @param ctx the parse tree
	 */
	void enterFull_condition_java(RulesParser.Full_condition_javaContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#full_condition_java}.
	 * @param ctx the parse tree
	 */
	void exitFull_condition_java(RulesParser.Full_condition_javaContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#multi_condition_java}.
	 * @param ctx the parse tree
	 */
	void enterMulti_condition_java(RulesParser.Multi_condition_javaContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#multi_condition_java}.
	 * @param ctx the parse tree
	 */
	void exitMulti_condition_java(RulesParser.Multi_condition_javaContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#header_if}.
	 * @param ctx the parse tree
	 */
	void enterHeader_if(RulesParser.Header_ifContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#header_if}.
	 * @param ctx the parse tree
	 */
	void exitHeader_if(RulesParser.Header_ifContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#one_condition_in_java}.
	 * @param ctx the parse tree
	 */
	void enterOne_condition_in_java(RulesParser.One_condition_in_javaContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#one_condition_in_java}.
	 * @param ctx the parse tree
	 */
	void exitOne_condition_in_java(RulesParser.One_condition_in_javaContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#not_var_name}.
	 * @param ctx the parse tree
	 */
	void enterNot_var_name(RulesParser.Not_var_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#not_var_name}.
	 * @param ctx the parse tree
	 */
	void exitNot_var_name(RulesParser.Not_var_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#not_one_condition}.
	 * @param ctx the parse tree
	 */
	void enterNot_one_condition(RulesParser.Not_one_conditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#not_one_condition}.
	 * @param ctx the parse tree
	 */
	void exitNot_one_condition(RulesParser.Not_one_conditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#number_condition}.
	 * @param ctx the parse tree
	 */
	void enterNumber_condition(RulesParser.Number_conditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#number_condition}.
	 * @param ctx the parse tree
	 */
	void exitNumber_condition(RulesParser.Number_conditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#logical_condition}.
	 * @param ctx the parse tree
	 */
	void enterLogical_condition(RulesParser.Logical_conditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#logical_condition}.
	 * @param ctx the parse tree
	 */
	void exitLogical_condition(RulesParser.Logical_conditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#expr_java}.
	 * @param ctx the parse tree
	 */
	void enterExpr_java(RulesParser.Expr_javaContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#expr_java}.
	 * @param ctx the parse tree
	 */
	void exitExpr_java(RulesParser.Expr_javaContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#number_value_java}.
	 * @param ctx the parse tree
	 */
	void enterNumber_value_java(RulesParser.Number_value_javaContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#number_value_java}.
	 * @param ctx the parse tree
	 */
	void exitNumber_value_java(RulesParser.Number_value_javaContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#logic_operation_statement_in_java}.
	 * @param ctx the parse tree
	 */
	void enterLogic_operation_statement_in_java(RulesParser.Logic_operation_statement_in_javaContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#logic_operation_statement_in_java}.
	 * @param ctx the parse tree
	 */
	void exitLogic_operation_statement_in_java(RulesParser.Logic_operation_statement_in_javaContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#operation_in_java}.
	 * @param ctx the parse tree
	 */
	void enterOperation_in_java(RulesParser.Operation_in_javaContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#operation_in_java}.
	 * @param ctx the parse tree
	 */
	void exitOperation_in_java(RulesParser.Operation_in_javaContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#comparison_in_java}.
	 * @param ctx the parse tree
	 */
	void enterComparison_in_java(RulesParser.Comparison_in_javaContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#comparison_in_java}.
	 * @param ctx the parse tree
	 */
	void exitComparison_in_java(RulesParser.Comparison_in_javaContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#logical_in_java}.
	 * @param ctx the parse tree
	 */
	void enterLogical_in_java(RulesParser.Logical_in_javaContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#logical_in_java}.
	 * @param ctx the parse tree
	 */
	void exitLogical_in_java(RulesParser.Logical_in_javaContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#line_condition__expression_java}.
	 * @param ctx the parse tree
	 */
	void enterLine_condition__expression_java(RulesParser.Line_condition__expression_javaContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#line_condition__expression_java}.
	 * @param ctx the parse tree
	 */
	void exitLine_condition__expression_java(RulesParser.Line_condition__expression_javaContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#line_condition_result}.
	 * @param ctx the parse tree
	 */
	void enterLine_condition_result(RulesParser.Line_condition_resultContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#line_condition_result}.
	 * @param ctx the parse tree
	 */
	void exitLine_condition_result(RulesParser.Line_condition_resultContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#if_statement}.
	 * @param ctx the parse tree
	 */
	void enterIf_statement(RulesParser.If_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#if_statement}.
	 * @param ctx the parse tree
	 */
	void exitIf_statement(RulesParser.If_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#else_if_statement}.
	 * @param ctx the parse tree
	 */
	void enterElse_if_statement(RulesParser.Else_if_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#else_if_statement}.
	 * @param ctx the parse tree
	 */
	void exitElse_if_statement(RulesParser.Else_if_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#else_statement}.
	 * @param ctx the parse tree
	 */
	void enterElse_statement(RulesParser.Else_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#else_statement}.
	 * @param ctx the parse tree
	 */
	void exitElse_statement(RulesParser.Else_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#switch_statement}.
	 * @param ctx the parse tree
	 */
	void enterSwitch_statement(RulesParser.Switch_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#switch_statement}.
	 * @param ctx the parse tree
	 */
	void exitSwitch_statement(RulesParser.Switch_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#case_statenemt}.
	 * @param ctx the parse tree
	 */
	void enterCase_statenemt(RulesParser.Case_statenemtContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#case_statenemt}.
	 * @param ctx the parse tree
	 */
	void exitCase_statenemt(RulesParser.Case_statenemtContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#case_value}.
	 * @param ctx the parse tree
	 */
	void enterCase_value(RulesParser.Case_valueContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#case_value}.
	 * @param ctx the parse tree
	 */
	void exitCase_value(RulesParser.Case_valueContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#default_statement}.
	 * @param ctx the parse tree
	 */
	void enterDefault_statement(RulesParser.Default_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#default_statement}.
	 * @param ctx the parse tree
	 */
	void exitDefault_statement(RulesParser.Default_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#body_for_case_and_default}.
	 * @param ctx the parse tree
	 */
	void enterBody_for_case_and_default(RulesParser.Body_for_case_and_defaultContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#body_for_case_and_default}.
	 * @param ctx the parse tree
	 */
	void exitBody_for_case_and_default(RulesParser.Body_for_case_and_defaultContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#for_stmt}.
	 * @param ctx the parse tree
	 */
	void enterFor_stmt(RulesParser.For_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#for_stmt}.
	 * @param ctx the parse tree
	 */
	void exitFor_stmt(RulesParser.For_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#for_parameter}.
	 * @param ctx the parse tree
	 */
	void enterFor_parameter(RulesParser.For_parameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#for_parameter}.
	 * @param ctx the parse tree
	 */
	void exitFor_parameter(RulesParser.For_parameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#initial_start_count_in_for}.
	 * @param ctx the parse tree
	 */
	void enterInitial_start_count_in_for(RulesParser.Initial_start_count_in_forContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#initial_start_count_in_for}.
	 * @param ctx the parse tree
	 */
	void exitInitial_start_count_in_for(RulesParser.Initial_start_count_in_forContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#condition_for}.
	 * @param ctx the parse tree
	 */
	void enterCondition_for(RulesParser.Condition_forContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#condition_for}.
	 * @param ctx the parse tree
	 */
	void exitCondition_for(RulesParser.Condition_forContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#advanced_increase}.
	 * @param ctx the parse tree
	 */
	void enterAdvanced_increase(RulesParser.Advanced_increaseContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#advanced_increase}.
	 * @param ctx the parse tree
	 */
	void exitAdvanced_increase(RulesParser.Advanced_increaseContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#delayed_increase}.
	 * @param ctx the parse tree
	 */
	void enterDelayed_increase(RulesParser.Delayed_increaseContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#delayed_increase}.
	 * @param ctx the parse tree
	 */
	void exitDelayed_increase(RulesParser.Delayed_increaseContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#specific_increase}.
	 * @param ctx the parse tree
	 */
	void enterSpecific_increase(RulesParser.Specific_increaseContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#specific_increase}.
	 * @param ctx the parse tree
	 */
	void exitSpecific_increase(RulesParser.Specific_increaseContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#increment_type}.
	 * @param ctx the parse tree
	 */
	void enterIncrement_type(RulesParser.Increment_typeContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#increment_type}.
	 * @param ctx the parse tree
	 */
	void exitIncrement_type(RulesParser.Increment_typeContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#increment_count_for}.
	 * @param ctx the parse tree
	 */
	void enterIncrement_count_for(RulesParser.Increment_count_forContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#increment_count_for}.
	 * @param ctx the parse tree
	 */
	void exitIncrement_count_for(RulesParser.Increment_count_forContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#increment_stmt}.
	 * @param ctx the parse tree
	 */
	void enterIncrement_stmt(RulesParser.Increment_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#increment_stmt}.
	 * @param ctx the parse tree
	 */
	void exitIncrement_stmt(RulesParser.Increment_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#while_statement}.
	 * @param ctx the parse tree
	 */
	void enterWhile_statement(RulesParser.While_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#while_statement}.
	 * @param ctx the parse tree
	 */
	void exitWhile_statement(RulesParser.While_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#do_while_statement}.
	 * @param ctx the parse tree
	 */
	void enterDo_while_statement(RulesParser.Do_while_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#do_while_statement}.
	 * @param ctx the parse tree
	 */
	void exitDo_while_statement(RulesParser.Do_while_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#callback_function}.
	 * @param ctx the parse tree
	 */
	void enterCallback_function(RulesParser.Callback_functionContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#callback_function}.
	 * @param ctx the parse tree
	 */
	void exitCallback_function(RulesParser.Callback_functionContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#callback_function_without_scol}.
	 * @param ctx the parse tree
	 */
	void enterCallback_function_without_scol(RulesParser.Callback_function_without_scolContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#callback_function_without_scol}.
	 * @param ctx the parse tree
	 */
	void exitCallback_function_without_scol(RulesParser.Callback_function_without_scolContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#callback_function_heder}.
	 * @param ctx the parse tree
	 */
	void enterCallback_function_heder(RulesParser.Callback_function_hederContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#callback_function_heder}.
	 * @param ctx the parse tree
	 */
	void exitCallback_function_heder(RulesParser.Callback_function_hederContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#callback_function_parameter}.
	 * @param ctx the parse tree
	 */
	void enterCallback_function_parameter(RulesParser.Callback_function_parameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#callback_function_parameter}.
	 * @param ctx the parse tree
	 */
	void exitCallback_function_parameter(RulesParser.Callback_function_parameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#callback_function_parameter_in_header}.
	 * @param ctx the parse tree
	 */
	void enterCallback_function_parameter_in_header(RulesParser.Callback_function_parameter_in_headerContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#callback_function_parameter_in_header}.
	 * @param ctx the parse tree
	 */
	void exitCallback_function_parameter_in_header(RulesParser.Callback_function_parameter_in_headerContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#function_parameter}.
	 * @param ctx the parse tree
	 */
	void enterFunction_parameter(RulesParser.Function_parameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#function_parameter}.
	 * @param ctx the parse tree
	 */
	void exitFunction_parameter(RulesParser.Function_parameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#var_name_in_parameter}.
	 * @param ctx the parse tree
	 */
	void enterVar_name_in_parameter(RulesParser.Var_name_in_parameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#var_name_in_parameter}.
	 * @param ctx the parse tree
	 */
	void exitVar_name_in_parameter(RulesParser.Var_name_in_parameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#return_stmt}.
	 * @param ctx the parse tree
	 */
	void enterReturn_stmt(RulesParser.Return_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#return_stmt}.
	 * @param ctx the parse tree
	 */
	void exitReturn_stmt(RulesParser.Return_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#json_declaration}.
	 * @param ctx the parse tree
	 */
	void enterJson_declaration(RulesParser.Json_declarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#json_declaration}.
	 * @param ctx the parse tree
	 */
	void exitJson_declaration(RulesParser.Json_declarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#all_fileds_in_json}.
	 * @param ctx the parse tree
	 */
	void enterAll_fileds_in_json(RulesParser.All_fileds_in_jsonContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#all_fileds_in_json}.
	 * @param ctx the parse tree
	 */
	void exitAll_fileds_in_json(RulesParser.All_fileds_in_jsonContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#filed_in_json}.
	 * @param ctx the parse tree
	 */
	void enterFiled_in_json(RulesParser.Filed_in_jsonContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#filed_in_json}.
	 * @param ctx the parse tree
	 */
	void exitFiled_in_json(RulesParser.Filed_in_jsonContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#key_filed}.
	 * @param ctx the parse tree
	 */
	void enterKey_filed(RulesParser.Key_filedContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#key_filed}.
	 * @param ctx the parse tree
	 */
	void exitKey_filed(RulesParser.Key_filedContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#array_json_declaration}.
	 * @param ctx the parse tree
	 */
	void enterArray_json_declaration(RulesParser.Array_json_declarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#array_json_declaration}.
	 * @param ctx the parse tree
	 */
	void exitArray_json_declaration(RulesParser.Array_json_declarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#access_to_data_in_json}.
	 * @param ctx the parse tree
	 */
	void enterAccess_to_data_in_json(RulesParser.Access_to_data_in_jsonContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#access_to_data_in_json}.
	 * @param ctx the parse tree
	 */
	void exitAccess_to_data_in_json(RulesParser.Access_to_data_in_jsonContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#foreach_statement}.
	 * @param ctx the parse tree
	 */
	void enterForeach_statement(RulesParser.Foreach_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#foreach_statement}.
	 * @param ctx the parse tree
	 */
	void exitForeach_statement(RulesParser.Foreach_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#variable_in_foreach}.
	 * @param ctx the parse tree
	 */
	void enterVariable_in_foreach(RulesParser.Variable_in_foreachContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#variable_in_foreach}.
	 * @param ctx the parse tree
	 */
	void exitVariable_in_foreach(RulesParser.Variable_in_foreachContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#print_statement}.
	 * @param ctx the parse tree
	 */
	void enterPrint_statement(RulesParser.Print_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#print_statement}.
	 * @param ctx the parse tree
	 */
	void exitPrint_statement(RulesParser.Print_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#error}.
	 * @param ctx the parse tree
	 */
	void enterError(RulesParser.ErrorContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#error}.
	 * @param ctx the parse tree
	 */
	void exitError(RulesParser.ErrorContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#sql_stmt_list}.
	 * @param ctx the parse tree
	 */
	void enterSql_stmt_list(RulesParser.Sql_stmt_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#sql_stmt_list}.
	 * @param ctx the parse tree
	 */
	void exitSql_stmt_list(RulesParser.Sql_stmt_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#sql_stmt}.
	 * @param ctx the parse tree
	 */
	void enterSql_stmt(RulesParser.Sql_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#sql_stmt}.
	 * @param ctx the parse tree
	 */
	void exitSql_stmt(RulesParser.Sql_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#alter_table_stmt}.
	 * @param ctx the parse tree
	 */
	void enterAlter_table_stmt(RulesParser.Alter_table_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#alter_table_stmt}.
	 * @param ctx the parse tree
	 */
	void exitAlter_table_stmt(RulesParser.Alter_table_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#alter_table_add_constraint}.
	 * @param ctx the parse tree
	 */
	void enterAlter_table_add_constraint(RulesParser.Alter_table_add_constraintContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#alter_table_add_constraint}.
	 * @param ctx the parse tree
	 */
	void exitAlter_table_add_constraint(RulesParser.Alter_table_add_constraintContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#alter_table_add}.
	 * @param ctx the parse tree
	 */
	void enterAlter_table_add(RulesParser.Alter_table_addContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#alter_table_add}.
	 * @param ctx the parse tree
	 */
	void exitAlter_table_add(RulesParser.Alter_table_addContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#create_table_stmt}.
	 * @param ctx the parse tree
	 */
	void enterCreate_table_stmt(RulesParser.Create_table_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#create_table_stmt}.
	 * @param ctx the parse tree
	 */
	void exitCreate_table_stmt(RulesParser.Create_table_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#columns_definition}.
	 * @param ctx the parse tree
	 */
	void enterColumns_definition(RulesParser.Columns_definitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#columns_definition}.
	 * @param ctx the parse tree
	 */
	void exitColumns_definition(RulesParser.Columns_definitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#column_definition}.
	 * @param ctx the parse tree
	 */
	void enterColumn_definition(RulesParser.Column_definitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#column_definition}.
	 * @param ctx the parse tree
	 */
	void exitColumn_definition(RulesParser.Column_definitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#create_table_stmt_type}.
	 * @param ctx the parse tree
	 */
	void enterCreate_table_stmt_type(RulesParser.Create_table_stmt_typeContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#create_table_stmt_type}.
	 * @param ctx the parse tree
	 */
	void exitCreate_table_stmt_type(RulesParser.Create_table_stmt_typeContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#type_faild}.
	 * @param ctx the parse tree
	 */
	void enterType_faild(RulesParser.Type_faildContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#type_faild}.
	 * @param ctx the parse tree
	 */
	void exitType_faild(RulesParser.Type_faildContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#create_table_stmt_path}.
	 * @param ctx the parse tree
	 */
	void enterCreate_table_stmt_path(RulesParser.Create_table_stmt_pathContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#create_table_stmt_path}.
	 * @param ctx the parse tree
	 */
	void exitCreate_table_stmt_path(RulesParser.Create_table_stmt_pathContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#create_type_stmt}.
	 * @param ctx the parse tree
	 */
	void enterCreate_type_stmt(RulesParser.Create_type_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#create_type_stmt}.
	 * @param ctx the parse tree
	 */
	void exitCreate_type_stmt(RulesParser.Create_type_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#create_aggregation_function}.
	 * @param ctx the parse tree
	 */
	void enterCreate_aggregation_function(RulesParser.Create_aggregation_functionContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#create_aggregation_function}.
	 * @param ctx the parse tree
	 */
	void exitCreate_aggregation_function(RulesParser.Create_aggregation_functionContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#create_aggregation_function_elements}.
	 * @param ctx the parse tree
	 */
	void enterCreate_aggregation_function_elements(RulesParser.Create_aggregation_function_elementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#create_aggregation_function_elements}.
	 * @param ctx the parse tree
	 */
	void exitCreate_aggregation_function_elements(RulesParser.Create_aggregation_function_elementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#return_type_array}.
	 * @param ctx the parse tree
	 */
	void enterReturn_type_array(RulesParser.Return_type_arrayContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#return_type_array}.
	 * @param ctx the parse tree
	 */
	void exitReturn_type_array(RulesParser.Return_type_arrayContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#return_type}.
	 * @param ctx the parse tree
	 */
	void enterReturn_type(RulesParser.Return_typeContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#return_type}.
	 * @param ctx the parse tree
	 */
	void exitReturn_type(RulesParser.Return_typeContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#class_name}.
	 * @param ctx the parse tree
	 */
	void enterClass_name(RulesParser.Class_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#class_name}.
	 * @param ctx the parse tree
	 */
	void exitClass_name(RulesParser.Class_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#method_name}.
	 * @param ctx the parse tree
	 */
	void enterMethod_name(RulesParser.Method_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#method_name}.
	 * @param ctx the parse tree
	 */
	void exitMethod_name(RulesParser.Method_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#path_string}.
	 * @param ctx the parse tree
	 */
	void enterPath_string(RulesParser.Path_stringContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#path_string}.
	 * @param ctx the parse tree
	 */
	void exitPath_string(RulesParser.Path_stringContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#any_name_with_quote}.
	 * @param ctx the parse tree
	 */
	void enterAny_name_with_quote(RulesParser.Any_name_with_quoteContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#any_name_with_quote}.
	 * @param ctx the parse tree
	 */
	void exitAny_name_with_quote(RulesParser.Any_name_with_quoteContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#columns_def_tabels_constraint}.
	 * @param ctx the parse tree
	 */
	void enterColumns_def_tabels_constraint(RulesParser.Columns_def_tabels_constraintContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#columns_def_tabels_constraint}.
	 * @param ctx the parse tree
	 */
	void exitColumns_def_tabels_constraint(RulesParser.Columns_def_tabels_constraintContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#columns_def_or_select_stmt}.
	 * @param ctx the parse tree
	 */
	void enterColumns_def_or_select_stmt(RulesParser.Columns_def_or_select_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#columns_def_or_select_stmt}.
	 * @param ctx the parse tree
	 */
	void exitColumns_def_or_select_stmt(RulesParser.Columns_def_or_select_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#delete_stmt}.
	 * @param ctx the parse tree
	 */
	void enterDelete_stmt(RulesParser.Delete_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#delete_stmt}.
	 * @param ctx the parse tree
	 */
	void exitDelete_stmt(RulesParser.Delete_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#drop_table_stmt}.
	 * @param ctx the parse tree
	 */
	void enterDrop_table_stmt(RulesParser.Drop_table_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#drop_table_stmt}.
	 * @param ctx the parse tree
	 */
	void exitDrop_table_stmt(RulesParser.Drop_table_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#factored_select_stmt}.
	 * @param ctx the parse tree
	 */
	void enterFactored_select_stmt(RulesParser.Factored_select_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#factored_select_stmt}.
	 * @param ctx the parse tree
	 */
	void exitFactored_select_stmt(RulesParser.Factored_select_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#order_by}.
	 * @param ctx the parse tree
	 */
	void enterOrder_by(RulesParser.Order_byContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#order_by}.
	 * @param ctx the parse tree
	 */
	void exitOrder_by(RulesParser.Order_byContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#limit_select}.
	 * @param ctx the parse tree
	 */
	void enterLimit_select(RulesParser.Limit_selectContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#limit_select}.
	 * @param ctx the parse tree
	 */
	void exitLimit_select(RulesParser.Limit_selectContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#insert_stmt}.
	 * @param ctx the parse tree
	 */
	void enterInsert_stmt(RulesParser.Insert_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#insert_stmt}.
	 * @param ctx the parse tree
	 */
	void exitInsert_stmt(RulesParser.Insert_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#insert_stmt_values}.
	 * @param ctx the parse tree
	 */
	void enterInsert_stmt_values(RulesParser.Insert_stmt_valuesContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#insert_stmt_values}.
	 * @param ctx the parse tree
	 */
	void exitInsert_stmt_values(RulesParser.Insert_stmt_valuesContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#select_stmt}.
	 * @param ctx the parse tree
	 */
	void enterSelect_stmt(RulesParser.Select_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#select_stmt}.
	 * @param ctx the parse tree
	 */
	void exitSelect_stmt(RulesParser.Select_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#update_stmt}.
	 * @param ctx the parse tree
	 */
	void enterUpdate_stmt(RulesParser.Update_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#update_stmt}.
	 * @param ctx the parse tree
	 */
	void exitUpdate_stmt(RulesParser.Update_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#column_name_equals_expr}.
	 * @param ctx the parse tree
	 */
	void enterColumn_name_equals_expr(RulesParser.Column_name_equals_exprContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#column_name_equals_expr}.
	 * @param ctx the parse tree
	 */
	void exitColumn_name_equals_expr(RulesParser.Column_name_equals_exprContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#column_def}.
	 * @param ctx the parse tree
	 */
	void enterColumn_def(RulesParser.Column_defContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#column_def}.
	 * @param ctx the parse tree
	 */
	void exitColumn_def(RulesParser.Column_defContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#column_constraint_or_type_name}.
	 * @param ctx the parse tree
	 */
	void enterColumn_constraint_or_type_name(RulesParser.Column_constraint_or_type_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#column_constraint_or_type_name}.
	 * @param ctx the parse tree
	 */
	void exitColumn_constraint_or_type_name(RulesParser.Column_constraint_or_type_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#type_name}.
	 * @param ctx the parse tree
	 */
	void enterType_name(RulesParser.Type_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#type_name}.
	 * @param ctx the parse tree
	 */
	void exitType_name(RulesParser.Type_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#signed_number_any_name}.
	 * @param ctx the parse tree
	 */
	void enterSigned_number_any_name(RulesParser.Signed_number_any_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#signed_number_any_name}.
	 * @param ctx the parse tree
	 */
	void exitSigned_number_any_name(RulesParser.Signed_number_any_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#column_constraint}.
	 * @param ctx the parse tree
	 */
	void enterColumn_constraint(RulesParser.Column_constraintContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#column_constraint}.
	 * @param ctx the parse tree
	 */
	void exitColumn_constraint(RulesParser.Column_constraintContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#column_constraint_primary_key}.
	 * @param ctx the parse tree
	 */
	void enterColumn_constraint_primary_key(RulesParser.Column_constraint_primary_keyContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#column_constraint_primary_key}.
	 * @param ctx the parse tree
	 */
	void exitColumn_constraint_primary_key(RulesParser.Column_constraint_primary_keyContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#column_constraint_foreign_key}.
	 * @param ctx the parse tree
	 */
	void enterColumn_constraint_foreign_key(RulesParser.Column_constraint_foreign_keyContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#column_constraint_foreign_key}.
	 * @param ctx the parse tree
	 */
	void exitColumn_constraint_foreign_key(RulesParser.Column_constraint_foreign_keyContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#column_constraint_not_null}.
	 * @param ctx the parse tree
	 */
	void enterColumn_constraint_not_null(RulesParser.Column_constraint_not_nullContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#column_constraint_not_null}.
	 * @param ctx the parse tree
	 */
	void exitColumn_constraint_not_null(RulesParser.Column_constraint_not_nullContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#column_constraint_null}.
	 * @param ctx the parse tree
	 */
	void enterColumn_constraint_null(RulesParser.Column_constraint_nullContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#column_constraint_null}.
	 * @param ctx the parse tree
	 */
	void exitColumn_constraint_null(RulesParser.Column_constraint_nullContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#column_default}.
	 * @param ctx the parse tree
	 */
	void enterColumn_default(RulesParser.Column_defaultContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#column_default}.
	 * @param ctx the parse tree
	 */
	void exitColumn_default(RulesParser.Column_defaultContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#column_default_content}.
	 * @param ctx the parse tree
	 */
	void enterColumn_default_content(RulesParser.Column_default_contentContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#column_default_content}.
	 * @param ctx the parse tree
	 */
	void exitColumn_default_content(RulesParser.Column_default_contentContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#column_default_value}.
	 * @param ctx the parse tree
	 */
	void enterColumn_default_value(RulesParser.Column_default_valueContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#column_default_value}.
	 * @param ctx the parse tree
	 */
	void exitColumn_default_value(RulesParser.Column_default_valueContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(RulesParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(RulesParser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#exists_case}.
	 * @param ctx the parse tree
	 */
	void enterExists_case(RulesParser.Exists_caseContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#exists_case}.
	 * @param ctx the parse tree
	 */
	void exitExists_case(RulesParser.Exists_caseContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#function_expr}.
	 * @param ctx the parse tree
	 */
	void enterFunction_expr(RulesParser.Function_exprContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#function_expr}.
	 * @param ctx the parse tree
	 */
	void exitFunction_expr(RulesParser.Function_exprContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#column_name_faild}.
	 * @param ctx the parse tree
	 */
	void enterColumn_name_faild(RulesParser.Column_name_faildContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#column_name_faild}.
	 * @param ctx the parse tree
	 */
	void exitColumn_name_faild(RulesParser.Column_name_faildContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#table_name_faild}.
	 * @param ctx the parse tree
	 */
	void enterTable_name_faild(RulesParser.Table_name_faildContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#table_name_faild}.
	 * @param ctx the parse tree
	 */
	void exitTable_name_faild(RulesParser.Table_name_faildContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#select_or_expr}.
	 * @param ctx the parse tree
	 */
	void enterSelect_or_expr(RulesParser.Select_or_exprContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#select_or_expr}.
	 * @param ctx the parse tree
	 */
	void exitSelect_or_expr(RulesParser.Select_or_exprContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#in_case}.
	 * @param ctx the parse tree
	 */
	void enterIn_case(RulesParser.In_caseContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#in_case}.
	 * @param ctx the parse tree
	 */
	void exitIn_case(RulesParser.In_caseContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#foreign_key_clause}.
	 * @param ctx the parse tree
	 */
	void enterForeign_key_clause(RulesParser.Foreign_key_clauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#foreign_key_clause}.
	 * @param ctx the parse tree
	 */
	void exitForeign_key_clause(RulesParser.Foreign_key_clauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#foreign_deferrable}.
	 * @param ctx the parse tree
	 */
	void enterForeign_deferrable(RulesParser.Foreign_deferrableContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#foreign_deferrable}.
	 * @param ctx the parse tree
	 */
	void exitForeign_deferrable(RulesParser.Foreign_deferrableContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#foreign_on}.
	 * @param ctx the parse tree
	 */
	void enterForeign_on(RulesParser.Foreign_onContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#foreign_on}.
	 * @param ctx the parse tree
	 */
	void exitForeign_on(RulesParser.Foreign_onContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#foreign_on_or_match}.
	 * @param ctx the parse tree
	 */
	void enterForeign_on_or_match(RulesParser.Foreign_on_or_matchContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#foreign_on_or_match}.
	 * @param ctx the parse tree
	 */
	void exitForeign_on_or_match(RulesParser.Foreign_on_or_matchContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#foreign_table_faild}.
	 * @param ctx the parse tree
	 */
	void enterForeign_table_faild(RulesParser.Foreign_table_faildContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#foreign_table_faild}.
	 * @param ctx the parse tree
	 */
	void exitForeign_table_faild(RulesParser.Foreign_table_faildContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#fk_target_column_name}.
	 * @param ctx the parse tree
	 */
	void enterFk_target_column_name(RulesParser.Fk_target_column_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#fk_target_column_name}.
	 * @param ctx the parse tree
	 */
	void exitFk_target_column_name(RulesParser.Fk_target_column_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#indexed_column}.
	 * @param ctx the parse tree
	 */
	void enterIndexed_column(RulesParser.Indexed_columnContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#indexed_column}.
	 * @param ctx the parse tree
	 */
	void exitIndexed_column(RulesParser.Indexed_columnContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#table_constraint}.
	 * @param ctx the parse tree
	 */
	void enterTable_constraint(RulesParser.Table_constraintContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#table_constraint}.
	 * @param ctx the parse tree
	 */
	void exitTable_constraint(RulesParser.Table_constraintContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#table_constraint_primary_key}.
	 * @param ctx the parse tree
	 */
	void enterTable_constraint_primary_key(RulesParser.Table_constraint_primary_keyContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#table_constraint_primary_key}.
	 * @param ctx the parse tree
	 */
	void exitTable_constraint_primary_key(RulesParser.Table_constraint_primary_keyContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#table_constraint_foreign_key}.
	 * @param ctx the parse tree
	 */
	void enterTable_constraint_foreign_key(RulesParser.Table_constraint_foreign_keyContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#table_constraint_foreign_key}.
	 * @param ctx the parse tree
	 */
	void exitTable_constraint_foreign_key(RulesParser.Table_constraint_foreign_keyContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#table_constraint_unique}.
	 * @param ctx the parse tree
	 */
	void enterTable_constraint_unique(RulesParser.Table_constraint_uniqueContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#table_constraint_unique}.
	 * @param ctx the parse tree
	 */
	void exitTable_constraint_unique(RulesParser.Table_constraint_uniqueContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#table_constraint_key}.
	 * @param ctx the parse tree
	 */
	void enterTable_constraint_key(RulesParser.Table_constraint_keyContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#table_constraint_key}.
	 * @param ctx the parse tree
	 */
	void exitTable_constraint_key(RulesParser.Table_constraint_keyContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#fk_origin_column_name}.
	 * @param ctx the parse tree
	 */
	void enterFk_origin_column_name(RulesParser.Fk_origin_column_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#fk_origin_column_name}.
	 * @param ctx the parse tree
	 */
	void exitFk_origin_column_name(RulesParser.Fk_origin_column_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#qualified_table_name}.
	 * @param ctx the parse tree
	 */
	void enterQualified_table_name(RulesParser.Qualified_table_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#qualified_table_name}.
	 * @param ctx the parse tree
	 */
	void exitQualified_table_name(RulesParser.Qualified_table_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#ordering_term}.
	 * @param ctx the parse tree
	 */
	void enterOrdering_term(RulesParser.Ordering_termContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#ordering_term}.
	 * @param ctx the parse tree
	 */
	void exitOrdering_term(RulesParser.Ordering_termContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#result_column}.
	 * @param ctx the parse tree
	 */
	void enterResult_column(RulesParser.Result_columnContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#result_column}.
	 * @param ctx the parse tree
	 */
	void exitResult_column(RulesParser.Result_columnContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#result_column_with_expr}.
	 * @param ctx the parse tree
	 */
	void enterResult_column_with_expr(RulesParser.Result_column_with_exprContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#result_column_with_expr}.
	 * @param ctx the parse tree
	 */
	void exitResult_column_with_expr(RulesParser.Result_column_with_exprContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#table_or_subquery}.
	 * @param ctx the parse tree
	 */
	void enterTable_or_subquery(RulesParser.Table_or_subqueryContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#table_or_subquery}.
	 * @param ctx the parse tree
	 */
	void exitTable_or_subquery(RulesParser.Table_or_subqueryContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#from_itmes_as_table}.
	 * @param ctx the parse tree
	 */
	void enterFrom_itmes_as_table(RulesParser.From_itmes_as_tableContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#from_itmes_as_table}.
	 * @param ctx the parse tree
	 */
	void exitFrom_itmes_as_table(RulesParser.From_itmes_as_tableContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#select_stmt_as_table}.
	 * @param ctx the parse tree
	 */
	void enterSelect_stmt_as_table(RulesParser.Select_stmt_as_tableContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#select_stmt_as_table}.
	 * @param ctx the parse tree
	 */
	void exitSelect_stmt_as_table(RulesParser.Select_stmt_as_tableContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#table_as_alias}.
	 * @param ctx the parse tree
	 */
	void enterTable_as_alias(RulesParser.Table_as_aliasContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#table_as_alias}.
	 * @param ctx the parse tree
	 */
	void exitTable_as_alias(RulesParser.Table_as_aliasContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#index_name_or_not_index}.
	 * @param ctx the parse tree
	 */
	void enterIndex_name_or_not_index(RulesParser.Index_name_or_not_indexContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#index_name_or_not_index}.
	 * @param ctx the parse tree
	 */
	void exitIndex_name_or_not_index(RulesParser.Index_name_or_not_indexContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#join_clause}.
	 * @param ctx the parse tree
	 */
	void enterJoin_clause(RulesParser.Join_clauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#join_clause}.
	 * @param ctx the parse tree
	 */
	void exitJoin_clause(RulesParser.Join_clauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#operator_table_constraint}.
	 * @param ctx the parse tree
	 */
	void enterOperator_table_constraint(RulesParser.Operator_table_constraintContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#operator_table_constraint}.
	 * @param ctx the parse tree
	 */
	void exitOperator_table_constraint(RulesParser.Operator_table_constraintContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#join_operator}.
	 * @param ctx the parse tree
	 */
	void enterJoin_operator(RulesParser.Join_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#join_operator}.
	 * @param ctx the parse tree
	 */
	void exitJoin_operator(RulesParser.Join_operatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#join_constraint}.
	 * @param ctx the parse tree
	 */
	void enterJoin_constraint(RulesParser.Join_constraintContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#join_constraint}.
	 * @param ctx the parse tree
	 */
	void exitJoin_constraint(RulesParser.Join_constraintContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#select_core}.
	 * @param ctx the parse tree
	 */
	void enterSelect_core(RulesParser.Select_coreContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#select_core}.
	 * @param ctx the parse tree
	 */
	void exitSelect_core(RulesParser.Select_coreContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#values_items}.
	 * @param ctx the parse tree
	 */
	void enterValues_items(RulesParser.Values_itemsContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#values_items}.
	 * @param ctx the parse tree
	 */
	void exitValues_items(RulesParser.Values_itemsContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#expr_comma_expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr_comma_expr(RulesParser.Expr_comma_exprContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#expr_comma_expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr_comma_expr(RulesParser.Expr_comma_exprContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#group_by}.
	 * @param ctx the parse tree
	 */
	void enterGroup_by(RulesParser.Group_byContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#group_by}.
	 * @param ctx the parse tree
	 */
	void exitGroup_by(RulesParser.Group_byContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#select_items}.
	 * @param ctx the parse tree
	 */
	void enterSelect_items(RulesParser.Select_itemsContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#select_items}.
	 * @param ctx the parse tree
	 */
	void exitSelect_items(RulesParser.Select_itemsContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#from_itmes}.
	 * @param ctx the parse tree
	 */
	void enterFrom_itmes(RulesParser.From_itmesContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#from_itmes}.
	 * @param ctx the parse tree
	 */
	void exitFrom_itmes(RulesParser.From_itmesContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#signed_number}.
	 * @param ctx the parse tree
	 */
	void enterSigned_number(RulesParser.Signed_numberContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#signed_number}.
	 * @param ctx the parse tree
	 */
	void exitSigned_number(RulesParser.Signed_numberContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#literal_value}.
	 * @param ctx the parse tree
	 */
	void enterLiteral_value(RulesParser.Literal_valueContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#literal_value}.
	 * @param ctx the parse tree
	 */
	void exitLiteral_value(RulesParser.Literal_valueContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#unary_operator}.
	 * @param ctx the parse tree
	 */
	void enterUnary_operator(RulesParser.Unary_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#unary_operator}.
	 * @param ctx the parse tree
	 */
	void exitUnary_operator(RulesParser.Unary_operatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#column_alias}.
	 * @param ctx the parse tree
	 */
	void enterColumn_alias(RulesParser.Column_aliasContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#column_alias}.
	 * @param ctx the parse tree
	 */
	void exitColumn_alias(RulesParser.Column_aliasContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#keyword}.
	 * @param ctx the parse tree
	 */
	void enterKeyword(RulesParser.KeywordContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#keyword}.
	 * @param ctx the parse tree
	 */
	void exitKeyword(RulesParser.KeywordContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#name}.
	 * @param ctx the parse tree
	 */
	void enterName(RulesParser.NameContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#name}.
	 * @param ctx the parse tree
	 */
	void exitName(RulesParser.NameContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#function_name}.
	 * @param ctx the parse tree
	 */
	void enterFunction_name(RulesParser.Function_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#function_name}.
	 * @param ctx the parse tree
	 */
	void exitFunction_name(RulesParser.Function_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#database_name}.
	 * @param ctx the parse tree
	 */
	void enterDatabase_name(RulesParser.Database_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#database_name}.
	 * @param ctx the parse tree
	 */
	void exitDatabase_name(RulesParser.Database_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#source_table_name}.
	 * @param ctx the parse tree
	 */
	void enterSource_table_name(RulesParser.Source_table_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#source_table_name}.
	 * @param ctx the parse tree
	 */
	void exitSource_table_name(RulesParser.Source_table_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#table_name}.
	 * @param ctx the parse tree
	 */
	void enterTable_name(RulesParser.Table_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#table_name}.
	 * @param ctx the parse tree
	 */
	void exitTable_name(RulesParser.Table_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#new_table_name}.
	 * @param ctx the parse tree
	 */
	void enterNew_table_name(RulesParser.New_table_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#new_table_name}.
	 * @param ctx the parse tree
	 */
	void exitNew_table_name(RulesParser.New_table_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#column_name}.
	 * @param ctx the parse tree
	 */
	void enterColumn_name(RulesParser.Column_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#column_name}.
	 * @param ctx the parse tree
	 */
	void exitColumn_name(RulesParser.Column_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#collation_name}.
	 * @param ctx the parse tree
	 */
	void enterCollation_name(RulesParser.Collation_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#collation_name}.
	 * @param ctx the parse tree
	 */
	void exitCollation_name(RulesParser.Collation_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#foreign_table}.
	 * @param ctx the parse tree
	 */
	void enterForeign_table(RulesParser.Foreign_tableContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#foreign_table}.
	 * @param ctx the parse tree
	 */
	void exitForeign_table(RulesParser.Foreign_tableContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#index_name}.
	 * @param ctx the parse tree
	 */
	void enterIndex_name(RulesParser.Index_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#index_name}.
	 * @param ctx the parse tree
	 */
	void exitIndex_name(RulesParser.Index_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#table_alias}.
	 * @param ctx the parse tree
	 */
	void enterTable_alias(RulesParser.Table_aliasContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#table_alias}.
	 * @param ctx the parse tree
	 */
	void exitTable_alias(RulesParser.Table_aliasContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#any_name}.
	 * @param ctx the parse tree
	 */
	void enterAny_name(RulesParser.Any_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#any_name}.
	 * @param ctx the parse tree
	 */
	void exitAny_name(RulesParser.Any_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#break_java}.
	 * @param ctx the parse tree
	 */
	void enterBreak_java(RulesParser.Break_javaContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#break_java}.
	 * @param ctx the parse tree
	 */
	void exitBreak_java(RulesParser.Break_javaContext ctx);
	/**
	 * Enter a parse tree produced by {@link RulesParser#continue_java}.
	 * @param ctx the parse tree
	 */
	void enterContinue_java(RulesParser.Continue_javaContext ctx);
	/**
	 * Exit a parse tree produced by {@link RulesParser#continue_java}.
	 * @param ctx the parse tree
	 */
	void exitContinue_java(RulesParser.Continue_javaContext ctx);
}