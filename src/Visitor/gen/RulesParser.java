package Visitor.gen;// Generated from C:/Users/mhmas/Desktop/My File/CompilerProject/src\Rules.g4 by ANTLR 4.8
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class RulesParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, NUMERIC_LITERAL=3, INT=4, SINGLE_LINE_COMMENT_JAVA=5, 
		SCOL=6, DUAL_DOT=7, DOT=8, OPEN_PAR=9, CLOSE_PAR=10, COMMA=11, ASSIGN=12, 
		STAR=13, PLUS=14, DOUBLEPLUS=15, DOUBLEMINUS=16, MINUS=17, TILDE=18, OR=19, 
		AND=20, DIV=21, MOD=22, LT2=23, GT2=24, AMP=25, PIPE=26, LT=27, LT_EQ=28, 
		GT=29, GT_EQ=30, EQ=31, NOT_EQ1=32, NOT_EQ2=33, NOT=34, OPEN_BRACKET=35, 
		CLOSE_BRACKET=36, OPEN_SQUARE=37, CLOSE_SQUARE=38, QUOTE=39, QUSTION_MAP=40, 
		UNDER_SCORE=41, K_FOREACH=42, K_VAR=43, K_PRINT=44, K_ABORT=45, K_ACTION=46, 
		K_ADD=47, K_AFTER=48, K_AGGREGATION_FUNCTION=49, K_ALL=50, K_ALTER=51, 
		K_ANALYZE=52, K_AND=53, K_AS=54, K_ASC=55, K_ATTACH=56, K_AUTOINCREMENT=57, 
		K_BEFORE=58, K_BEGIN=59, K_BETWEEN=60, K_BREAK=61, K_BY=62, K_BOOLEAN=63, 
		K_CASCADE=64, K_CASE=65, K_CAST=66, K_CHECK=67, K_COLLATE=68, K_COLUMN=69, 
		K_COMMIT=70, K_CONFLICT=71, K_CONSTRAINT=72, K_CONTINUE=73, K_CREATE=74, 
		K_CROSS=75, K_CSV=76, K_CURRENT_DATE=77, K_CURRENT_TIME=78, K_CURRENT_TIMESTAMP=79, 
		K_DATABASE=80, K_DEFAULT=81, K_DEFERRABLE=82, K_DEFERRED=83, K_DELETE=84, 
		K_DESC=85, K_DETACH=86, K_DISTINCT=87, K_DO=88, K_DROP=89, K_EACH=90, 
		K_ELSE=91, K_ELSE_IF=92, K_END=93, K_ENABLE=94, K_ESCAPE=95, K_EXCEPT=96, 
		K_EXCLUSIVE=97, K_EXISTS=98, K_EXPLAIN=99, K_FAIL=100, K_FALSE=101, K_FOR=102, 
		K_FOREIGN=103, K_FROM=104, K_FUNCTION=105, K_FULL=106, K_GLOB=107, K_GROUP=108, 
		K_HAVING=109, K_IF=110, K_IGNORE=111, K_IMMEDIATE=112, K_IN=113, K_INDEX=114, 
		K_INDEXED=115, K_INITIALLY=116, K_INNER=117, K_INSERT=118, K_INSTEAD=119, 
		K_INTERSECT=120, K_INTO=121, K_IS=122, K_ISNULL=123, K_JOIN=124, K_KEY=125, 
		K_LEFT=126, K_LIKE=127, K_LIMIT=128, K_MATCH=129, K_NATURAL=130, K_NEXTVAL=131, 
		K_NEW=132, K_NO=133, K_NOT=134, K_NOTNULL=135, K_NUMBER=136, K_NULL=137, 
		K_OF=138, K_OFFSET=139, K_ON=140, K_ONLY=141, K_OR=142, K_ORDER=143, K_OUTER=144, 
		K_PATH=145, K_PLAN=146, K_PRAGMA=147, K_PRIMARY=148, K_QUERY=149, K_RAISE=150, 
		K_RECURSIVE=151, K_REFERENCES=152, K_REGEXP=153, K_REINDEX=154, K_RELEASE=155, 
		K_RENAME=156, K_REPLACE=157, K_RESTRICT=158, K_RETURN=159, K_RIGHT=160, 
		K_ROLLBACK=161, K_ROW=162, K_SAVEPOINT=163, K_SELECT=164, K_SET=165, K_STRING=166, 
		K_SWITCH=167, K_TABLE=168, K_TYPE=169, K_TEMP=170, K_TEMPORARY=171, K_THEN=172, 
		K_TO=173, K_TRANSACTION=174, K_TRIGGER=175, K_TRUE=176, K_UNION=177, K_UNIQUE=178, 
		K_UPDATE=179, K_USING=180, K_VACUUM=181, K_VALUES=182, K_VIEW=183, K_VIRTUAL=184, 
		K_WHEN=185, K_WHERE=186, K_WHILE=187, K_WITH=188, K_WITHOUT=189, K_XML=190, 
		VALID_NAME=191, IDENTIFIER=192, BIND_PARAMETER=193, STRING_LITERAL=194, 
		BLOB_LITERAL=195, MULTILINE_COMMENT=196, SPACES=197, UNEXPECTED_CHAR=198;
	public static final int
		RULE_parse = 0, RULE_all_statement_starts = 1, RULE_java_stmt_list = 2, 
		RULE_create_variable_stmt = 3, RULE_post_square = 4, RULE_previous_square = 5, 
		RULE_assign_variable_in_creation = 6, RULE_assign_variable = 7, RULE_assign_array = 8, 
		RULE_assign_array_variable = 9, RULE_assign_variable_without_var = 10, 
		RULE_direct_array_values = 11, RULE_var_name_item_in_array = 12, RULE_value_of_one_item_array = 13, 
		RULE_variable_name = 14, RULE_array_value = 15, RULE_variable_value = 16, 
		RULE_value_in_quote = 17, RULE_default_value_parameter = 18, RULE_initial_variable_declaration = 19, 
		RULE_prototype_function = 20, RULE_prototype_function_parameter = 21, 
		RULE_function_parameter_in_prototype = 22, RULE_function_parameter_with_default = 23, 
		RULE_initial_variable_declaration_with_default_value = 24, RULE_body_function = 25, 
		RULE_body_prototype_function = 26, RULE_java_stmt = 27, RULE_full_condition_java = 28, 
		RULE_multi_condition_java = 29, RULE_header_if = 30, RULE_one_condition_in_java = 31, 
		RULE_not_var_name = 32, RULE_not_one_condition = 33, RULE_number_condition = 34, 
		RULE_logical_condition = 35, RULE_expr_java = 36, RULE_number_value_java = 37, 
		RULE_logic_operation_statement_in_java = 38, RULE_operation_in_java = 39, 
		RULE_comparison_in_java = 40, RULE_logical_in_java = 41, RULE_line_condition__expression_java = 42, 
		RULE_line_condition_result = 43, RULE_if_statement = 44, RULE_else_if_statement = 45, 
		RULE_else_statement = 46, RULE_switch_statement = 47, RULE_case_statenemt = 48, 
		RULE_case_value = 49, RULE_default_statement = 50, RULE_body_for_case_and_default = 51, 
		RULE_for_stmt = 52, RULE_for_parameter = 53, RULE_initial_start_count_in_for = 54, 
		RULE_condition_for = 55, RULE_advanced_increase = 56, RULE_delayed_increase = 57, 
		RULE_specific_increase = 58, RULE_increment_type = 59, RULE_increment_count_for = 60, 
		RULE_increment_stmt = 61, RULE_while_statement = 62, RULE_do_while_statement = 63, 
		RULE_callback_function = 64, RULE_callback_function_without_scol = 65, 
		RULE_callback_function_heder = 66, RULE_callback_function_parameter = 67, 
		RULE_callback_function_parameter_in_header = 68, RULE_function_parameter = 69, 
		RULE_var_name_in_parameter = 70, RULE_return_stmt = 71, RULE_json_declaration = 72, 
		RULE_all_fileds_in_json = 73, RULE_filed_in_json = 74, RULE_key_filed = 75, 
		RULE_array_json_declaration = 76, RULE_access_to_data_in_json = 77, RULE_foreach_statement = 78, 
		RULE_variable_in_foreach = 79, RULE_print_statement = 80, RULE_error = 81, 
		RULE_sql_stmt_list = 82, RULE_sql_stmt = 83, RULE_alter_table_stmt = 84, 
		RULE_alter_table_add_constraint = 85, RULE_alter_table_add = 86, RULE_create_table_stmt = 87, 
		RULE_columns_definition = 88, RULE_column_definition = 89, RULE_create_table_stmt_type = 90, 
		RULE_type_faild = 91, RULE_create_table_stmt_path = 92, RULE_create_type_stmt = 93, 
		RULE_create_aggregation_function = 94, RULE_create_aggregation_function_elements = 95, 
		RULE_return_type_array = 96, RULE_return_type = 97, RULE_class_name = 98, 
		RULE_method_name = 99, RULE_path_string = 100, RULE_any_name_with_quote = 101, 
		RULE_columns_def_tabels_constraint = 102, RULE_columns_def_or_select_stmt = 103, 
		RULE_delete_stmt = 104, RULE_drop_table_stmt = 105, RULE_factored_select_stmt = 106, 
		RULE_order_by = 107, RULE_limit_select = 108, RULE_insert_stmt = 109, 
		RULE_insert_stmt_values = 110, RULE_select_stmt = 111, RULE_update_stmt = 112, 
		RULE_column_name_equals_expr = 113, RULE_column_def = 114, RULE_column_constraint_or_type_name = 115, 
		RULE_type_name = 116, RULE_signed_number_any_name = 117, RULE_column_constraint = 118, 
		RULE_column_constraint_primary_key = 119, RULE_column_constraint_foreign_key = 120, 
		RULE_column_constraint_not_null = 121, RULE_column_constraint_null = 122, 
		RULE_column_default = 123, RULE_column_default_content = 124, RULE_column_default_value = 125, 
		RULE_expr = 126, RULE_exists_case = 127, RULE_function_expr = 128, RULE_column_name_faild = 129, 
		RULE_table_name_faild = 130, RULE_select_or_expr = 131, RULE_in_case = 132, 
		RULE_foreign_key_clause = 133, RULE_foreign_deferrable = 134, RULE_foreign_on = 135, 
		RULE_foreign_on_or_match = 136, RULE_foreign_table_faild = 137, RULE_fk_target_column_name = 138, 
		RULE_indexed_column = 139, RULE_table_constraint = 140, RULE_table_constraint_primary_key = 141, 
		RULE_table_constraint_foreign_key = 142, RULE_table_constraint_unique = 143, 
		RULE_table_constraint_key = 144, RULE_fk_origin_column_name = 145, RULE_qualified_table_name = 146, 
		RULE_ordering_term = 147, RULE_result_column = 148, RULE_result_column_with_expr = 149, 
		RULE_table_or_subquery = 150, RULE_from_itmes_as_table = 151, RULE_select_stmt_as_table = 152, 
		RULE_table_as_alias = 153, RULE_index_name_or_not_index = 154, RULE_join_clause = 155, 
		RULE_operator_table_constraint = 156, RULE_join_operator = 157, RULE_join_constraint = 158, 
		RULE_select_core = 159, RULE_values_items = 160, RULE_expr_comma_expr = 161, 
		RULE_group_by = 162, RULE_select_items = 163, RULE_from_itmes = 164, RULE_signed_number = 165, 
		RULE_literal_value = 166, RULE_unary_operator = 167, RULE_column_alias = 168, 
		RULE_keyword = 169, RULE_name = 170, RULE_function_name = 171, RULE_database_name = 172, 
		RULE_source_table_name = 173, RULE_table_name = 174, RULE_new_table_name = 175, 
		RULE_column_name = 176, RULE_collation_name = 177, RULE_foreign_table = 178, 
		RULE_index_name = 179, RULE_table_alias = 180, RULE_any_name = 181, RULE_break_java = 182, 
		RULE_continue_java = 183;
	private static String[] makeRuleNames() {
		return new String[] {
			"parse", "all_statement_starts", "java_stmt_list", "create_variable_stmt", 
			"post_square", "previous_square", "assign_variable_in_creation", "assign_variable", 
			"assign_array", "assign_array_variable", "assign_variable_without_var", 
			"direct_array_values", "var_name_item_in_array", "value_of_one_item_array", 
			"variable_name", "array_value", "variable_value", "value_in_quote", "default_value_parameter", 
			"initial_variable_declaration", "prototype_function", "prototype_function_parameter", 
			"function_parameter_in_prototype", "function_parameter_with_default", 
			"initial_variable_declaration_with_default_value", "body_function", "body_prototype_function", 
			"java_stmt", "full_condition_java", "multi_condition_java", "header_if", 
			"one_condition_in_java", "not_var_name", "not_one_condition", "number_condition", 
			"logical_condition", "expr_java", "number_value_java", "logic_operation_statement_in_java", 
			"operation_in_java", "comparison_in_java", "logical_in_java", "line_condition__expression_java", 
			"line_condition_result", "if_statement", "else_if_statement", "else_statement", 
			"switch_statement", "case_statenemt", "case_value", "default_statement", 
			"body_for_case_and_default", "for_stmt", "for_parameter", "initial_start_count_in_for", 
			"condition_for", "advanced_increase", "delayed_increase", "specific_increase", 
			"increment_type", "increment_count_for", "increment_stmt", "while_statement", 
			"do_while_statement", "callback_function", "callback_function_without_scol", 
			"callback_function_heder", "callback_function_parameter", "callback_function_parameter_in_header", 
			"function_parameter", "var_name_in_parameter", "return_stmt", "json_declaration", 
			"all_fileds_in_json", "filed_in_json", "key_filed", "array_json_declaration", 
			"access_to_data_in_json", "foreach_statement", "variable_in_foreach", 
			"print_statement", "error", "sql_stmt_list", "sql_stmt", "alter_table_stmt", 
			"alter_table_add_constraint", "alter_table_add", "create_table_stmt", 
			"columns_definition", "column_definition", "create_table_stmt_type", 
			"type_faild", "create_table_stmt_path", "create_type_stmt", "create_aggregation_function", 
			"create_aggregation_function_elements", "return_type_array", "return_type", 
			"class_name", "method_name", "path_string", "any_name_with_quote", "columns_def_tabels_constraint", 
			"columns_def_or_select_stmt", "delete_stmt", "drop_table_stmt", "factored_select_stmt", 
			"order_by", "limit_select", "insert_stmt", "insert_stmt_values", "select_stmt", 
			"update_stmt", "column_name_equals_expr", "column_def", "column_constraint_or_type_name", 
			"type_name", "signed_number_any_name", "column_constraint", "column_constraint_primary_key", 
			"column_constraint_foreign_key", "column_constraint_not_null", "column_constraint_null", 
			"column_default", "column_default_content", "column_default_value", "expr", 
			"exists_case", "function_expr", "column_name_faild", "table_name_faild", 
			"select_or_expr", "in_case", "foreign_key_clause", "foreign_deferrable", 
			"foreign_on", "foreign_on_or_match", "foreign_table_faild", "fk_target_column_name", 
			"indexed_column", "table_constraint", "table_constraint_primary_key", 
			"table_constraint_foreign_key", "table_constraint_unique", "table_constraint_key", 
			"fk_origin_column_name", "qualified_table_name", "ordering_term", "result_column", 
			"result_column_with_expr", "table_or_subquery", "from_itmes_as_table", 
			"select_stmt_as_table", "table_as_alias", "index_name_or_not_index", 
			"join_clause", "operator_table_constraint", "join_operator", "join_constraint", 
			"select_core", "values_items", "expr_comma_expr", "group_by", "select_items", 
			"from_itmes", "signed_number", "literal_value", "unary_operator", "column_alias", 
			"keyword", "name", "function_name", "database_name", "source_table_name", 
			"table_name", "new_table_name", "column_name", "collation_name", "foreign_table", 
			"index_name", "table_alias", "any_name", "break_java", "continue_java"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'json'", "'::'", null, null, null, "';'", "':'", "'.'", "'('", 
			"')'", "','", "'='", "'*'", "'+'", "'++'", "'--'", "'-'", "'~'", "'||'", 
			"'&&'", "'/'", "'%'", "'<<'", "'>>'", "'&'", "'|'", "'<'", "'<='", "'>'", 
			"'>='", "'=='", "'!='", "'<>'", "'!'", "'{'", "'}'", "'['", "']'", "'\"'", 
			"'?'", "'_'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, "NUMERIC_LITERAL", "INT", "SINGLE_LINE_COMMENT_JAVA", 
			"SCOL", "DUAL_DOT", "DOT", "OPEN_PAR", "CLOSE_PAR", "COMMA", "ASSIGN", 
			"STAR", "PLUS", "DOUBLEPLUS", "DOUBLEMINUS", "MINUS", "TILDE", "OR", 
			"AND", "DIV", "MOD", "LT2", "GT2", "AMP", "PIPE", "LT", "LT_EQ", "GT", 
			"GT_EQ", "EQ", "NOT_EQ1", "NOT_EQ2", "NOT", "OPEN_BRACKET", "CLOSE_BRACKET", 
			"OPEN_SQUARE", "CLOSE_SQUARE", "QUOTE", "QUSTION_MAP", "UNDER_SCORE", 
			"K_FOREACH", "K_VAR", "K_PRINT", "K_ABORT", "K_ACTION", "K_ADD", "K_AFTER", 
			"K_AGGREGATION_FUNCTION", "K_ALL", "K_ALTER", "K_ANALYZE", "K_AND", "K_AS", 
			"K_ASC", "K_ATTACH", "K_AUTOINCREMENT", "K_BEFORE", "K_BEGIN", "K_BETWEEN", 
			"K_BREAK", "K_BY", "K_BOOLEAN", "K_CASCADE", "K_CASE", "K_CAST", "K_CHECK", 
			"K_COLLATE", "K_COLUMN", "K_COMMIT", "K_CONFLICT", "K_CONSTRAINT", "K_CONTINUE", 
			"K_CREATE", "K_CROSS", "K_CSV", "K_CURRENT_DATE", "K_CURRENT_TIME", "K_CURRENT_TIMESTAMP", 
			"K_DATABASE", "K_DEFAULT", "K_DEFERRABLE", "K_DEFERRED", "K_DELETE", 
			"K_DESC", "K_DETACH", "K_DISTINCT", "K_DO", "K_DROP", "K_EACH", "K_ELSE", 
			"K_ELSE_IF", "K_END", "K_ENABLE", "K_ESCAPE", "K_EXCEPT", "K_EXCLUSIVE", 
			"K_EXISTS", "K_EXPLAIN", "K_FAIL", "K_FALSE", "K_FOR", "K_FOREIGN", "K_FROM", 
			"K_FUNCTION", "K_FULL", "K_GLOB", "K_GROUP", "K_HAVING", "K_IF", "K_IGNORE", 
			"K_IMMEDIATE", "K_IN", "K_INDEX", "K_INDEXED", "K_INITIALLY", "K_INNER", 
			"K_INSERT", "K_INSTEAD", "K_INTERSECT", "K_INTO", "K_IS", "K_ISNULL", 
			"K_JOIN", "K_KEY", "K_LEFT", "K_LIKE", "K_LIMIT", "K_MATCH", "K_NATURAL", 
			"K_NEXTVAL", "K_NEW", "K_NO", "K_NOT", "K_NOTNULL", "K_NUMBER", "K_NULL", 
			"K_OF", "K_OFFSET", "K_ON", "K_ONLY", "K_OR", "K_ORDER", "K_OUTER", "K_PATH", 
			"K_PLAN", "K_PRAGMA", "K_PRIMARY", "K_QUERY", "K_RAISE", "K_RECURSIVE", 
			"K_REFERENCES", "K_REGEXP", "K_REINDEX", "K_RELEASE", "K_RENAME", "K_REPLACE", 
			"K_RESTRICT", "K_RETURN", "K_RIGHT", "K_ROLLBACK", "K_ROW", "K_SAVEPOINT", 
			"K_SELECT", "K_SET", "K_STRING", "K_SWITCH", "K_TABLE", "K_TYPE", "K_TEMP", 
			"K_TEMPORARY", "K_THEN", "K_TO", "K_TRANSACTION", "K_TRIGGER", "K_TRUE", 
			"K_UNION", "K_UNIQUE", "K_UPDATE", "K_USING", "K_VACUUM", "K_VALUES", 
			"K_VIEW", "K_VIRTUAL", "K_WHEN", "K_WHERE", "K_WHILE", "K_WITH", "K_WITHOUT", 
			"K_XML", "VALID_NAME", "IDENTIFIER", "BIND_PARAMETER", "STRING_LITERAL", 
			"BLOB_LITERAL", "MULTILINE_COMMENT", "SPACES", "UNEXPECTED_CHAR"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Rules.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public RulesParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class ParseContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(RulesParser.EOF, 0); }
		public List<All_statement_startsContext> all_statement_starts() {
			return getRuleContexts(All_statement_startsContext.class);
		}
		public All_statement_startsContext all_statement_starts(int i) {
			return getRuleContext(All_statement_startsContext.class,i);
		}
		public ParseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterParse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitParse(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitParse(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParseContext parse() throws RecognitionException {
		ParseContext _localctx = new ParseContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_parse);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(371);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << SCOL) | (1L << DOUBLEPLUS) | (1L << DOUBLEMINUS) | (1L << K_VAR) | (1L << K_PRINT) | (1L << K_ALTER) | (1L << K_BREAK))) != 0) || ((((_la - 73)) & ~0x3f) == 0 && ((1L << (_la - 73)) & ((1L << (K_CONTINUE - 73)) | (1L << (K_CREATE - 73)) | (1L << (K_DELETE - 73)) | (1L << (K_DO - 73)) | (1L << (K_DROP - 73)) | (1L << (K_FOR - 73)) | (1L << (K_IF - 73)) | (1L << (K_INSERT - 73)))) != 0) || ((((_la - 164)) & ~0x3f) == 0 && ((1L << (_la - 164)) & ((1L << (K_SELECT - 164)) | (1L << (K_SWITCH - 164)) | (1L << (K_UPDATE - 164)) | (1L << (K_VALUES - 164)) | (1L << (K_WHILE - 164)) | (1L << (VALID_NAME - 164)) | (1L << (UNEXPECTED_CHAR - 164)))) != 0)) {
				{
				{
				setState(368);
				all_statement_starts();
				}
				}
				setState(373);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(374);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class All_statement_startsContext extends ParserRuleContext {
		public Sql_stmt_listContext sql_stmt_list() {
			return getRuleContext(Sql_stmt_listContext.class,0);
		}
		public Java_stmt_listContext java_stmt_list() {
			return getRuleContext(Java_stmt_listContext.class,0);
		}
		public ErrorContext error() {
			return getRuleContext(ErrorContext.class,0);
		}
		public All_statement_startsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_all_statement_starts; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterAll_statement_starts(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitAll_statement_starts(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitAll_statement_starts(this);
			else return visitor.visitChildren(this);
		}
	}

	public final All_statement_startsContext all_statement_starts() throws RecognitionException {
		All_statement_startsContext _localctx = new All_statement_startsContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_all_statement_starts);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(379);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case SCOL:
			case K_ALTER:
			case K_CREATE:
			case K_DELETE:
			case K_DROP:
			case K_INSERT:
			case K_SELECT:
			case K_UPDATE:
			case K_VALUES:
				{
				setState(376);
				sql_stmt_list();
				}
				break;
			case DOUBLEPLUS:
			case DOUBLEMINUS:
			case K_VAR:
			case K_PRINT:
			case K_BREAK:
			case K_CONTINUE:
			case K_DO:
			case K_FOR:
			case K_IF:
			case K_SWITCH:
			case K_WHILE:
			case VALID_NAME:
				{
				setState(377);
				java_stmt_list();
				}
				break;
			case UNEXPECTED_CHAR:
				{
				setState(378);
				error();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Java_stmt_listContext extends ParserRuleContext {
		public Java_stmtContext java_stmt() {
			return getRuleContext(Java_stmtContext.class,0);
		}
		public Prototype_functionContext prototype_function() {
			return getRuleContext(Prototype_functionContext.class,0);
		}
		public Java_stmt_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_java_stmt_list; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterJava_stmt_list(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitJava_stmt_list(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitJava_stmt_list(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Java_stmt_listContext java_stmt_list() throws RecognitionException {
		Java_stmt_listContext _localctx = new Java_stmt_listContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_java_stmt_list);
		try {
			setState(383);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(381);
				java_stmt();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(382);
				prototype_function();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Create_variable_stmtContext extends ParserRuleContext {
		public TerminalNode K_VAR() { return getToken(RulesParser.K_VAR, 0); }
		public TerminalNode SCOL() { return getToken(RulesParser.SCOL, 0); }
		public Assign_variable_in_creationContext assign_variable_in_creation() {
			return getRuleContext(Assign_variable_in_creationContext.class,0);
		}
		public Create_variable_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_create_variable_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterCreate_variable_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitCreate_variable_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitCreate_variable_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Create_variable_stmtContext create_variable_stmt() throws RecognitionException {
		Create_variable_stmtContext _localctx = new Create_variable_stmtContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_create_variable_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(385);
			match(K_VAR);
			{
			setState(386);
			assign_variable_in_creation();
			}
			setState(387);
			match(SCOL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Post_squareContext extends ParserRuleContext {
		public Variable_nameContext variable_name() {
			return getRuleContext(Variable_nameContext.class,0);
		}
		public TerminalNode OPEN_SQUARE() { return getToken(RulesParser.OPEN_SQUARE, 0); }
		public TerminalNode CLOSE_SQUARE() { return getToken(RulesParser.CLOSE_SQUARE, 0); }
		public Post_squareContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_post_square; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterPost_square(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitPost_square(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitPost_square(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Post_squareContext post_square() throws RecognitionException {
		Post_squareContext _localctx = new Post_squareContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_post_square);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(389);
			variable_name();
			setState(390);
			match(OPEN_SQUARE);
			setState(391);
			match(CLOSE_SQUARE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Previous_squareContext extends ParserRuleContext {
		public TerminalNode OPEN_SQUARE() { return getToken(RulesParser.OPEN_SQUARE, 0); }
		public TerminalNode CLOSE_SQUARE() { return getToken(RulesParser.CLOSE_SQUARE, 0); }
		public Variable_nameContext variable_name() {
			return getRuleContext(Variable_nameContext.class,0);
		}
		public Previous_squareContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_previous_square; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterPrevious_square(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitPrevious_square(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitPrevious_square(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Previous_squareContext previous_square() throws RecognitionException {
		Previous_squareContext _localctx = new Previous_squareContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_previous_square);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(393);
			match(OPEN_SQUARE);
			setState(394);
			match(CLOSE_SQUARE);
			setState(395);
			variable_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Assign_variable_in_creationContext extends ParserRuleContext {
		public List<Assign_array_variableContext> assign_array_variable() {
			return getRuleContexts(Assign_array_variableContext.class);
		}
		public Assign_array_variableContext assign_array_variable(int i) {
			return getRuleContext(Assign_array_variableContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(RulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(RulesParser.COMMA, i);
		}
		public Assign_variable_in_creationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assign_variable_in_creation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterAssign_variable_in_creation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitAssign_variable_in_creation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitAssign_variable_in_creation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Assign_variable_in_creationContext assign_variable_in_creation() throws RecognitionException {
		Assign_variable_in_creationContext _localctx = new Assign_variable_in_creationContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_assign_variable_in_creation);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(397);
			assign_array_variable();
			setState(402);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(398);
				match(COMMA);
				setState(399);
				assign_array_variable();
				}
				}
				setState(404);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Assign_variableContext extends ParserRuleContext {
		public Variable_nameContext variable_name() {
			return getRuleContext(Variable_nameContext.class,0);
		}
		public TerminalNode ASSIGN() { return getToken(RulesParser.ASSIGN, 0); }
		public Variable_valueContext variable_value() {
			return getRuleContext(Variable_valueContext.class,0);
		}
		public Assign_variableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assign_variable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterAssign_variable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitAssign_variable(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitAssign_variable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Assign_variableContext assign_variable() throws RecognitionException {
		Assign_variableContext _localctx = new Assign_variableContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_assign_variable);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(405);
			variable_name();
			setState(408);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ASSIGN) {
				{
				setState(406);
				match(ASSIGN);
				setState(407);
				variable_value();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Assign_arrayContext extends ParserRuleContext {
		public TerminalNode ASSIGN() { return getToken(RulesParser.ASSIGN, 0); }
		public Array_valueContext array_value() {
			return getRuleContext(Array_valueContext.class,0);
		}
		public Post_squareContext post_square() {
			return getRuleContext(Post_squareContext.class,0);
		}
		public Previous_squareContext previous_square() {
			return getRuleContext(Previous_squareContext.class,0);
		}
		public Assign_arrayContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assign_array; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterAssign_array(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitAssign_array(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitAssign_array(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Assign_arrayContext assign_array() throws RecognitionException {
		Assign_arrayContext _localctx = new Assign_arrayContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_assign_array);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(412);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case VALID_NAME:
				{
				setState(410);
				post_square();
				}
				break;
			case OPEN_SQUARE:
				{
				setState(411);
				previous_square();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(414);
			match(ASSIGN);
			setState(415);
			array_value();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Assign_array_variableContext extends ParserRuleContext {
		public Assign_arrayContext assign_array() {
			return getRuleContext(Assign_arrayContext.class,0);
		}
		public Assign_variableContext assign_variable() {
			return getRuleContext(Assign_variableContext.class,0);
		}
		public Assign_array_variableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assign_array_variable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterAssign_array_variable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitAssign_array_variable(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitAssign_array_variable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Assign_array_variableContext assign_array_variable() throws RecognitionException {
		Assign_array_variableContext _localctx = new Assign_array_variableContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_assign_array_variable);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(419);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				{
				setState(417);
				assign_array();
				}
				break;
			case 2:
				{
				setState(418);
				assign_variable();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Assign_variable_without_varContext extends ParserRuleContext {
		public TerminalNode ASSIGN() { return getToken(RulesParser.ASSIGN, 0); }
		public Variable_valueContext variable_value() {
			return getRuleContext(Variable_valueContext.class,0);
		}
		public TerminalNode SCOL() { return getToken(RulesParser.SCOL, 0); }
		public Variable_nameContext variable_name() {
			return getRuleContext(Variable_nameContext.class,0);
		}
		public Var_name_item_in_arrayContext var_name_item_in_array() {
			return getRuleContext(Var_name_item_in_arrayContext.class,0);
		}
		public Assign_variable_without_varContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assign_variable_without_var; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterAssign_variable_without_var(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitAssign_variable_without_var(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitAssign_variable_without_var(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Assign_variable_without_varContext assign_variable_without_var() throws RecognitionException {
		Assign_variable_without_varContext _localctx = new Assign_variable_without_varContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_assign_variable_without_var);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(423);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
			case 1:
				{
				setState(421);
				variable_name();
				}
				break;
			case 2:
				{
				setState(422);
				var_name_item_in_array();
				}
				break;
			}
			setState(425);
			match(ASSIGN);
			setState(426);
			variable_value();
			setState(427);
			match(SCOL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Direct_array_valuesContext extends ParserRuleContext {
		public TerminalNode OPEN_BRACKET() { return getToken(RulesParser.OPEN_BRACKET, 0); }
		public List<Variable_valueContext> variable_value() {
			return getRuleContexts(Variable_valueContext.class);
		}
		public Variable_valueContext variable_value(int i) {
			return getRuleContext(Variable_valueContext.class,i);
		}
		public TerminalNode CLOSE_BRACKET() { return getToken(RulesParser.CLOSE_BRACKET, 0); }
		public List<TerminalNode> COMMA() { return getTokens(RulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(RulesParser.COMMA, i);
		}
		public Direct_array_valuesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_direct_array_values; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterDirect_array_values(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitDirect_array_values(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitDirect_array_values(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Direct_array_valuesContext direct_array_values() throws RecognitionException {
		Direct_array_valuesContext _localctx = new Direct_array_valuesContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_direct_array_values);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(429);
			match(OPEN_BRACKET);
			setState(430);
			variable_value();
			setState(435);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(431);
				match(COMMA);
				setState(432);
				variable_value();
				}
				}
				setState(437);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(438);
			match(CLOSE_BRACKET);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Var_name_item_in_arrayContext extends ParserRuleContext {
		public Variable_nameContext variable_name() {
			return getRuleContext(Variable_nameContext.class,0);
		}
		public Value_of_one_item_arrayContext value_of_one_item_array() {
			return getRuleContext(Value_of_one_item_arrayContext.class,0);
		}
		public Var_name_item_in_arrayContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_var_name_item_in_array; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterVar_name_item_in_array(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitVar_name_item_in_array(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitVar_name_item_in_array(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Var_name_item_in_arrayContext var_name_item_in_array() throws RecognitionException {
		Var_name_item_in_arrayContext _localctx = new Var_name_item_in_arrayContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_var_name_item_in_array);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(440);
			variable_name();
			setState(441);
			value_of_one_item_array();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Value_of_one_item_arrayContext extends ParserRuleContext {
		public TerminalNode OPEN_SQUARE() { return getToken(RulesParser.OPEN_SQUARE, 0); }
		public TerminalNode CLOSE_SQUARE() { return getToken(RulesParser.CLOSE_SQUARE, 0); }
		public Variable_valueContext variable_value() {
			return getRuleContext(Variable_valueContext.class,0);
		}
		public Value_of_one_item_arrayContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_value_of_one_item_array; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterValue_of_one_item_array(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitValue_of_one_item_array(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitValue_of_one_item_array(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Value_of_one_item_arrayContext value_of_one_item_array() throws RecognitionException {
		Value_of_one_item_arrayContext _localctx = new Value_of_one_item_arrayContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_value_of_one_item_array);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(443);
			match(OPEN_SQUARE);
			{
			setState(444);
			variable_value();
			}
			setState(445);
			match(CLOSE_SQUARE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Variable_nameContext extends ParserRuleContext {
		public TerminalNode VALID_NAME() { return getToken(RulesParser.VALID_NAME, 0); }
		public Variable_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variable_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterVariable_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitVariable_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitVariable_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Variable_nameContext variable_name() throws RecognitionException {
		Variable_nameContext _localctx = new Variable_nameContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_variable_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(447);
			match(VALID_NAME);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Array_valueContext extends ParserRuleContext {
		public TerminalNode K_NEW() { return getToken(RulesParser.K_NEW, 0); }
		public TerminalNode K_VAR() { return getToken(RulesParser.K_VAR, 0); }
		public TerminalNode OPEN_SQUARE() { return getToken(RulesParser.OPEN_SQUARE, 0); }
		public TerminalNode CLOSE_SQUARE() { return getToken(RulesParser.CLOSE_SQUARE, 0); }
		public Direct_array_valuesContext direct_array_values() {
			return getRuleContext(Direct_array_valuesContext.class,0);
		}
		public Number_value_javaContext number_value_java() {
			return getRuleContext(Number_value_javaContext.class,0);
		}
		public Select_stmtContext select_stmt() {
			return getRuleContext(Select_stmtContext.class,0);
		}
		public Callback_function_without_scolContext callback_function_without_scol() {
			return getRuleContext(Callback_function_without_scolContext.class,0);
		}
		public Array_valueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_array_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterArray_value(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitArray_value(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitArray_value(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Array_valueContext array_value() throws RecognitionException {
		Array_valueContext _localctx = new Array_valueContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_array_value);
		try {
			setState(463);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case OPEN_SQUARE:
			case K_NEW:
				enterOuterAlt(_localctx, 1);
				{
				setState(458);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case K_NEW:
					{
					setState(449);
					match(K_NEW);
					setState(450);
					match(K_VAR);
					{
					setState(451);
					match(OPEN_SQUARE);
					{
					setState(452);
					number_value_java();
					}
					setState(453);
					match(CLOSE_SQUARE);
					}
					}
					break;
				case OPEN_SQUARE:
					{
					{
					setState(455);
					match(OPEN_SQUARE);
					setState(456);
					match(CLOSE_SQUARE);
					setState(457);
					direct_array_values();
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			case OPEN_BRACKET:
				enterOuterAlt(_localctx, 2);
				{
				setState(460);
				direct_array_values();
				}
				break;
			case K_SELECT:
			case K_VALUES:
				enterOuterAlt(_localctx, 3);
				{
				setState(461);
				select_stmt();
				}
				break;
			case VALID_NAME:
				enterOuterAlt(_localctx, 4);
				{
				setState(462);
				callback_function_without_scol();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Variable_valueContext extends ParserRuleContext {
		public Select_stmtContext select_stmt() {
			return getRuleContext(Select_stmtContext.class,0);
		}
		public TerminalNode K_TRUE() { return getToken(RulesParser.K_TRUE, 0); }
		public TerminalNode K_FALSE() { return getToken(RulesParser.K_FALSE, 0); }
		public TerminalNode K_NULL() { return getToken(RulesParser.K_NULL, 0); }
		public TerminalNode NUMERIC_LITERAL() { return getToken(RulesParser.NUMERIC_LITERAL, 0); }
		public List<TerminalNode> QUOTE() { return getTokens(RulesParser.QUOTE); }
		public TerminalNode QUOTE(int i) {
			return getToken(RulesParser.QUOTE, i);
		}
		public Value_in_quoteContext value_in_quote() {
			return getRuleContext(Value_in_quoteContext.class,0);
		}
		public Variable_nameContext variable_name() {
			return getRuleContext(Variable_nameContext.class,0);
		}
		public Var_name_item_in_arrayContext var_name_item_in_array() {
			return getRuleContext(Var_name_item_in_arrayContext.class,0);
		}
		public Callback_function_without_scolContext callback_function_without_scol() {
			return getRuleContext(Callback_function_without_scolContext.class,0);
		}
		public Access_to_data_in_jsonContext access_to_data_in_json() {
			return getRuleContext(Access_to_data_in_jsonContext.class,0);
		}
		public Array_valueContext array_value() {
			return getRuleContext(Array_valueContext.class,0);
		}
		public Expr_javaContext expr_java() {
			return getRuleContext(Expr_javaContext.class,0);
		}
		public Number_conditionContext number_condition() {
			return getRuleContext(Number_conditionContext.class,0);
		}
		public Logical_conditionContext logical_condition() {
			return getRuleContext(Logical_conditionContext.class,0);
		}
		public Increment_typeContext increment_type() {
			return getRuleContext(Increment_typeContext.class,0);
		}
		public Full_condition_javaContext full_condition_java() {
			return getRuleContext(Full_condition_javaContext.class,0);
		}
		public Line_condition__expression_javaContext line_condition__expression_java() {
			return getRuleContext(Line_condition__expression_javaContext.class,0);
		}
		public Json_declarationContext json_declaration() {
			return getRuleContext(Json_declarationContext.class,0);
		}
		public Array_json_declarationContext array_json_declaration() {
			return getRuleContext(Array_json_declarationContext.class,0);
		}
		public Variable_valueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variable_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterVariable_value(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitVariable_value(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitVariable_value(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Variable_valueContext variable_value() throws RecognitionException {
		Variable_valueContext _localctx = new Variable_valueContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_variable_value);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(487);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
			case 1:
				{
				setState(465);
				select_stmt();
				}
				break;
			case 2:
				{
				setState(466);
				match(K_TRUE);
				}
				break;
			case 3:
				{
				setState(467);
				match(K_FALSE);
				}
				break;
			case 4:
				{
				setState(468);
				match(K_NULL);
				}
				break;
			case 5:
				{
				setState(469);
				match(NUMERIC_LITERAL);
				}
				break;
			case 6:
				{
				setState(470);
				match(QUOTE);
				setState(471);
				value_in_quote();
				setState(472);
				match(QUOTE);
				}
				break;
			case 7:
				{
				setState(474);
				variable_name();
				}
				break;
			case 8:
				{
				setState(475);
				var_name_item_in_array();
				}
				break;
			case 9:
				{
				setState(476);
				callback_function_without_scol();
				}
				break;
			case 10:
				{
				setState(477);
				access_to_data_in_json();
				}
				break;
			case 11:
				{
				setState(478);
				array_value();
				}
				break;
			case 12:
				{
				setState(479);
				expr_java(0);
				}
				break;
			case 13:
				{
				setState(480);
				number_condition();
				}
				break;
			case 14:
				{
				setState(481);
				logical_condition();
				}
				break;
			case 15:
				{
				setState(482);
				increment_type();
				}
				break;
			case 16:
				{
				setState(483);
				full_condition_java();
				}
				break;
			case 17:
				{
				setState(484);
				line_condition__expression_java();
				}
				break;
			case 18:
				{
				setState(485);
				json_declaration();
				}
				break;
			case 19:
				{
				setState(486);
				array_json_declaration();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Value_in_quoteContext extends ParserRuleContext {
		public List<TerminalNode> STAR() { return getTokens(RulesParser.STAR); }
		public TerminalNode STAR(int i) {
			return getToken(RulesParser.STAR, i);
		}
		public List<TerminalNode> QUOTE() { return getTokens(RulesParser.QUOTE); }
		public TerminalNode QUOTE(int i) {
			return getToken(RulesParser.QUOTE, i);
		}
		public Value_in_quoteContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_value_in_quote; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterValue_in_quote(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitValue_in_quote(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitValue_in_quote(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Value_in_quoteContext value_in_quote() throws RecognitionException {
		Value_in_quoteContext _localctx = new Value_in_quoteContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_value_in_quote);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(499);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,14,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					setState(497);
					_errHandler.sync(this);
					switch (_input.LA(1)) {
					case T__0:
					case T__1:
					case NUMERIC_LITERAL:
					case INT:
					case SINGLE_LINE_COMMENT_JAVA:
					case SCOL:
					case DUAL_DOT:
					case DOT:
					case OPEN_PAR:
					case CLOSE_PAR:
					case COMMA:
					case ASSIGN:
					case PLUS:
					case DOUBLEPLUS:
					case DOUBLEMINUS:
					case MINUS:
					case TILDE:
					case OR:
					case AND:
					case DIV:
					case MOD:
					case LT2:
					case GT2:
					case AMP:
					case PIPE:
					case LT:
					case LT_EQ:
					case GT:
					case GT_EQ:
					case EQ:
					case NOT_EQ1:
					case NOT_EQ2:
					case NOT:
					case OPEN_BRACKET:
					case CLOSE_BRACKET:
					case OPEN_SQUARE:
					case CLOSE_SQUARE:
					case QUOTE:
					case QUSTION_MAP:
					case UNDER_SCORE:
					case K_FOREACH:
					case K_VAR:
					case K_PRINT:
					case K_ABORT:
					case K_ACTION:
					case K_ADD:
					case K_AFTER:
					case K_AGGREGATION_FUNCTION:
					case K_ALL:
					case K_ALTER:
					case K_ANALYZE:
					case K_AND:
					case K_AS:
					case K_ASC:
					case K_ATTACH:
					case K_AUTOINCREMENT:
					case K_BEFORE:
					case K_BEGIN:
					case K_BETWEEN:
					case K_BREAK:
					case K_BY:
					case K_BOOLEAN:
					case K_CASCADE:
					case K_CASE:
					case K_CAST:
					case K_CHECK:
					case K_COLLATE:
					case K_COLUMN:
					case K_COMMIT:
					case K_CONFLICT:
					case K_CONSTRAINT:
					case K_CONTINUE:
					case K_CREATE:
					case K_CROSS:
					case K_CSV:
					case K_CURRENT_DATE:
					case K_CURRENT_TIME:
					case K_CURRENT_TIMESTAMP:
					case K_DATABASE:
					case K_DEFAULT:
					case K_DEFERRABLE:
					case K_DEFERRED:
					case K_DELETE:
					case K_DESC:
					case K_DETACH:
					case K_DISTINCT:
					case K_DO:
					case K_DROP:
					case K_EACH:
					case K_ELSE:
					case K_ELSE_IF:
					case K_END:
					case K_ENABLE:
					case K_ESCAPE:
					case K_EXCEPT:
					case K_EXCLUSIVE:
					case K_EXISTS:
					case K_EXPLAIN:
					case K_FAIL:
					case K_FALSE:
					case K_FOR:
					case K_FOREIGN:
					case K_FROM:
					case K_FUNCTION:
					case K_FULL:
					case K_GLOB:
					case K_GROUP:
					case K_HAVING:
					case K_IF:
					case K_IGNORE:
					case K_IMMEDIATE:
					case K_IN:
					case K_INDEX:
					case K_INDEXED:
					case K_INITIALLY:
					case K_INNER:
					case K_INSERT:
					case K_INSTEAD:
					case K_INTERSECT:
					case K_INTO:
					case K_IS:
					case K_ISNULL:
					case K_JOIN:
					case K_KEY:
					case K_LEFT:
					case K_LIKE:
					case K_LIMIT:
					case K_MATCH:
					case K_NATURAL:
					case K_NEXTVAL:
					case K_NEW:
					case K_NO:
					case K_NOT:
					case K_NOTNULL:
					case K_NUMBER:
					case K_NULL:
					case K_OF:
					case K_OFFSET:
					case K_ON:
					case K_ONLY:
					case K_OR:
					case K_ORDER:
					case K_OUTER:
					case K_PATH:
					case K_PLAN:
					case K_PRAGMA:
					case K_PRIMARY:
					case K_QUERY:
					case K_RAISE:
					case K_RECURSIVE:
					case K_REFERENCES:
					case K_REGEXP:
					case K_REINDEX:
					case K_RELEASE:
					case K_RENAME:
					case K_REPLACE:
					case K_RESTRICT:
					case K_RETURN:
					case K_RIGHT:
					case K_ROLLBACK:
					case K_ROW:
					case K_SAVEPOINT:
					case K_SELECT:
					case K_SET:
					case K_STRING:
					case K_SWITCH:
					case K_TABLE:
					case K_TYPE:
					case K_TEMP:
					case K_TEMPORARY:
					case K_THEN:
					case K_TO:
					case K_TRANSACTION:
					case K_TRIGGER:
					case K_TRUE:
					case K_UNION:
					case K_UNIQUE:
					case K_UPDATE:
					case K_USING:
					case K_VACUUM:
					case K_VALUES:
					case K_VIEW:
					case K_VIRTUAL:
					case K_WHEN:
					case K_WHERE:
					case K_WHILE:
					case K_WITH:
					case K_WITHOUT:
					case K_XML:
					case VALID_NAME:
					case IDENTIFIER:
					case BIND_PARAMETER:
					case STRING_LITERAL:
					case BLOB_LITERAL:
					case MULTILINE_COMMENT:
					case SPACES:
					case UNEXPECTED_CHAR:
						{
						setState(489);
						_la = _input.LA(1);
						if ( _la <= 0 || (_la==STAR) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(490);
						_la = _input.LA(1);
						if ( _la <= 0 || (_la==QUOTE) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						}
						break;
					case STAR:
						{
						{
						setState(492); 
						_errHandler.sync(this);
						_alt = 1;
						do {
							switch (_alt) {
							case 1:
								{
								{
								setState(491);
								match(STAR);
								}
								}
								break;
							default:
								throw new NoViableAltException(this);
							}
							setState(494); 
							_errHandler.sync(this);
							_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
						} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
						setState(496);
						_la = _input.LA(1);
						if ( _la <= 0 || (_la==QUOTE) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					} 
				}
				setState(501);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,14,_ctx);
			}
			setState(505);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,15,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(502);
					match(STAR);
					}
					} 
				}
				setState(507);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,15,_ctx);
			}
			setState(511);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__1) | (1L << NUMERIC_LITERAL) | (1L << INT) | (1L << SINGLE_LINE_COMMENT_JAVA) | (1L << SCOL) | (1L << DUAL_DOT) | (1L << DOT) | (1L << OPEN_PAR) | (1L << CLOSE_PAR) | (1L << COMMA) | (1L << ASSIGN) | (1L << STAR) | (1L << PLUS) | (1L << DOUBLEPLUS) | (1L << DOUBLEMINUS) | (1L << MINUS) | (1L << TILDE) | (1L << OR) | (1L << AND) | (1L << DIV) | (1L << MOD) | (1L << LT2) | (1L << GT2) | (1L << AMP) | (1L << PIPE) | (1L << LT) | (1L << LT_EQ) | (1L << GT) | (1L << GT_EQ) | (1L << EQ) | (1L << NOT_EQ1) | (1L << NOT_EQ2) | (1L << NOT) | (1L << OPEN_BRACKET) | (1L << CLOSE_BRACKET) | (1L << OPEN_SQUARE) | (1L << CLOSE_SQUARE) | (1L << QUSTION_MAP) | (1L << UNDER_SCORE) | (1L << K_FOREACH) | (1L << K_VAR) | (1L << K_PRINT) | (1L << K_ABORT) | (1L << K_ACTION) | (1L << K_ADD) | (1L << K_AFTER) | (1L << K_AGGREGATION_FUNCTION) | (1L << K_ALL) | (1L << K_ALTER) | (1L << K_ANALYZE) | (1L << K_AND) | (1L << K_AS) | (1L << K_ASC) | (1L << K_ATTACH) | (1L << K_AUTOINCREMENT) | (1L << K_BEFORE) | (1L << K_BEGIN) | (1L << K_BETWEEN) | (1L << K_BREAK) | (1L << K_BY) | (1L << K_BOOLEAN))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (K_CASCADE - 64)) | (1L << (K_CASE - 64)) | (1L << (K_CAST - 64)) | (1L << (K_CHECK - 64)) | (1L << (K_COLLATE - 64)) | (1L << (K_COLUMN - 64)) | (1L << (K_COMMIT - 64)) | (1L << (K_CONFLICT - 64)) | (1L << (K_CONSTRAINT - 64)) | (1L << (K_CONTINUE - 64)) | (1L << (K_CREATE - 64)) | (1L << (K_CROSS - 64)) | (1L << (K_CSV - 64)) | (1L << (K_CURRENT_DATE - 64)) | (1L << (K_CURRENT_TIME - 64)) | (1L << (K_CURRENT_TIMESTAMP - 64)) | (1L << (K_DATABASE - 64)) | (1L << (K_DEFAULT - 64)) | (1L << (K_DEFERRABLE - 64)) | (1L << (K_DEFERRED - 64)) | (1L << (K_DELETE - 64)) | (1L << (K_DESC - 64)) | (1L << (K_DETACH - 64)) | (1L << (K_DISTINCT - 64)) | (1L << (K_DO - 64)) | (1L << (K_DROP - 64)) | (1L << (K_EACH - 64)) | (1L << (K_ELSE - 64)) | (1L << (K_ELSE_IF - 64)) | (1L << (K_END - 64)) | (1L << (K_ENABLE - 64)) | (1L << (K_ESCAPE - 64)) | (1L << (K_EXCEPT - 64)) | (1L << (K_EXCLUSIVE - 64)) | (1L << (K_EXISTS - 64)) | (1L << (K_EXPLAIN - 64)) | (1L << (K_FAIL - 64)) | (1L << (K_FALSE - 64)) | (1L << (K_FOR - 64)) | (1L << (K_FOREIGN - 64)) | (1L << (K_FROM - 64)) | (1L << (K_FUNCTION - 64)) | (1L << (K_FULL - 64)) | (1L << (K_GLOB - 64)) | (1L << (K_GROUP - 64)) | (1L << (K_HAVING - 64)) | (1L << (K_IF - 64)) | (1L << (K_IGNORE - 64)) | (1L << (K_IMMEDIATE - 64)) | (1L << (K_IN - 64)) | (1L << (K_INDEX - 64)) | (1L << (K_INDEXED - 64)) | (1L << (K_INITIALLY - 64)) | (1L << (K_INNER - 64)) | (1L << (K_INSERT - 64)) | (1L << (K_INSTEAD - 64)) | (1L << (K_INTERSECT - 64)) | (1L << (K_INTO - 64)) | (1L << (K_IS - 64)) | (1L << (K_ISNULL - 64)) | (1L << (K_JOIN - 64)) | (1L << (K_KEY - 64)) | (1L << (K_LEFT - 64)) | (1L << (K_LIKE - 64)))) != 0) || ((((_la - 128)) & ~0x3f) == 0 && ((1L << (_la - 128)) & ((1L << (K_LIMIT - 128)) | (1L << (K_MATCH - 128)) | (1L << (K_NATURAL - 128)) | (1L << (K_NEXTVAL - 128)) | (1L << (K_NEW - 128)) | (1L << (K_NO - 128)) | (1L << (K_NOT - 128)) | (1L << (K_NOTNULL - 128)) | (1L << (K_NUMBER - 128)) | (1L << (K_NULL - 128)) | (1L << (K_OF - 128)) | (1L << (K_OFFSET - 128)) | (1L << (K_ON - 128)) | (1L << (K_ONLY - 128)) | (1L << (K_OR - 128)) | (1L << (K_ORDER - 128)) | (1L << (K_OUTER - 128)) | (1L << (K_PATH - 128)) | (1L << (K_PLAN - 128)) | (1L << (K_PRAGMA - 128)) | (1L << (K_PRIMARY - 128)) | (1L << (K_QUERY - 128)) | (1L << (K_RAISE - 128)) | (1L << (K_RECURSIVE - 128)) | (1L << (K_REFERENCES - 128)) | (1L << (K_REGEXP - 128)) | (1L << (K_REINDEX - 128)) | (1L << (K_RELEASE - 128)) | (1L << (K_RENAME - 128)) | (1L << (K_REPLACE - 128)) | (1L << (K_RESTRICT - 128)) | (1L << (K_RETURN - 128)) | (1L << (K_RIGHT - 128)) | (1L << (K_ROLLBACK - 128)) | (1L << (K_ROW - 128)) | (1L << (K_SAVEPOINT - 128)) | (1L << (K_SELECT - 128)) | (1L << (K_SET - 128)) | (1L << (K_STRING - 128)) | (1L << (K_SWITCH - 128)) | (1L << (K_TABLE - 128)) | (1L << (K_TYPE - 128)) | (1L << (K_TEMP - 128)) | (1L << (K_TEMPORARY - 128)) | (1L << (K_THEN - 128)) | (1L << (K_TO - 128)) | (1L << (K_TRANSACTION - 128)) | (1L << (K_TRIGGER - 128)) | (1L << (K_TRUE - 128)) | (1L << (K_UNION - 128)) | (1L << (K_UNIQUE - 128)) | (1L << (K_UPDATE - 128)) | (1L << (K_USING - 128)) | (1L << (K_VACUUM - 128)) | (1L << (K_VALUES - 128)) | (1L << (K_VIEW - 128)) | (1L << (K_VIRTUAL - 128)) | (1L << (K_WHEN - 128)) | (1L << (K_WHERE - 128)) | (1L << (K_WHILE - 128)) | (1L << (K_WITH - 128)) | (1L << (K_WITHOUT - 128)) | (1L << (K_XML - 128)) | (1L << (VALID_NAME - 128)))) != 0) || ((((_la - 192)) & ~0x3f) == 0 && ((1L << (_la - 192)) & ((1L << (IDENTIFIER - 192)) | (1L << (BIND_PARAMETER - 192)) | (1L << (STRING_LITERAL - 192)) | (1L << (BLOB_LITERAL - 192)) | (1L << (MULTILINE_COMMENT - 192)) | (1L << (SPACES - 192)) | (1L << (UNEXPECTED_CHAR - 192)))) != 0)) {
				{
				{
				setState(508);
				_la = _input.LA(1);
				if ( _la <= 0 || (_la==QUOTE) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				}
				setState(513);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Default_value_parameterContext extends ParserRuleContext {
		public TerminalNode NUMERIC_LITERAL() { return getToken(RulesParser.NUMERIC_LITERAL, 0); }
		public List<TerminalNode> QUOTE() { return getTokens(RulesParser.QUOTE); }
		public TerminalNode QUOTE(int i) {
			return getToken(RulesParser.QUOTE, i);
		}
		public Value_in_quoteContext value_in_quote() {
			return getRuleContext(Value_in_quoteContext.class,0);
		}
		public TerminalNode K_TRUE() { return getToken(RulesParser.K_TRUE, 0); }
		public TerminalNode K_FALSE() { return getToken(RulesParser.K_FALSE, 0); }
		public TerminalNode K_NULL() { return getToken(RulesParser.K_NULL, 0); }
		public Default_value_parameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_default_value_parameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterDefault_value_parameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitDefault_value_parameter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitDefault_value_parameter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Default_value_parameterContext default_value_parameter() throws RecognitionException {
		Default_value_parameterContext _localctx = new Default_value_parameterContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_default_value_parameter);
		try {
			setState(522);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case NUMERIC_LITERAL:
				enterOuterAlt(_localctx, 1);
				{
				setState(514);
				match(NUMERIC_LITERAL);
				}
				break;
			case QUOTE:
				enterOuterAlt(_localctx, 2);
				{
				setState(515);
				match(QUOTE);
				setState(516);
				value_in_quote();
				setState(517);
				match(QUOTE);
				}
				break;
			case K_TRUE:
				enterOuterAlt(_localctx, 3);
				{
				setState(519);
				match(K_TRUE);
				}
				break;
			case K_FALSE:
				enterOuterAlt(_localctx, 4);
				{
				setState(520);
				match(K_FALSE);
				}
				break;
			case K_NULL:
				enterOuterAlt(_localctx, 5);
				{
				setState(521);
				match(K_NULL);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Initial_variable_declarationContext extends ParserRuleContext {
		public TerminalNode K_VAR() { return getToken(RulesParser.K_VAR, 0); }
		public Variable_nameContext variable_name() {
			return getRuleContext(Variable_nameContext.class,0);
		}
		public Initial_variable_declarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_initial_variable_declaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterInitial_variable_declaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitInitial_variable_declaration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitInitial_variable_declaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Initial_variable_declarationContext initial_variable_declaration() throws RecognitionException {
		Initial_variable_declarationContext _localctx = new Initial_variable_declarationContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_initial_variable_declaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(524);
			match(K_VAR);
			setState(525);
			variable_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Prototype_functionContext extends ParserRuleContext {
		public Variable_nameContext variable_name() {
			return getRuleContext(Variable_nameContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public Body_prototype_functionContext body_prototype_function() {
			return getRuleContext(Body_prototype_functionContext.class,0);
		}
		public Prototype_function_parameterContext prototype_function_parameter() {
			return getRuleContext(Prototype_function_parameterContext.class,0);
		}
		public Prototype_functionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prototype_function; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterPrototype_function(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitPrototype_function(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitPrototype_function(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Prototype_functionContext prototype_function() throws RecognitionException {
		Prototype_functionContext _localctx = new Prototype_functionContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_prototype_function);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(527);
			variable_name();
			setState(528);
			match(OPEN_PAR);
			{
			setState(529);
			prototype_function_parameter();
			}
			setState(530);
			match(CLOSE_PAR);
			setState(531);
			body_prototype_function();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Prototype_function_parameterContext extends ParserRuleContext {
		public Function_parameter_in_prototypeContext function_parameter_in_prototype() {
			return getRuleContext(Function_parameter_in_prototypeContext.class,0);
		}
		public Prototype_function_parameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prototype_function_parameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterPrototype_function_parameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitPrototype_function_parameter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitPrototype_function_parameter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Prototype_function_parameterContext prototype_function_parameter() throws RecognitionException {
		Prototype_function_parameterContext _localctx = new Prototype_function_parameterContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_prototype_function_parameter);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(534);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_VAR) {
				{
				setState(533);
				function_parameter_in_prototype();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_parameter_in_prototypeContext extends ParserRuleContext {
		public List<Initial_variable_declarationContext> initial_variable_declaration() {
			return getRuleContexts(Initial_variable_declarationContext.class);
		}
		public Initial_variable_declarationContext initial_variable_declaration(int i) {
			return getRuleContext(Initial_variable_declarationContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(RulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(RulesParser.COMMA, i);
		}
		public Function_parameter_with_defaultContext function_parameter_with_default() {
			return getRuleContext(Function_parameter_with_defaultContext.class,0);
		}
		public Function_parameter_in_prototypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_parameter_in_prototype; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterFunction_parameter_in_prototype(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitFunction_parameter_in_prototype(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitFunction_parameter_in_prototype(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Function_parameter_in_prototypeContext function_parameter_in_prototype() throws RecognitionException {
		Function_parameter_in_prototypeContext _localctx = new Function_parameter_in_prototypeContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_function_parameter_in_prototype);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(536);
			initial_variable_declaration();
			setState(541);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,19,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(537);
					match(COMMA);
					setState(538);
					initial_variable_declaration();
					}
					} 
				}
				setState(543);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,19,_ctx);
			}
			}
			setState(546);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,20,_ctx) ) {
			case 1+1:
				{
				setState(544);
				match(COMMA);
				setState(545);
				function_parameter_with_default();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_parameter_with_defaultContext extends ParserRuleContext {
		public List<Initial_variable_declaration_with_default_valueContext> initial_variable_declaration_with_default_value() {
			return getRuleContexts(Initial_variable_declaration_with_default_valueContext.class);
		}
		public Initial_variable_declaration_with_default_valueContext initial_variable_declaration_with_default_value(int i) {
			return getRuleContext(Initial_variable_declaration_with_default_valueContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(RulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(RulesParser.COMMA, i);
		}
		public Function_parameter_with_defaultContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_parameter_with_default; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterFunction_parameter_with_default(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitFunction_parameter_with_default(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitFunction_parameter_with_default(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Function_parameter_with_defaultContext function_parameter_with_default() throws RecognitionException {
		Function_parameter_with_defaultContext _localctx = new Function_parameter_with_defaultContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_function_parameter_with_default);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(548);
			initial_variable_declaration_with_default_value();
			setState(553);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(549);
				match(COMMA);
				setState(550);
				initial_variable_declaration_with_default_value();
				}
				}
				setState(555);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Initial_variable_declaration_with_default_valueContext extends ParserRuleContext {
		public Initial_variable_declarationContext initial_variable_declaration() {
			return getRuleContext(Initial_variable_declarationContext.class,0);
		}
		public TerminalNode ASSIGN() { return getToken(RulesParser.ASSIGN, 0); }
		public Default_value_parameterContext default_value_parameter() {
			return getRuleContext(Default_value_parameterContext.class,0);
		}
		public Initial_variable_declaration_with_default_valueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_initial_variable_declaration_with_default_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterInitial_variable_declaration_with_default_value(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitInitial_variable_declaration_with_default_value(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitInitial_variable_declaration_with_default_value(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Initial_variable_declaration_with_default_valueContext initial_variable_declaration_with_default_value() throws RecognitionException {
		Initial_variable_declaration_with_default_valueContext _localctx = new Initial_variable_declaration_with_default_valueContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_initial_variable_declaration_with_default_value);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(556);
			initial_variable_declaration();
			setState(557);
			match(ASSIGN);
			setState(558);
			default_value_parameter();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Body_functionContext extends ParserRuleContext {
		public TerminalNode OPEN_BRACKET() { return getToken(RulesParser.OPEN_BRACKET, 0); }
		public TerminalNode CLOSE_BRACKET() { return getToken(RulesParser.CLOSE_BRACKET, 0); }
		public List<Java_stmtContext> java_stmt() {
			return getRuleContexts(Java_stmtContext.class);
		}
		public Java_stmtContext java_stmt(int i) {
			return getRuleContext(Java_stmtContext.class,i);
		}
		public Body_functionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_body_function; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterBody_function(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitBody_function(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitBody_function(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Body_functionContext body_function() throws RecognitionException {
		Body_functionContext _localctx = new Body_functionContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_body_function);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(560);
			match(OPEN_BRACKET);
			setState(564);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << DOUBLEPLUS) | (1L << DOUBLEMINUS) | (1L << K_VAR) | (1L << K_PRINT) | (1L << K_BREAK))) != 0) || ((((_la - 73)) & ~0x3f) == 0 && ((1L << (_la - 73)) & ((1L << (K_CONTINUE - 73)) | (1L << (K_DO - 73)) | (1L << (K_FOR - 73)) | (1L << (K_IF - 73)))) != 0) || ((((_la - 167)) & ~0x3f) == 0 && ((1L << (_la - 167)) & ((1L << (K_SWITCH - 167)) | (1L << (K_WHILE - 167)) | (1L << (VALID_NAME - 167)))) != 0)) {
				{
				{
				setState(561);
				java_stmt();
				}
				}
				setState(566);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(567);
			match(CLOSE_BRACKET);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Body_prototype_functionContext extends ParserRuleContext {
		public TerminalNode OPEN_BRACKET() { return getToken(RulesParser.OPEN_BRACKET, 0); }
		public TerminalNode CLOSE_BRACKET() { return getToken(RulesParser.CLOSE_BRACKET, 0); }
		public List<Java_stmtContext> java_stmt() {
			return getRuleContexts(Java_stmtContext.class);
		}
		public Java_stmtContext java_stmt(int i) {
			return getRuleContext(Java_stmtContext.class,i);
		}
		public Return_stmtContext return_stmt() {
			return getRuleContext(Return_stmtContext.class,0);
		}
		public Body_prototype_functionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_body_prototype_function; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterBody_prototype_function(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitBody_prototype_function(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitBody_prototype_function(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Body_prototype_functionContext body_prototype_function() throws RecognitionException {
		Body_prototype_functionContext _localctx = new Body_prototype_functionContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_body_prototype_function);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(569);
			match(OPEN_BRACKET);
			setState(573);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << DOUBLEPLUS) | (1L << DOUBLEMINUS) | (1L << K_VAR) | (1L << K_PRINT) | (1L << K_BREAK))) != 0) || ((((_la - 73)) & ~0x3f) == 0 && ((1L << (_la - 73)) & ((1L << (K_CONTINUE - 73)) | (1L << (K_DO - 73)) | (1L << (K_FOR - 73)) | (1L << (K_IF - 73)))) != 0) || ((((_la - 167)) & ~0x3f) == 0 && ((1L << (_la - 167)) & ((1L << (K_SWITCH - 167)) | (1L << (K_WHILE - 167)) | (1L << (VALID_NAME - 167)))) != 0)) {
				{
				{
				setState(570);
				java_stmt();
				}
				}
				setState(575);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(577);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_RETURN) {
				{
				setState(576);
				return_stmt();
				}
			}

			setState(579);
			match(CLOSE_BRACKET);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Java_stmtContext extends ParserRuleContext {
		public For_stmtContext for_stmt() {
			return getRuleContext(For_stmtContext.class,0);
		}
		public If_statementContext if_statement() {
			return getRuleContext(If_statementContext.class,0);
		}
		public Callback_functionContext callback_function() {
			return getRuleContext(Callback_functionContext.class,0);
		}
		public While_statementContext while_statement() {
			return getRuleContext(While_statementContext.class,0);
		}
		public Do_while_statementContext do_while_statement() {
			return getRuleContext(Do_while_statementContext.class,0);
		}
		public Create_variable_stmtContext create_variable_stmt() {
			return getRuleContext(Create_variable_stmtContext.class,0);
		}
		public Switch_statementContext switch_statement() {
			return getRuleContext(Switch_statementContext.class,0);
		}
		public Foreach_statementContext foreach_statement() {
			return getRuleContext(Foreach_statementContext.class,0);
		}
		public Print_statementContext print_statement() {
			return getRuleContext(Print_statementContext.class,0);
		}
		public Assign_variable_without_varContext assign_variable_without_var() {
			return getRuleContext(Assign_variable_without_varContext.class,0);
		}
		public Break_javaContext break_java() {
			return getRuleContext(Break_javaContext.class,0);
		}
		public Continue_javaContext continue_java() {
			return getRuleContext(Continue_javaContext.class,0);
		}
		public Increment_stmtContext increment_stmt() {
			return getRuleContext(Increment_stmtContext.class,0);
		}
		public Java_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_java_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterJava_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitJava_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitJava_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Java_stmtContext java_stmt() throws RecognitionException {
		Java_stmtContext _localctx = new Java_stmtContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_java_stmt);
		try {
			setState(594);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,25,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(581);
				for_stmt();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(582);
				if_statement();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(583);
				callback_function();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(584);
				while_statement();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(585);
				do_while_statement();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(586);
				create_variable_stmt();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(587);
				switch_statement();
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(588);
				foreach_statement();
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(589);
				print_statement();
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(590);
				assign_variable_without_var();
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(591);
				break_java();
				}
				break;
			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(592);
				continue_java();
				}
				break;
			case 13:
				enterOuterAlt(_localctx, 13);
				{
				setState(593);
				increment_stmt();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Full_condition_javaContext extends ParserRuleContext {
		public One_condition_in_javaContext one_condition_in_java() {
			return getRuleContext(One_condition_in_javaContext.class,0);
		}
		public List<Multi_condition_javaContext> multi_condition_java() {
			return getRuleContexts(Multi_condition_javaContext.class);
		}
		public Multi_condition_javaContext multi_condition_java(int i) {
			return getRuleContext(Multi_condition_javaContext.class,i);
		}
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public Full_condition_javaContext full_condition_java() {
			return getRuleContext(Full_condition_javaContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public Full_condition_javaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_full_condition_java; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterFull_condition_java(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitFull_condition_java(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitFull_condition_java(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Full_condition_javaContext full_condition_java() throws RecognitionException {
		Full_condition_javaContext _localctx = new Full_condition_javaContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_full_condition_java);
		try {
			int _alt;
			setState(607);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,27,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(596);
				one_condition_in_java();
				setState(600);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,26,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(597);
						multi_condition_java();
						}
						} 
					}
					setState(602);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,26,_ctx);
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(603);
				match(OPEN_PAR);
				setState(604);
				full_condition_java();
				setState(605);
				match(CLOSE_PAR);
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Multi_condition_javaContext extends ParserRuleContext {
		public Logical_in_javaContext logical_in_java() {
			return getRuleContext(Logical_in_javaContext.class,0);
		}
		public One_condition_in_javaContext one_condition_in_java() {
			return getRuleContext(One_condition_in_javaContext.class,0);
		}
		public Multi_condition_javaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multi_condition_java; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterMulti_condition_java(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitMulti_condition_java(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitMulti_condition_java(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Multi_condition_javaContext multi_condition_java() throws RecognitionException {
		Multi_condition_javaContext _localctx = new Multi_condition_javaContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_multi_condition_java);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(609);
			logical_in_java();
			setState(610);
			one_condition_in_java();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Header_ifContext extends ParserRuleContext {
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public Full_condition_javaContext full_condition_java() {
			return getRuleContext(Full_condition_javaContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public Header_ifContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_header_if; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterHeader_if(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitHeader_if(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitHeader_if(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Header_ifContext header_if() throws RecognitionException {
		Header_ifContext _localctx = new Header_ifContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_header_if);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(612);
			match(OPEN_PAR);
			setState(613);
			full_condition_java();
			setState(614);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class One_condition_in_javaContext extends ParserRuleContext {
		public Not_var_nameContext not_var_name() {
			return getRuleContext(Not_var_nameContext.class,0);
		}
		public Not_one_conditionContext not_one_condition() {
			return getRuleContext(Not_one_conditionContext.class,0);
		}
		public TerminalNode K_TRUE() { return getToken(RulesParser.K_TRUE, 0); }
		public TerminalNode K_FALSE() { return getToken(RulesParser.K_FALSE, 0); }
		public Number_conditionContext number_condition() {
			return getRuleContext(Number_conditionContext.class,0);
		}
		public Logical_conditionContext logical_condition() {
			return getRuleContext(Logical_conditionContext.class,0);
		}
		public Callback_function_without_scolContext callback_function_without_scol() {
			return getRuleContext(Callback_function_without_scolContext.class,0);
		}
		public One_condition_in_javaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_one_condition_in_java; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterOne_condition_in_java(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitOne_condition_in_java(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitOne_condition_in_java(this);
			else return visitor.visitChildren(this);
		}
	}

	public final One_condition_in_javaContext one_condition_in_java() throws RecognitionException {
		One_condition_in_javaContext _localctx = new One_condition_in_javaContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_one_condition_in_java);
		try {
			setState(623);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,28,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(616);
				not_var_name();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(617);
				not_one_condition();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(618);
				match(K_TRUE);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(619);
				match(K_FALSE);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(620);
				number_condition();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(621);
				logical_condition();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(622);
				callback_function_without_scol();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Not_var_nameContext extends ParserRuleContext {
		public Variable_nameContext variable_name() {
			return getRuleContext(Variable_nameContext.class,0);
		}
		public TerminalNode NOT() { return getToken(RulesParser.NOT, 0); }
		public Not_var_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_not_var_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterNot_var_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitNot_var_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitNot_var_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Not_var_nameContext not_var_name() throws RecognitionException {
		Not_var_nameContext _localctx = new Not_var_nameContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_not_var_name);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(626);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==NOT) {
				{
				setState(625);
				match(NOT);
				}
			}

			setState(628);
			variable_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Not_one_conditionContext extends ParserRuleContext {
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public One_condition_in_javaContext one_condition_in_java() {
			return getRuleContext(One_condition_in_javaContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public TerminalNode NOT() { return getToken(RulesParser.NOT, 0); }
		public Not_one_conditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_not_one_condition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterNot_one_condition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitNot_one_condition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitNot_one_condition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Not_one_conditionContext not_one_condition() throws RecognitionException {
		Not_one_conditionContext _localctx = new Not_one_conditionContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_not_one_condition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(631);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==NOT) {
				{
				setState(630);
				match(NOT);
				}
			}

			setState(633);
			match(OPEN_PAR);
			setState(634);
			one_condition_in_java();
			setState(635);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Number_conditionContext extends ParserRuleContext {
		public List<Expr_javaContext> expr_java() {
			return getRuleContexts(Expr_javaContext.class);
		}
		public Expr_javaContext expr_java(int i) {
			return getRuleContext(Expr_javaContext.class,i);
		}
		public Comparison_in_javaContext comparison_in_java() {
			return getRuleContext(Comparison_in_javaContext.class,0);
		}
		public Number_conditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_number_condition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterNumber_condition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitNumber_condition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitNumber_condition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Number_conditionContext number_condition() throws RecognitionException {
		Number_conditionContext _localctx = new Number_conditionContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_number_condition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(637);
			expr_java(0);
			setState(638);
			comparison_in_java();
			setState(639);
			expr_java(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Logical_conditionContext extends ParserRuleContext {
		public List<Logic_operation_statement_in_javaContext> logic_operation_statement_in_java() {
			return getRuleContexts(Logic_operation_statement_in_javaContext.class);
		}
		public Logic_operation_statement_in_javaContext logic_operation_statement_in_java(int i) {
			return getRuleContext(Logic_operation_statement_in_javaContext.class,i);
		}
		public Logical_in_javaContext logical_in_java() {
			return getRuleContext(Logical_in_javaContext.class,0);
		}
		public Logical_conditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logical_condition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterLogical_condition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitLogical_condition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitLogical_condition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Logical_conditionContext logical_condition() throws RecognitionException {
		Logical_conditionContext _localctx = new Logical_conditionContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_logical_condition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(641);
			logic_operation_statement_in_java(0);
			setState(642);
			logical_in_java();
			setState(643);
			logic_operation_statement_in_java(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expr_javaContext extends ParserRuleContext {
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public List<Expr_javaContext> expr_java() {
			return getRuleContexts(Expr_javaContext.class);
		}
		public Expr_javaContext expr_java(int i) {
			return getRuleContext(Expr_javaContext.class,i);
		}
		public Operation_in_javaContext operation_in_java() {
			return getRuleContext(Operation_in_javaContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public Number_value_javaContext number_value_java() {
			return getRuleContext(Number_value_javaContext.class,0);
		}
		public Expr_javaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr_java; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterExpr_java(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitExpr_java(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitExpr_java(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Expr_javaContext expr_java() throws RecognitionException {
		return expr_java(0);
	}

	private Expr_javaContext expr_java(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Expr_javaContext _localctx = new Expr_javaContext(_ctx, _parentState);
		Expr_javaContext _prevctx = _localctx;
		int _startState = 72;
		enterRecursionRule(_localctx, 72, RULE_expr_java, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(653);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case OPEN_PAR:
				{
				setState(646);
				match(OPEN_PAR);
				setState(647);
				expr_java(0);
				setState(648);
				operation_in_java();
				setState(649);
				expr_java(0);
				setState(650);
				match(CLOSE_PAR);
				}
				break;
			case NUMERIC_LITERAL:
			case DOUBLEPLUS:
			case DOUBLEMINUS:
			case QUOTE:
			case VALID_NAME:
				{
				setState(652);
				number_value_java();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(661);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,32,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Expr_javaContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_expr_java);
					setState(655);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(656);
					operation_in_java();
					setState(657);
					expr_java(3);
					}
					} 
				}
				setState(663);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,32,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Number_value_javaContext extends ParserRuleContext {
		public TerminalNode NUMERIC_LITERAL() { return getToken(RulesParser.NUMERIC_LITERAL, 0); }
		public Variable_nameContext variable_name() {
			return getRuleContext(Variable_nameContext.class,0);
		}
		public List<TerminalNode> QUOTE() { return getTokens(RulesParser.QUOTE); }
		public TerminalNode QUOTE(int i) {
			return getToken(RulesParser.QUOTE, i);
		}
		public Variable_valueContext variable_value() {
			return getRuleContext(Variable_valueContext.class,0);
		}
		public Var_name_item_in_arrayContext var_name_item_in_array() {
			return getRuleContext(Var_name_item_in_arrayContext.class,0);
		}
		public Access_to_data_in_jsonContext access_to_data_in_json() {
			return getRuleContext(Access_to_data_in_jsonContext.class,0);
		}
		public Callback_function_without_scolContext callback_function_without_scol() {
			return getRuleContext(Callback_function_without_scolContext.class,0);
		}
		public Advanced_increaseContext advanced_increase() {
			return getRuleContext(Advanced_increaseContext.class,0);
		}
		public Delayed_increaseContext delayed_increase() {
			return getRuleContext(Delayed_increaseContext.class,0);
		}
		public Number_value_javaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_number_value_java; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterNumber_value_java(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitNumber_value_java(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitNumber_value_java(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Number_value_javaContext number_value_java() throws RecognitionException {
		Number_value_javaContext _localctx = new Number_value_javaContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_number_value_java);
		try {
			setState(675);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,33,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(664);
				match(NUMERIC_LITERAL);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(665);
				variable_name();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(666);
				match(QUOTE);
				setState(667);
				variable_value();
				setState(668);
				match(QUOTE);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(670);
				var_name_item_in_array();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(671);
				access_to_data_in_json();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(672);
				callback_function_without_scol();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(673);
				advanced_increase();
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(674);
				delayed_increase();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Logic_operation_statement_in_javaContext extends ParserRuleContext {
		public Variable_nameContext variable_name() {
			return getRuleContext(Variable_nameContext.class,0);
		}
		public List<TerminalNode> QUOTE() { return getTokens(RulesParser.QUOTE); }
		public TerminalNode QUOTE(int i) {
			return getToken(RulesParser.QUOTE, i);
		}
		public Variable_valueContext variable_value() {
			return getRuleContext(Variable_valueContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public List<Logic_operation_statement_in_javaContext> logic_operation_statement_in_java() {
			return getRuleContexts(Logic_operation_statement_in_javaContext.class);
		}
		public Logic_operation_statement_in_javaContext logic_operation_statement_in_java(int i) {
			return getRuleContext(Logic_operation_statement_in_javaContext.class,i);
		}
		public Logical_in_javaContext logical_in_java() {
			return getRuleContext(Logical_in_javaContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public Logic_operation_statement_in_javaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logic_operation_statement_in_java; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterLogic_operation_statement_in_java(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitLogic_operation_statement_in_java(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitLogic_operation_statement_in_java(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Logic_operation_statement_in_javaContext logic_operation_statement_in_java() throws RecognitionException {
		return logic_operation_statement_in_java(0);
	}

	private Logic_operation_statement_in_javaContext logic_operation_statement_in_java(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		Logic_operation_statement_in_javaContext _localctx = new Logic_operation_statement_in_javaContext(_ctx, _parentState);
		Logic_operation_statement_in_javaContext _prevctx = _localctx;
		int _startState = 76;
		enterRecursionRule(_localctx, 76, RULE_logic_operation_statement_in_java, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(689);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case VALID_NAME:
				{
				setState(678);
				variable_name();
				}
				break;
			case QUOTE:
				{
				setState(679);
				match(QUOTE);
				setState(680);
				variable_value();
				setState(681);
				match(QUOTE);
				}
				break;
			case OPEN_PAR:
				{
				setState(683);
				match(OPEN_PAR);
				setState(684);
				logic_operation_statement_in_java(0);
				setState(685);
				logical_in_java();
				setState(686);
				logic_operation_statement_in_java(0);
				setState(687);
				match(CLOSE_PAR);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(697);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,35,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Logic_operation_statement_in_javaContext(_parentctx, _parentState);
					pushNewRecursionContext(_localctx, _startState, RULE_logic_operation_statement_in_java);
					setState(691);
					if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
					setState(692);
					logical_in_java();
					setState(693);
					logic_operation_statement_in_java(3);
					}
					} 
				}
				setState(699);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,35,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Operation_in_javaContext extends ParserRuleContext {
		public TerminalNode PLUS() { return getToken(RulesParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(RulesParser.MINUS, 0); }
		public TerminalNode STAR() { return getToken(RulesParser.STAR, 0); }
		public TerminalNode DIV() { return getToken(RulesParser.DIV, 0); }
		public TerminalNode MOD() { return getToken(RulesParser.MOD, 0); }
		public Operation_in_javaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_operation_in_java; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterOperation_in_java(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitOperation_in_java(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitOperation_in_java(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Operation_in_javaContext operation_in_java() throws RecognitionException {
		Operation_in_javaContext _localctx = new Operation_in_javaContext(_ctx, getState());
		enterRule(_localctx, 78, RULE_operation_in_java);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(700);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << STAR) | (1L << PLUS) | (1L << MINUS) | (1L << DIV) | (1L << MOD))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Comparison_in_javaContext extends ParserRuleContext {
		public TerminalNode LT() { return getToken(RulesParser.LT, 0); }
		public TerminalNode LT_EQ() { return getToken(RulesParser.LT_EQ, 0); }
		public TerminalNode GT() { return getToken(RulesParser.GT, 0); }
		public TerminalNode GT_EQ() { return getToken(RulesParser.GT_EQ, 0); }
		public TerminalNode EQ() { return getToken(RulesParser.EQ, 0); }
		public TerminalNode NOT_EQ1() { return getToken(RulesParser.NOT_EQ1, 0); }
		public Comparison_in_javaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_comparison_in_java; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterComparison_in_java(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitComparison_in_java(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitComparison_in_java(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Comparison_in_javaContext comparison_in_java() throws RecognitionException {
		Comparison_in_javaContext _localctx = new Comparison_in_javaContext(_ctx, getState());
		enterRule(_localctx, 80, RULE_comparison_in_java);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(702);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LT) | (1L << LT_EQ) | (1L << GT) | (1L << GT_EQ) | (1L << EQ) | (1L << NOT_EQ1))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Logical_in_javaContext extends ParserRuleContext {
		public TerminalNode OR() { return getToken(RulesParser.OR, 0); }
		public TerminalNode AND() { return getToken(RulesParser.AND, 0); }
		public Logical_in_javaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logical_in_java; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterLogical_in_java(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitLogical_in_java(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitLogical_in_java(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Logical_in_javaContext logical_in_java() throws RecognitionException {
		Logical_in_javaContext _localctx = new Logical_in_javaContext(_ctx, getState());
		enterRule(_localctx, 82, RULE_logical_in_java);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(704);
			_la = _input.LA(1);
			if ( !(_la==OR || _la==AND) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Line_condition__expression_javaContext extends ParserRuleContext {
		public Full_condition_javaContext full_condition_java() {
			return getRuleContext(Full_condition_javaContext.class,0);
		}
		public TerminalNode QUSTION_MAP() { return getToken(RulesParser.QUSTION_MAP, 0); }
		public List<Line_condition_resultContext> line_condition_result() {
			return getRuleContexts(Line_condition_resultContext.class);
		}
		public Line_condition_resultContext line_condition_result(int i) {
			return getRuleContext(Line_condition_resultContext.class,i);
		}
		public TerminalNode DUAL_DOT() { return getToken(RulesParser.DUAL_DOT, 0); }
		public Line_condition__expression_javaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_line_condition__expression_java; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterLine_condition__expression_java(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitLine_condition__expression_java(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitLine_condition__expression_java(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Line_condition__expression_javaContext line_condition__expression_java() throws RecognitionException {
		Line_condition__expression_javaContext _localctx = new Line_condition__expression_javaContext(_ctx, getState());
		enterRule(_localctx, 84, RULE_line_condition__expression_java);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(706);
			full_condition_java();
			setState(707);
			match(QUSTION_MAP);
			setState(708);
			line_condition_result();
			setState(709);
			match(DUAL_DOT);
			setState(710);
			line_condition_result();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Line_condition_resultContext extends ParserRuleContext {
		public Variable_valueContext variable_value() {
			return getRuleContext(Variable_valueContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public Line_condition__expression_javaContext line_condition__expression_java() {
			return getRuleContext(Line_condition__expression_javaContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public Line_condition_resultContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_line_condition_result; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterLine_condition_result(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitLine_condition_result(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitLine_condition_result(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Line_condition_resultContext line_condition_result() throws RecognitionException {
		Line_condition_resultContext _localctx = new Line_condition_resultContext(_ctx, getState());
		enterRule(_localctx, 86, RULE_line_condition_result);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(717);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,36,_ctx) ) {
			case 1:
				{
				setState(712);
				variable_value();
				}
				break;
			case 2:
				{
				setState(713);
				match(OPEN_PAR);
				setState(714);
				line_condition__expression_java();
				setState(715);
				match(CLOSE_PAR);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class If_statementContext extends ParserRuleContext {
		public TerminalNode K_IF() { return getToken(RulesParser.K_IF, 0); }
		public Header_ifContext header_if() {
			return getRuleContext(Header_ifContext.class,0);
		}
		public Java_stmtContext java_stmt() {
			return getRuleContext(Java_stmtContext.class,0);
		}
		public Body_functionContext body_function() {
			return getRuleContext(Body_functionContext.class,0);
		}
		public List<Else_if_statementContext> else_if_statement() {
			return getRuleContexts(Else_if_statementContext.class);
		}
		public Else_if_statementContext else_if_statement(int i) {
			return getRuleContext(Else_if_statementContext.class,i);
		}
		public Else_statementContext else_statement() {
			return getRuleContext(Else_statementContext.class,0);
		}
		public If_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_if_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterIf_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitIf_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitIf_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final If_statementContext if_statement() throws RecognitionException {
		If_statementContext _localctx = new If_statementContext(_ctx, getState());
		enterRule(_localctx, 88, RULE_if_statement);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(719);
			match(K_IF);
			setState(720);
			header_if();
			setState(723);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DOUBLEPLUS:
			case DOUBLEMINUS:
			case K_VAR:
			case K_PRINT:
			case K_BREAK:
			case K_CONTINUE:
			case K_DO:
			case K_FOR:
			case K_IF:
			case K_SWITCH:
			case K_WHILE:
			case VALID_NAME:
				{
				setState(721);
				java_stmt();
				}
				break;
			case OPEN_BRACKET:
				{
				setState(722);
				body_function();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(728);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,38,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(725);
					else_if_statement();
					}
					} 
				}
				setState(730);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,38,_ctx);
			}
			setState(732);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,39,_ctx) ) {
			case 1:
				{
				setState(731);
				else_statement();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Else_if_statementContext extends ParserRuleContext {
		public TerminalNode K_ELSE_IF() { return getToken(RulesParser.K_ELSE_IF, 0); }
		public Header_ifContext header_if() {
			return getRuleContext(Header_ifContext.class,0);
		}
		public Body_functionContext body_function() {
			return getRuleContext(Body_functionContext.class,0);
		}
		public Else_if_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_else_if_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterElse_if_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitElse_if_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitElse_if_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Else_if_statementContext else_if_statement() throws RecognitionException {
		Else_if_statementContext _localctx = new Else_if_statementContext(_ctx, getState());
		enterRule(_localctx, 90, RULE_else_if_statement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(734);
			match(K_ELSE_IF);
			setState(735);
			header_if();
			setState(736);
			body_function();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Else_statementContext extends ParserRuleContext {
		public TerminalNode K_ELSE() { return getToken(RulesParser.K_ELSE, 0); }
		public Java_stmtContext java_stmt() {
			return getRuleContext(Java_stmtContext.class,0);
		}
		public Body_functionContext body_function() {
			return getRuleContext(Body_functionContext.class,0);
		}
		public Else_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_else_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterElse_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitElse_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitElse_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Else_statementContext else_statement() throws RecognitionException {
		Else_statementContext _localctx = new Else_statementContext(_ctx, getState());
		enterRule(_localctx, 92, RULE_else_statement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(738);
			match(K_ELSE);
			setState(741);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DOUBLEPLUS:
			case DOUBLEMINUS:
			case K_VAR:
			case K_PRINT:
			case K_BREAK:
			case K_CONTINUE:
			case K_DO:
			case K_FOR:
			case K_IF:
			case K_SWITCH:
			case K_WHILE:
			case VALID_NAME:
				{
				setState(739);
				java_stmt();
				}
				break;
			case OPEN_BRACKET:
				{
				setState(740);
				body_function();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Switch_statementContext extends ParserRuleContext {
		public TerminalNode K_SWITCH() { return getToken(RulesParser.K_SWITCH, 0); }
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public Variable_valueContext variable_value() {
			return getRuleContext(Variable_valueContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public TerminalNode OPEN_BRACKET() { return getToken(RulesParser.OPEN_BRACKET, 0); }
		public TerminalNode CLOSE_BRACKET() { return getToken(RulesParser.CLOSE_BRACKET, 0); }
		public List<Case_statenemtContext> case_statenemt() {
			return getRuleContexts(Case_statenemtContext.class);
		}
		public Case_statenemtContext case_statenemt(int i) {
			return getRuleContext(Case_statenemtContext.class,i);
		}
		public Default_statementContext default_statement() {
			return getRuleContext(Default_statementContext.class,0);
		}
		public Switch_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_switch_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterSwitch_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitSwitch_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitSwitch_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Switch_statementContext switch_statement() throws RecognitionException {
		Switch_statementContext _localctx = new Switch_statementContext(_ctx, getState());
		enterRule(_localctx, 94, RULE_switch_statement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(743);
			match(K_SWITCH);
			setState(744);
			match(OPEN_PAR);
			setState(745);
			variable_value();
			setState(746);
			match(CLOSE_PAR);
			setState(747);
			match(OPEN_BRACKET);
			{
			setState(751);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==K_CASE) {
				{
				{
				setState(748);
				case_statenemt();
				}
				}
				setState(753);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(755);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_DEFAULT) {
				{
				setState(754);
				default_statement();
				}
			}

			}
			setState(757);
			match(CLOSE_BRACKET);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Case_statenemtContext extends ParserRuleContext {
		public TerminalNode K_CASE() { return getToken(RulesParser.K_CASE, 0); }
		public Case_valueContext case_value() {
			return getRuleContext(Case_valueContext.class,0);
		}
		public TerminalNode DUAL_DOT() { return getToken(RulesParser.DUAL_DOT, 0); }
		public Body_for_case_and_defaultContext body_for_case_and_default() {
			return getRuleContext(Body_for_case_and_defaultContext.class,0);
		}
		public Case_statenemtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_case_statenemt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterCase_statenemt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitCase_statenemt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitCase_statenemt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Case_statenemtContext case_statenemt() throws RecognitionException {
		Case_statenemtContext _localctx = new Case_statenemtContext(_ctx, getState());
		enterRule(_localctx, 96, RULE_case_statenemt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(759);
			match(K_CASE);
			setState(760);
			case_value();
			setState(761);
			match(DUAL_DOT);
			setState(762);
			body_for_case_and_default();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Case_valueContext extends ParserRuleContext {
		public TerminalNode NUMERIC_LITERAL() { return getToken(RulesParser.NUMERIC_LITERAL, 0); }
		public List<TerminalNode> QUOTE() { return getTokens(RulesParser.QUOTE); }
		public TerminalNode QUOTE(int i) {
			return getToken(RulesParser.QUOTE, i);
		}
		public Value_in_quoteContext value_in_quote() {
			return getRuleContext(Value_in_quoteContext.class,0);
		}
		public Case_valueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_case_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterCase_value(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitCase_value(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitCase_value(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Case_valueContext case_value() throws RecognitionException {
		Case_valueContext _localctx = new Case_valueContext(_ctx, getState());
		enterRule(_localctx, 98, RULE_case_value);
		try {
			setState(769);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case NUMERIC_LITERAL:
				enterOuterAlt(_localctx, 1);
				{
				setState(764);
				match(NUMERIC_LITERAL);
				}
				break;
			case QUOTE:
				enterOuterAlt(_localctx, 2);
				{
				setState(765);
				match(QUOTE);
				setState(766);
				value_in_quote();
				setState(767);
				match(QUOTE);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Default_statementContext extends ParserRuleContext {
		public TerminalNode K_DEFAULT() { return getToken(RulesParser.K_DEFAULT, 0); }
		public TerminalNode DUAL_DOT() { return getToken(RulesParser.DUAL_DOT, 0); }
		public Body_for_case_and_defaultContext body_for_case_and_default() {
			return getRuleContext(Body_for_case_and_defaultContext.class,0);
		}
		public Default_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_default_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterDefault_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitDefault_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitDefault_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Default_statementContext default_statement() throws RecognitionException {
		Default_statementContext _localctx = new Default_statementContext(_ctx, getState());
		enterRule(_localctx, 100, RULE_default_statement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(771);
			match(K_DEFAULT);
			setState(772);
			match(DUAL_DOT);
			setState(773);
			body_for_case_and_default();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Body_for_case_and_defaultContext extends ParserRuleContext {
		public List<Java_stmtContext> java_stmt() {
			return getRuleContexts(Java_stmtContext.class);
		}
		public Java_stmtContext java_stmt(int i) {
			return getRuleContext(Java_stmtContext.class,i);
		}
		public Body_functionContext body_function() {
			return getRuleContext(Body_functionContext.class,0);
		}
		public Body_for_case_and_defaultContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_body_for_case_and_default; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterBody_for_case_and_default(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitBody_for_case_and_default(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitBody_for_case_and_default(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Body_for_case_and_defaultContext body_for_case_and_default() throws RecognitionException {
		Body_for_case_and_defaultContext _localctx = new Body_for_case_and_defaultContext(_ctx, getState());
		enterRule(_localctx, 102, RULE_body_for_case_and_default);
		int _la;
		try {
			setState(782);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DOUBLEPLUS:
			case DOUBLEMINUS:
			case CLOSE_BRACKET:
			case K_VAR:
			case K_PRINT:
			case K_BREAK:
			case K_CASE:
			case K_CONTINUE:
			case K_DEFAULT:
			case K_DO:
			case K_FOR:
			case K_IF:
			case K_SWITCH:
			case K_WHILE:
			case VALID_NAME:
				enterOuterAlt(_localctx, 1);
				{
				setState(778);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << DOUBLEPLUS) | (1L << DOUBLEMINUS) | (1L << K_VAR) | (1L << K_PRINT) | (1L << K_BREAK))) != 0) || ((((_la - 73)) & ~0x3f) == 0 && ((1L << (_la - 73)) & ((1L << (K_CONTINUE - 73)) | (1L << (K_DO - 73)) | (1L << (K_FOR - 73)) | (1L << (K_IF - 73)))) != 0) || ((((_la - 167)) & ~0x3f) == 0 && ((1L << (_la - 167)) & ((1L << (K_SWITCH - 167)) | (1L << (K_WHILE - 167)) | (1L << (VALID_NAME - 167)))) != 0)) {
					{
					{
					setState(775);
					java_stmt();
					}
					}
					setState(780);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case OPEN_BRACKET:
				enterOuterAlt(_localctx, 2);
				{
				setState(781);
				body_function();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class For_stmtContext extends ParserRuleContext {
		public TerminalNode K_FOR() { return getToken(RulesParser.K_FOR, 0); }
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public For_parameterContext for_parameter() {
			return getRuleContext(For_parameterContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public Java_stmtContext java_stmt() {
			return getRuleContext(Java_stmtContext.class,0);
		}
		public Body_functionContext body_function() {
			return getRuleContext(Body_functionContext.class,0);
		}
		public For_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_for_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterFor_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitFor_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitFor_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final For_stmtContext for_stmt() throws RecognitionException {
		For_stmtContext _localctx = new For_stmtContext(_ctx, getState());
		enterRule(_localctx, 104, RULE_for_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(784);
			match(K_FOR);
			setState(785);
			match(OPEN_PAR);
			setState(786);
			for_parameter();
			setState(787);
			match(CLOSE_PAR);
			setState(790);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DOUBLEPLUS:
			case DOUBLEMINUS:
			case K_VAR:
			case K_PRINT:
			case K_BREAK:
			case K_CONTINUE:
			case K_DO:
			case K_FOR:
			case K_IF:
			case K_SWITCH:
			case K_WHILE:
			case VALID_NAME:
				{
				setState(788);
				java_stmt();
				}
				break;
			case OPEN_BRACKET:
				{
				setState(789);
				body_function();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class For_parameterContext extends ParserRuleContext {
		public Initial_start_count_in_forContext initial_start_count_in_for() {
			return getRuleContext(Initial_start_count_in_forContext.class,0);
		}
		public List<TerminalNode> SCOL() { return getTokens(RulesParser.SCOL); }
		public TerminalNode SCOL(int i) {
			return getToken(RulesParser.SCOL, i);
		}
		public Condition_forContext condition_for() {
			return getRuleContext(Condition_forContext.class,0);
		}
		public Increment_count_forContext increment_count_for() {
			return getRuleContext(Increment_count_forContext.class,0);
		}
		public For_parameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_for_parameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterFor_parameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitFor_parameter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitFor_parameter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final For_parameterContext for_parameter() throws RecognitionException {
		For_parameterContext _localctx = new For_parameterContext(_ctx, getState());
		enterRule(_localctx, 106, RULE_for_parameter);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(792);
			initial_start_count_in_for();
			setState(793);
			match(SCOL);
			setState(794);
			condition_for();
			setState(795);
			match(SCOL);
			setState(796);
			increment_count_for();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Initial_start_count_in_forContext extends ParserRuleContext {
		public Initial_variable_declarationContext initial_variable_declaration() {
			return getRuleContext(Initial_variable_declarationContext.class,0);
		}
		public TerminalNode ASSIGN() { return getToken(RulesParser.ASSIGN, 0); }
		public Number_value_javaContext number_value_java() {
			return getRuleContext(Number_value_javaContext.class,0);
		}
		public List<TerminalNode> COMMA() { return getTokens(RulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(RulesParser.COMMA, i);
		}
		public List<Initial_start_count_in_forContext> initial_start_count_in_for() {
			return getRuleContexts(Initial_start_count_in_forContext.class);
		}
		public Initial_start_count_in_forContext initial_start_count_in_for(int i) {
			return getRuleContext(Initial_start_count_in_forContext.class,i);
		}
		public Initial_start_count_in_forContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_initial_start_count_in_for; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterInitial_start_count_in_for(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitInitial_start_count_in_for(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitInitial_start_count_in_for(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Initial_start_count_in_forContext initial_start_count_in_for() throws RecognitionException {
		Initial_start_count_in_forContext _localctx = new Initial_start_count_in_forContext(_ctx, getState());
		enterRule(_localctx, 108, RULE_initial_start_count_in_for);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(798);
			initial_variable_declaration();
			setState(801);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ASSIGN) {
				{
				setState(799);
				match(ASSIGN);
				setState(800);
				number_value_java();
				}
			}

			setState(807);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,48,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(803);
					match(COMMA);
					setState(804);
					initial_start_count_in_for();
					}
					} 
				}
				setState(809);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,48,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Condition_forContext extends ParserRuleContext {
		public List<Full_condition_javaContext> full_condition_java() {
			return getRuleContexts(Full_condition_javaContext.class);
		}
		public Full_condition_javaContext full_condition_java(int i) {
			return getRuleContext(Full_condition_javaContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(RulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(RulesParser.COMMA, i);
		}
		public Condition_forContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_condition_for; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterCondition_for(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitCondition_for(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitCondition_for(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Condition_forContext condition_for() throws RecognitionException {
		Condition_forContext _localctx = new Condition_forContext(_ctx, getState());
		enterRule(_localctx, 110, RULE_condition_for);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(810);
			full_condition_java();
			setState(815);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(811);
				match(COMMA);
				setState(812);
				full_condition_java();
				}
				}
				setState(817);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Advanced_increaseContext extends ParserRuleContext {
		public Variable_nameContext variable_name() {
			return getRuleContext(Variable_nameContext.class,0);
		}
		public TerminalNode DOUBLEPLUS() { return getToken(RulesParser.DOUBLEPLUS, 0); }
		public TerminalNode DOUBLEMINUS() { return getToken(RulesParser.DOUBLEMINUS, 0); }
		public Advanced_increaseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_advanced_increase; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterAdvanced_increase(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitAdvanced_increase(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitAdvanced_increase(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Advanced_increaseContext advanced_increase() throws RecognitionException {
		Advanced_increaseContext _localctx = new Advanced_increaseContext(_ctx, getState());
		enterRule(_localctx, 112, RULE_advanced_increase);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(818);
			variable_name();
			setState(819);
			_la = _input.LA(1);
			if ( !(_la==DOUBLEPLUS || _la==DOUBLEMINUS) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Delayed_increaseContext extends ParserRuleContext {
		public Variable_nameContext variable_name() {
			return getRuleContext(Variable_nameContext.class,0);
		}
		public TerminalNode DOUBLEPLUS() { return getToken(RulesParser.DOUBLEPLUS, 0); }
		public TerminalNode DOUBLEMINUS() { return getToken(RulesParser.DOUBLEMINUS, 0); }
		public Delayed_increaseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_delayed_increase; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterDelayed_increase(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitDelayed_increase(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitDelayed_increase(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Delayed_increaseContext delayed_increase() throws RecognitionException {
		Delayed_increaseContext _localctx = new Delayed_increaseContext(_ctx, getState());
		enterRule(_localctx, 114, RULE_delayed_increase);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(821);
			_la = _input.LA(1);
			if ( !(_la==DOUBLEPLUS || _la==DOUBLEMINUS) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(822);
			variable_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Specific_increaseContext extends ParserRuleContext {
		public Variable_nameContext variable_name() {
			return getRuleContext(Variable_nameContext.class,0);
		}
		public TerminalNode ASSIGN() { return getToken(RulesParser.ASSIGN, 0); }
		public TerminalNode PLUS() { return getToken(RulesParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(RulesParser.MINUS, 0); }
		public Expr_javaContext expr_java() {
			return getRuleContext(Expr_javaContext.class,0);
		}
		public Specific_increaseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_specific_increase; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterSpecific_increase(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitSpecific_increase(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitSpecific_increase(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Specific_increaseContext specific_increase() throws RecognitionException {
		Specific_increaseContext _localctx = new Specific_increaseContext(_ctx, getState());
		enterRule(_localctx, 116, RULE_specific_increase);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(824);
			variable_name();
			setState(825);
			_la = _input.LA(1);
			if ( !(_la==PLUS || _la==MINUS) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(826);
			match(ASSIGN);
			{
			setState(827);
			expr_java(0);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Increment_typeContext extends ParserRuleContext {
		public Advanced_increaseContext advanced_increase() {
			return getRuleContext(Advanced_increaseContext.class,0);
		}
		public Delayed_increaseContext delayed_increase() {
			return getRuleContext(Delayed_increaseContext.class,0);
		}
		public Specific_increaseContext specific_increase() {
			return getRuleContext(Specific_increaseContext.class,0);
		}
		public Increment_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_increment_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterIncrement_type(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitIncrement_type(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitIncrement_type(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Increment_typeContext increment_type() throws RecognitionException {
		Increment_typeContext _localctx = new Increment_typeContext(_ctx, getState());
		enterRule(_localctx, 118, RULE_increment_type);
		try {
			setState(832);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,50,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(829);
				advanced_increase();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(830);
				delayed_increase();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(831);
				specific_increase();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Increment_count_forContext extends ParserRuleContext {
		public List<Increment_typeContext> increment_type() {
			return getRuleContexts(Increment_typeContext.class);
		}
		public Increment_typeContext increment_type(int i) {
			return getRuleContext(Increment_typeContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(RulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(RulesParser.COMMA, i);
		}
		public Increment_count_forContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_increment_count_for; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterIncrement_count_for(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitIncrement_count_for(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitIncrement_count_for(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Increment_count_forContext increment_count_for() throws RecognitionException {
		Increment_count_forContext _localctx = new Increment_count_forContext(_ctx, getState());
		enterRule(_localctx, 120, RULE_increment_count_for);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(834);
			increment_type();
			setState(839);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(835);
				match(COMMA);
				setState(836);
				increment_type();
				}
				}
				setState(841);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Increment_stmtContext extends ParserRuleContext {
		public Increment_typeContext increment_type() {
			return getRuleContext(Increment_typeContext.class,0);
		}
		public TerminalNode SCOL() { return getToken(RulesParser.SCOL, 0); }
		public Increment_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_increment_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterIncrement_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitIncrement_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitIncrement_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Increment_stmtContext increment_stmt() throws RecognitionException {
		Increment_stmtContext _localctx = new Increment_stmtContext(_ctx, getState());
		enterRule(_localctx, 122, RULE_increment_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(842);
			increment_type();
			setState(843);
			match(SCOL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class While_statementContext extends ParserRuleContext {
		public TerminalNode K_WHILE() { return getToken(RulesParser.K_WHILE, 0); }
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public Full_condition_javaContext full_condition_java() {
			return getRuleContext(Full_condition_javaContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public Java_stmtContext java_stmt() {
			return getRuleContext(Java_stmtContext.class,0);
		}
		public Body_functionContext body_function() {
			return getRuleContext(Body_functionContext.class,0);
		}
		public While_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_while_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterWhile_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitWhile_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitWhile_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final While_statementContext while_statement() throws RecognitionException {
		While_statementContext _localctx = new While_statementContext(_ctx, getState());
		enterRule(_localctx, 124, RULE_while_statement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(845);
			match(K_WHILE);
			setState(846);
			match(OPEN_PAR);
			setState(847);
			full_condition_java();
			setState(848);
			match(CLOSE_PAR);
			setState(851);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DOUBLEPLUS:
			case DOUBLEMINUS:
			case K_VAR:
			case K_PRINT:
			case K_BREAK:
			case K_CONTINUE:
			case K_DO:
			case K_FOR:
			case K_IF:
			case K_SWITCH:
			case K_WHILE:
			case VALID_NAME:
				{
				setState(849);
				java_stmt();
				}
				break;
			case OPEN_BRACKET:
				{
				setState(850);
				body_function();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Do_while_statementContext extends ParserRuleContext {
		public TerminalNode K_DO() { return getToken(RulesParser.K_DO, 0); }
		public TerminalNode K_WHILE() { return getToken(RulesParser.K_WHILE, 0); }
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public Full_condition_javaContext full_condition_java() {
			return getRuleContext(Full_condition_javaContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public TerminalNode SCOL() { return getToken(RulesParser.SCOL, 0); }
		public Java_stmtContext java_stmt() {
			return getRuleContext(Java_stmtContext.class,0);
		}
		public Body_functionContext body_function() {
			return getRuleContext(Body_functionContext.class,0);
		}
		public Do_while_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_do_while_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterDo_while_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitDo_while_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitDo_while_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Do_while_statementContext do_while_statement() throws RecognitionException {
		Do_while_statementContext _localctx = new Do_while_statementContext(_ctx, getState());
		enterRule(_localctx, 126, RULE_do_while_statement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(853);
			match(K_DO);
			setState(856);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DOUBLEPLUS:
			case DOUBLEMINUS:
			case K_VAR:
			case K_PRINT:
			case K_BREAK:
			case K_CONTINUE:
			case K_DO:
			case K_FOR:
			case K_IF:
			case K_SWITCH:
			case K_WHILE:
			case VALID_NAME:
				{
				setState(854);
				java_stmt();
				}
				break;
			case OPEN_BRACKET:
				{
				setState(855);
				body_function();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(858);
			match(K_WHILE);
			setState(859);
			match(OPEN_PAR);
			setState(860);
			full_condition_java();
			setState(861);
			match(CLOSE_PAR);
			setState(862);
			match(SCOL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Callback_functionContext extends ParserRuleContext {
		public Callback_function_without_scolContext callback_function_without_scol() {
			return getRuleContext(Callback_function_without_scolContext.class,0);
		}
		public TerminalNode SCOL() { return getToken(RulesParser.SCOL, 0); }
		public Callback_functionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_callback_function; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterCallback_function(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitCallback_function(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitCallback_function(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Callback_functionContext callback_function() throws RecognitionException {
		Callback_functionContext _localctx = new Callback_functionContext(_ctx, getState());
		enterRule(_localctx, 128, RULE_callback_function);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(864);
			callback_function_without_scol();
			setState(865);
			match(SCOL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Callback_function_without_scolContext extends ParserRuleContext {
		public Variable_nameContext variable_name() {
			return getRuleContext(Variable_nameContext.class,0);
		}
		public Callback_function_hederContext callback_function_heder() {
			return getRuleContext(Callback_function_hederContext.class,0);
		}
		public Callback_function_without_scolContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_callback_function_without_scol; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterCallback_function_without_scol(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitCallback_function_without_scol(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitCallback_function_without_scol(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Callback_function_without_scolContext callback_function_without_scol() throws RecognitionException {
		Callback_function_without_scolContext _localctx = new Callback_function_without_scolContext(_ctx, getState());
		enterRule(_localctx, 130, RULE_callback_function_without_scol);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(867);
			variable_name();
			setState(868);
			callback_function_heder();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Callback_function_hederContext extends ParserRuleContext {
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public List<Callback_function_parameter_in_headerContext> callback_function_parameter_in_header() {
			return getRuleContexts(Callback_function_parameter_in_headerContext.class);
		}
		public Callback_function_parameter_in_headerContext callback_function_parameter_in_header(int i) {
			return getRuleContext(Callback_function_parameter_in_headerContext.class,i);
		}
		public Callback_function_hederContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_callback_function_heder; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterCallback_function_heder(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitCallback_function_heder(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitCallback_function_heder(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Callback_function_hederContext callback_function_heder() throws RecognitionException {
		Callback_function_hederContext _localctx = new Callback_function_hederContext(_ctx, getState());
		enterRule(_localctx, 132, RULE_callback_function_heder);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(870);
			match(OPEN_PAR);
			setState(874);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NUMERIC_LITERAL) | (1L << OPEN_PAR) | (1L << DOUBLEPLUS) | (1L << DOUBLEMINUS) | (1L << QUOTE))) != 0) || _la==K_FUNCTION || _la==VALID_NAME) {
				{
				{
				setState(871);
				callback_function_parameter_in_header();
				}
				}
				setState(876);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(877);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Callback_function_parameterContext extends ParserRuleContext {
		public Variable_nameContext variable_name() {
			return getRuleContext(Variable_nameContext.class,0);
		}
		public TerminalNode NUMERIC_LITERAL() { return getToken(RulesParser.NUMERIC_LITERAL, 0); }
		public Function_parameterContext function_parameter() {
			return getRuleContext(Function_parameterContext.class,0);
		}
		public Callback_function_without_scolContext callback_function_without_scol() {
			return getRuleContext(Callback_function_without_scolContext.class,0);
		}
		public Expr_javaContext expr_java() {
			return getRuleContext(Expr_javaContext.class,0);
		}
		public Callback_function_parameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_callback_function_parameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterCallback_function_parameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitCallback_function_parameter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitCallback_function_parameter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Callback_function_parameterContext callback_function_parameter() throws RecognitionException {
		Callback_function_parameterContext _localctx = new Callback_function_parameterContext(_ctx, getState());
		enterRule(_localctx, 134, RULE_callback_function_parameter);
		try {
			setState(886);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,56,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(881);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case VALID_NAME:
					{
					setState(879);
					variable_name();
					}
					break;
				case NUMERIC_LITERAL:
					{
					setState(880);
					match(NUMERIC_LITERAL);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(883);
				function_parameter();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(884);
				callback_function_without_scol();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(885);
				expr_java(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Callback_function_parameter_in_headerContext extends ParserRuleContext {
		public List<Callback_function_parameterContext> callback_function_parameter() {
			return getRuleContexts(Callback_function_parameterContext.class);
		}
		public Callback_function_parameterContext callback_function_parameter(int i) {
			return getRuleContext(Callback_function_parameterContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(RulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(RulesParser.COMMA, i);
		}
		public Callback_function_parameter_in_headerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_callback_function_parameter_in_header; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterCallback_function_parameter_in_header(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitCallback_function_parameter_in_header(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitCallback_function_parameter_in_header(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Callback_function_parameter_in_headerContext callback_function_parameter_in_header() throws RecognitionException {
		Callback_function_parameter_in_headerContext _localctx = new Callback_function_parameter_in_headerContext(_ctx, getState());
		enterRule(_localctx, 136, RULE_callback_function_parameter_in_header);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(888);
			callback_function_parameter();
			setState(893);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(889);
				match(COMMA);
				setState(890);
				callback_function_parameter();
				}
				}
				setState(895);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_parameterContext extends ParserRuleContext {
		public TerminalNode K_FUNCTION() { return getToken(RulesParser.K_FUNCTION, 0); }
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public Var_name_in_parameterContext var_name_in_parameter() {
			return getRuleContext(Var_name_in_parameterContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public Body_functionContext body_function() {
			return getRuleContext(Body_functionContext.class,0);
		}
		public Function_parameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_parameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterFunction_parameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitFunction_parameter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitFunction_parameter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Function_parameterContext function_parameter() throws RecognitionException {
		Function_parameterContext _localctx = new Function_parameterContext(_ctx, getState());
		enterRule(_localctx, 138, RULE_function_parameter);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(896);
			match(K_FUNCTION);
			setState(897);
			match(OPEN_PAR);
			setState(898);
			var_name_in_parameter();
			setState(899);
			match(CLOSE_PAR);
			setState(900);
			body_function();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Var_name_in_parameterContext extends ParserRuleContext {
		public List<Variable_nameContext> variable_name() {
			return getRuleContexts(Variable_nameContext.class);
		}
		public Variable_nameContext variable_name(int i) {
			return getRuleContext(Variable_nameContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(RulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(RulesParser.COMMA, i);
		}
		public Var_name_in_parameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_var_name_in_parameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterVar_name_in_parameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitVar_name_in_parameter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitVar_name_in_parameter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Var_name_in_parameterContext var_name_in_parameter() throws RecognitionException {
		Var_name_in_parameterContext _localctx = new Var_name_in_parameterContext(_ctx, getState());
		enterRule(_localctx, 140, RULE_var_name_in_parameter);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(902);
			variable_name();
			setState(907);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(903);
				match(COMMA);
				setState(904);
				variable_name();
				}
				}
				setState(909);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Return_stmtContext extends ParserRuleContext {
		public TerminalNode K_RETURN() { return getToken(RulesParser.K_RETURN, 0); }
		public TerminalNode SCOL() { return getToken(RulesParser.SCOL, 0); }
		public Variable_valueContext variable_value() {
			return getRuleContext(Variable_valueContext.class,0);
		}
		public Return_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_return_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterReturn_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitReturn_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitReturn_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Return_stmtContext return_stmt() throws RecognitionException {
		Return_stmtContext _localctx = new Return_stmtContext(_ctx, getState());
		enterRule(_localctx, 142, RULE_return_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(910);
			match(K_RETURN);
			setState(912);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NUMERIC_LITERAL) | (1L << OPEN_PAR) | (1L << DOUBLEPLUS) | (1L << DOUBLEMINUS) | (1L << NOT) | (1L << OPEN_BRACKET) | (1L << OPEN_SQUARE) | (1L << QUOTE))) != 0) || ((((_la - 101)) & ~0x3f) == 0 && ((1L << (_la - 101)) & ((1L << (K_FALSE - 101)) | (1L << (K_NEW - 101)) | (1L << (K_NULL - 101)) | (1L << (K_SELECT - 101)))) != 0) || ((((_la - 176)) & ~0x3f) == 0 && ((1L << (_la - 176)) & ((1L << (K_TRUE - 176)) | (1L << (K_VALUES - 176)) | (1L << (VALID_NAME - 176)))) != 0)) {
				{
				setState(911);
				variable_value();
				}
			}

			setState(914);
			match(SCOL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Json_declarationContext extends ParserRuleContext {
		public TerminalNode OPEN_BRACKET() { return getToken(RulesParser.OPEN_BRACKET, 0); }
		public All_fileds_in_jsonContext all_fileds_in_json() {
			return getRuleContext(All_fileds_in_jsonContext.class,0);
		}
		public TerminalNode CLOSE_BRACKET() { return getToken(RulesParser.CLOSE_BRACKET, 0); }
		public Json_declarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_json_declaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterJson_declaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitJson_declaration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitJson_declaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Json_declarationContext json_declaration() throws RecognitionException {
		Json_declarationContext _localctx = new Json_declarationContext(_ctx, getState());
		enterRule(_localctx, 144, RULE_json_declaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(916);
			match(OPEN_BRACKET);
			setState(917);
			all_fileds_in_json();
			setState(918);
			match(CLOSE_BRACKET);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class All_fileds_in_jsonContext extends ParserRuleContext {
		public List<Filed_in_jsonContext> filed_in_json() {
			return getRuleContexts(Filed_in_jsonContext.class);
		}
		public Filed_in_jsonContext filed_in_json(int i) {
			return getRuleContext(Filed_in_jsonContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(RulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(RulesParser.COMMA, i);
		}
		public All_fileds_in_jsonContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_all_fileds_in_json; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterAll_fileds_in_json(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitAll_fileds_in_json(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitAll_fileds_in_json(this);
			else return visitor.visitChildren(this);
		}
	}

	public final All_fileds_in_jsonContext all_fileds_in_json() throws RecognitionException {
		All_fileds_in_jsonContext _localctx = new All_fileds_in_jsonContext(_ctx, getState());
		enterRule(_localctx, 146, RULE_all_fileds_in_json);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(920);
			filed_in_json();
			setState(925);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(921);
				match(COMMA);
				setState(922);
				filed_in_json();
				}
				}
				setState(927);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Filed_in_jsonContext extends ParserRuleContext {
		public Key_filedContext key_filed() {
			return getRuleContext(Key_filedContext.class,0);
		}
		public TerminalNode DUAL_DOT() { return getToken(RulesParser.DUAL_DOT, 0); }
		public Variable_valueContext variable_value() {
			return getRuleContext(Variable_valueContext.class,0);
		}
		public Filed_in_jsonContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_filed_in_json; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterFiled_in_json(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitFiled_in_json(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitFiled_in_json(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Filed_in_jsonContext filed_in_json() throws RecognitionException {
		Filed_in_jsonContext _localctx = new Filed_in_jsonContext(_ctx, getState());
		enterRule(_localctx, 148, RULE_filed_in_json);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(928);
			key_filed();
			setState(929);
			match(DUAL_DOT);
			setState(930);
			variable_value();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Key_filedContext extends ParserRuleContext {
		public List<TerminalNode> QUOTE() { return getTokens(RulesParser.QUOTE); }
		public TerminalNode QUOTE(int i) {
			return getToken(RulesParser.QUOTE, i);
		}
		public Variable_nameContext variable_name() {
			return getRuleContext(Variable_nameContext.class,0);
		}
		public Key_filedContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_key_filed; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterKey_filed(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitKey_filed(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitKey_filed(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Key_filedContext key_filed() throws RecognitionException {
		Key_filedContext _localctx = new Key_filedContext(_ctx, getState());
		enterRule(_localctx, 150, RULE_key_filed);
		try {
			setState(937);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case QUOTE:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(932);
				match(QUOTE);
				setState(933);
				variable_name();
				setState(934);
				match(QUOTE);
				}
				}
				break;
			case VALID_NAME:
				enterOuterAlt(_localctx, 2);
				{
				setState(936);
				variable_name();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Array_json_declarationContext extends ParserRuleContext {
		public TerminalNode OPEN_SQUARE() { return getToken(RulesParser.OPEN_SQUARE, 0); }
		public List<Json_declarationContext> json_declaration() {
			return getRuleContexts(Json_declarationContext.class);
		}
		public Json_declarationContext json_declaration(int i) {
			return getRuleContext(Json_declarationContext.class,i);
		}
		public TerminalNode CLOSE_SQUARE() { return getToken(RulesParser.CLOSE_SQUARE, 0); }
		public List<TerminalNode> COMMA() { return getTokens(RulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(RulesParser.COMMA, i);
		}
		public Array_json_declarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_array_json_declaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterArray_json_declaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitArray_json_declaration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitArray_json_declaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Array_json_declarationContext array_json_declaration() throws RecognitionException {
		Array_json_declarationContext _localctx = new Array_json_declarationContext(_ctx, getState());
		enterRule(_localctx, 152, RULE_array_json_declaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(939);
			match(OPEN_SQUARE);
			setState(940);
			json_declaration();
			setState(945);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(941);
				match(COMMA);
				setState(942);
				json_declaration();
				}
				}
				setState(947);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(948);
			match(CLOSE_SQUARE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Access_to_data_in_jsonContext extends ParserRuleContext {
		public Variable_nameContext variable_name() {
			return getRuleContext(Variable_nameContext.class,0);
		}
		public TerminalNode DOT() { return getToken(RulesParser.DOT, 0); }
		public Variable_valueContext variable_value() {
			return getRuleContext(Variable_valueContext.class,0);
		}
		public Access_to_data_in_jsonContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_access_to_data_in_json; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterAccess_to_data_in_json(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitAccess_to_data_in_json(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitAccess_to_data_in_json(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Access_to_data_in_jsonContext access_to_data_in_json() throws RecognitionException {
		Access_to_data_in_jsonContext _localctx = new Access_to_data_in_jsonContext(_ctx, getState());
		enterRule(_localctx, 154, RULE_access_to_data_in_json);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(950);
			variable_name();
			setState(951);
			match(DOT);
			setState(952);
			variable_value();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Foreach_statementContext extends ParserRuleContext {
		public List<Variable_in_foreachContext> variable_in_foreach() {
			return getRuleContexts(Variable_in_foreachContext.class);
		}
		public Variable_in_foreachContext variable_in_foreach(int i) {
			return getRuleContext(Variable_in_foreachContext.class,i);
		}
		public TerminalNode DOT() { return getToken(RulesParser.DOT, 0); }
		public TerminalNode K_FOREACH() { return getToken(RulesParser.K_FOREACH, 0); }
		public List<TerminalNode> OPEN_PAR() { return getTokens(RulesParser.OPEN_PAR); }
		public TerminalNode OPEN_PAR(int i) {
			return getToken(RulesParser.OPEN_PAR, i);
		}
		public TerminalNode MINUS() { return getToken(RulesParser.MINUS, 0); }
		public TerminalNode GT() { return getToken(RulesParser.GT, 0); }
		public List<TerminalNode> CLOSE_PAR() { return getTokens(RulesParser.CLOSE_PAR); }
		public TerminalNode CLOSE_PAR(int i) {
			return getToken(RulesParser.CLOSE_PAR, i);
		}
		public TerminalNode SCOL() { return getToken(RulesParser.SCOL, 0); }
		public Java_stmtContext java_stmt() {
			return getRuleContext(Java_stmtContext.class,0);
		}
		public Body_functionContext body_function() {
			return getRuleContext(Body_functionContext.class,0);
		}
		public Foreach_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_foreach_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterForeach_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitForeach_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitForeach_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Foreach_statementContext foreach_statement() throws RecognitionException {
		Foreach_statementContext _localctx = new Foreach_statementContext(_ctx, getState());
		enterRule(_localctx, 156, RULE_foreach_statement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(954);
			variable_in_foreach();
			setState(955);
			match(DOT);
			setState(956);
			match(K_FOREACH);
			setState(957);
			match(OPEN_PAR);
			setState(963);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case OPEN_PAR:
				{
				setState(958);
				match(OPEN_PAR);
				setState(959);
				variable_in_foreach();
				setState(960);
				match(CLOSE_PAR);
				}
				break;
			case VALID_NAME:
				{
				setState(962);
				variable_in_foreach();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(965);
			match(MINUS);
			setState(966);
			match(GT);
			setState(969);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DOUBLEPLUS:
			case DOUBLEMINUS:
			case K_VAR:
			case K_PRINT:
			case K_BREAK:
			case K_CONTINUE:
			case K_DO:
			case K_FOR:
			case K_IF:
			case K_SWITCH:
			case K_WHILE:
			case VALID_NAME:
				{
				setState(967);
				java_stmt();
				}
				break;
			case OPEN_BRACKET:
				{
				setState(968);
				body_function();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(971);
			match(CLOSE_PAR);
			setState(972);
			match(SCOL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Variable_in_foreachContext extends ParserRuleContext {
		public Variable_nameContext variable_name() {
			return getRuleContext(Variable_nameContext.class,0);
		}
		public Var_name_item_in_arrayContext var_name_item_in_array() {
			return getRuleContext(Var_name_item_in_arrayContext.class,0);
		}
		public Variable_in_foreachContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variable_in_foreach; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterVariable_in_foreach(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitVariable_in_foreach(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitVariable_in_foreach(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Variable_in_foreachContext variable_in_foreach() throws RecognitionException {
		Variable_in_foreachContext _localctx = new Variable_in_foreachContext(_ctx, getState());
		enterRule(_localctx, 158, RULE_variable_in_foreach);
		try {
			setState(976);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,65,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(974);
				variable_name();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(975);
				var_name_item_in_array();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Print_statementContext extends ParserRuleContext {
		public TerminalNode K_PRINT() { return getToken(RulesParser.K_PRINT, 0); }
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public List<Variable_valueContext> variable_value() {
			return getRuleContexts(Variable_valueContext.class);
		}
		public Variable_valueContext variable_value(int i) {
			return getRuleContext(Variable_valueContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public TerminalNode SCOL() { return getToken(RulesParser.SCOL, 0); }
		public List<TerminalNode> PLUS() { return getTokens(RulesParser.PLUS); }
		public TerminalNode PLUS(int i) {
			return getToken(RulesParser.PLUS, i);
		}
		public Print_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_print_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterPrint_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitPrint_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitPrint_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Print_statementContext print_statement() throws RecognitionException {
		Print_statementContext _localctx = new Print_statementContext(_ctx, getState());
		enterRule(_localctx, 160, RULE_print_statement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(978);
			match(K_PRINT);
			setState(979);
			match(OPEN_PAR);
			setState(980);
			variable_value();
			setState(985);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==PLUS) {
				{
				{
				setState(981);
				match(PLUS);
				setState(982);
				variable_value();
				}
				}
				setState(987);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(988);
			match(CLOSE_PAR);
			setState(989);
			match(SCOL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ErrorContext extends ParserRuleContext {
		public Token UNEXPECTED_CHAR;
		public TerminalNode UNEXPECTED_CHAR() { return getToken(RulesParser.UNEXPECTED_CHAR, 0); }
		public ErrorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_error; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterError(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitError(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitError(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ErrorContext error() throws RecognitionException {
		ErrorContext _localctx = new ErrorContext(_ctx, getState());
		enterRule(_localctx, 162, RULE_error);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(991);
			((ErrorContext)_localctx).UNEXPECTED_CHAR = match(UNEXPECTED_CHAR);

			     throw new RuntimeException("UNEXPECTED_CHAR=" + (((ErrorContext)_localctx).UNEXPECTED_CHAR!=null?((ErrorContext)_localctx).UNEXPECTED_CHAR.getText():null));
			   
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Sql_stmt_listContext extends ParserRuleContext {
		public List<Sql_stmtContext> sql_stmt() {
			return getRuleContexts(Sql_stmtContext.class);
		}
		public Sql_stmtContext sql_stmt(int i) {
			return getRuleContext(Sql_stmtContext.class,i);
		}
		public List<TerminalNode> SCOL() { return getTokens(RulesParser.SCOL); }
		public TerminalNode SCOL(int i) {
			return getToken(RulesParser.SCOL, i);
		}
		public Sql_stmt_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sql_stmt_list; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterSql_stmt_list(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitSql_stmt_list(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitSql_stmt_list(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Sql_stmt_listContext sql_stmt_list() throws RecognitionException {
		Sql_stmt_listContext _localctx = new Sql_stmt_listContext(_ctx, getState());
		enterRule(_localctx, 164, RULE_sql_stmt_list);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(997);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SCOL) {
				{
				{
				setState(994);
				match(SCOL);
				}
				}
				setState(999);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1000);
			sql_stmt();
			setState(1009);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,69,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1002); 
					_errHandler.sync(this);
					_la = _input.LA(1);
					do {
						{
						{
						setState(1001);
						match(SCOL);
						}
						}
						setState(1004); 
						_errHandler.sync(this);
						_la = _input.LA(1);
					} while ( _la==SCOL );
					setState(1006);
					sql_stmt();
					}
					} 
				}
				setState(1011);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,69,_ctx);
			}
			setState(1015);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,70,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1012);
					match(SCOL);
					}
					} 
				}
				setState(1017);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,70,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Sql_stmtContext extends ParserRuleContext {
		public Alter_table_stmtContext alter_table_stmt() {
			return getRuleContext(Alter_table_stmtContext.class,0);
		}
		public Create_table_stmtContext create_table_stmt() {
			return getRuleContext(Create_table_stmtContext.class,0);
		}
		public Delete_stmtContext delete_stmt() {
			return getRuleContext(Delete_stmtContext.class,0);
		}
		public Drop_table_stmtContext drop_table_stmt() {
			return getRuleContext(Drop_table_stmtContext.class,0);
		}
		public Factored_select_stmtContext factored_select_stmt() {
			return getRuleContext(Factored_select_stmtContext.class,0);
		}
		public Insert_stmtContext insert_stmt() {
			return getRuleContext(Insert_stmtContext.class,0);
		}
		public Update_stmtContext update_stmt() {
			return getRuleContext(Update_stmtContext.class,0);
		}
		public Create_type_stmtContext create_type_stmt() {
			return getRuleContext(Create_type_stmtContext.class,0);
		}
		public Create_aggregation_functionContext create_aggregation_function() {
			return getRuleContext(Create_aggregation_functionContext.class,0);
		}
		public Sql_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sql_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterSql_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitSql_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitSql_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Sql_stmtContext sql_stmt() throws RecognitionException {
		Sql_stmtContext _localctx = new Sql_stmtContext(_ctx, getState());
		enterRule(_localctx, 166, RULE_sql_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1027);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,71,_ctx) ) {
			case 1:
				{
				setState(1018);
				alter_table_stmt();
				}
				break;
			case 2:
				{
				setState(1019);
				create_table_stmt();
				}
				break;
			case 3:
				{
				setState(1020);
				delete_stmt();
				}
				break;
			case 4:
				{
				setState(1021);
				drop_table_stmt();
				}
				break;
			case 5:
				{
				setState(1022);
				factored_select_stmt();
				}
				break;
			case 6:
				{
				setState(1023);
				insert_stmt();
				}
				break;
			case 7:
				{
				setState(1024);
				update_stmt();
				}
				break;
			case 8:
				{
				setState(1025);
				create_type_stmt();
				}
				break;
			case 9:
				{
				setState(1026);
				create_aggregation_function();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Alter_table_stmtContext extends ParserRuleContext {
		public TerminalNode K_ALTER() { return getToken(RulesParser.K_ALTER, 0); }
		public TerminalNode K_TABLE() { return getToken(RulesParser.K_TABLE, 0); }
		public Source_table_nameContext source_table_name() {
			return getRuleContext(Source_table_nameContext.class,0);
		}
		public TerminalNode K_RENAME() { return getToken(RulesParser.K_RENAME, 0); }
		public TerminalNode K_TO() { return getToken(RulesParser.K_TO, 0); }
		public New_table_nameContext new_table_name() {
			return getRuleContext(New_table_nameContext.class,0);
		}
		public Alter_table_addContext alter_table_add() {
			return getRuleContext(Alter_table_addContext.class,0);
		}
		public Alter_table_add_constraintContext alter_table_add_constraint() {
			return getRuleContext(Alter_table_add_constraintContext.class,0);
		}
		public TerminalNode K_ADD() { return getToken(RulesParser.K_ADD, 0); }
		public Column_defContext column_def() {
			return getRuleContext(Column_defContext.class,0);
		}
		public Database_nameContext database_name() {
			return getRuleContext(Database_nameContext.class,0);
		}
		public TerminalNode DOT() { return getToken(RulesParser.DOT, 0); }
		public TerminalNode K_COLUMN() { return getToken(RulesParser.K_COLUMN, 0); }
		public Alter_table_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_alter_table_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterAlter_table_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitAlter_table_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitAlter_table_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Alter_table_stmtContext alter_table_stmt() throws RecognitionException {
		Alter_table_stmtContext _localctx = new Alter_table_stmtContext(_ctx, getState());
		enterRule(_localctx, 168, RULE_alter_table_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1029);
			match(K_ALTER);
			setState(1030);
			match(K_TABLE);
			setState(1034);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,72,_ctx) ) {
			case 1:
				{
				setState(1031);
				database_name();
				setState(1032);
				match(DOT);
				}
				break;
			}
			setState(1036);
			source_table_name();
			setState(1047);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,74,_ctx) ) {
			case 1:
				{
				setState(1037);
				match(K_RENAME);
				setState(1038);
				match(K_TO);
				setState(1039);
				new_table_name();
				}
				break;
			case 2:
				{
				setState(1040);
				alter_table_add();
				}
				break;
			case 3:
				{
				setState(1041);
				alter_table_add_constraint();
				}
				break;
			case 4:
				{
				setState(1042);
				match(K_ADD);
				setState(1044);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_COLUMN) {
					{
					setState(1043);
					match(K_COLUMN);
					}
				}

				setState(1046);
				column_def();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Alter_table_add_constraintContext extends ParserRuleContext {
		public TerminalNode K_ADD() { return getToken(RulesParser.K_ADD, 0); }
		public TerminalNode K_CONSTRAINT() { return getToken(RulesParser.K_CONSTRAINT, 0); }
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Table_constraintContext table_constraint() {
			return getRuleContext(Table_constraintContext.class,0);
		}
		public Alter_table_add_constraintContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_alter_table_add_constraint; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterAlter_table_add_constraint(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitAlter_table_add_constraint(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitAlter_table_add_constraint(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Alter_table_add_constraintContext alter_table_add_constraint() throws RecognitionException {
		Alter_table_add_constraintContext _localctx = new Alter_table_add_constraintContext(_ctx, getState());
		enterRule(_localctx, 170, RULE_alter_table_add_constraint);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1049);
			match(K_ADD);
			setState(1050);
			match(K_CONSTRAINT);
			setState(1051);
			any_name();
			setState(1052);
			table_constraint();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Alter_table_addContext extends ParserRuleContext {
		public TerminalNode K_ADD() { return getToken(RulesParser.K_ADD, 0); }
		public Table_constraintContext table_constraint() {
			return getRuleContext(Table_constraintContext.class,0);
		}
		public Alter_table_addContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_alter_table_add; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterAlter_table_add(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitAlter_table_add(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitAlter_table_add(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Alter_table_addContext alter_table_add() throws RecognitionException {
		Alter_table_addContext _localctx = new Alter_table_addContext(_ctx, getState());
		enterRule(_localctx, 172, RULE_alter_table_add);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1054);
			match(K_ADD);
			setState(1055);
			table_constraint();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Create_table_stmtContext extends ParserRuleContext {
		public TerminalNode K_CREATE() { return getToken(RulesParser.K_CREATE, 0); }
		public TerminalNode K_TABLE() { return getToken(RulesParser.K_TABLE, 0); }
		public Table_name_faildContext table_name_faild() {
			return getRuleContext(Table_name_faildContext.class,0);
		}
		public Create_table_stmt_typeContext create_table_stmt_type() {
			return getRuleContext(Create_table_stmt_typeContext.class,0);
		}
		public Create_table_stmt_pathContext create_table_stmt_path() {
			return getRuleContext(Create_table_stmt_pathContext.class,0);
		}
		public Columns_definitionContext columns_definition() {
			return getRuleContext(Columns_definitionContext.class,0);
		}
		public TerminalNode K_IF() { return getToken(RulesParser.K_IF, 0); }
		public TerminalNode K_NOT() { return getToken(RulesParser.K_NOT, 0); }
		public TerminalNode K_EXISTS() { return getToken(RulesParser.K_EXISTS, 0); }
		public Create_table_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_create_table_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterCreate_table_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitCreate_table_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitCreate_table_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Create_table_stmtContext create_table_stmt() throws RecognitionException {
		Create_table_stmtContext _localctx = new Create_table_stmtContext(_ctx, getState());
		enterRule(_localctx, 174, RULE_create_table_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1057);
			match(K_CREATE);
			setState(1058);
			match(K_TABLE);
			setState(1062);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_IF) {
				{
				setState(1059);
				match(K_IF);
				setState(1060);
				match(K_NOT);
				setState(1061);
				match(K_EXISTS);
				}
			}

			setState(1064);
			table_name_faild();
			{
			setState(1065);
			columns_definition();
			}
			setState(1066);
			create_table_stmt_type();
			setState(1067);
			create_table_stmt_path();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Columns_definitionContext extends ParserRuleContext {
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public List<Column_definitionContext> column_definition() {
			return getRuleContexts(Column_definitionContext.class);
		}
		public Column_definitionContext column_definition(int i) {
			return getRuleContext(Column_definitionContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public List<TerminalNode> COMMA() { return getTokens(RulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(RulesParser.COMMA, i);
		}
		public Columns_definitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_columns_definition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterColumns_definition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitColumns_definition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitColumns_definition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Columns_definitionContext columns_definition() throws RecognitionException {
		Columns_definitionContext _localctx = new Columns_definitionContext(_ctx, getState());
		enterRule(_localctx, 176, RULE_columns_definition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1069);
			match(OPEN_PAR);
			setState(1070);
			column_definition();
			setState(1075);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(1071);
				match(COMMA);
				setState(1072);
				column_definition();
				}
				}
				setState(1077);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1078);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_definitionContext extends ParserRuleContext {
		public Column_nameContext column_name() {
			return getRuleContext(Column_nameContext.class,0);
		}
		public Type_nameContext type_name() {
			return getRuleContext(Type_nameContext.class,0);
		}
		public Column_definitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_definition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterColumn_definition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitColumn_definition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitColumn_definition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_definitionContext column_definition() throws RecognitionException {
		Column_definitionContext _localctx = new Column_definitionContext(_ctx, getState());
		enterRule(_localctx, 178, RULE_column_definition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1080);
			column_name();
			setState(1081);
			type_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Create_table_stmt_typeContext extends ParserRuleContext {
		public TerminalNode K_TYPE() { return getToken(RulesParser.K_TYPE, 0); }
		public TerminalNode ASSIGN() { return getToken(RulesParser.ASSIGN, 0); }
		public List<TerminalNode> QUOTE() { return getTokens(RulesParser.QUOTE); }
		public TerminalNode QUOTE(int i) {
			return getToken(RulesParser.QUOTE, i);
		}
		public Type_faildContext type_faild() {
			return getRuleContext(Type_faildContext.class,0);
		}
		public Create_table_stmt_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_create_table_stmt_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterCreate_table_stmt_type(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitCreate_table_stmt_type(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitCreate_table_stmt_type(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Create_table_stmt_typeContext create_table_stmt_type() throws RecognitionException {
		Create_table_stmt_typeContext _localctx = new Create_table_stmt_typeContext(_ctx, getState());
		enterRule(_localctx, 180, RULE_create_table_stmt_type);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1083);
			match(K_TYPE);
			setState(1084);
			match(ASSIGN);
			setState(1085);
			match(QUOTE);
			setState(1086);
			type_faild();
			setState(1087);
			match(QUOTE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_faildContext extends ParserRuleContext {
		public TerminalNode K_CSV() { return getToken(RulesParser.K_CSV, 0); }
		public TerminalNode K_XML() { return getToken(RulesParser.K_XML, 0); }
		public Type_faildContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_faild; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterType_faild(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitType_faild(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitType_faild(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Type_faildContext type_faild() throws RecognitionException {
		Type_faildContext _localctx = new Type_faildContext(_ctx, getState());
		enterRule(_localctx, 182, RULE_type_faild);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1089);
			_la = _input.LA(1);
			if ( !(_la==T__0 || _la==K_CSV || _la==K_XML) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Create_table_stmt_pathContext extends ParserRuleContext {
		public TerminalNode K_PATH() { return getToken(RulesParser.K_PATH, 0); }
		public TerminalNode ASSIGN() { return getToken(RulesParser.ASSIGN, 0); }
		public Path_stringContext path_string() {
			return getRuleContext(Path_stringContext.class,0);
		}
		public Create_table_stmt_pathContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_create_table_stmt_path; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterCreate_table_stmt_path(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitCreate_table_stmt_path(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitCreate_table_stmt_path(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Create_table_stmt_pathContext create_table_stmt_path() throws RecognitionException {
		Create_table_stmt_pathContext _localctx = new Create_table_stmt_pathContext(_ctx, getState());
		enterRule(_localctx, 184, RULE_create_table_stmt_path);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1091);
			match(K_PATH);
			setState(1092);
			match(ASSIGN);
			setState(1093);
			path_string();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Create_type_stmtContext extends ParserRuleContext {
		public TerminalNode K_CREATE() { return getToken(RulesParser.K_CREATE, 0); }
		public TerminalNode K_TYPE() { return getToken(RulesParser.K_TYPE, 0); }
		public Table_name_faildContext table_name_faild() {
			return getRuleContext(Table_name_faildContext.class,0);
		}
		public Columns_definitionContext columns_definition() {
			return getRuleContext(Columns_definitionContext.class,0);
		}
		public TerminalNode K_IF() { return getToken(RulesParser.K_IF, 0); }
		public TerminalNode K_NOT() { return getToken(RulesParser.K_NOT, 0); }
		public TerminalNode K_EXISTS() { return getToken(RulesParser.K_EXISTS, 0); }
		public Create_type_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_create_type_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterCreate_type_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitCreate_type_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitCreate_type_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Create_type_stmtContext create_type_stmt() throws RecognitionException {
		Create_type_stmtContext _localctx = new Create_type_stmtContext(_ctx, getState());
		enterRule(_localctx, 186, RULE_create_type_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1095);
			match(K_CREATE);
			setState(1096);
			match(K_TYPE);
			setState(1100);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_IF) {
				{
				setState(1097);
				match(K_IF);
				setState(1098);
				match(K_NOT);
				setState(1099);
				match(K_EXISTS);
				}
			}

			setState(1102);
			table_name_faild();
			{
			setState(1103);
			columns_definition();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Create_aggregation_functionContext extends ParserRuleContext {
		public TerminalNode K_CREATE() { return getToken(RulesParser.K_CREATE, 0); }
		public TerminalNode K_AGGREGATION_FUNCTION() { return getToken(RulesParser.K_AGGREGATION_FUNCTION, 0); }
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Create_aggregation_function_elementsContext create_aggregation_function_elements() {
			return getRuleContext(Create_aggregation_function_elementsContext.class,0);
		}
		public Create_aggregation_functionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_create_aggregation_function; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterCreate_aggregation_function(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitCreate_aggregation_function(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitCreate_aggregation_function(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Create_aggregation_functionContext create_aggregation_function() throws RecognitionException {
		Create_aggregation_functionContext _localctx = new Create_aggregation_functionContext(_ctx, getState());
		enterRule(_localctx, 188, RULE_create_aggregation_function);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1105);
			match(K_CREATE);
			setState(1106);
			match(K_AGGREGATION_FUNCTION);
			setState(1107);
			any_name();
			setState(1108);
			create_aggregation_function_elements();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Create_aggregation_function_elementsContext extends ParserRuleContext {
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public Path_stringContext path_string() {
			return getRuleContext(Path_stringContext.class,0);
		}
		public List<TerminalNode> COMMA() { return getTokens(RulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(RulesParser.COMMA, i);
		}
		public Class_nameContext class_name() {
			return getRuleContext(Class_nameContext.class,0);
		}
		public Method_nameContext method_name() {
			return getRuleContext(Method_nameContext.class,0);
		}
		public Return_typeContext return_type() {
			return getRuleContext(Return_typeContext.class,0);
		}
		public Return_type_arrayContext return_type_array() {
			return getRuleContext(Return_type_arrayContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public Create_aggregation_function_elementsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_create_aggregation_function_elements; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterCreate_aggregation_function_elements(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitCreate_aggregation_function_elements(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitCreate_aggregation_function_elements(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Create_aggregation_function_elementsContext create_aggregation_function_elements() throws RecognitionException {
		Create_aggregation_function_elementsContext _localctx = new Create_aggregation_function_elementsContext(_ctx, getState());
		enterRule(_localctx, 190, RULE_create_aggregation_function_elements);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1110);
			match(OPEN_PAR);
			setState(1111);
			path_string();
			setState(1112);
			match(COMMA);
			setState(1113);
			class_name();
			setState(1114);
			match(COMMA);
			setState(1115);
			method_name();
			setState(1116);
			match(COMMA);
			setState(1117);
			return_type();
			setState(1118);
			match(COMMA);
			setState(1119);
			return_type_array();
			setState(1120);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Return_type_arrayContext extends ParserRuleContext {
		public TerminalNode OPEN_SQUARE() { return getToken(RulesParser.OPEN_SQUARE, 0); }
		public TerminalNode CLOSE_SQUARE() { return getToken(RulesParser.CLOSE_SQUARE, 0); }
		public List<Return_typeContext> return_type() {
			return getRuleContexts(Return_typeContext.class);
		}
		public Return_typeContext return_type(int i) {
			return getRuleContext(Return_typeContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(RulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(RulesParser.COMMA, i);
		}
		public Return_type_arrayContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_return_type_array; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterReturn_type_array(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitReturn_type_array(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitReturn_type_array(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Return_type_arrayContext return_type_array() throws RecognitionException {
		Return_type_arrayContext _localctx = new Return_type_arrayContext(_ctx, getState());
		enterRule(_localctx, 192, RULE_return_type_array);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1122);
			match(OPEN_SQUARE);
			{
			setState(1123);
			return_type();
			setState(1128);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(1124);
				match(COMMA);
				setState(1125);
				return_type();
				}
				}
				setState(1130);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
			setState(1131);
			match(CLOSE_SQUARE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Return_typeContext extends ParserRuleContext {
		public TerminalNode K_NUMBER() { return getToken(RulesParser.K_NUMBER, 0); }
		public TerminalNode K_STRING() { return getToken(RulesParser.K_STRING, 0); }
		public TerminalNode K_BOOLEAN() { return getToken(RulesParser.K_BOOLEAN, 0); }
		public Return_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_return_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterReturn_type(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitReturn_type(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitReturn_type(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Return_typeContext return_type() throws RecognitionException {
		Return_typeContext _localctx = new Return_typeContext(_ctx, getState());
		enterRule(_localctx, 194, RULE_return_type);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1133);
			_la = _input.LA(1);
			if ( !(_la==K_BOOLEAN || _la==K_NUMBER || _la==K_STRING) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Class_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Class_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_class_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterClass_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitClass_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitClass_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Class_nameContext class_name() throws RecognitionException {
		Class_nameContext _localctx = new Class_nameContext(_ctx, getState());
		enterRule(_localctx, 196, RULE_class_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1135);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Method_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Method_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_method_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterMethod_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitMethod_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitMethod_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Method_nameContext method_name() throws RecognitionException {
		Method_nameContext _localctx = new Method_nameContext(_ctx, getState());
		enterRule(_localctx, 198, RULE_method_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1137);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Path_stringContext extends ParserRuleContext {
		public Any_name_with_quoteContext any_name_with_quote() {
			return getRuleContext(Any_name_with_quoteContext.class,0);
		}
		public Path_stringContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_path_string; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterPath_string(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitPath_string(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitPath_string(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Path_stringContext path_string() throws RecognitionException {
		Path_stringContext _localctx = new Path_stringContext(_ctx, getState());
		enterRule(_localctx, 200, RULE_path_string);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1139);
			any_name_with_quote();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Any_name_with_quoteContext extends ParserRuleContext {
		public List<TerminalNode> QUOTE() { return getTokens(RulesParser.QUOTE); }
		public TerminalNode QUOTE(int i) {
			return getToken(RulesParser.QUOTE, i);
		}
		public Value_in_quoteContext value_in_quote() {
			return getRuleContext(Value_in_quoteContext.class,0);
		}
		public Any_name_with_quoteContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_any_name_with_quote; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterAny_name_with_quote(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitAny_name_with_quote(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitAny_name_with_quote(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Any_name_with_quoteContext any_name_with_quote() throws RecognitionException {
		Any_name_with_quoteContext _localctx = new Any_name_with_quoteContext(_ctx, getState());
		enterRule(_localctx, 202, RULE_any_name_with_quote);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1141);
			match(QUOTE);
			setState(1142);
			value_in_quote();
			setState(1143);
			match(QUOTE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Columns_def_tabels_constraintContext extends ParserRuleContext {
		public List<Column_defContext> column_def() {
			return getRuleContexts(Column_defContext.class);
		}
		public Column_defContext column_def(int i) {
			return getRuleContext(Column_defContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(RulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(RulesParser.COMMA, i);
		}
		public List<Table_constraintContext> table_constraint() {
			return getRuleContexts(Table_constraintContext.class);
		}
		public Table_constraintContext table_constraint(int i) {
			return getRuleContext(Table_constraintContext.class,i);
		}
		public Columns_def_tabels_constraintContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_columns_def_tabels_constraint; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterColumns_def_tabels_constraint(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitColumns_def_tabels_constraint(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitColumns_def_tabels_constraint(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Columns_def_tabels_constraintContext columns_def_tabels_constraint() throws RecognitionException {
		Columns_def_tabels_constraintContext _localctx = new Columns_def_tabels_constraintContext(_ctx, getState());
		enterRule(_localctx, 204, RULE_columns_def_tabels_constraint);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1145);
			column_def();
			setState(1152);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				setState(1150);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,79,_ctx) ) {
				case 1:
					{
					setState(1146);
					match(COMMA);
					setState(1147);
					table_constraint();
					}
					break;
				case 2:
					{
					setState(1148);
					match(COMMA);
					setState(1149);
					column_def();
					}
					break;
				}
				}
				setState(1154);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Columns_def_or_select_stmtContext extends ParserRuleContext {
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public Columns_def_tabels_constraintContext columns_def_tabels_constraint() {
			return getRuleContext(Columns_def_tabels_constraintContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public TerminalNode K_AS() { return getToken(RulesParser.K_AS, 0); }
		public Select_stmtContext select_stmt() {
			return getRuleContext(Select_stmtContext.class,0);
		}
		public Columns_def_or_select_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_columns_def_or_select_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterColumns_def_or_select_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitColumns_def_or_select_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitColumns_def_or_select_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Columns_def_or_select_stmtContext columns_def_or_select_stmt() throws RecognitionException {
		Columns_def_or_select_stmtContext _localctx = new Columns_def_or_select_stmtContext(_ctx, getState());
		enterRule(_localctx, 206, RULE_columns_def_or_select_stmt);
		try {
			setState(1161);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case OPEN_PAR:
				enterOuterAlt(_localctx, 1);
				{
				setState(1155);
				match(OPEN_PAR);
				setState(1156);
				columns_def_tabels_constraint();
				setState(1157);
				match(CLOSE_PAR);
				}
				break;
			case K_AS:
				enterOuterAlt(_localctx, 2);
				{
				setState(1159);
				match(K_AS);
				setState(1160);
				select_stmt();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Delete_stmtContext extends ParserRuleContext {
		public TerminalNode K_DELETE() { return getToken(RulesParser.K_DELETE, 0); }
		public TerminalNode K_FROM() { return getToken(RulesParser.K_FROM, 0); }
		public Qualified_table_nameContext qualified_table_name() {
			return getRuleContext(Qualified_table_nameContext.class,0);
		}
		public TerminalNode K_WHERE() { return getToken(RulesParser.K_WHERE, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Delete_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_delete_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterDelete_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitDelete_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitDelete_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Delete_stmtContext delete_stmt() throws RecognitionException {
		Delete_stmtContext _localctx = new Delete_stmtContext(_ctx, getState());
		enterRule(_localctx, 208, RULE_delete_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1163);
			match(K_DELETE);
			setState(1164);
			match(K_FROM);
			setState(1165);
			qualified_table_name();
			setState(1168);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_WHERE) {
				{
				setState(1166);
				match(K_WHERE);
				setState(1167);
				expr(0);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Drop_table_stmtContext extends ParserRuleContext {
		public TerminalNode K_DROP() { return getToken(RulesParser.K_DROP, 0); }
		public TerminalNode K_TABLE() { return getToken(RulesParser.K_TABLE, 0); }
		public Table_name_faildContext table_name_faild() {
			return getRuleContext(Table_name_faildContext.class,0);
		}
		public TerminalNode K_IF() { return getToken(RulesParser.K_IF, 0); }
		public TerminalNode K_EXISTS() { return getToken(RulesParser.K_EXISTS, 0); }
		public Drop_table_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_drop_table_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterDrop_table_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitDrop_table_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitDrop_table_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Drop_table_stmtContext drop_table_stmt() throws RecognitionException {
		Drop_table_stmtContext _localctx = new Drop_table_stmtContext(_ctx, getState());
		enterRule(_localctx, 210, RULE_drop_table_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1170);
			match(K_DROP);
			setState(1171);
			match(K_TABLE);
			setState(1174);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_IF) {
				{
				setState(1172);
				match(K_IF);
				setState(1173);
				match(K_EXISTS);
				}
			}

			setState(1176);
			table_name_faild();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Factored_select_stmtContext extends ParserRuleContext {
		public Select_coreContext select_core() {
			return getRuleContext(Select_coreContext.class,0);
		}
		public Order_byContext order_by() {
			return getRuleContext(Order_byContext.class,0);
		}
		public Limit_selectContext limit_select() {
			return getRuleContext(Limit_selectContext.class,0);
		}
		public Factored_select_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_factored_select_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterFactored_select_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitFactored_select_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitFactored_select_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Factored_select_stmtContext factored_select_stmt() throws RecognitionException {
		Factored_select_stmtContext _localctx = new Factored_select_stmtContext(_ctx, getState());
		enterRule(_localctx, 212, RULE_factored_select_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1178);
			select_core();
			setState(1180);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,84,_ctx) ) {
			case 1:
				{
				setState(1179);
				order_by();
				}
				break;
			}
			setState(1183);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,85,_ctx) ) {
			case 1:
				{
				setState(1182);
				limit_select();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Order_byContext extends ParserRuleContext {
		public TerminalNode K_ORDER() { return getToken(RulesParser.K_ORDER, 0); }
		public TerminalNode K_BY() { return getToken(RulesParser.K_BY, 0); }
		public List<Ordering_termContext> ordering_term() {
			return getRuleContexts(Ordering_termContext.class);
		}
		public Ordering_termContext ordering_term(int i) {
			return getRuleContext(Ordering_termContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(RulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(RulesParser.COMMA, i);
		}
		public Order_byContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_order_by; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterOrder_by(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitOrder_by(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitOrder_by(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Order_byContext order_by() throws RecognitionException {
		Order_byContext _localctx = new Order_byContext(_ctx, getState());
		enterRule(_localctx, 214, RULE_order_by);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1185);
			match(K_ORDER);
			setState(1186);
			match(K_BY);
			setState(1187);
			ordering_term();
			setState(1192);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,86,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1188);
					match(COMMA);
					setState(1189);
					ordering_term();
					}
					} 
				}
				setState(1194);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,86,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Limit_selectContext extends ParserRuleContext {
		public TerminalNode K_LIMIT() { return getToken(RulesParser.K_LIMIT, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode K_OFFSET() { return getToken(RulesParser.K_OFFSET, 0); }
		public TerminalNode COMMA() { return getToken(RulesParser.COMMA, 0); }
		public Limit_selectContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_limit_select; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterLimit_select(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitLimit_select(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitLimit_select(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Limit_selectContext limit_select() throws RecognitionException {
		Limit_selectContext _localctx = new Limit_selectContext(_ctx, getState());
		enterRule(_localctx, 216, RULE_limit_select);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1195);
			match(K_LIMIT);
			setState(1196);
			expr(0);
			setState(1199);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,87,_ctx) ) {
			case 1:
				{
				setState(1197);
				_la = _input.LA(1);
				if ( !(_la==COMMA || _la==K_OFFSET) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1198);
				expr(0);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Insert_stmtContext extends ParserRuleContext {
		public TerminalNode K_INSERT() { return getToken(RulesParser.K_INSERT, 0); }
		public TerminalNode K_INTO() { return getToken(RulesParser.K_INTO, 0); }
		public Table_name_faildContext table_name_faild() {
			return getRuleContext(Table_name_faildContext.class,0);
		}
		public Insert_stmt_valuesContext insert_stmt_values() {
			return getRuleContext(Insert_stmt_valuesContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public List<Column_nameContext> column_name() {
			return getRuleContexts(Column_nameContext.class);
		}
		public Column_nameContext column_name(int i) {
			return getRuleContext(Column_nameContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public List<TerminalNode> COMMA() { return getTokens(RulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(RulesParser.COMMA, i);
		}
		public Insert_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_insert_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterInsert_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitInsert_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitInsert_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Insert_stmtContext insert_stmt() throws RecognitionException {
		Insert_stmtContext _localctx = new Insert_stmtContext(_ctx, getState());
		enterRule(_localctx, 218, RULE_insert_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1201);
			match(K_INSERT);
			setState(1202);
			match(K_INTO);
			setState(1203);
			table_name_faild();
			setState(1215);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==OPEN_PAR) {
				{
				setState(1204);
				match(OPEN_PAR);
				setState(1205);
				column_name();
				setState(1210);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(1206);
					match(COMMA);
					setState(1207);
					column_name();
					}
					}
					setState(1212);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(1213);
				match(CLOSE_PAR);
				}
			}

			{
			setState(1217);
			insert_stmt_values();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Insert_stmt_valuesContext extends ParserRuleContext {
		public Values_itemsContext values_items() {
			return getRuleContext(Values_itemsContext.class,0);
		}
		public Select_stmtContext select_stmt() {
			return getRuleContext(Select_stmtContext.class,0);
		}
		public TerminalNode K_DEFAULT() { return getToken(RulesParser.K_DEFAULT, 0); }
		public TerminalNode K_VALUES() { return getToken(RulesParser.K_VALUES, 0); }
		public Insert_stmt_valuesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_insert_stmt_values; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterInsert_stmt_values(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitInsert_stmt_values(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitInsert_stmt_values(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Insert_stmt_valuesContext insert_stmt_values() throws RecognitionException {
		Insert_stmt_valuesContext _localctx = new Insert_stmt_valuesContext(_ctx, getState());
		enterRule(_localctx, 220, RULE_insert_stmt_values);
		try {
			setState(1223);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,90,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1219);
				values_items();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1220);
				select_stmt();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1221);
				match(K_DEFAULT);
				setState(1222);
				match(K_VALUES);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Select_stmtContext extends ParserRuleContext {
		public Factored_select_stmtContext factored_select_stmt() {
			return getRuleContext(Factored_select_stmtContext.class,0);
		}
		public Select_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_select_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterSelect_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitSelect_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitSelect_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Select_stmtContext select_stmt() throws RecognitionException {
		Select_stmtContext _localctx = new Select_stmtContext(_ctx, getState());
		enterRule(_localctx, 222, RULE_select_stmt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1225);
			factored_select_stmt();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Update_stmtContext extends ParserRuleContext {
		public TerminalNode K_UPDATE() { return getToken(RulesParser.K_UPDATE, 0); }
		public Qualified_table_nameContext qualified_table_name() {
			return getRuleContext(Qualified_table_nameContext.class,0);
		}
		public TerminalNode K_SET() { return getToken(RulesParser.K_SET, 0); }
		public List<Column_name_equals_exprContext> column_name_equals_expr() {
			return getRuleContexts(Column_name_equals_exprContext.class);
		}
		public Column_name_equals_exprContext column_name_equals_expr(int i) {
			return getRuleContext(Column_name_equals_exprContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(RulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(RulesParser.COMMA, i);
		}
		public TerminalNode K_WHERE() { return getToken(RulesParser.K_WHERE, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Update_stmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_update_stmt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterUpdate_stmt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitUpdate_stmt(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitUpdate_stmt(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Update_stmtContext update_stmt() throws RecognitionException {
		Update_stmtContext _localctx = new Update_stmtContext(_ctx, getState());
		enterRule(_localctx, 224, RULE_update_stmt);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1227);
			match(K_UPDATE);
			setState(1228);
			qualified_table_name();
			setState(1229);
			match(K_SET);
			setState(1230);
			column_name_equals_expr();
			setState(1235);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(1231);
				match(COMMA);
				setState(1232);
				column_name_equals_expr();
				}
				}
				setState(1237);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1240);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_WHERE) {
				{
				setState(1238);
				match(K_WHERE);
				setState(1239);
				expr(0);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_name_equals_exprContext extends ParserRuleContext {
		public Column_nameContext column_name() {
			return getRuleContext(Column_nameContext.class,0);
		}
		public TerminalNode ASSIGN() { return getToken(RulesParser.ASSIGN, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Column_name_equals_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_name_equals_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterColumn_name_equals_expr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitColumn_name_equals_expr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitColumn_name_equals_expr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_name_equals_exprContext column_name_equals_expr() throws RecognitionException {
		Column_name_equals_exprContext _localctx = new Column_name_equals_exprContext(_ctx, getState());
		enterRule(_localctx, 226, RULE_column_name_equals_expr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1242);
			column_name();
			setState(1243);
			match(ASSIGN);
			setState(1244);
			expr(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_defContext extends ParserRuleContext {
		public Column_nameContext column_name() {
			return getRuleContext(Column_nameContext.class,0);
		}
		public List<Column_constraint_or_type_nameContext> column_constraint_or_type_name() {
			return getRuleContexts(Column_constraint_or_type_nameContext.class);
		}
		public Column_constraint_or_type_nameContext column_constraint_or_type_name(int i) {
			return getRuleContext(Column_constraint_or_type_nameContext.class,i);
		}
		public Column_defContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_def; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterColumn_def(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitColumn_def(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitColumn_def(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_defContext column_def() throws RecognitionException {
		Column_defContext _localctx = new Column_defContext(_ctx, getState());
		enterRule(_localctx, 228, RULE_column_def);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1246);
			column_name();
			setState(1250);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,93,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1247);
					column_constraint_or_type_name();
					}
					} 
				}
				setState(1252);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,93,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_constraint_or_type_nameContext extends ParserRuleContext {
		public Column_constraintContext column_constraint() {
			return getRuleContext(Column_constraintContext.class,0);
		}
		public Type_nameContext type_name() {
			return getRuleContext(Type_nameContext.class,0);
		}
		public Column_constraint_or_type_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_constraint_or_type_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterColumn_constraint_or_type_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitColumn_constraint_or_type_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitColumn_constraint_or_type_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_constraint_or_type_nameContext column_constraint_or_type_name() throws RecognitionException {
		Column_constraint_or_type_nameContext _localctx = new Column_constraint_or_type_nameContext(_ctx, getState());
		enterRule(_localctx, 230, RULE_column_constraint_or_type_name);
		try {
			setState(1255);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case K_CHECK:
			case K_COLLATE:
			case K_CONSTRAINT:
			case K_DEFAULT:
			case K_NOT:
			case K_NULL:
			case K_PRIMARY:
			case K_REFERENCES:
				enterOuterAlt(_localctx, 1);
				{
				setState(1253);
				column_constraint();
				}
				break;
			case OPEN_PAR:
			case K_BOOLEAN:
			case K_NUMBER:
			case K_STRING:
			case VALID_NAME:
			case IDENTIFIER:
			case STRING_LITERAL:
				enterOuterAlt(_localctx, 2);
				{
				setState(1254);
				type_name();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_nameContext extends ParserRuleContext {
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public List<Signed_number_any_nameContext> signed_number_any_name() {
			return getRuleContexts(Signed_number_any_nameContext.class);
		}
		public Signed_number_any_nameContext signed_number_any_name(int i) {
			return getRuleContext(Signed_number_any_nameContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public TerminalNode COMMA() { return getToken(RulesParser.COMMA, 0); }
		public Return_typeContext return_type() {
			return getRuleContext(Return_typeContext.class,0);
		}
		public Type_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterType_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitType_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitType_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Type_nameContext type_name() throws RecognitionException {
		Type_nameContext _localctx = new Type_nameContext(_ctx, getState());
		enterRule(_localctx, 232, RULE_type_name);
		try {
			setState(1271);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case OPEN_PAR:
			case VALID_NAME:
			case IDENTIFIER:
			case STRING_LITERAL:
				enterOuterAlt(_localctx, 1);
				{
				setState(1257);
				name();
				setState(1268);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,95,_ctx) ) {
				case 1:
					{
					setState(1258);
					match(OPEN_PAR);
					setState(1259);
					signed_number_any_name();
					setState(1260);
					match(CLOSE_PAR);
					}
					break;
				case 2:
					{
					setState(1262);
					match(OPEN_PAR);
					setState(1263);
					signed_number_any_name();
					setState(1264);
					match(COMMA);
					setState(1265);
					signed_number_any_name();
					setState(1266);
					match(CLOSE_PAR);
					}
					break;
				}
				}
				break;
			case K_BOOLEAN:
			case K_NUMBER:
			case K_STRING:
				enterOuterAlt(_localctx, 2);
				{
				setState(1270);
				return_type();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Signed_number_any_nameContext extends ParserRuleContext {
		public Signed_numberContext signed_number() {
			return getRuleContext(Signed_numberContext.class,0);
		}
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Signed_number_any_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_signed_number_any_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterSigned_number_any_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitSigned_number_any_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitSigned_number_any_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Signed_number_any_nameContext signed_number_any_name() throws RecognitionException {
		Signed_number_any_nameContext _localctx = new Signed_number_any_nameContext(_ctx, getState());
		enterRule(_localctx, 234, RULE_signed_number_any_name);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1273);
			signed_number();
			setState(1275);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==OPEN_PAR || ((((_la - 191)) & ~0x3f) == 0 && ((1L << (_la - 191)) & ((1L << (VALID_NAME - 191)) | (1L << (IDENTIFIER - 191)) | (1L << (STRING_LITERAL - 191)))) != 0)) {
				{
				setState(1274);
				any_name();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_constraintContext extends ParserRuleContext {
		public Column_constraint_primary_keyContext column_constraint_primary_key() {
			return getRuleContext(Column_constraint_primary_keyContext.class,0);
		}
		public Column_constraint_foreign_keyContext column_constraint_foreign_key() {
			return getRuleContext(Column_constraint_foreign_keyContext.class,0);
		}
		public Column_constraint_not_nullContext column_constraint_not_null() {
			return getRuleContext(Column_constraint_not_nullContext.class,0);
		}
		public Column_constraint_nullContext column_constraint_null() {
			return getRuleContext(Column_constraint_nullContext.class,0);
		}
		public TerminalNode K_CHECK() { return getToken(RulesParser.K_CHECK, 0); }
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public Column_defaultContext column_default() {
			return getRuleContext(Column_defaultContext.class,0);
		}
		public TerminalNode K_COLLATE() { return getToken(RulesParser.K_COLLATE, 0); }
		public Collation_nameContext collation_name() {
			return getRuleContext(Collation_nameContext.class,0);
		}
		public TerminalNode K_CONSTRAINT() { return getToken(RulesParser.K_CONSTRAINT, 0); }
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public Column_constraintContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_constraint; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterColumn_constraint(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitColumn_constraint(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitColumn_constraint(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_constraintContext column_constraint() throws RecognitionException {
		Column_constraintContext _localctx = new Column_constraintContext(_ctx, getState());
		enterRule(_localctx, 236, RULE_column_constraint);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1279);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_CONSTRAINT) {
				{
				setState(1277);
				match(K_CONSTRAINT);
				setState(1278);
				name();
				}
			}

			setState(1293);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case K_PRIMARY:
				{
				setState(1281);
				column_constraint_primary_key();
				}
				break;
			case K_REFERENCES:
				{
				setState(1282);
				column_constraint_foreign_key();
				}
				break;
			case K_NOT:
				{
				setState(1283);
				column_constraint_not_null();
				}
				break;
			case K_NULL:
				{
				setState(1284);
				column_constraint_null();
				}
				break;
			case K_CHECK:
				{
				setState(1285);
				match(K_CHECK);
				setState(1286);
				match(OPEN_PAR);
				setState(1287);
				expr(0);
				setState(1288);
				match(CLOSE_PAR);
				}
				break;
			case K_DEFAULT:
				{
				setState(1290);
				column_default();
				}
				break;
			case K_COLLATE:
				{
				setState(1291);
				match(K_COLLATE);
				setState(1292);
				collation_name();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_constraint_primary_keyContext extends ParserRuleContext {
		public TerminalNode K_PRIMARY() { return getToken(RulesParser.K_PRIMARY, 0); }
		public TerminalNode K_KEY() { return getToken(RulesParser.K_KEY, 0); }
		public TerminalNode K_AUTOINCREMENT() { return getToken(RulesParser.K_AUTOINCREMENT, 0); }
		public TerminalNode K_ASC() { return getToken(RulesParser.K_ASC, 0); }
		public TerminalNode K_DESC() { return getToken(RulesParser.K_DESC, 0); }
		public Column_constraint_primary_keyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_constraint_primary_key; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterColumn_constraint_primary_key(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitColumn_constraint_primary_key(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitColumn_constraint_primary_key(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_constraint_primary_keyContext column_constraint_primary_key() throws RecognitionException {
		Column_constraint_primary_keyContext _localctx = new Column_constraint_primary_keyContext(_ctx, getState());
		enterRule(_localctx, 238, RULE_column_constraint_primary_key);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1295);
			match(K_PRIMARY);
			setState(1296);
			match(K_KEY);
			setState(1298);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_ASC || _la==K_DESC) {
				{
				setState(1297);
				_la = _input.LA(1);
				if ( !(_la==K_ASC || _la==K_DESC) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			setState(1301);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_AUTOINCREMENT) {
				{
				setState(1300);
				match(K_AUTOINCREMENT);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_constraint_foreign_keyContext extends ParserRuleContext {
		public Foreign_key_clauseContext foreign_key_clause() {
			return getRuleContext(Foreign_key_clauseContext.class,0);
		}
		public Column_constraint_foreign_keyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_constraint_foreign_key; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterColumn_constraint_foreign_key(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitColumn_constraint_foreign_key(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitColumn_constraint_foreign_key(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_constraint_foreign_keyContext column_constraint_foreign_key() throws RecognitionException {
		Column_constraint_foreign_keyContext _localctx = new Column_constraint_foreign_keyContext(_ctx, getState());
		enterRule(_localctx, 240, RULE_column_constraint_foreign_key);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1303);
			foreign_key_clause();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_constraint_not_nullContext extends ParserRuleContext {
		public TerminalNode K_NOT() { return getToken(RulesParser.K_NOT, 0); }
		public TerminalNode K_NULL() { return getToken(RulesParser.K_NULL, 0); }
		public Column_constraint_not_nullContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_constraint_not_null; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterColumn_constraint_not_null(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitColumn_constraint_not_null(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitColumn_constraint_not_null(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_constraint_not_nullContext column_constraint_not_null() throws RecognitionException {
		Column_constraint_not_nullContext _localctx = new Column_constraint_not_nullContext(_ctx, getState());
		enterRule(_localctx, 242, RULE_column_constraint_not_null);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1305);
			match(K_NOT);
			setState(1306);
			match(K_NULL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_constraint_nullContext extends ParserRuleContext {
		public TerminalNode K_NULL() { return getToken(RulesParser.K_NULL, 0); }
		public Column_constraint_nullContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_constraint_null; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterColumn_constraint_null(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitColumn_constraint_null(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitColumn_constraint_null(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_constraint_nullContext column_constraint_null() throws RecognitionException {
		Column_constraint_nullContext _localctx = new Column_constraint_nullContext(_ctx, getState());
		enterRule(_localctx, 244, RULE_column_constraint_null);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1308);
			match(K_NULL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_defaultContext extends ParserRuleContext {
		public TerminalNode K_DEFAULT() { return getToken(RulesParser.K_DEFAULT, 0); }
		public Column_default_contentContext column_default_content() {
			return getRuleContext(Column_default_contentContext.class,0);
		}
		public List<Any_nameContext> any_name() {
			return getRuleContexts(Any_nameContext.class);
		}
		public Any_nameContext any_name(int i) {
			return getRuleContext(Any_nameContext.class,i);
		}
		public Column_defaultContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_default; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterColumn_default(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitColumn_default(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitColumn_default(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_defaultContext column_default() throws RecognitionException {
		Column_defaultContext _localctx = new Column_defaultContext(_ctx, getState());
		enterRule(_localctx, 246, RULE_column_default);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1310);
			match(K_DEFAULT);
			{
			setState(1311);
			column_default_content();
			}
			setState(1318);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__1) {
				{
				setState(1312);
				match(T__1);
				setState(1314); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(1313);
						any_name();
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(1316); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,102,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_default_contentContext extends ParserRuleContext {
		public Column_default_valueContext column_default_value() {
			return getRuleContext(Column_default_valueContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public TerminalNode K_NEXTVAL() { return getToken(RulesParser.K_NEXTVAL, 0); }
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Column_default_contentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_default_content; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterColumn_default_content(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitColumn_default_content(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitColumn_default_content(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_default_contentContext column_default_content() throws RecognitionException {
		Column_default_contentContext _localctx = new Column_default_contentContext(_ctx, getState());
		enterRule(_localctx, 248, RULE_column_default_content);
		try {
			setState(1331);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,104,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1320);
				column_default_value();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1321);
				match(OPEN_PAR);
				setState(1322);
				expr(0);
				setState(1323);
				match(CLOSE_PAR);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1325);
				match(K_NEXTVAL);
				setState(1326);
				match(OPEN_PAR);
				setState(1327);
				expr(0);
				setState(1328);
				match(CLOSE_PAR);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(1330);
				any_name();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_default_valueContext extends ParserRuleContext {
		public Signed_numberContext signed_number() {
			return getRuleContext(Signed_numberContext.class,0);
		}
		public Literal_valueContext literal_value() {
			return getRuleContext(Literal_valueContext.class,0);
		}
		public Column_default_valueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_default_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterColumn_default_value(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitColumn_default_value(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitColumn_default_value(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_default_valueContext column_default_value() throws RecognitionException {
		Column_default_valueContext _localctx = new Column_default_valueContext(_ctx, getState());
		enterRule(_localctx, 250, RULE_column_default_value);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1335);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,105,_ctx) ) {
			case 1:
				{
				setState(1333);
				signed_number();
				}
				break;
			case 2:
				{
				setState(1334);
				literal_value();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public Literal_valueContext literal_value() {
			return getRuleContext(Literal_valueContext.class,0);
		}
		public Column_name_faildContext column_name_faild() {
			return getRuleContext(Column_name_faildContext.class,0);
		}
		public Unary_operatorContext unary_operator() {
			return getRuleContext(Unary_operatorContext.class,0);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public Function_exprContext function_expr() {
			return getRuleContext(Function_exprContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public Exists_caseContext exists_case() {
			return getRuleContext(Exists_caseContext.class,0);
		}
		public TerminalNode OR() { return getToken(RulesParser.OR, 0); }
		public TerminalNode STAR() { return getToken(RulesParser.STAR, 0); }
		public TerminalNode DIV() { return getToken(RulesParser.DIV, 0); }
		public TerminalNode MOD() { return getToken(RulesParser.MOD, 0); }
		public TerminalNode PLUS() { return getToken(RulesParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(RulesParser.MINUS, 0); }
		public TerminalNode LT2() { return getToken(RulesParser.LT2, 0); }
		public TerminalNode GT2() { return getToken(RulesParser.GT2, 0); }
		public TerminalNode AMP() { return getToken(RulesParser.AMP, 0); }
		public TerminalNode PIPE() { return getToken(RulesParser.PIPE, 0); }
		public TerminalNode LT() { return getToken(RulesParser.LT, 0); }
		public TerminalNode LT_EQ() { return getToken(RulesParser.LT_EQ, 0); }
		public TerminalNode GT() { return getToken(RulesParser.GT, 0); }
		public TerminalNode GT_EQ() { return getToken(RulesParser.GT_EQ, 0); }
		public TerminalNode ASSIGN() { return getToken(RulesParser.ASSIGN, 0); }
		public TerminalNode EQ() { return getToken(RulesParser.EQ, 0); }
		public TerminalNode NOT_EQ1() { return getToken(RulesParser.NOT_EQ1, 0); }
		public TerminalNode NOT_EQ2() { return getToken(RulesParser.NOT_EQ2, 0); }
		public TerminalNode K_IS() { return getToken(RulesParser.K_IS, 0); }
		public TerminalNode K_NOT() { return getToken(RulesParser.K_NOT, 0); }
		public TerminalNode K_LIKE() { return getToken(RulesParser.K_LIKE, 0); }
		public TerminalNode K_GLOB() { return getToken(RulesParser.K_GLOB, 0); }
		public TerminalNode K_MATCH() { return getToken(RulesParser.K_MATCH, 0); }
		public TerminalNode K_REGEXP() { return getToken(RulesParser.K_REGEXP, 0); }
		public TerminalNode K_AND() { return getToken(RulesParser.K_AND, 0); }
		public TerminalNode K_OR() { return getToken(RulesParser.K_OR, 0); }
		public In_caseContext in_case() {
			return getRuleContext(In_caseContext.class,0);
		}
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		return expr(0);
	}

	private ExprContext expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExprContext _localctx = new ExprContext(_ctx, _parentState);
		ExprContext _prevctx = _localctx;
		int _startState = 252;
		enterRecursionRule(_localctx, 252, RULE_expr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1349);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,106,_ctx) ) {
			case 1:
				{
				setState(1338);
				literal_value();
				}
				break;
			case 2:
				{
				setState(1339);
				column_name_faild();
				}
				break;
			case 3:
				{
				setState(1340);
				unary_operator();
				setState(1341);
				expr(12);
				}
				break;
			case 4:
				{
				setState(1343);
				function_expr();
				}
				break;
			case 5:
				{
				setState(1344);
				match(OPEN_PAR);
				setState(1345);
				expr(0);
				setState(1346);
				match(CLOSE_PAR);
				}
				break;
			case 6:
				{
				setState(1348);
				exists_case();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(1391);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,109,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(1389);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,108,_ctx) ) {
					case 1:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(1351);
						if (!(precpred(_ctx, 11))) throw new FailedPredicateException(this, "precpred(_ctx, 11)");
						setState(1352);
						match(OR);
						setState(1353);
						expr(12);
						}
						break;
					case 2:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(1354);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						setState(1355);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << STAR) | (1L << DIV) | (1L << MOD))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(1356);
						expr(11);
						}
						break;
					case 3:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(1357);
						if (!(precpred(_ctx, 9))) throw new FailedPredicateException(this, "precpred(_ctx, 9)");
						setState(1358);
						_la = _input.LA(1);
						if ( !(_la==PLUS || _la==MINUS) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(1359);
						expr(10);
						}
						break;
					case 4:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(1360);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(1361);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LT2) | (1L << GT2) | (1L << AMP) | (1L << PIPE))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(1362);
						expr(9);
						}
						break;
					case 5:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(1363);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(1364);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LT) | (1L << LT_EQ) | (1L << GT) | (1L << GT_EQ))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(1365);
						expr(8);
						}
						break;
					case 6:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(1366);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(1378);
						_errHandler.sync(this);
						switch ( getInterpreter().adaptivePredict(_input,107,_ctx) ) {
						case 1:
							{
							setState(1367);
							match(ASSIGN);
							}
							break;
						case 2:
							{
							setState(1368);
							match(EQ);
							}
							break;
						case 3:
							{
							setState(1369);
							match(NOT_EQ1);
							}
							break;
						case 4:
							{
							setState(1370);
							match(NOT_EQ2);
							}
							break;
						case 5:
							{
							setState(1371);
							match(K_IS);
							}
							break;
						case 6:
							{
							setState(1372);
							match(K_IS);
							setState(1373);
							match(K_NOT);
							}
							break;
						case 7:
							{
							setState(1374);
							match(K_LIKE);
							}
							break;
						case 8:
							{
							setState(1375);
							match(K_GLOB);
							}
							break;
						case 9:
							{
							setState(1376);
							match(K_MATCH);
							}
							break;
						case 10:
							{
							setState(1377);
							match(K_REGEXP);
							}
							break;
						}
						setState(1380);
						expr(7);
						}
						break;
					case 7:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(1381);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(1382);
						match(K_AND);
						setState(1383);
						expr(6);
						}
						break;
					case 8:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(1384);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(1385);
						match(K_OR);
						setState(1386);
						expr(5);
						}
						break;
					case 9:
						{
						_localctx = new ExprContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(1387);
						if (!(precpred(_ctx, 14))) throw new FailedPredicateException(this, "precpred(_ctx, 14)");
						setState(1388);
						in_case();
						}
						break;
					}
					} 
				}
				setState(1393);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,109,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Exists_caseContext extends ParserRuleContext {
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public Select_stmtContext select_stmt() {
			return getRuleContext(Select_stmtContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public TerminalNode K_EXISTS() { return getToken(RulesParser.K_EXISTS, 0); }
		public TerminalNode K_NOT() { return getToken(RulesParser.K_NOT, 0); }
		public Exists_caseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exists_case; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterExists_case(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitExists_case(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitExists_case(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Exists_caseContext exists_case() throws RecognitionException {
		Exists_caseContext _localctx = new Exists_caseContext(_ctx, getState());
		enterRule(_localctx, 254, RULE_exists_case);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1398);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_EXISTS || _la==K_NOT) {
				{
				setState(1395);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_NOT) {
					{
					setState(1394);
					match(K_NOT);
					}
				}

				setState(1397);
				match(K_EXISTS);
				}
			}

			setState(1400);
			match(OPEN_PAR);
			setState(1401);
			select_stmt();
			setState(1402);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_exprContext extends ParserRuleContext {
		public Function_nameContext function_name() {
			return getRuleContext(Function_nameContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode STAR() { return getToken(RulesParser.STAR, 0); }
		public TerminalNode K_DISTINCT() { return getToken(RulesParser.K_DISTINCT, 0); }
		public List<TerminalNode> COMMA() { return getTokens(RulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(RulesParser.COMMA, i);
		}
		public Function_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterFunction_expr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitFunction_expr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitFunction_expr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Function_exprContext function_expr() throws RecognitionException {
		Function_exprContext _localctx = new Function_exprContext(_ctx, getState());
		enterRule(_localctx, 256, RULE_function_expr);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1404);
			function_name();
			setState(1405);
			match(OPEN_PAR);
			setState(1418);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case NUMERIC_LITERAL:
			case OPEN_PAR:
			case PLUS:
			case MINUS:
			case TILDE:
			case K_CURRENT_DATE:
			case K_CURRENT_TIME:
			case K_CURRENT_TIMESTAMP:
			case K_DISTINCT:
			case K_EXISTS:
			case K_NOT:
			case K_NULL:
			case VALID_NAME:
			case IDENTIFIER:
			case STRING_LITERAL:
			case BLOB_LITERAL:
				{
				setState(1407);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_DISTINCT) {
					{
					setState(1406);
					match(K_DISTINCT);
					}
				}

				setState(1409);
				expr(0);
				setState(1414);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(1410);
					match(COMMA);
					setState(1411);
					expr(0);
					}
					}
					setState(1416);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case STAR:
				{
				setState(1417);
				match(STAR);
				}
				break;
			case CLOSE_PAR:
				break;
			default:
				break;
			}
			setState(1420);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_name_faildContext extends ParserRuleContext {
		public Column_nameContext column_name() {
			return getRuleContext(Column_nameContext.class,0);
		}
		public Table_name_faildContext table_name_faild() {
			return getRuleContext(Table_name_faildContext.class,0);
		}
		public TerminalNode DOT() { return getToken(RulesParser.DOT, 0); }
		public Column_name_faildContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_name_faild; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterColumn_name_faild(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitColumn_name_faild(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitColumn_name_faild(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_name_faildContext column_name_faild() throws RecognitionException {
		Column_name_faildContext _localctx = new Column_name_faildContext(_ctx, getState());
		enterRule(_localctx, 258, RULE_column_name_faild);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1425);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,115,_ctx) ) {
			case 1:
				{
				setState(1422);
				table_name_faild();
				setState(1423);
				match(DOT);
				}
				break;
			}
			setState(1427);
			column_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_name_faildContext extends ParserRuleContext {
		public Table_nameContext table_name() {
			return getRuleContext(Table_nameContext.class,0);
		}
		public Table_name_faildContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_name_faild; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterTable_name_faild(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitTable_name_faild(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitTable_name_faild(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_name_faildContext table_name_faild() throws RecognitionException {
		Table_name_faildContext _localctx = new Table_name_faildContext(_ctx, getState());
		enterRule(_localctx, 260, RULE_table_name_faild);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1429);
			table_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Select_or_exprContext extends ParserRuleContext {
		public Select_stmtContext select_stmt() {
			return getRuleContext(Select_stmtContext.class,0);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(RulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(RulesParser.COMMA, i);
		}
		public Select_or_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_select_or_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterSelect_or_expr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitSelect_or_expr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitSelect_or_expr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Select_or_exprContext select_or_expr() throws RecognitionException {
		Select_or_exprContext _localctx = new Select_or_exprContext(_ctx, getState());
		enterRule(_localctx, 262, RULE_select_or_expr);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1440);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case K_SELECT:
			case K_VALUES:
				{
				setState(1431);
				select_stmt();
				}
				break;
			case NUMERIC_LITERAL:
			case OPEN_PAR:
			case PLUS:
			case MINUS:
			case TILDE:
			case K_CURRENT_DATE:
			case K_CURRENT_TIME:
			case K_CURRENT_TIMESTAMP:
			case K_EXISTS:
			case K_NOT:
			case K_NULL:
			case VALID_NAME:
			case IDENTIFIER:
			case STRING_LITERAL:
			case BLOB_LITERAL:
				{
				setState(1432);
				expr(0);
				setState(1437);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(1433);
					match(COMMA);
					setState(1434);
					expr(0);
					}
					}
					setState(1439);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case CLOSE_PAR:
				break;
			default:
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class In_caseContext extends ParserRuleContext {
		public TerminalNode K_IN() { return getToken(RulesParser.K_IN, 0); }
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public Select_or_exprContext select_or_expr() {
			return getRuleContext(Select_or_exprContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public Table_name_faildContext table_name_faild() {
			return getRuleContext(Table_name_faildContext.class,0);
		}
		public TerminalNode K_NOT() { return getToken(RulesParser.K_NOT, 0); }
		public In_caseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_in_case; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterIn_case(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitIn_case(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitIn_case(this);
			else return visitor.visitChildren(this);
		}
	}

	public final In_caseContext in_case() throws RecognitionException {
		In_caseContext _localctx = new In_caseContext(_ctx, getState());
		enterRule(_localctx, 264, RULE_in_case);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1443);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_NOT) {
				{
				setState(1442);
				match(K_NOT);
				}
			}

			setState(1445);
			match(K_IN);
			setState(1451);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,119,_ctx) ) {
			case 1:
				{
				setState(1446);
				match(OPEN_PAR);
				setState(1447);
				select_or_expr();
				setState(1448);
				match(CLOSE_PAR);
				}
				break;
			case 2:
				{
				setState(1450);
				table_name_faild();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Foreign_key_clauseContext extends ParserRuleContext {
		public TerminalNode K_REFERENCES() { return getToken(RulesParser.K_REFERENCES, 0); }
		public Foreign_table_faildContext foreign_table_faild() {
			return getRuleContext(Foreign_table_faildContext.class,0);
		}
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public List<Fk_target_column_nameContext> fk_target_column_name() {
			return getRuleContexts(Fk_target_column_nameContext.class);
		}
		public Fk_target_column_nameContext fk_target_column_name(int i) {
			return getRuleContext(Fk_target_column_nameContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public List<Foreign_on_or_matchContext> foreign_on_or_match() {
			return getRuleContexts(Foreign_on_or_matchContext.class);
		}
		public Foreign_on_or_matchContext foreign_on_or_match(int i) {
			return getRuleContext(Foreign_on_or_matchContext.class,i);
		}
		public Foreign_deferrableContext foreign_deferrable() {
			return getRuleContext(Foreign_deferrableContext.class,0);
		}
		public List<TerminalNode> COMMA() { return getTokens(RulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(RulesParser.COMMA, i);
		}
		public Foreign_key_clauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_foreign_key_clause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterForeign_key_clause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitForeign_key_clause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitForeign_key_clause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Foreign_key_clauseContext foreign_key_clause() throws RecognitionException {
		Foreign_key_clauseContext _localctx = new Foreign_key_clauseContext(_ctx, getState());
		enterRule(_localctx, 266, RULE_foreign_key_clause);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1453);
			match(K_REFERENCES);
			setState(1454);
			foreign_table_faild();
			setState(1466);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,121,_ctx) ) {
			case 1:
				{
				setState(1455);
				match(OPEN_PAR);
				setState(1456);
				fk_target_column_name();
				setState(1461);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(1457);
					match(COMMA);
					setState(1458);
					fk_target_column_name();
					}
					}
					setState(1463);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(1464);
				match(CLOSE_PAR);
				}
				break;
			}
			setState(1471);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==K_MATCH || _la==K_ON) {
				{
				{
				setState(1468);
				foreign_on_or_match();
				}
				}
				setState(1473);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1475);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,123,_ctx) ) {
			case 1:
				{
				setState(1474);
				foreign_deferrable();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Foreign_deferrableContext extends ParserRuleContext {
		public TerminalNode K_DEFERRABLE() { return getToken(RulesParser.K_DEFERRABLE, 0); }
		public TerminalNode K_NOT() { return getToken(RulesParser.K_NOT, 0); }
		public TerminalNode K_INITIALLY() { return getToken(RulesParser.K_INITIALLY, 0); }
		public TerminalNode K_DEFERRED() { return getToken(RulesParser.K_DEFERRED, 0); }
		public TerminalNode K_IMMEDIATE() { return getToken(RulesParser.K_IMMEDIATE, 0); }
		public TerminalNode K_ENABLE() { return getToken(RulesParser.K_ENABLE, 0); }
		public Foreign_deferrableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_foreign_deferrable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterForeign_deferrable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitForeign_deferrable(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitForeign_deferrable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Foreign_deferrableContext foreign_deferrable() throws RecognitionException {
		Foreign_deferrableContext _localctx = new Foreign_deferrableContext(_ctx, getState());
		enterRule(_localctx, 268, RULE_foreign_deferrable);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1478);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_NOT) {
				{
				setState(1477);
				match(K_NOT);
				}
			}

			setState(1480);
			match(K_DEFERRABLE);
			setState(1485);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,125,_ctx) ) {
			case 1:
				{
				setState(1481);
				match(K_INITIALLY);
				setState(1482);
				match(K_DEFERRED);
				}
				break;
			case 2:
				{
				setState(1483);
				match(K_INITIALLY);
				setState(1484);
				match(K_IMMEDIATE);
				}
				break;
			}
			setState(1488);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_ENABLE) {
				{
				setState(1487);
				match(K_ENABLE);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Foreign_onContext extends ParserRuleContext {
		public TerminalNode K_ON() { return getToken(RulesParser.K_ON, 0); }
		public TerminalNode K_DELETE() { return getToken(RulesParser.K_DELETE, 0); }
		public TerminalNode K_UPDATE() { return getToken(RulesParser.K_UPDATE, 0); }
		public TerminalNode K_SET() { return getToken(RulesParser.K_SET, 0); }
		public TerminalNode K_NULL() { return getToken(RulesParser.K_NULL, 0); }
		public TerminalNode K_DEFAULT() { return getToken(RulesParser.K_DEFAULT, 0); }
		public TerminalNode K_CASCADE() { return getToken(RulesParser.K_CASCADE, 0); }
		public TerminalNode K_RESTRICT() { return getToken(RulesParser.K_RESTRICT, 0); }
		public TerminalNode K_NO() { return getToken(RulesParser.K_NO, 0); }
		public TerminalNode K_ACTION() { return getToken(RulesParser.K_ACTION, 0); }
		public Foreign_onContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_foreign_on; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterForeign_on(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitForeign_on(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitForeign_on(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Foreign_onContext foreign_on() throws RecognitionException {
		Foreign_onContext _localctx = new Foreign_onContext(_ctx, getState());
		enterRule(_localctx, 270, RULE_foreign_on);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1490);
			match(K_ON);
			setState(1491);
			_la = _input.LA(1);
			if ( !(_la==K_DELETE || _la==K_UPDATE) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(1500);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,127,_ctx) ) {
			case 1:
				{
				setState(1492);
				match(K_SET);
				setState(1493);
				match(K_NULL);
				}
				break;
			case 2:
				{
				setState(1494);
				match(K_SET);
				setState(1495);
				match(K_DEFAULT);
				}
				break;
			case 3:
				{
				setState(1496);
				match(K_CASCADE);
				}
				break;
			case 4:
				{
				setState(1497);
				match(K_RESTRICT);
				}
				break;
			case 5:
				{
				setState(1498);
				match(K_NO);
				setState(1499);
				match(K_ACTION);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Foreign_on_or_matchContext extends ParserRuleContext {
		public Foreign_onContext foreign_on() {
			return getRuleContext(Foreign_onContext.class,0);
		}
		public TerminalNode K_MATCH() { return getToken(RulesParser.K_MATCH, 0); }
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public Foreign_on_or_matchContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_foreign_on_or_match; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterForeign_on_or_match(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitForeign_on_or_match(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitForeign_on_or_match(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Foreign_on_or_matchContext foreign_on_or_match() throws RecognitionException {
		Foreign_on_or_matchContext _localctx = new Foreign_on_or_matchContext(_ctx, getState());
		enterRule(_localctx, 272, RULE_foreign_on_or_match);
		try {
			setState(1505);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case K_ON:
				enterOuterAlt(_localctx, 1);
				{
				setState(1502);
				foreign_on();
				}
				break;
			case K_MATCH:
				enterOuterAlt(_localctx, 2);
				{
				setState(1503);
				match(K_MATCH);
				setState(1504);
				name();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Foreign_table_faildContext extends ParserRuleContext {
		public Foreign_tableContext foreign_table() {
			return getRuleContext(Foreign_tableContext.class,0);
		}
		public Database_nameContext database_name() {
			return getRuleContext(Database_nameContext.class,0);
		}
		public TerminalNode DOT() { return getToken(RulesParser.DOT, 0); }
		public Foreign_table_faildContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_foreign_table_faild; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterForeign_table_faild(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitForeign_table_faild(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitForeign_table_faild(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Foreign_table_faildContext foreign_table_faild() throws RecognitionException {
		Foreign_table_faildContext _localctx = new Foreign_table_faildContext(_ctx, getState());
		enterRule(_localctx, 274, RULE_foreign_table_faild);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1510);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,129,_ctx) ) {
			case 1:
				{
				setState(1507);
				database_name();
				setState(1508);
				match(DOT);
				}
				break;
			}
			setState(1512);
			foreign_table();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Fk_target_column_nameContext extends ParserRuleContext {
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public Fk_target_column_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fk_target_column_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterFk_target_column_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitFk_target_column_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitFk_target_column_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Fk_target_column_nameContext fk_target_column_name() throws RecognitionException {
		Fk_target_column_nameContext _localctx = new Fk_target_column_nameContext(_ctx, getState());
		enterRule(_localctx, 276, RULE_fk_target_column_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1514);
			name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Indexed_columnContext extends ParserRuleContext {
		public Column_nameContext column_name() {
			return getRuleContext(Column_nameContext.class,0);
		}
		public TerminalNode K_COLLATE() { return getToken(RulesParser.K_COLLATE, 0); }
		public Collation_nameContext collation_name() {
			return getRuleContext(Collation_nameContext.class,0);
		}
		public TerminalNode K_ASC() { return getToken(RulesParser.K_ASC, 0); }
		public TerminalNode K_DESC() { return getToken(RulesParser.K_DESC, 0); }
		public Indexed_columnContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_indexed_column; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterIndexed_column(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitIndexed_column(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitIndexed_column(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Indexed_columnContext indexed_column() throws RecognitionException {
		Indexed_columnContext _localctx = new Indexed_columnContext(_ctx, getState());
		enterRule(_localctx, 278, RULE_indexed_column);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1516);
			column_name();
			setState(1519);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_COLLATE) {
				{
				setState(1517);
				match(K_COLLATE);
				setState(1518);
				collation_name();
				}
			}

			setState(1522);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_ASC || _la==K_DESC) {
				{
				setState(1521);
				_la = _input.LA(1);
				if ( !(_la==K_ASC || _la==K_DESC) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_constraintContext extends ParserRuleContext {
		public Table_constraint_primary_keyContext table_constraint_primary_key() {
			return getRuleContext(Table_constraint_primary_keyContext.class,0);
		}
		public Table_constraint_keyContext table_constraint_key() {
			return getRuleContext(Table_constraint_keyContext.class,0);
		}
		public Table_constraint_uniqueContext table_constraint_unique() {
			return getRuleContext(Table_constraint_uniqueContext.class,0);
		}
		public TerminalNode K_CHECK() { return getToken(RulesParser.K_CHECK, 0); }
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public Table_constraint_foreign_keyContext table_constraint_foreign_key() {
			return getRuleContext(Table_constraint_foreign_keyContext.class,0);
		}
		public TerminalNode K_CONSTRAINT() { return getToken(RulesParser.K_CONSTRAINT, 0); }
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public Table_constraintContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_constraint; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterTable_constraint(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitTable_constraint(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitTable_constraint(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_constraintContext table_constraint() throws RecognitionException {
		Table_constraintContext _localctx = new Table_constraintContext(_ctx, getState());
		enterRule(_localctx, 280, RULE_table_constraint);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1526);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_CONSTRAINT) {
				{
				setState(1524);
				match(K_CONSTRAINT);
				setState(1525);
				name();
				}
			}

			setState(1537);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case K_PRIMARY:
				{
				setState(1528);
				table_constraint_primary_key();
				}
				break;
			case K_KEY:
				{
				setState(1529);
				table_constraint_key();
				}
				break;
			case K_UNIQUE:
				{
				setState(1530);
				table_constraint_unique();
				}
				break;
			case K_CHECK:
				{
				setState(1531);
				match(K_CHECK);
				setState(1532);
				match(OPEN_PAR);
				setState(1533);
				expr(0);
				setState(1534);
				match(CLOSE_PAR);
				}
				break;
			case K_FOREIGN:
				{
				setState(1536);
				table_constraint_foreign_key();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_constraint_primary_keyContext extends ParserRuleContext {
		public TerminalNode K_PRIMARY() { return getToken(RulesParser.K_PRIMARY, 0); }
		public TerminalNode K_KEY() { return getToken(RulesParser.K_KEY, 0); }
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public List<Indexed_columnContext> indexed_column() {
			return getRuleContexts(Indexed_columnContext.class);
		}
		public Indexed_columnContext indexed_column(int i) {
			return getRuleContext(Indexed_columnContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public List<TerminalNode> COMMA() { return getTokens(RulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(RulesParser.COMMA, i);
		}
		public Table_constraint_primary_keyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_constraint_primary_key; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterTable_constraint_primary_key(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitTable_constraint_primary_key(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitTable_constraint_primary_key(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_constraint_primary_keyContext table_constraint_primary_key() throws RecognitionException {
		Table_constraint_primary_keyContext _localctx = new Table_constraint_primary_keyContext(_ctx, getState());
		enterRule(_localctx, 282, RULE_table_constraint_primary_key);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1539);
			match(K_PRIMARY);
			setState(1540);
			match(K_KEY);
			setState(1541);
			match(OPEN_PAR);
			setState(1542);
			indexed_column();
			setState(1547);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(1543);
				match(COMMA);
				setState(1544);
				indexed_column();
				}
				}
				setState(1549);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1550);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_constraint_foreign_keyContext extends ParserRuleContext {
		public TerminalNode K_FOREIGN() { return getToken(RulesParser.K_FOREIGN, 0); }
		public TerminalNode K_KEY() { return getToken(RulesParser.K_KEY, 0); }
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public List<Fk_origin_column_nameContext> fk_origin_column_name() {
			return getRuleContexts(Fk_origin_column_nameContext.class);
		}
		public Fk_origin_column_nameContext fk_origin_column_name(int i) {
			return getRuleContext(Fk_origin_column_nameContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public Foreign_key_clauseContext foreign_key_clause() {
			return getRuleContext(Foreign_key_clauseContext.class,0);
		}
		public List<TerminalNode> COMMA() { return getTokens(RulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(RulesParser.COMMA, i);
		}
		public Table_constraint_foreign_keyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_constraint_foreign_key; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterTable_constraint_foreign_key(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitTable_constraint_foreign_key(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitTable_constraint_foreign_key(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_constraint_foreign_keyContext table_constraint_foreign_key() throws RecognitionException {
		Table_constraint_foreign_keyContext _localctx = new Table_constraint_foreign_keyContext(_ctx, getState());
		enterRule(_localctx, 284, RULE_table_constraint_foreign_key);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1552);
			match(K_FOREIGN);
			setState(1553);
			match(K_KEY);
			setState(1554);
			match(OPEN_PAR);
			setState(1555);
			fk_origin_column_name();
			setState(1560);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(1556);
				match(COMMA);
				setState(1557);
				fk_origin_column_name();
				}
				}
				setState(1562);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1563);
			match(CLOSE_PAR);
			setState(1564);
			foreign_key_clause();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_constraint_uniqueContext extends ParserRuleContext {
		public TerminalNode K_UNIQUE() { return getToken(RulesParser.K_UNIQUE, 0); }
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public List<Indexed_columnContext> indexed_column() {
			return getRuleContexts(Indexed_columnContext.class);
		}
		public Indexed_columnContext indexed_column(int i) {
			return getRuleContext(Indexed_columnContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public TerminalNode K_KEY() { return getToken(RulesParser.K_KEY, 0); }
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public List<TerminalNode> COMMA() { return getTokens(RulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(RulesParser.COMMA, i);
		}
		public Table_constraint_uniqueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_constraint_unique; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterTable_constraint_unique(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitTable_constraint_unique(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitTable_constraint_unique(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_constraint_uniqueContext table_constraint_unique() throws RecognitionException {
		Table_constraint_uniqueContext _localctx = new Table_constraint_uniqueContext(_ctx, getState());
		enterRule(_localctx, 286, RULE_table_constraint_unique);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1566);
			match(K_UNIQUE);
			setState(1568);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_KEY) {
				{
				setState(1567);
				match(K_KEY);
				}
			}

			setState(1571);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,137,_ctx) ) {
			case 1:
				{
				setState(1570);
				name();
				}
				break;
			}
			setState(1573);
			match(OPEN_PAR);
			setState(1574);
			indexed_column();
			setState(1579);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(1575);
				match(COMMA);
				setState(1576);
				indexed_column();
				}
				}
				setState(1581);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1582);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_constraint_keyContext extends ParserRuleContext {
		public TerminalNode K_KEY() { return getToken(RulesParser.K_KEY, 0); }
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public List<Indexed_columnContext> indexed_column() {
			return getRuleContexts(Indexed_columnContext.class);
		}
		public Indexed_columnContext indexed_column(int i) {
			return getRuleContext(Indexed_columnContext.class,i);
		}
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public NameContext name() {
			return getRuleContext(NameContext.class,0);
		}
		public List<TerminalNode> COMMA() { return getTokens(RulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(RulesParser.COMMA, i);
		}
		public Table_constraint_keyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_constraint_key; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterTable_constraint_key(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitTable_constraint_key(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitTable_constraint_key(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_constraint_keyContext table_constraint_key() throws RecognitionException {
		Table_constraint_keyContext _localctx = new Table_constraint_keyContext(_ctx, getState());
		enterRule(_localctx, 288, RULE_table_constraint_key);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1584);
			match(K_KEY);
			setState(1586);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,139,_ctx) ) {
			case 1:
				{
				setState(1585);
				name();
				}
				break;
			}
			setState(1588);
			match(OPEN_PAR);
			setState(1589);
			indexed_column();
			setState(1594);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(1590);
				match(COMMA);
				setState(1591);
				indexed_column();
				}
				}
				setState(1596);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1597);
			match(CLOSE_PAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Fk_origin_column_nameContext extends ParserRuleContext {
		public Column_nameContext column_name() {
			return getRuleContext(Column_nameContext.class,0);
		}
		public Fk_origin_column_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fk_origin_column_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterFk_origin_column_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitFk_origin_column_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitFk_origin_column_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Fk_origin_column_nameContext fk_origin_column_name() throws RecognitionException {
		Fk_origin_column_nameContext _localctx = new Fk_origin_column_nameContext(_ctx, getState());
		enterRule(_localctx, 290, RULE_fk_origin_column_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1599);
			column_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Qualified_table_nameContext extends ParserRuleContext {
		public Table_name_faildContext table_name_faild() {
			return getRuleContext(Table_name_faildContext.class,0);
		}
		public TerminalNode K_INDEXED() { return getToken(RulesParser.K_INDEXED, 0); }
		public TerminalNode K_BY() { return getToken(RulesParser.K_BY, 0); }
		public Index_nameContext index_name() {
			return getRuleContext(Index_nameContext.class,0);
		}
		public TerminalNode K_NOT() { return getToken(RulesParser.K_NOT, 0); }
		public Qualified_table_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_qualified_table_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterQualified_table_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitQualified_table_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitQualified_table_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Qualified_table_nameContext qualified_table_name() throws RecognitionException {
		Qualified_table_nameContext _localctx = new Qualified_table_nameContext(_ctx, getState());
		enterRule(_localctx, 292, RULE_qualified_table_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1601);
			table_name_faild();
			setState(1607);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case K_INDEXED:
				{
				setState(1602);
				match(K_INDEXED);
				setState(1603);
				match(K_BY);
				setState(1604);
				index_name();
				}
				break;
			case K_NOT:
				{
				setState(1605);
				match(K_NOT);
				setState(1606);
				match(K_INDEXED);
				}
				break;
			case EOF:
			case SCOL:
			case DOUBLEPLUS:
			case DOUBLEMINUS:
			case K_VAR:
			case K_PRINT:
			case K_ALTER:
			case K_BREAK:
			case K_CONTINUE:
			case K_CREATE:
			case K_DELETE:
			case K_DO:
			case K_DROP:
			case K_FOR:
			case K_IF:
			case K_INSERT:
			case K_SELECT:
			case K_SET:
			case K_SWITCH:
			case K_UPDATE:
			case K_VALUES:
			case K_WHERE:
			case K_WHILE:
			case VALID_NAME:
			case UNEXPECTED_CHAR:
				break;
			default:
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Ordering_termContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode K_COLLATE() { return getToken(RulesParser.K_COLLATE, 0); }
		public Collation_nameContext collation_name() {
			return getRuleContext(Collation_nameContext.class,0);
		}
		public TerminalNode K_ASC() { return getToken(RulesParser.K_ASC, 0); }
		public TerminalNode K_DESC() { return getToken(RulesParser.K_DESC, 0); }
		public Ordering_termContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ordering_term; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterOrdering_term(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitOrdering_term(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitOrdering_term(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Ordering_termContext ordering_term() throws RecognitionException {
		Ordering_termContext _localctx = new Ordering_termContext(_ctx, getState());
		enterRule(_localctx, 294, RULE_ordering_term);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1609);
			expr(0);
			setState(1612);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,142,_ctx) ) {
			case 1:
				{
				setState(1610);
				match(K_COLLATE);
				setState(1611);
				collation_name();
				}
				break;
			}
			setState(1615);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,143,_ctx) ) {
			case 1:
				{
				setState(1614);
				_la = _input.LA(1);
				if ( !(_la==K_ASC || _la==K_DESC) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Result_columnContext extends ParserRuleContext {
		public TerminalNode STAR() { return getToken(RulesParser.STAR, 0); }
		public Table_nameContext table_name() {
			return getRuleContext(Table_nameContext.class,0);
		}
		public TerminalNode DOT() { return getToken(RulesParser.DOT, 0); }
		public Result_column_with_exprContext result_column_with_expr() {
			return getRuleContext(Result_column_with_exprContext.class,0);
		}
		public Result_columnContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_result_column; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterResult_column(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitResult_column(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitResult_column(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Result_columnContext result_column() throws RecognitionException {
		Result_columnContext _localctx = new Result_columnContext(_ctx, getState());
		enterRule(_localctx, 296, RULE_result_column);
		try {
			setState(1623);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,144,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1617);
				match(STAR);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1618);
				table_name();
				setState(1619);
				match(DOT);
				setState(1620);
				match(STAR);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1622);
				result_column_with_expr();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Result_column_with_exprContext extends ParserRuleContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Column_aliasContext column_alias() {
			return getRuleContext(Column_aliasContext.class,0);
		}
		public TerminalNode K_AS() { return getToken(RulesParser.K_AS, 0); }
		public Result_column_with_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_result_column_with_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterResult_column_with_expr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitResult_column_with_expr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitResult_column_with_expr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Result_column_with_exprContext result_column_with_expr() throws RecognitionException {
		Result_column_with_exprContext _localctx = new Result_column_with_exprContext(_ctx, getState());
		enterRule(_localctx, 298, RULE_result_column_with_expr);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1625);
			expr(0);
			setState(1630);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,146,_ctx) ) {
			case 1:
				{
				setState(1627);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_AS) {
					{
					setState(1626);
					match(K_AS);
					}
				}

				setState(1629);
				column_alias();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_or_subqueryContext extends ParserRuleContext {
		public Table_as_aliasContext table_as_alias() {
			return getRuleContext(Table_as_aliasContext.class,0);
		}
		public From_itmes_as_tableContext from_itmes_as_table() {
			return getRuleContext(From_itmes_as_tableContext.class,0);
		}
		public Select_stmt_as_tableContext select_stmt_as_table() {
			return getRuleContext(Select_stmt_as_tableContext.class,0);
		}
		public Table_or_subqueryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_or_subquery; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterTable_or_subquery(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitTable_or_subquery(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitTable_or_subquery(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_or_subqueryContext table_or_subquery() throws RecognitionException {
		Table_or_subqueryContext _localctx = new Table_or_subqueryContext(_ctx, getState());
		enterRule(_localctx, 300, RULE_table_or_subquery);
		try {
			setState(1635);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,147,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1632);
				table_as_alias();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1633);
				from_itmes_as_table();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1634);
				select_stmt_as_table();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class From_itmes_as_tableContext extends ParserRuleContext {
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public From_itmesContext from_itmes() {
			return getRuleContext(From_itmesContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public Table_aliasContext table_alias() {
			return getRuleContext(Table_aliasContext.class,0);
		}
		public TerminalNode K_AS() { return getToken(RulesParser.K_AS, 0); }
		public From_itmes_as_tableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_from_itmes_as_table; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterFrom_itmes_as_table(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitFrom_itmes_as_table(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitFrom_itmes_as_table(this);
			else return visitor.visitChildren(this);
		}
	}

	public final From_itmes_as_tableContext from_itmes_as_table() throws RecognitionException {
		From_itmes_as_tableContext _localctx = new From_itmes_as_tableContext(_ctx, getState());
		enterRule(_localctx, 302, RULE_from_itmes_as_table);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1637);
			match(OPEN_PAR);
			setState(1638);
			from_itmes();
			setState(1639);
			match(CLOSE_PAR);
			setState(1644);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,149,_ctx) ) {
			case 1:
				{
				setState(1641);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_AS) {
					{
					setState(1640);
					match(K_AS);
					}
				}

				setState(1643);
				table_alias();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Select_stmt_as_tableContext extends ParserRuleContext {
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public Select_stmtContext select_stmt() {
			return getRuleContext(Select_stmtContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public Table_aliasContext table_alias() {
			return getRuleContext(Table_aliasContext.class,0);
		}
		public TerminalNode K_AS() { return getToken(RulesParser.K_AS, 0); }
		public Select_stmt_as_tableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_select_stmt_as_table; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterSelect_stmt_as_table(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitSelect_stmt_as_table(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitSelect_stmt_as_table(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Select_stmt_as_tableContext select_stmt_as_table() throws RecognitionException {
		Select_stmt_as_tableContext _localctx = new Select_stmt_as_tableContext(_ctx, getState());
		enterRule(_localctx, 304, RULE_select_stmt_as_table);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1646);
			match(OPEN_PAR);
			setState(1647);
			select_stmt();
			setState(1648);
			match(CLOSE_PAR);
			setState(1653);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,151,_ctx) ) {
			case 1:
				{
				setState(1650);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_AS) {
					{
					setState(1649);
					match(K_AS);
					}
				}

				setState(1652);
				table_alias();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_as_aliasContext extends ParserRuleContext {
		public Table_name_faildContext table_name_faild() {
			return getRuleContext(Table_name_faildContext.class,0);
		}
		public Table_aliasContext table_alias() {
			return getRuleContext(Table_aliasContext.class,0);
		}
		public Index_name_or_not_indexContext index_name_or_not_index() {
			return getRuleContext(Index_name_or_not_indexContext.class,0);
		}
		public TerminalNode K_AS() { return getToken(RulesParser.K_AS, 0); }
		public Table_as_aliasContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_as_alias; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterTable_as_alias(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitTable_as_alias(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitTable_as_alias(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_as_aliasContext table_as_alias() throws RecognitionException {
		Table_as_aliasContext _localctx = new Table_as_aliasContext(_ctx, getState());
		enterRule(_localctx, 306, RULE_table_as_alias);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1655);
			table_name_faild();
			setState(1660);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,153,_ctx) ) {
			case 1:
				{
				setState(1657);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==K_AS) {
					{
					setState(1656);
					match(K_AS);
					}
				}

				setState(1659);
				table_alias();
				}
				break;
			}
			setState(1663);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,154,_ctx) ) {
			case 1:
				{
				setState(1662);
				index_name_or_not_index();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Index_name_or_not_indexContext extends ParserRuleContext {
		public TerminalNode K_INDEXED() { return getToken(RulesParser.K_INDEXED, 0); }
		public TerminalNode K_BY() { return getToken(RulesParser.K_BY, 0); }
		public Index_nameContext index_name() {
			return getRuleContext(Index_nameContext.class,0);
		}
		public TerminalNode K_NOT() { return getToken(RulesParser.K_NOT, 0); }
		public Index_name_or_not_indexContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_index_name_or_not_index; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterIndex_name_or_not_index(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitIndex_name_or_not_index(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitIndex_name_or_not_index(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Index_name_or_not_indexContext index_name_or_not_index() throws RecognitionException {
		Index_name_or_not_indexContext _localctx = new Index_name_or_not_indexContext(_ctx, getState());
		enterRule(_localctx, 308, RULE_index_name_or_not_index);
		try {
			setState(1670);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case K_INDEXED:
				enterOuterAlt(_localctx, 1);
				{
				setState(1665);
				match(K_INDEXED);
				setState(1666);
				match(K_BY);
				setState(1667);
				index_name();
				}
				break;
			case K_NOT:
				enterOuterAlt(_localctx, 2);
				{
				setState(1668);
				match(K_NOT);
				setState(1669);
				match(K_INDEXED);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Join_clauseContext extends ParserRuleContext {
		public Table_or_subqueryContext table_or_subquery() {
			return getRuleContext(Table_or_subqueryContext.class,0);
		}
		public List<Operator_table_constraintContext> operator_table_constraint() {
			return getRuleContexts(Operator_table_constraintContext.class);
		}
		public Operator_table_constraintContext operator_table_constraint(int i) {
			return getRuleContext(Operator_table_constraintContext.class,i);
		}
		public Join_clauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_join_clause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterJoin_clause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitJoin_clause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitJoin_clause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Join_clauseContext join_clause() throws RecognitionException {
		Join_clauseContext _localctx = new Join_clauseContext(_ctx, getState());
		enterRule(_localctx, 310, RULE_join_clause);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1672);
			table_or_subquery();
			setState(1676);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,156,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1673);
					operator_table_constraint();
					}
					} 
				}
				setState(1678);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,156,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Operator_table_constraintContext extends ParserRuleContext {
		public Join_operatorContext join_operator() {
			return getRuleContext(Join_operatorContext.class,0);
		}
		public Table_or_subqueryContext table_or_subquery() {
			return getRuleContext(Table_or_subqueryContext.class,0);
		}
		public Join_constraintContext join_constraint() {
			return getRuleContext(Join_constraintContext.class,0);
		}
		public Operator_table_constraintContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_operator_table_constraint; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterOperator_table_constraint(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitOperator_table_constraint(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitOperator_table_constraint(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Operator_table_constraintContext operator_table_constraint() throws RecognitionException {
		Operator_table_constraintContext _localctx = new Operator_table_constraintContext(_ctx, getState());
		enterRule(_localctx, 312, RULE_operator_table_constraint);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1679);
			join_operator();
			setState(1680);
			table_or_subquery();
			setState(1681);
			join_constraint();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Join_operatorContext extends ParserRuleContext {
		public TerminalNode COMMA() { return getToken(RulesParser.COMMA, 0); }
		public TerminalNode K_JOIN() { return getToken(RulesParser.K_JOIN, 0); }
		public TerminalNode K_LEFT() { return getToken(RulesParser.K_LEFT, 0); }
		public TerminalNode K_INNER() { return getToken(RulesParser.K_INNER, 0); }
		public TerminalNode K_OUTER() { return getToken(RulesParser.K_OUTER, 0); }
		public Join_operatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_join_operator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterJoin_operator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitJoin_operator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitJoin_operator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Join_operatorContext join_operator() throws RecognitionException {
		Join_operatorContext _localctx = new Join_operatorContext(_ctx, getState());
		enterRule(_localctx, 314, RULE_join_operator);
		int _la;
		try {
			setState(1692);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case COMMA:
				enterOuterAlt(_localctx, 1);
				{
				setState(1683);
				match(COMMA);
				}
				break;
			case K_INNER:
			case K_JOIN:
			case K_LEFT:
				enterOuterAlt(_localctx, 2);
				{
				setState(1689);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case K_LEFT:
					{
					setState(1684);
					match(K_LEFT);
					setState(1686);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==K_OUTER) {
						{
						setState(1685);
						match(K_OUTER);
						}
					}

					}
					break;
				case K_INNER:
					{
					setState(1688);
					match(K_INNER);
					}
					break;
				case K_JOIN:
					break;
				default:
					break;
				}
				setState(1691);
				match(K_JOIN);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Join_constraintContext extends ParserRuleContext {
		public TerminalNode K_ON() { return getToken(RulesParser.K_ON, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Join_constraintContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_join_constraint; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterJoin_constraint(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitJoin_constraint(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitJoin_constraint(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Join_constraintContext join_constraint() throws RecognitionException {
		Join_constraintContext _localctx = new Join_constraintContext(_ctx, getState());
		enterRule(_localctx, 316, RULE_join_constraint);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1696);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,160,_ctx) ) {
			case 1:
				{
				setState(1694);
				match(K_ON);
				setState(1695);
				expr(0);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Select_coreContext extends ParserRuleContext {
		public Select_itemsContext select_items() {
			return getRuleContext(Select_itemsContext.class,0);
		}
		public TerminalNode K_FROM() { return getToken(RulesParser.K_FROM, 0); }
		public From_itmesContext from_itmes() {
			return getRuleContext(From_itmesContext.class,0);
		}
		public TerminalNode K_WHERE() { return getToken(RulesParser.K_WHERE, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public Group_byContext group_by() {
			return getRuleContext(Group_byContext.class,0);
		}
		public Values_itemsContext values_items() {
			return getRuleContext(Values_itemsContext.class,0);
		}
		public Select_coreContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_select_core; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterSelect_core(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitSelect_core(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitSelect_core(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Select_coreContext select_core() throws RecognitionException {
		Select_coreContext _localctx = new Select_coreContext(_ctx, getState());
		enterRule(_localctx, 318, RULE_select_core);
		try {
			setState(1711);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case K_SELECT:
				enterOuterAlt(_localctx, 1);
				{
				setState(1698);
				select_items();
				setState(1701);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,161,_ctx) ) {
				case 1:
					{
					setState(1699);
					match(K_FROM);
					setState(1700);
					from_itmes();
					}
					break;
				}
				setState(1705);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,162,_ctx) ) {
				case 1:
					{
					setState(1703);
					match(K_WHERE);
					setState(1704);
					expr(0);
					}
					break;
				}
				setState(1708);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,163,_ctx) ) {
				case 1:
					{
					setState(1707);
					group_by();
					}
					break;
				}
				}
				break;
			case K_VALUES:
				enterOuterAlt(_localctx, 2);
				{
				setState(1710);
				values_items();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Values_itemsContext extends ParserRuleContext {
		public TerminalNode K_VALUES() { return getToken(RulesParser.K_VALUES, 0); }
		public List<TerminalNode> OPEN_PAR() { return getTokens(RulesParser.OPEN_PAR); }
		public TerminalNode OPEN_PAR(int i) {
			return getToken(RulesParser.OPEN_PAR, i);
		}
		public List<Expr_comma_exprContext> expr_comma_expr() {
			return getRuleContexts(Expr_comma_exprContext.class);
		}
		public Expr_comma_exprContext expr_comma_expr(int i) {
			return getRuleContext(Expr_comma_exprContext.class,i);
		}
		public List<TerminalNode> CLOSE_PAR() { return getTokens(RulesParser.CLOSE_PAR); }
		public TerminalNode CLOSE_PAR(int i) {
			return getToken(RulesParser.CLOSE_PAR, i);
		}
		public List<TerminalNode> COMMA() { return getTokens(RulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(RulesParser.COMMA, i);
		}
		public Values_itemsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_values_items; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterValues_items(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitValues_items(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitValues_items(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Values_itemsContext values_items() throws RecognitionException {
		Values_itemsContext _localctx = new Values_itemsContext(_ctx, getState());
		enterRule(_localctx, 320, RULE_values_items);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1713);
			match(K_VALUES);
			setState(1714);
			match(OPEN_PAR);
			setState(1715);
			expr_comma_expr();
			setState(1716);
			match(CLOSE_PAR);
			setState(1724);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,165,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1717);
					match(COMMA);
					setState(1718);
					match(OPEN_PAR);
					setState(1719);
					expr_comma_expr();
					setState(1720);
					match(CLOSE_PAR);
					}
					} 
				}
				setState(1726);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,165,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expr_comma_exprContext extends ParserRuleContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(RulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(RulesParser.COMMA, i);
		}
		public Expr_comma_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr_comma_expr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterExpr_comma_expr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitExpr_comma_expr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitExpr_comma_expr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Expr_comma_exprContext expr_comma_expr() throws RecognitionException {
		Expr_comma_exprContext _localctx = new Expr_comma_exprContext(_ctx, getState());
		enterRule(_localctx, 322, RULE_expr_comma_expr);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1727);
			expr(0);
			setState(1732);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(1728);
				match(COMMA);
				setState(1729);
				expr(0);
				}
				}
				setState(1734);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Group_byContext extends ParserRuleContext {
		public TerminalNode K_GROUP() { return getToken(RulesParser.K_GROUP, 0); }
		public TerminalNode K_BY() { return getToken(RulesParser.K_BY, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(RulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(RulesParser.COMMA, i);
		}
		public TerminalNode K_HAVING() { return getToken(RulesParser.K_HAVING, 0); }
		public Group_byContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_group_by; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterGroup_by(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitGroup_by(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitGroup_by(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Group_byContext group_by() throws RecognitionException {
		Group_byContext _localctx = new Group_byContext(_ctx, getState());
		enterRule(_localctx, 324, RULE_group_by);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1735);
			match(K_GROUP);
			setState(1736);
			match(K_BY);
			setState(1737);
			expr(0);
			setState(1742);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,167,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1738);
					match(COMMA);
					setState(1739);
					expr(0);
					}
					} 
				}
				setState(1744);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,167,_ctx);
			}
			setState(1747);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,168,_ctx) ) {
			case 1:
				{
				setState(1745);
				match(K_HAVING);
				setState(1746);
				expr(0);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Select_itemsContext extends ParserRuleContext {
		public TerminalNode K_SELECT() { return getToken(RulesParser.K_SELECT, 0); }
		public List<Result_columnContext> result_column() {
			return getRuleContexts(Result_columnContext.class);
		}
		public Result_columnContext result_column(int i) {
			return getRuleContext(Result_columnContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(RulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(RulesParser.COMMA, i);
		}
		public TerminalNode K_DISTINCT() { return getToken(RulesParser.K_DISTINCT, 0); }
		public TerminalNode K_ALL() { return getToken(RulesParser.K_ALL, 0); }
		public Select_itemsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_select_items; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterSelect_items(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitSelect_items(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitSelect_items(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Select_itemsContext select_items() throws RecognitionException {
		Select_itemsContext _localctx = new Select_itemsContext(_ctx, getState());
		enterRule(_localctx, 326, RULE_select_items);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1749);
			match(K_SELECT);
			setState(1751);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==K_ALL || _la==K_DISTINCT) {
				{
				setState(1750);
				_la = _input.LA(1);
				if ( !(_la==K_ALL || _la==K_DISTINCT) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			setState(1753);
			result_column();
			setState(1758);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,170,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1754);
					match(COMMA);
					setState(1755);
					result_column();
					}
					} 
				}
				setState(1760);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,170,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class From_itmesContext extends ParserRuleContext {
		public List<Table_or_subqueryContext> table_or_subquery() {
			return getRuleContexts(Table_or_subqueryContext.class);
		}
		public Table_or_subqueryContext table_or_subquery(int i) {
			return getRuleContext(Table_or_subqueryContext.class,i);
		}
		public Join_clauseContext join_clause() {
			return getRuleContext(Join_clauseContext.class,0);
		}
		public List<TerminalNode> COMMA() { return getTokens(RulesParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(RulesParser.COMMA, i);
		}
		public From_itmesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_from_itmes; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterFrom_itmes(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitFrom_itmes(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitFrom_itmes(this);
			else return visitor.visitChildren(this);
		}
	}

	public final From_itmesContext from_itmes() throws RecognitionException {
		From_itmesContext _localctx = new From_itmesContext(_ctx, getState());
		enterRule(_localctx, 328, RULE_from_itmes);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1770);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,172,_ctx) ) {
			case 1:
				{
				setState(1761);
				table_or_subquery();
				setState(1766);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,171,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(1762);
						match(COMMA);
						setState(1763);
						table_or_subquery();
						}
						} 
					}
					setState(1768);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,171,_ctx);
				}
				}
				break;
			case 2:
				{
				setState(1769);
				join_clause();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Signed_numberContext extends ParserRuleContext {
		public TerminalNode NUMERIC_LITERAL() { return getToken(RulesParser.NUMERIC_LITERAL, 0); }
		public TerminalNode STAR() { return getToken(RulesParser.STAR, 0); }
		public TerminalNode PLUS() { return getToken(RulesParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(RulesParser.MINUS, 0); }
		public Signed_numberContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_signed_number; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterSigned_number(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitSigned_number(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitSigned_number(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Signed_numberContext signed_number() throws RecognitionException {
		Signed_numberContext _localctx = new Signed_numberContext(_ctx, getState());
		enterRule(_localctx, 330, RULE_signed_number);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1777);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case NUMERIC_LITERAL:
			case PLUS:
			case MINUS:
				{
				setState(1773);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==PLUS || _la==MINUS) {
					{
					setState(1772);
					_la = _input.LA(1);
					if ( !(_la==PLUS || _la==MINUS) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
				}

				setState(1775);
				match(NUMERIC_LITERAL);
				}
				break;
			case STAR:
				{
				setState(1776);
				match(STAR);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Literal_valueContext extends ParserRuleContext {
		public TerminalNode NUMERIC_LITERAL() { return getToken(RulesParser.NUMERIC_LITERAL, 0); }
		public TerminalNode STRING_LITERAL() { return getToken(RulesParser.STRING_LITERAL, 0); }
		public TerminalNode BLOB_LITERAL() { return getToken(RulesParser.BLOB_LITERAL, 0); }
		public TerminalNode K_NULL() { return getToken(RulesParser.K_NULL, 0); }
		public TerminalNode K_CURRENT_TIME() { return getToken(RulesParser.K_CURRENT_TIME, 0); }
		public TerminalNode K_CURRENT_DATE() { return getToken(RulesParser.K_CURRENT_DATE, 0); }
		public TerminalNode K_CURRENT_TIMESTAMP() { return getToken(RulesParser.K_CURRENT_TIMESTAMP, 0); }
		public Literal_valueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literal_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterLiteral_value(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitLiteral_value(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitLiteral_value(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Literal_valueContext literal_value() throws RecognitionException {
		Literal_valueContext _localctx = new Literal_valueContext(_ctx, getState());
		enterRule(_localctx, 332, RULE_literal_value);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1779);
			_la = _input.LA(1);
			if ( !(_la==NUMERIC_LITERAL || ((((_la - 77)) & ~0x3f) == 0 && ((1L << (_la - 77)) & ((1L << (K_CURRENT_DATE - 77)) | (1L << (K_CURRENT_TIME - 77)) | (1L << (K_CURRENT_TIMESTAMP - 77)) | (1L << (K_NULL - 77)))) != 0) || _la==STRING_LITERAL || _la==BLOB_LITERAL) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Unary_operatorContext extends ParserRuleContext {
		public TerminalNode MINUS() { return getToken(RulesParser.MINUS, 0); }
		public TerminalNode PLUS() { return getToken(RulesParser.PLUS, 0); }
		public TerminalNode TILDE() { return getToken(RulesParser.TILDE, 0); }
		public TerminalNode K_NOT() { return getToken(RulesParser.K_NOT, 0); }
		public Unary_operatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unary_operator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterUnary_operator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitUnary_operator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitUnary_operator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Unary_operatorContext unary_operator() throws RecognitionException {
		Unary_operatorContext _localctx = new Unary_operatorContext(_ctx, getState());
		enterRule(_localctx, 334, RULE_unary_operator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1781);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << PLUS) | (1L << MINUS) | (1L << TILDE))) != 0) || _la==K_NOT) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_aliasContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(RulesParser.IDENTIFIER, 0); }
		public TerminalNode VALID_NAME() { return getToken(RulesParser.VALID_NAME, 0); }
		public TerminalNode STRING_LITERAL() { return getToken(RulesParser.STRING_LITERAL, 0); }
		public Column_aliasContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_alias; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterColumn_alias(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitColumn_alias(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitColumn_alias(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_aliasContext column_alias() throws RecognitionException {
		Column_aliasContext _localctx = new Column_aliasContext(_ctx, getState());
		enterRule(_localctx, 336, RULE_column_alias);
		int _la;
		try {
			setState(1785);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case VALID_NAME:
			case IDENTIFIER:
				enterOuterAlt(_localctx, 1);
				{
				setState(1783);
				_la = _input.LA(1);
				if ( !(_la==VALID_NAME || _la==IDENTIFIER) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case STRING_LITERAL:
				enterOuterAlt(_localctx, 2);
				{
				setState(1784);
				match(STRING_LITERAL);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KeywordContext extends ParserRuleContext {
		public TerminalNode K_ABORT() { return getToken(RulesParser.K_ABORT, 0); }
		public TerminalNode K_ACTION() { return getToken(RulesParser.K_ACTION, 0); }
		public TerminalNode K_ADD() { return getToken(RulesParser.K_ADD, 0); }
		public TerminalNode K_AFTER() { return getToken(RulesParser.K_AFTER, 0); }
		public TerminalNode K_AGGREGATION_FUNCTION() { return getToken(RulesParser.K_AGGREGATION_FUNCTION, 0); }
		public TerminalNode K_ALL() { return getToken(RulesParser.K_ALL, 0); }
		public TerminalNode K_ALTER() { return getToken(RulesParser.K_ALTER, 0); }
		public TerminalNode K_ANALYZE() { return getToken(RulesParser.K_ANALYZE, 0); }
		public TerminalNode K_AND() { return getToken(RulesParser.K_AND, 0); }
		public TerminalNode K_AS() { return getToken(RulesParser.K_AS, 0); }
		public TerminalNode K_ASC() { return getToken(RulesParser.K_ASC, 0); }
		public TerminalNode K_ATTACH() { return getToken(RulesParser.K_ATTACH, 0); }
		public TerminalNode K_AUTOINCREMENT() { return getToken(RulesParser.K_AUTOINCREMENT, 0); }
		public TerminalNode K_BEFORE() { return getToken(RulesParser.K_BEFORE, 0); }
		public TerminalNode K_BEGIN() { return getToken(RulesParser.K_BEGIN, 0); }
		public TerminalNode K_BETWEEN() { return getToken(RulesParser.K_BETWEEN, 0); }
		public TerminalNode K_BY() { return getToken(RulesParser.K_BY, 0); }
		public TerminalNode K_CASCADE() { return getToken(RulesParser.K_CASCADE, 0); }
		public TerminalNode K_CASE() { return getToken(RulesParser.K_CASE, 0); }
		public TerminalNode K_CAST() { return getToken(RulesParser.K_CAST, 0); }
		public TerminalNode K_CHECK() { return getToken(RulesParser.K_CHECK, 0); }
		public TerminalNode K_COLLATE() { return getToken(RulesParser.K_COLLATE, 0); }
		public TerminalNode K_COLUMN() { return getToken(RulesParser.K_COLUMN, 0); }
		public TerminalNode K_COMMIT() { return getToken(RulesParser.K_COMMIT, 0); }
		public TerminalNode K_CONFLICT() { return getToken(RulesParser.K_CONFLICT, 0); }
		public TerminalNode K_CONSTRAINT() { return getToken(RulesParser.K_CONSTRAINT, 0); }
		public TerminalNode K_CREATE() { return getToken(RulesParser.K_CREATE, 0); }
		public TerminalNode K_CROSS() { return getToken(RulesParser.K_CROSS, 0); }
		public TerminalNode K_CSV() { return getToken(RulesParser.K_CSV, 0); }
		public TerminalNode K_CURRENT_DATE() { return getToken(RulesParser.K_CURRENT_DATE, 0); }
		public TerminalNode K_CURRENT_TIME() { return getToken(RulesParser.K_CURRENT_TIME, 0); }
		public TerminalNode K_CURRENT_TIMESTAMP() { return getToken(RulesParser.K_CURRENT_TIMESTAMP, 0); }
		public TerminalNode K_DATABASE() { return getToken(RulesParser.K_DATABASE, 0); }
		public TerminalNode K_DEFAULT() { return getToken(RulesParser.K_DEFAULT, 0); }
		public TerminalNode K_DEFERRABLE() { return getToken(RulesParser.K_DEFERRABLE, 0); }
		public TerminalNode K_DEFERRED() { return getToken(RulesParser.K_DEFERRED, 0); }
		public TerminalNode K_DELETE() { return getToken(RulesParser.K_DELETE, 0); }
		public TerminalNode K_DESC() { return getToken(RulesParser.K_DESC, 0); }
		public TerminalNode K_DETACH() { return getToken(RulesParser.K_DETACH, 0); }
		public TerminalNode K_DISTINCT() { return getToken(RulesParser.K_DISTINCT, 0); }
		public TerminalNode K_DROP() { return getToken(RulesParser.K_DROP, 0); }
		public TerminalNode K_EACH() { return getToken(RulesParser.K_EACH, 0); }
		public TerminalNode K_ELSE() { return getToken(RulesParser.K_ELSE, 0); }
		public TerminalNode K_END() { return getToken(RulesParser.K_END, 0); }
		public TerminalNode K_ENABLE() { return getToken(RulesParser.K_ENABLE, 0); }
		public TerminalNode K_ESCAPE() { return getToken(RulesParser.K_ESCAPE, 0); }
		public TerminalNode K_EXCEPT() { return getToken(RulesParser.K_EXCEPT, 0); }
		public TerminalNode K_EXCLUSIVE() { return getToken(RulesParser.K_EXCLUSIVE, 0); }
		public TerminalNode K_EXISTS() { return getToken(RulesParser.K_EXISTS, 0); }
		public TerminalNode K_EXPLAIN() { return getToken(RulesParser.K_EXPLAIN, 0); }
		public TerminalNode K_FAIL() { return getToken(RulesParser.K_FAIL, 0); }
		public TerminalNode K_FOR() { return getToken(RulesParser.K_FOR, 0); }
		public TerminalNode K_FOREIGN() { return getToken(RulesParser.K_FOREIGN, 0); }
		public TerminalNode K_FROM() { return getToken(RulesParser.K_FROM, 0); }
		public TerminalNode K_FULL() { return getToken(RulesParser.K_FULL, 0); }
		public TerminalNode K_GLOB() { return getToken(RulesParser.K_GLOB, 0); }
		public TerminalNode K_GROUP() { return getToken(RulesParser.K_GROUP, 0); }
		public TerminalNode K_HAVING() { return getToken(RulesParser.K_HAVING, 0); }
		public TerminalNode K_IF() { return getToken(RulesParser.K_IF, 0); }
		public TerminalNode K_IGNORE() { return getToken(RulesParser.K_IGNORE, 0); }
		public TerminalNode K_IMMEDIATE() { return getToken(RulesParser.K_IMMEDIATE, 0); }
		public TerminalNode K_IN() { return getToken(RulesParser.K_IN, 0); }
		public TerminalNode K_INDEX() { return getToken(RulesParser.K_INDEX, 0); }
		public TerminalNode K_INDEXED() { return getToken(RulesParser.K_INDEXED, 0); }
		public TerminalNode K_INITIALLY() { return getToken(RulesParser.K_INITIALLY, 0); }
		public TerminalNode K_INNER() { return getToken(RulesParser.K_INNER, 0); }
		public TerminalNode K_INSERT() { return getToken(RulesParser.K_INSERT, 0); }
		public TerminalNode K_INSTEAD() { return getToken(RulesParser.K_INSTEAD, 0); }
		public TerminalNode K_INTERSECT() { return getToken(RulesParser.K_INTERSECT, 0); }
		public TerminalNode K_INTO() { return getToken(RulesParser.K_INTO, 0); }
		public TerminalNode K_IS() { return getToken(RulesParser.K_IS, 0); }
		public TerminalNode K_ISNULL() { return getToken(RulesParser.K_ISNULL, 0); }
		public TerminalNode K_JOIN() { return getToken(RulesParser.K_JOIN, 0); }
		public TerminalNode K_KEY() { return getToken(RulesParser.K_KEY, 0); }
		public TerminalNode K_LEFT() { return getToken(RulesParser.K_LEFT, 0); }
		public TerminalNode K_LIKE() { return getToken(RulesParser.K_LIKE, 0); }
		public TerminalNode K_LIMIT() { return getToken(RulesParser.K_LIMIT, 0); }
		public TerminalNode K_MATCH() { return getToken(RulesParser.K_MATCH, 0); }
		public TerminalNode K_NATURAL() { return getToken(RulesParser.K_NATURAL, 0); }
		public TerminalNode K_NO() { return getToken(RulesParser.K_NO, 0); }
		public TerminalNode K_NOT() { return getToken(RulesParser.K_NOT, 0); }
		public TerminalNode K_NOTNULL() { return getToken(RulesParser.K_NOTNULL, 0); }
		public TerminalNode K_NULL() { return getToken(RulesParser.K_NULL, 0); }
		public TerminalNode K_OF() { return getToken(RulesParser.K_OF, 0); }
		public TerminalNode K_OFFSET() { return getToken(RulesParser.K_OFFSET, 0); }
		public TerminalNode K_ON() { return getToken(RulesParser.K_ON, 0); }
		public TerminalNode K_OR() { return getToken(RulesParser.K_OR, 0); }
		public TerminalNode K_ORDER() { return getToken(RulesParser.K_ORDER, 0); }
		public TerminalNode K_OUTER() { return getToken(RulesParser.K_OUTER, 0); }
		public TerminalNode K_PATH() { return getToken(RulesParser.K_PATH, 0); }
		public TerminalNode K_PLAN() { return getToken(RulesParser.K_PLAN, 0); }
		public TerminalNode K_PRAGMA() { return getToken(RulesParser.K_PRAGMA, 0); }
		public TerminalNode K_PRIMARY() { return getToken(RulesParser.K_PRIMARY, 0); }
		public TerminalNode K_QUERY() { return getToken(RulesParser.K_QUERY, 0); }
		public TerminalNode K_RAISE() { return getToken(RulesParser.K_RAISE, 0); }
		public TerminalNode K_RECURSIVE() { return getToken(RulesParser.K_RECURSIVE, 0); }
		public TerminalNode K_REFERENCES() { return getToken(RulesParser.K_REFERENCES, 0); }
		public TerminalNode K_REGEXP() { return getToken(RulesParser.K_REGEXP, 0); }
		public TerminalNode K_REINDEX() { return getToken(RulesParser.K_REINDEX, 0); }
		public TerminalNode K_RELEASE() { return getToken(RulesParser.K_RELEASE, 0); }
		public TerminalNode K_RENAME() { return getToken(RulesParser.K_RENAME, 0); }
		public TerminalNode K_REPLACE() { return getToken(RulesParser.K_REPLACE, 0); }
		public TerminalNode K_RESTRICT() { return getToken(RulesParser.K_RESTRICT, 0); }
		public TerminalNode K_RIGHT() { return getToken(RulesParser.K_RIGHT, 0); }
		public TerminalNode K_ROLLBACK() { return getToken(RulesParser.K_ROLLBACK, 0); }
		public TerminalNode K_ROW() { return getToken(RulesParser.K_ROW, 0); }
		public TerminalNode K_SAVEPOINT() { return getToken(RulesParser.K_SAVEPOINT, 0); }
		public TerminalNode K_SELECT() { return getToken(RulesParser.K_SELECT, 0); }
		public TerminalNode K_SET() { return getToken(RulesParser.K_SET, 0); }
		public TerminalNode K_SWITCH() { return getToken(RulesParser.K_SWITCH, 0); }
		public TerminalNode K_TABLE() { return getToken(RulesParser.K_TABLE, 0); }
		public TerminalNode K_TYPE() { return getToken(RulesParser.K_TYPE, 0); }
		public TerminalNode K_TEMP() { return getToken(RulesParser.K_TEMP, 0); }
		public TerminalNode K_TEMPORARY() { return getToken(RulesParser.K_TEMPORARY, 0); }
		public TerminalNode K_THEN() { return getToken(RulesParser.K_THEN, 0); }
		public TerminalNode K_TO() { return getToken(RulesParser.K_TO, 0); }
		public TerminalNode K_TRANSACTION() { return getToken(RulesParser.K_TRANSACTION, 0); }
		public TerminalNode K_TRIGGER() { return getToken(RulesParser.K_TRIGGER, 0); }
		public TerminalNode K_UNION() { return getToken(RulesParser.K_UNION, 0); }
		public TerminalNode K_UNIQUE() { return getToken(RulesParser.K_UNIQUE, 0); }
		public TerminalNode K_UPDATE() { return getToken(RulesParser.K_UPDATE, 0); }
		public TerminalNode K_USING() { return getToken(RulesParser.K_USING, 0); }
		public TerminalNode K_VACUUM() { return getToken(RulesParser.K_VACUUM, 0); }
		public TerminalNode K_VALUES() { return getToken(RulesParser.K_VALUES, 0); }
		public TerminalNode K_VIEW() { return getToken(RulesParser.K_VIEW, 0); }
		public TerminalNode K_VIRTUAL() { return getToken(RulesParser.K_VIRTUAL, 0); }
		public TerminalNode K_WHEN() { return getToken(RulesParser.K_WHEN, 0); }
		public TerminalNode K_WHERE() { return getToken(RulesParser.K_WHERE, 0); }
		public TerminalNode K_WITH() { return getToken(RulesParser.K_WITH, 0); }
		public TerminalNode K_WITHOUT() { return getToken(RulesParser.K_WITHOUT, 0); }
		public TerminalNode K_NEXTVAL() { return getToken(RulesParser.K_NEXTVAL, 0); }
		public TerminalNode K_XML() { return getToken(RulesParser.K_XML, 0); }
		public KeywordContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_keyword; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterKeyword(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitKeyword(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitKeyword(this);
			else return visitor.visitChildren(this);
		}
	}

	public final KeywordContext keyword() throws RecognitionException {
		KeywordContext _localctx = new KeywordContext(_ctx, getState());
		enterRule(_localctx, 338, RULE_keyword);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1787);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << K_ABORT) | (1L << K_ACTION) | (1L << K_ADD) | (1L << K_AFTER) | (1L << K_AGGREGATION_FUNCTION) | (1L << K_ALL) | (1L << K_ALTER) | (1L << K_ANALYZE) | (1L << K_AND) | (1L << K_AS) | (1L << K_ASC) | (1L << K_ATTACH) | (1L << K_AUTOINCREMENT) | (1L << K_BEFORE) | (1L << K_BEGIN) | (1L << K_BETWEEN) | (1L << K_BY))) != 0) || ((((_la - 64)) & ~0x3f) == 0 && ((1L << (_la - 64)) & ((1L << (K_CASCADE - 64)) | (1L << (K_CASE - 64)) | (1L << (K_CAST - 64)) | (1L << (K_CHECK - 64)) | (1L << (K_COLLATE - 64)) | (1L << (K_COLUMN - 64)) | (1L << (K_COMMIT - 64)) | (1L << (K_CONFLICT - 64)) | (1L << (K_CONSTRAINT - 64)) | (1L << (K_CREATE - 64)) | (1L << (K_CROSS - 64)) | (1L << (K_CSV - 64)) | (1L << (K_CURRENT_DATE - 64)) | (1L << (K_CURRENT_TIME - 64)) | (1L << (K_CURRENT_TIMESTAMP - 64)) | (1L << (K_DATABASE - 64)) | (1L << (K_DEFAULT - 64)) | (1L << (K_DEFERRABLE - 64)) | (1L << (K_DEFERRED - 64)) | (1L << (K_DELETE - 64)) | (1L << (K_DESC - 64)) | (1L << (K_DETACH - 64)) | (1L << (K_DISTINCT - 64)) | (1L << (K_DROP - 64)) | (1L << (K_EACH - 64)) | (1L << (K_ELSE - 64)) | (1L << (K_END - 64)) | (1L << (K_ENABLE - 64)) | (1L << (K_ESCAPE - 64)) | (1L << (K_EXCEPT - 64)) | (1L << (K_EXCLUSIVE - 64)) | (1L << (K_EXISTS - 64)) | (1L << (K_EXPLAIN - 64)) | (1L << (K_FAIL - 64)) | (1L << (K_FOR - 64)) | (1L << (K_FOREIGN - 64)) | (1L << (K_FROM - 64)) | (1L << (K_FULL - 64)) | (1L << (K_GLOB - 64)) | (1L << (K_GROUP - 64)) | (1L << (K_HAVING - 64)) | (1L << (K_IF - 64)) | (1L << (K_IGNORE - 64)) | (1L << (K_IMMEDIATE - 64)) | (1L << (K_IN - 64)) | (1L << (K_INDEX - 64)) | (1L << (K_INDEXED - 64)) | (1L << (K_INITIALLY - 64)) | (1L << (K_INNER - 64)) | (1L << (K_INSERT - 64)) | (1L << (K_INSTEAD - 64)) | (1L << (K_INTERSECT - 64)) | (1L << (K_INTO - 64)) | (1L << (K_IS - 64)) | (1L << (K_ISNULL - 64)) | (1L << (K_JOIN - 64)) | (1L << (K_KEY - 64)) | (1L << (K_LEFT - 64)) | (1L << (K_LIKE - 64)))) != 0) || ((((_la - 128)) & ~0x3f) == 0 && ((1L << (_la - 128)) & ((1L << (K_LIMIT - 128)) | (1L << (K_MATCH - 128)) | (1L << (K_NATURAL - 128)) | (1L << (K_NEXTVAL - 128)) | (1L << (K_NO - 128)) | (1L << (K_NOT - 128)) | (1L << (K_NOTNULL - 128)) | (1L << (K_NULL - 128)) | (1L << (K_OF - 128)) | (1L << (K_OFFSET - 128)) | (1L << (K_ON - 128)) | (1L << (K_OR - 128)) | (1L << (K_ORDER - 128)) | (1L << (K_OUTER - 128)) | (1L << (K_PATH - 128)) | (1L << (K_PLAN - 128)) | (1L << (K_PRAGMA - 128)) | (1L << (K_PRIMARY - 128)) | (1L << (K_QUERY - 128)) | (1L << (K_RAISE - 128)) | (1L << (K_RECURSIVE - 128)) | (1L << (K_REFERENCES - 128)) | (1L << (K_REGEXP - 128)) | (1L << (K_REINDEX - 128)) | (1L << (K_RELEASE - 128)) | (1L << (K_RENAME - 128)) | (1L << (K_REPLACE - 128)) | (1L << (K_RESTRICT - 128)) | (1L << (K_RIGHT - 128)) | (1L << (K_ROLLBACK - 128)) | (1L << (K_ROW - 128)) | (1L << (K_SAVEPOINT - 128)) | (1L << (K_SELECT - 128)) | (1L << (K_SET - 128)) | (1L << (K_SWITCH - 128)) | (1L << (K_TABLE - 128)) | (1L << (K_TYPE - 128)) | (1L << (K_TEMP - 128)) | (1L << (K_TEMPORARY - 128)) | (1L << (K_THEN - 128)) | (1L << (K_TO - 128)) | (1L << (K_TRANSACTION - 128)) | (1L << (K_TRIGGER - 128)) | (1L << (K_UNION - 128)) | (1L << (K_UNIQUE - 128)) | (1L << (K_UPDATE - 128)) | (1L << (K_USING - 128)) | (1L << (K_VACUUM - 128)) | (1L << (K_VALUES - 128)) | (1L << (K_VIEW - 128)) | (1L << (K_VIRTUAL - 128)) | (1L << (K_WHEN - 128)) | (1L << (K_WHERE - 128)) | (1L << (K_WITH - 128)) | (1L << (K_WITHOUT - 128)) | (1L << (K_XML - 128)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public NameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitName(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NameContext name() throws RecognitionException {
		NameContext _localctx = new NameContext(_ctx, getState());
		enterRule(_localctx, 340, RULE_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1789);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Function_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterFunction_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitFunction_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitFunction_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Function_nameContext function_name() throws RecognitionException {
		Function_nameContext _localctx = new Function_nameContext(_ctx, getState());
		enterRule(_localctx, 342, RULE_function_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1791);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Database_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Database_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_database_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterDatabase_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitDatabase_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitDatabase_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Database_nameContext database_name() throws RecognitionException {
		Database_nameContext _localctx = new Database_nameContext(_ctx, getState());
		enterRule(_localctx, 344, RULE_database_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1793);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Source_table_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Source_table_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_source_table_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterSource_table_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitSource_table_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitSource_table_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Source_table_nameContext source_table_name() throws RecognitionException {
		Source_table_nameContext _localctx = new Source_table_nameContext(_ctx, getState());
		enterRule(_localctx, 346, RULE_source_table_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1795);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Table_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterTable_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitTable_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitTable_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_nameContext table_name() throws RecognitionException {
		Table_nameContext _localctx = new Table_nameContext(_ctx, getState());
		enterRule(_localctx, 348, RULE_table_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1797);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class New_table_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public New_table_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_new_table_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterNew_table_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitNew_table_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitNew_table_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final New_table_nameContext new_table_name() throws RecognitionException {
		New_table_nameContext _localctx = new New_table_nameContext(_ctx, getState());
		enterRule(_localctx, 350, RULE_new_table_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1799);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Column_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Column_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_column_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterColumn_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitColumn_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitColumn_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Column_nameContext column_name() throws RecognitionException {
		Column_nameContext _localctx = new Column_nameContext(_ctx, getState());
		enterRule(_localctx, 352, RULE_column_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1801);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Collation_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Collation_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_collation_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterCollation_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitCollation_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitCollation_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Collation_nameContext collation_name() throws RecognitionException {
		Collation_nameContext _localctx = new Collation_nameContext(_ctx, getState());
		enterRule(_localctx, 354, RULE_collation_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1803);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Foreign_tableContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Foreign_tableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_foreign_table; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterForeign_table(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitForeign_table(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitForeign_table(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Foreign_tableContext foreign_table() throws RecognitionException {
		Foreign_tableContext _localctx = new Foreign_tableContext(_ctx, getState());
		enterRule(_localctx, 356, RULE_foreign_table);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1805);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Index_nameContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Index_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_index_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterIndex_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitIndex_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitIndex_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Index_nameContext index_name() throws RecognitionException {
		Index_nameContext _localctx = new Index_nameContext(_ctx, getState());
		enterRule(_localctx, 358, RULE_index_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1807);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Table_aliasContext extends ParserRuleContext {
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public Table_aliasContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table_alias; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterTable_alias(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitTable_alias(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitTable_alias(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Table_aliasContext table_alias() throws RecognitionException {
		Table_aliasContext _localctx = new Table_aliasContext(_ctx, getState());
		enterRule(_localctx, 360, RULE_table_alias);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1809);
			any_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Any_nameContext extends ParserRuleContext {
		public TerminalNode VALID_NAME() { return getToken(RulesParser.VALID_NAME, 0); }
		public TerminalNode IDENTIFIER() { return getToken(RulesParser.IDENTIFIER, 0); }
		public TerminalNode STRING_LITERAL() { return getToken(RulesParser.STRING_LITERAL, 0); }
		public TerminalNode OPEN_PAR() { return getToken(RulesParser.OPEN_PAR, 0); }
		public Any_nameContext any_name() {
			return getRuleContext(Any_nameContext.class,0);
		}
		public TerminalNode CLOSE_PAR() { return getToken(RulesParser.CLOSE_PAR, 0); }
		public Any_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_any_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterAny_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitAny_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitAny_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Any_nameContext any_name() throws RecognitionException {
		Any_nameContext _localctx = new Any_nameContext(_ctx, getState());
		enterRule(_localctx, 362, RULE_any_name);
		int _la;
		try {
			setState(1817);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case VALID_NAME:
			case IDENTIFIER:
				enterOuterAlt(_localctx, 1);
				{
				setState(1811);
				_la = _input.LA(1);
				if ( !(_la==VALID_NAME || _la==IDENTIFIER) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case STRING_LITERAL:
				enterOuterAlt(_localctx, 2);
				{
				setState(1812);
				match(STRING_LITERAL);
				}
				break;
			case OPEN_PAR:
				enterOuterAlt(_localctx, 3);
				{
				setState(1813);
				match(OPEN_PAR);
				setState(1814);
				any_name();
				setState(1815);
				match(CLOSE_PAR);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Break_javaContext extends ParserRuleContext {
		public TerminalNode K_BREAK() { return getToken(RulesParser.K_BREAK, 0); }
		public TerminalNode SCOL() { return getToken(RulesParser.SCOL, 0); }
		public Break_javaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_break_java; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterBreak_java(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitBreak_java(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitBreak_java(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Break_javaContext break_java() throws RecognitionException {
		Break_javaContext _localctx = new Break_javaContext(_ctx, getState());
		enterRule(_localctx, 364, RULE_break_java);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1819);
			match(K_BREAK);
			setState(1820);
			match(SCOL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Continue_javaContext extends ParserRuleContext {
		public TerminalNode K_CONTINUE() { return getToken(RulesParser.K_CONTINUE, 0); }
		public TerminalNode SCOL() { return getToken(RulesParser.SCOL, 0); }
		public Continue_javaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_continue_java; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).enterContinue_java(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RulesListener ) ((RulesListener)listener).exitContinue_java(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof RulesVisitor ) return ((RulesVisitor<? extends T>)visitor).visitContinue_java(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Continue_javaContext continue_java() throws RecognitionException {
		Continue_javaContext _localctx = new Continue_javaContext(_ctx, getState());
		enterRule(_localctx, 366, RULE_continue_java);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1822);
			match(K_CONTINUE);
			setState(1823);
			match(SCOL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 36:
			return expr_java_sempred((Expr_javaContext)_localctx, predIndex);
		case 38:
			return logic_operation_statement_in_java_sempred((Logic_operation_statement_in_javaContext)_localctx, predIndex);
		case 126:
			return expr_sempred((ExprContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expr_java_sempred(Expr_javaContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean logic_operation_statement_in_java_sempred(Logic_operation_statement_in_javaContext _localctx, int predIndex) {
		switch (predIndex) {
		case 1:
			return precpred(_ctx, 2);
		}
		return true;
	}
	private boolean expr_sempred(ExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 2:
			return precpred(_ctx, 11);
		case 3:
			return precpred(_ctx, 10);
		case 4:
			return precpred(_ctx, 9);
		case 5:
			return precpred(_ctx, 8);
		case 6:
			return precpred(_ctx, 7);
		case 7:
			return precpred(_ctx, 6);
		case 8:
			return precpred(_ctx, 5);
		case 9:
			return precpred(_ctx, 4);
		case 10:
			return precpred(_ctx, 14);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\u00c8\u0724\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\tC\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I"+
		"\tI\4J\tJ\4K\tK\4L\tL\4M\tM\4N\tN\4O\tO\4P\tP\4Q\tQ\4R\tR\4S\tS\4T\tT"+
		"\4U\tU\4V\tV\4W\tW\4X\tX\4Y\tY\4Z\tZ\4[\t[\4\\\t\\\4]\t]\4^\t^\4_\t_\4"+
		"`\t`\4a\ta\4b\tb\4c\tc\4d\td\4e\te\4f\tf\4g\tg\4h\th\4i\ti\4j\tj\4k\t"+
		"k\4l\tl\4m\tm\4n\tn\4o\to\4p\tp\4q\tq\4r\tr\4s\ts\4t\tt\4u\tu\4v\tv\4"+
		"w\tw\4x\tx\4y\ty\4z\tz\4{\t{\4|\t|\4}\t}\4~\t~\4\177\t\177\4\u0080\t\u0080"+
		"\4\u0081\t\u0081\4\u0082\t\u0082\4\u0083\t\u0083\4\u0084\t\u0084\4\u0085"+
		"\t\u0085\4\u0086\t\u0086\4\u0087\t\u0087\4\u0088\t\u0088\4\u0089\t\u0089"+
		"\4\u008a\t\u008a\4\u008b\t\u008b\4\u008c\t\u008c\4\u008d\t\u008d\4\u008e"+
		"\t\u008e\4\u008f\t\u008f\4\u0090\t\u0090\4\u0091\t\u0091\4\u0092\t\u0092"+
		"\4\u0093\t\u0093\4\u0094\t\u0094\4\u0095\t\u0095\4\u0096\t\u0096\4\u0097"+
		"\t\u0097\4\u0098\t\u0098\4\u0099\t\u0099\4\u009a\t\u009a\4\u009b\t\u009b"+
		"\4\u009c\t\u009c\4\u009d\t\u009d\4\u009e\t\u009e\4\u009f\t\u009f\4\u00a0"+
		"\t\u00a0\4\u00a1\t\u00a1\4\u00a2\t\u00a2\4\u00a3\t\u00a3\4\u00a4\t\u00a4"+
		"\4\u00a5\t\u00a5\4\u00a6\t\u00a6\4\u00a7\t\u00a7\4\u00a8\t\u00a8\4\u00a9"+
		"\t\u00a9\4\u00aa\t\u00aa\4\u00ab\t\u00ab\4\u00ac\t\u00ac\4\u00ad\t\u00ad"+
		"\4\u00ae\t\u00ae\4\u00af\t\u00af\4\u00b0\t\u00b0\4\u00b1\t\u00b1\4\u00b2"+
		"\t\u00b2\4\u00b3\t\u00b3\4\u00b4\t\u00b4\4\u00b5\t\u00b5\4\u00b6\t\u00b6"+
		"\4\u00b7\t\u00b7\4\u00b8\t\u00b8\4\u00b9\t\u00b9\3\2\7\2\u0174\n\2\f\2"+
		"\16\2\u0177\13\2\3\2\3\2\3\3\3\3\3\3\5\3\u017e\n\3\3\4\3\4\5\4\u0182\n"+
		"\4\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\b\3\b\3\b\7\b\u0193"+
		"\n\b\f\b\16\b\u0196\13\b\3\t\3\t\3\t\5\t\u019b\n\t\3\n\3\n\5\n\u019f\n"+
		"\n\3\n\3\n\3\n\3\13\3\13\5\13\u01a6\n\13\3\f\3\f\5\f\u01aa\n\f\3\f\3\f"+
		"\3\f\3\f\3\r\3\r\3\r\3\r\7\r\u01b4\n\r\f\r\16\r\u01b7\13\r\3\r\3\r\3\16"+
		"\3\16\3\16\3\17\3\17\3\17\3\17\3\20\3\20\3\21\3\21\3\21\3\21\3\21\3\21"+
		"\3\21\3\21\3\21\5\21\u01cd\n\21\3\21\3\21\3\21\5\21\u01d2\n\21\3\22\3"+
		"\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3"+
		"\22\3\22\3\22\3\22\3\22\3\22\3\22\5\22\u01ea\n\22\3\23\3\23\3\23\6\23"+
		"\u01ef\n\23\r\23\16\23\u01f0\3\23\7\23\u01f4\n\23\f\23\16\23\u01f7\13"+
		"\23\3\23\7\23\u01fa\n\23\f\23\16\23\u01fd\13\23\3\23\7\23\u0200\n\23\f"+
		"\23\16\23\u0203\13\23\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\5\24\u020d"+
		"\n\24\3\25\3\25\3\25\3\26\3\26\3\26\3\26\3\26\3\26\3\27\5\27\u0219\n\27"+
		"\3\30\3\30\3\30\7\30\u021e\n\30\f\30\16\30\u0221\13\30\3\30\3\30\5\30"+
		"\u0225\n\30\3\31\3\31\3\31\7\31\u022a\n\31\f\31\16\31\u022d\13\31\3\32"+
		"\3\32\3\32\3\32\3\33\3\33\7\33\u0235\n\33\f\33\16\33\u0238\13\33\3\33"+
		"\3\33\3\34\3\34\7\34\u023e\n\34\f\34\16\34\u0241\13\34\3\34\5\34\u0244"+
		"\n\34\3\34\3\34\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35"+
		"\3\35\3\35\5\35\u0255\n\35\3\36\3\36\7\36\u0259\n\36\f\36\16\36\u025c"+
		"\13\36\3\36\3\36\3\36\3\36\5\36\u0262\n\36\3\37\3\37\3\37\3 \3 \3 \3 "+
		"\3!\3!\3!\3!\3!\3!\3!\5!\u0272\n!\3\"\5\"\u0275\n\"\3\"\3\"\3#\5#\u027a"+
		"\n#\3#\3#\3#\3#\3$\3$\3$\3$\3%\3%\3%\3%\3&\3&\3&\3&\3&\3&\3&\3&\5&\u0290"+
		"\n&\3&\3&\3&\3&\7&\u0296\n&\f&\16&\u0299\13&\3\'\3\'\3\'\3\'\3\'\3\'\3"+
		"\'\3\'\3\'\3\'\3\'\5\'\u02a6\n\'\3(\3(\3(\3(\3(\3(\3(\3(\3(\3(\3(\3(\5"+
		"(\u02b4\n(\3(\3(\3(\3(\7(\u02ba\n(\f(\16(\u02bd\13(\3)\3)\3*\3*\3+\3+"+
		"\3,\3,\3,\3,\3,\3,\3-\3-\3-\3-\3-\5-\u02d0\n-\3.\3.\3.\3.\5.\u02d6\n."+
		"\3.\7.\u02d9\n.\f.\16.\u02dc\13.\3.\5.\u02df\n.\3/\3/\3/\3/\3\60\3\60"+
		"\3\60\5\60\u02e8\n\60\3\61\3\61\3\61\3\61\3\61\3\61\7\61\u02f0\n\61\f"+
		"\61\16\61\u02f3\13\61\3\61\5\61\u02f6\n\61\3\61\3\61\3\62\3\62\3\62\3"+
		"\62\3\62\3\63\3\63\3\63\3\63\3\63\5\63\u0304\n\63\3\64\3\64\3\64\3\64"+
		"\3\65\7\65\u030b\n\65\f\65\16\65\u030e\13\65\3\65\5\65\u0311\n\65\3\66"+
		"\3\66\3\66\3\66\3\66\3\66\5\66\u0319\n\66\3\67\3\67\3\67\3\67\3\67\3\67"+
		"\38\38\38\58\u0324\n8\38\38\78\u0328\n8\f8\168\u032b\138\39\39\39\79\u0330"+
		"\n9\f9\169\u0333\139\3:\3:\3:\3;\3;\3;\3<\3<\3<\3<\3<\3=\3=\3=\5=\u0343"+
		"\n=\3>\3>\3>\7>\u0348\n>\f>\16>\u034b\13>\3?\3?\3?\3@\3@\3@\3@\3@\3@\5"+
		"@\u0356\n@\3A\3A\3A\5A\u035b\nA\3A\3A\3A\3A\3A\3A\3B\3B\3B\3C\3C\3C\3"+
		"D\3D\7D\u036b\nD\fD\16D\u036e\13D\3D\3D\3E\3E\5E\u0374\nE\3E\3E\3E\5E"+
		"\u0379\nE\3F\3F\3F\7F\u037e\nF\fF\16F\u0381\13F\3G\3G\3G\3G\3G\3G\3H\3"+
		"H\3H\7H\u038c\nH\fH\16H\u038f\13H\3I\3I\5I\u0393\nI\3I\3I\3J\3J\3J\3J"+
		"\3K\3K\3K\7K\u039e\nK\fK\16K\u03a1\13K\3L\3L\3L\3L\3M\3M\3M\3M\3M\5M\u03ac"+
		"\nM\3N\3N\3N\3N\7N\u03b2\nN\fN\16N\u03b5\13N\3N\3N\3O\3O\3O\3O\3P\3P\3"+
		"P\3P\3P\3P\3P\3P\3P\5P\u03c6\nP\3P\3P\3P\3P\5P\u03cc\nP\3P\3P\3P\3Q\3"+
		"Q\5Q\u03d3\nQ\3R\3R\3R\3R\3R\7R\u03da\nR\fR\16R\u03dd\13R\3R\3R\3R\3S"+
		"\3S\3S\3T\7T\u03e6\nT\fT\16T\u03e9\13T\3T\3T\6T\u03ed\nT\rT\16T\u03ee"+
		"\3T\7T\u03f2\nT\fT\16T\u03f5\13T\3T\7T\u03f8\nT\fT\16T\u03fb\13T\3U\3"+
		"U\3U\3U\3U\3U\3U\3U\3U\5U\u0406\nU\3V\3V\3V\3V\3V\5V\u040d\nV\3V\3V\3"+
		"V\3V\3V\3V\3V\3V\5V\u0417\nV\3V\5V\u041a\nV\3W\3W\3W\3W\3W\3X\3X\3X\3"+
		"Y\3Y\3Y\3Y\3Y\5Y\u0429\nY\3Y\3Y\3Y\3Y\3Y\3Z\3Z\3Z\3Z\7Z\u0434\nZ\fZ\16"+
		"Z\u0437\13Z\3Z\3Z\3[\3[\3[\3\\\3\\\3\\\3\\\3\\\3\\\3]\3]\3^\3^\3^\3^\3"+
		"_\3_\3_\3_\3_\5_\u044f\n_\3_\3_\3_\3`\3`\3`\3`\3`\3a\3a\3a\3a\3a\3a\3"+
		"a\3a\3a\3a\3a\3a\3b\3b\3b\3b\7b\u0469\nb\fb\16b\u046c\13b\3b\3b\3c\3c"+
		"\3d\3d\3e\3e\3f\3f\3g\3g\3g\3g\3h\3h\3h\3h\3h\7h\u0481\nh\fh\16h\u0484"+
		"\13h\3i\3i\3i\3i\3i\3i\5i\u048c\ni\3j\3j\3j\3j\3j\5j\u0493\nj\3k\3k\3"+
		"k\3k\5k\u0499\nk\3k\3k\3l\3l\5l\u049f\nl\3l\5l\u04a2\nl\3m\3m\3m\3m\3"+
		"m\7m\u04a9\nm\fm\16m\u04ac\13m\3n\3n\3n\3n\5n\u04b2\nn\3o\3o\3o\3o\3o"+
		"\3o\3o\7o\u04bb\no\fo\16o\u04be\13o\3o\3o\5o\u04c2\no\3o\3o\3p\3p\3p\3"+
		"p\5p\u04ca\np\3q\3q\3r\3r\3r\3r\3r\3r\7r\u04d4\nr\fr\16r\u04d7\13r\3r"+
		"\3r\5r\u04db\nr\3s\3s\3s\3s\3t\3t\7t\u04e3\nt\ft\16t\u04e6\13t\3u\3u\5"+
		"u\u04ea\nu\3v\3v\3v\3v\3v\3v\3v\3v\3v\3v\3v\5v\u04f7\nv\3v\5v\u04fa\n"+
		"v\3w\3w\5w\u04fe\nw\3x\3x\5x\u0502\nx\3x\3x\3x\3x\3x\3x\3x\3x\3x\3x\3"+
		"x\3x\5x\u0510\nx\3y\3y\3y\5y\u0515\ny\3y\5y\u0518\ny\3z\3z\3{\3{\3{\3"+
		"|\3|\3}\3}\3}\3}\6}\u0525\n}\r}\16}\u0526\5}\u0529\n}\3~\3~\3~\3~\3~\3"+
		"~\3~\3~\3~\3~\3~\5~\u0536\n~\3\177\3\177\5\177\u053a\n\177\3\u0080\3\u0080"+
		"\3\u0080\3\u0080\3\u0080\3\u0080\3\u0080\3\u0080\3\u0080\3\u0080\3\u0080"+
		"\3\u0080\5\u0080\u0548\n\u0080\3\u0080\3\u0080\3\u0080\3\u0080\3\u0080"+
		"\3\u0080\3\u0080\3\u0080\3\u0080\3\u0080\3\u0080\3\u0080\3\u0080\3\u0080"+
		"\3\u0080\3\u0080\3\u0080\3\u0080\3\u0080\3\u0080\3\u0080\3\u0080\3\u0080"+
		"\3\u0080\3\u0080\3\u0080\3\u0080\5\u0080\u0565\n\u0080\3\u0080\3\u0080"+
		"\3\u0080\3\u0080\3\u0080\3\u0080\3\u0080\3\u0080\3\u0080\7\u0080\u0570"+
		"\n\u0080\f\u0080\16\u0080\u0573\13\u0080\3\u0081\5\u0081\u0576\n\u0081"+
		"\3\u0081\5\u0081\u0579\n\u0081\3\u0081\3\u0081\3\u0081\3\u0081\3\u0082"+
		"\3\u0082\3\u0082\5\u0082\u0582\n\u0082\3\u0082\3\u0082\3\u0082\7\u0082"+
		"\u0587\n\u0082\f\u0082\16\u0082\u058a\13\u0082\3\u0082\5\u0082\u058d\n"+
		"\u0082\3\u0082\3\u0082\3\u0083\3\u0083\3\u0083\5\u0083\u0594\n\u0083\3"+
		"\u0083\3\u0083\3\u0084\3\u0084\3\u0085\3\u0085\3\u0085\3\u0085\7\u0085"+
		"\u059e\n\u0085\f\u0085\16\u0085\u05a1\13\u0085\5\u0085\u05a3\n\u0085\3"+
		"\u0086\5\u0086\u05a6\n\u0086\3\u0086\3\u0086\3\u0086\3\u0086\3\u0086\3"+
		"\u0086\5\u0086\u05ae\n\u0086\3\u0087\3\u0087\3\u0087\3\u0087\3\u0087\3"+
		"\u0087\7\u0087\u05b6\n\u0087\f\u0087\16\u0087\u05b9\13\u0087\3\u0087\3"+
		"\u0087\5\u0087\u05bd\n\u0087\3\u0087\7\u0087\u05c0\n\u0087\f\u0087\16"+
		"\u0087\u05c3\13\u0087\3\u0087\5\u0087\u05c6\n\u0087\3\u0088\5\u0088\u05c9"+
		"\n\u0088\3\u0088\3\u0088\3\u0088\3\u0088\3\u0088\5\u0088\u05d0\n\u0088"+
		"\3\u0088\5\u0088\u05d3\n\u0088\3\u0089\3\u0089\3\u0089\3\u0089\3\u0089"+
		"\3\u0089\3\u0089\3\u0089\3\u0089\3\u0089\5\u0089\u05df\n\u0089\3\u008a"+
		"\3\u008a\3\u008a\5\u008a\u05e4\n\u008a\3\u008b\3\u008b\3\u008b\5\u008b"+
		"\u05e9\n\u008b\3\u008b\3\u008b\3\u008c\3\u008c\3\u008d\3\u008d\3\u008d"+
		"\5\u008d\u05f2\n\u008d\3\u008d\5\u008d\u05f5\n\u008d\3\u008e\3\u008e\5"+
		"\u008e\u05f9\n\u008e\3\u008e\3\u008e\3\u008e\3\u008e\3\u008e\3\u008e\3"+
		"\u008e\3\u008e\3\u008e\5\u008e\u0604\n\u008e\3\u008f\3\u008f\3\u008f\3"+
		"\u008f\3\u008f\3\u008f\7\u008f\u060c\n\u008f\f\u008f\16\u008f\u060f\13"+
		"\u008f\3\u008f\3\u008f\3\u0090\3\u0090\3\u0090\3\u0090\3\u0090\3\u0090"+
		"\7\u0090\u0619\n\u0090\f\u0090\16\u0090\u061c\13\u0090\3\u0090\3\u0090"+
		"\3\u0090\3\u0091\3\u0091\5\u0091\u0623\n\u0091\3\u0091\5\u0091\u0626\n"+
		"\u0091\3\u0091\3\u0091\3\u0091\3\u0091\7\u0091\u062c\n\u0091\f\u0091\16"+
		"\u0091\u062f\13\u0091\3\u0091\3\u0091\3\u0092\3\u0092\5\u0092\u0635\n"+
		"\u0092\3\u0092\3\u0092\3\u0092\3\u0092\7\u0092\u063b\n\u0092\f\u0092\16"+
		"\u0092\u063e\13\u0092\3\u0092\3\u0092\3\u0093\3\u0093\3\u0094\3\u0094"+
		"\3\u0094\3\u0094\3\u0094\3\u0094\5\u0094\u064a\n\u0094\3\u0095\3\u0095"+
		"\3\u0095\5\u0095\u064f\n\u0095\3\u0095\5\u0095\u0652\n\u0095\3\u0096\3"+
		"\u0096\3\u0096\3\u0096\3\u0096\3\u0096\5\u0096\u065a\n\u0096\3\u0097\3"+
		"\u0097\5\u0097\u065e\n\u0097\3\u0097\5\u0097\u0661\n\u0097\3\u0098\3\u0098"+
		"\3\u0098\5\u0098\u0666\n\u0098\3\u0099\3\u0099\3\u0099\3\u0099\5\u0099"+
		"\u066c\n\u0099\3\u0099\5\u0099\u066f\n\u0099\3\u009a\3\u009a\3\u009a\3"+
		"\u009a\5\u009a\u0675\n\u009a\3\u009a\5\u009a\u0678\n\u009a\3\u009b\3\u009b"+
		"\5\u009b\u067c\n\u009b\3\u009b\5\u009b\u067f\n\u009b\3\u009b\5\u009b\u0682"+
		"\n\u009b\3\u009c\3\u009c\3\u009c\3\u009c\3\u009c\5\u009c\u0689\n\u009c"+
		"\3\u009d\3\u009d\7\u009d\u068d\n\u009d\f\u009d\16\u009d\u0690\13\u009d"+
		"\3\u009e\3\u009e\3\u009e\3\u009e\3\u009f\3\u009f\3\u009f\5\u009f\u0699"+
		"\n\u009f\3\u009f\5\u009f\u069c\n\u009f\3\u009f\5\u009f\u069f\n\u009f\3"+
		"\u00a0\3\u00a0\5\u00a0\u06a3\n\u00a0\3\u00a1\3\u00a1\3\u00a1\5\u00a1\u06a8"+
		"\n\u00a1\3\u00a1\3\u00a1\5\u00a1\u06ac\n\u00a1\3\u00a1\5\u00a1\u06af\n"+
		"\u00a1\3\u00a1\5\u00a1\u06b2\n\u00a1\3\u00a2\3\u00a2\3\u00a2\3\u00a2\3"+
		"\u00a2\3\u00a2\3\u00a2\3\u00a2\3\u00a2\7\u00a2\u06bd\n\u00a2\f\u00a2\16"+
		"\u00a2\u06c0\13\u00a2\3\u00a3\3\u00a3\3\u00a3\7\u00a3\u06c5\n\u00a3\f"+
		"\u00a3\16\u00a3\u06c8\13\u00a3\3\u00a4\3\u00a4\3\u00a4\3\u00a4\3\u00a4"+
		"\7\u00a4\u06cf\n\u00a4\f\u00a4\16\u00a4\u06d2\13\u00a4\3\u00a4\3\u00a4"+
		"\5\u00a4\u06d6\n\u00a4\3\u00a5\3\u00a5\5\u00a5\u06da\n\u00a5\3\u00a5\3"+
		"\u00a5\3\u00a5\7\u00a5\u06df\n\u00a5\f\u00a5\16\u00a5\u06e2\13\u00a5\3"+
		"\u00a6\3\u00a6\3\u00a6\7\u00a6\u06e7\n\u00a6\f\u00a6\16\u00a6\u06ea\13"+
		"\u00a6\3\u00a6\5\u00a6\u06ed\n\u00a6\3\u00a7\5\u00a7\u06f0\n\u00a7\3\u00a7"+
		"\3\u00a7\5\u00a7\u06f4\n\u00a7\3\u00a8\3\u00a8\3\u00a9\3\u00a9\3\u00aa"+
		"\3\u00aa\5\u00aa\u06fc\n\u00aa\3\u00ab\3\u00ab\3\u00ac\3\u00ac\3\u00ad"+
		"\3\u00ad\3\u00ae\3\u00ae\3\u00af\3\u00af\3\u00b0\3\u00b0\3\u00b1\3\u00b1"+
		"\3\u00b2\3\u00b2\3\u00b3\3\u00b3\3\u00b4\3\u00b4\3\u00b5\3\u00b5\3\u00b6"+
		"\3\u00b6\3\u00b7\3\u00b7\3\u00b7\3\u00b7\3\u00b7\3\u00b7\5\u00b7\u071c"+
		"\n\u00b7\3\u00b8\3\u00b8\3\u00b8\3\u00b9\3\u00b9\3\u00b9\3\u00b9\3\u0224"+
		"\5JN\u00fe\u00ba\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62"+
		"\64\668:<>@BDFHJLNPRTVXZ\\^`bdfhjlnprtvxz|~\u0080\u0082\u0084\u0086\u0088"+
		"\u008a\u008c\u008e\u0090\u0092\u0094\u0096\u0098\u009a\u009c\u009e\u00a0"+
		"\u00a2\u00a4\u00a6\u00a8\u00aa\u00ac\u00ae\u00b0\u00b2\u00b4\u00b6\u00b8"+
		"\u00ba\u00bc\u00be\u00c0\u00c2\u00c4\u00c6\u00c8\u00ca\u00cc\u00ce\u00d0"+
		"\u00d2\u00d4\u00d6\u00d8\u00da\u00dc\u00de\u00e0\u00e2\u00e4\u00e6\u00e8"+
		"\u00ea\u00ec\u00ee\u00f0\u00f2\u00f4\u00f6\u00f8\u00fa\u00fc\u00fe\u0100"+
		"\u0102\u0104\u0106\u0108\u010a\u010c\u010e\u0110\u0112\u0114\u0116\u0118"+
		"\u011a\u011c\u011e\u0120\u0122\u0124\u0126\u0128\u012a\u012c\u012e\u0130"+
		"\u0132\u0134\u0136\u0138\u013a\u013c\u013e\u0140\u0142\u0144\u0146\u0148"+
		"\u014a\u014c\u014e\u0150\u0152\u0154\u0156\u0158\u015a\u015c\u015e\u0160"+
		"\u0162\u0164\u0166\u0168\u016a\u016c\u016e\u0170\2\26\3\2\17\17\3\2))"+
		"\5\2\17\20\23\23\27\30\3\2\35\"\3\2\25\26\3\2\21\22\4\2\20\20\23\23\5"+
		"\2\3\3NN\u00c0\u00c0\5\2AA\u008a\u008a\u00a8\u00a8\4\2\r\r\u008d\u008d"+
		"\4\299WW\4\2\17\17\27\30\3\2\31\34\3\2\35 \4\2VV\u00b5\u00b5\4\2\64\64"+
		"YY\6\2\5\5OQ\u008b\u008b\u00c4\u00c5\5\2\20\20\23\24\u0088\u0088\3\2\u00c1"+
		"\u00c2\21\2/>@@BJLY[]_fhjl\u0085\u0087\u0089\u008b\u008e\u0090\u00a0\u00a2"+
		"\u00a7\u00a9\u00b1\u00b3\u00bc\u00be\u00c0\2\u0780\2\u0175\3\2\2\2\4\u017d"+
		"\3\2\2\2\6\u0181\3\2\2\2\b\u0183\3\2\2\2\n\u0187\3\2\2\2\f\u018b\3\2\2"+
		"\2\16\u018f\3\2\2\2\20\u0197\3\2\2\2\22\u019e\3\2\2\2\24\u01a5\3\2\2\2"+
		"\26\u01a9\3\2\2\2\30\u01af\3\2\2\2\32\u01ba\3\2\2\2\34\u01bd\3\2\2\2\36"+
		"\u01c1\3\2\2\2 \u01d1\3\2\2\2\"\u01e9\3\2\2\2$\u01f5\3\2\2\2&\u020c\3"+
		"\2\2\2(\u020e\3\2\2\2*\u0211\3\2\2\2,\u0218\3\2\2\2.\u021a\3\2\2\2\60"+
		"\u0226\3\2\2\2\62\u022e\3\2\2\2\64\u0232\3\2\2\2\66\u023b\3\2\2\28\u0254"+
		"\3\2\2\2:\u0261\3\2\2\2<\u0263\3\2\2\2>\u0266\3\2\2\2@\u0271\3\2\2\2B"+
		"\u0274\3\2\2\2D\u0279\3\2\2\2F\u027f\3\2\2\2H\u0283\3\2\2\2J\u028f\3\2"+
		"\2\2L\u02a5\3\2\2\2N\u02b3\3\2\2\2P\u02be\3\2\2\2R\u02c0\3\2\2\2T\u02c2"+
		"\3\2\2\2V\u02c4\3\2\2\2X\u02cf\3\2\2\2Z\u02d1\3\2\2\2\\\u02e0\3\2\2\2"+
		"^\u02e4\3\2\2\2`\u02e9\3\2\2\2b\u02f9\3\2\2\2d\u0303\3\2\2\2f\u0305\3"+
		"\2\2\2h\u0310\3\2\2\2j\u0312\3\2\2\2l\u031a\3\2\2\2n\u0320\3\2\2\2p\u032c"+
		"\3\2\2\2r\u0334\3\2\2\2t\u0337\3\2\2\2v\u033a\3\2\2\2x\u0342\3\2\2\2z"+
		"\u0344\3\2\2\2|\u034c\3\2\2\2~\u034f\3\2\2\2\u0080\u0357\3\2\2\2\u0082"+
		"\u0362\3\2\2\2\u0084\u0365\3\2\2\2\u0086\u0368\3\2\2\2\u0088\u0378\3\2"+
		"\2\2\u008a\u037a\3\2\2\2\u008c\u0382\3\2\2\2\u008e\u0388\3\2\2\2\u0090"+
		"\u0390\3\2\2\2\u0092\u0396\3\2\2\2\u0094\u039a\3\2\2\2\u0096\u03a2\3\2"+
		"\2\2\u0098\u03ab\3\2\2\2\u009a\u03ad\3\2\2\2\u009c\u03b8\3\2\2\2\u009e"+
		"\u03bc\3\2\2\2\u00a0\u03d2\3\2\2\2\u00a2\u03d4\3\2\2\2\u00a4\u03e1\3\2"+
		"\2\2\u00a6\u03e7\3\2\2\2\u00a8\u0405\3\2\2\2\u00aa\u0407\3\2\2\2\u00ac"+
		"\u041b\3\2\2\2\u00ae\u0420\3\2\2\2\u00b0\u0423\3\2\2\2\u00b2\u042f\3\2"+
		"\2\2\u00b4\u043a\3\2\2\2\u00b6\u043d\3\2\2\2\u00b8\u0443\3\2\2\2\u00ba"+
		"\u0445\3\2\2\2\u00bc\u0449\3\2\2\2\u00be\u0453\3\2\2\2\u00c0\u0458\3\2"+
		"\2\2\u00c2\u0464\3\2\2\2\u00c4\u046f\3\2\2\2\u00c6\u0471\3\2\2\2\u00c8"+
		"\u0473\3\2\2\2\u00ca\u0475\3\2\2\2\u00cc\u0477\3\2\2\2\u00ce\u047b\3\2"+
		"\2\2\u00d0\u048b\3\2\2\2\u00d2\u048d\3\2\2\2\u00d4\u0494\3\2\2\2\u00d6"+
		"\u049c\3\2\2\2\u00d8\u04a3\3\2\2\2\u00da\u04ad\3\2\2\2\u00dc\u04b3\3\2"+
		"\2\2\u00de\u04c9\3\2\2\2\u00e0\u04cb\3\2\2\2\u00e2\u04cd\3\2\2\2\u00e4"+
		"\u04dc\3\2\2\2\u00e6\u04e0\3\2\2\2\u00e8\u04e9\3\2\2\2\u00ea\u04f9\3\2"+
		"\2\2\u00ec\u04fb\3\2\2\2\u00ee\u0501\3\2\2\2\u00f0\u0511\3\2\2\2\u00f2"+
		"\u0519\3\2\2\2\u00f4\u051b\3\2\2\2\u00f6\u051e\3\2\2\2\u00f8\u0520\3\2"+
		"\2\2\u00fa\u0535\3\2\2\2\u00fc\u0539\3\2\2\2\u00fe\u0547\3\2\2\2\u0100"+
		"\u0578\3\2\2\2\u0102\u057e\3\2\2\2\u0104\u0593\3\2\2\2\u0106\u0597\3\2"+
		"\2\2\u0108\u05a2\3\2\2\2\u010a\u05a5\3\2\2\2\u010c\u05af\3\2\2\2\u010e"+
		"\u05c8\3\2\2\2\u0110\u05d4\3\2\2\2\u0112\u05e3\3\2\2\2\u0114\u05e8\3\2"+
		"\2\2\u0116\u05ec\3\2\2\2\u0118\u05ee\3\2\2\2\u011a\u05f8\3\2\2\2\u011c"+
		"\u0605\3\2\2\2\u011e\u0612\3\2\2\2\u0120\u0620\3\2\2\2\u0122\u0632\3\2"+
		"\2\2\u0124\u0641\3\2\2\2\u0126\u0643\3\2\2\2\u0128\u064b\3\2\2\2\u012a"+
		"\u0659\3\2\2\2\u012c\u065b\3\2\2\2\u012e\u0665\3\2\2\2\u0130\u0667\3\2"+
		"\2\2\u0132\u0670\3\2\2\2\u0134\u0679\3\2\2\2\u0136\u0688\3\2\2\2\u0138"+
		"\u068a\3\2\2\2\u013a\u0691\3\2\2\2\u013c\u069e\3\2\2\2\u013e\u06a2\3\2"+
		"\2\2\u0140\u06b1\3\2\2\2\u0142\u06b3\3\2\2\2\u0144\u06c1\3\2\2\2\u0146"+
		"\u06c9\3\2\2\2\u0148\u06d7\3\2\2\2\u014a\u06ec\3\2\2\2\u014c\u06f3\3\2"+
		"\2\2\u014e\u06f5\3\2\2\2\u0150\u06f7\3\2\2\2\u0152\u06fb\3\2\2\2\u0154"+
		"\u06fd\3\2\2\2\u0156\u06ff\3\2\2\2\u0158\u0701\3\2\2\2\u015a\u0703\3\2"+
		"\2\2\u015c\u0705\3\2\2\2\u015e\u0707\3\2\2\2\u0160\u0709\3\2\2\2\u0162"+
		"\u070b\3\2\2\2\u0164\u070d\3\2\2\2\u0166\u070f\3\2\2\2\u0168\u0711\3\2"+
		"\2\2\u016a\u0713\3\2\2\2\u016c\u071b\3\2\2\2\u016e\u071d\3\2\2\2\u0170"+
		"\u0720\3\2\2\2\u0172\u0174\5\4\3\2\u0173\u0172\3\2\2\2\u0174\u0177\3\2"+
		"\2\2\u0175\u0173\3\2\2\2\u0175\u0176\3\2\2\2\u0176\u0178\3\2\2\2\u0177"+
		"\u0175\3\2\2\2\u0178\u0179\7\2\2\3\u0179\3\3\2\2\2\u017a\u017e\5\u00a6"+
		"T\2\u017b\u017e\5\6\4\2\u017c\u017e\5\u00a4S\2\u017d\u017a\3\2\2\2\u017d"+
		"\u017b\3\2\2\2\u017d\u017c\3\2\2\2\u017e\5\3\2\2\2\u017f\u0182\58\35\2"+
		"\u0180\u0182\5*\26\2\u0181\u017f\3\2\2\2\u0181\u0180\3\2\2\2\u0182\7\3"+
		"\2\2\2\u0183\u0184\7-\2\2\u0184\u0185\5\16\b\2\u0185\u0186\7\b\2\2\u0186"+
		"\t\3\2\2\2\u0187\u0188\5\36\20\2\u0188\u0189\7\'\2\2\u0189\u018a\7(\2"+
		"\2\u018a\13\3\2\2\2\u018b\u018c\7\'\2\2\u018c\u018d\7(\2\2\u018d\u018e"+
		"\5\36\20\2\u018e\r\3\2\2\2\u018f\u0194\5\24\13\2\u0190\u0191\7\r\2\2\u0191"+
		"\u0193\5\24\13\2\u0192\u0190\3\2\2\2\u0193\u0196\3\2\2\2\u0194\u0192\3"+
		"\2\2\2\u0194\u0195\3\2\2\2\u0195\17\3\2\2\2\u0196\u0194\3\2\2\2\u0197"+
		"\u019a\5\36\20\2\u0198\u0199\7\16\2\2\u0199\u019b\5\"\22\2\u019a\u0198"+
		"\3\2\2\2\u019a\u019b\3\2\2\2\u019b\21\3\2\2\2\u019c\u019f\5\n\6\2\u019d"+
		"\u019f\5\f\7\2\u019e\u019c\3\2\2\2\u019e\u019d\3\2\2\2\u019f\u01a0\3\2"+
		"\2\2\u01a0\u01a1\7\16\2\2\u01a1\u01a2\5 \21\2\u01a2\23\3\2\2\2\u01a3\u01a6"+
		"\5\22\n\2\u01a4\u01a6\5\20\t\2\u01a5\u01a3\3\2\2\2\u01a5\u01a4\3\2\2\2"+
		"\u01a6\25\3\2\2\2\u01a7\u01aa\5\36\20\2\u01a8\u01aa\5\32\16\2\u01a9\u01a7"+
		"\3\2\2\2\u01a9\u01a8\3\2\2\2\u01aa\u01ab\3\2\2\2\u01ab\u01ac\7\16\2\2"+
		"\u01ac\u01ad\5\"\22\2\u01ad\u01ae\7\b\2\2\u01ae\27\3\2\2\2\u01af\u01b0"+
		"\7%\2\2\u01b0\u01b5\5\"\22\2\u01b1\u01b2\7\r\2\2\u01b2\u01b4\5\"\22\2"+
		"\u01b3\u01b1\3\2\2\2\u01b4\u01b7\3\2\2\2\u01b5\u01b3\3\2\2\2\u01b5\u01b6"+
		"\3\2\2\2\u01b6\u01b8\3\2\2\2\u01b7\u01b5\3\2\2\2\u01b8\u01b9\7&\2\2\u01b9"+
		"\31\3\2\2\2\u01ba\u01bb\5\36\20\2\u01bb\u01bc\5\34\17\2\u01bc\33\3\2\2"+
		"\2\u01bd\u01be\7\'\2\2\u01be\u01bf\5\"\22\2\u01bf\u01c0\7(\2\2\u01c0\35"+
		"\3\2\2\2\u01c1\u01c2\7\u00c1\2\2\u01c2\37\3\2\2\2\u01c3\u01c4\7\u0086"+
		"\2\2\u01c4\u01c5\7-\2\2\u01c5\u01c6\7\'\2\2\u01c6\u01c7\5L\'\2\u01c7\u01c8"+
		"\7(\2\2\u01c8\u01cd\3\2\2\2\u01c9\u01ca\7\'\2\2\u01ca\u01cb\7(\2\2\u01cb"+
		"\u01cd\5\30\r\2\u01cc\u01c3\3\2\2\2\u01cc\u01c9\3\2\2\2\u01cd\u01d2\3"+
		"\2\2\2\u01ce\u01d2\5\30\r\2\u01cf\u01d2\5\u00e0q\2\u01d0\u01d2\5\u0084"+
		"C\2\u01d1\u01cc\3\2\2\2\u01d1\u01ce\3\2\2\2\u01d1\u01cf\3\2\2\2\u01d1"+
		"\u01d0\3\2\2\2\u01d2!\3\2\2\2\u01d3\u01ea\5\u00e0q\2\u01d4\u01ea\7\u00b2"+
		"\2\2\u01d5\u01ea\7g\2\2\u01d6\u01ea\7\u008b\2\2\u01d7\u01ea\7\5\2\2\u01d8"+
		"\u01d9\7)\2\2\u01d9\u01da\5$\23\2\u01da\u01db\7)\2\2\u01db\u01ea\3\2\2"+
		"\2\u01dc\u01ea\5\36\20\2\u01dd\u01ea\5\32\16\2\u01de\u01ea\5\u0084C\2"+
		"\u01df\u01ea\5\u009cO\2\u01e0\u01ea\5 \21\2\u01e1\u01ea\5J&\2\u01e2\u01ea"+
		"\5F$\2\u01e3\u01ea\5H%\2\u01e4\u01ea\5x=\2\u01e5\u01ea\5:\36\2\u01e6\u01ea"+
		"\5V,\2\u01e7\u01ea\5\u0092J\2\u01e8\u01ea\5\u009aN\2\u01e9\u01d3\3\2\2"+
		"\2\u01e9\u01d4\3\2\2\2\u01e9\u01d5\3\2\2\2\u01e9\u01d6\3\2\2\2\u01e9\u01d7"+
		"\3\2\2\2\u01e9\u01d8\3\2\2\2\u01e9\u01dc\3\2\2\2\u01e9\u01dd\3\2\2\2\u01e9"+
		"\u01de\3\2\2\2\u01e9\u01df\3\2\2\2\u01e9\u01e0\3\2\2\2\u01e9\u01e1\3\2"+
		"\2\2\u01e9\u01e2\3\2\2\2\u01e9\u01e3\3\2\2\2\u01e9\u01e4\3\2\2\2\u01e9"+
		"\u01e5\3\2\2\2\u01e9\u01e6\3\2\2\2\u01e9\u01e7\3\2\2\2\u01e9\u01e8\3\2"+
		"\2\2\u01ea#\3\2\2\2\u01eb\u01ec\n\2\2\2\u01ec\u01f4\n\3\2\2\u01ed\u01ef"+
		"\7\17\2\2\u01ee\u01ed\3\2\2\2\u01ef\u01f0\3\2\2\2\u01f0\u01ee\3\2\2\2"+
		"\u01f0\u01f1\3\2\2\2\u01f1\u01f2\3\2\2\2\u01f2\u01f4\n\3\2\2\u01f3\u01eb"+
		"\3\2\2\2\u01f3\u01ee\3\2\2\2\u01f4\u01f7\3\2\2\2\u01f5\u01f3\3\2\2\2\u01f5"+
		"\u01f6\3\2\2\2\u01f6\u01fb\3\2\2\2\u01f7\u01f5\3\2\2\2\u01f8\u01fa\7\17"+
		"\2\2\u01f9\u01f8\3\2\2\2\u01fa\u01fd\3\2\2\2\u01fb\u01f9\3\2\2\2\u01fb"+
		"\u01fc\3\2\2\2\u01fc\u0201\3\2\2\2\u01fd\u01fb\3\2\2\2\u01fe\u0200\n\3"+
		"\2\2\u01ff\u01fe\3\2\2\2\u0200\u0203\3\2\2\2\u0201\u01ff\3\2\2\2\u0201"+
		"\u0202\3\2\2\2\u0202%\3\2\2\2\u0203\u0201\3\2\2\2\u0204\u020d\7\5\2\2"+
		"\u0205\u0206\7)\2\2\u0206\u0207\5$\23\2\u0207\u0208\7)\2\2\u0208\u020d"+
		"\3\2\2\2\u0209\u020d\7\u00b2\2\2\u020a\u020d\7g\2\2\u020b\u020d\7\u008b"+
		"\2\2\u020c\u0204\3\2\2\2\u020c\u0205\3\2\2\2\u020c\u0209\3\2\2\2\u020c"+
		"\u020a\3\2\2\2\u020c\u020b\3\2\2\2\u020d\'\3\2\2\2\u020e\u020f\7-\2\2"+
		"\u020f\u0210\5\36\20\2\u0210)\3\2\2\2\u0211\u0212\5\36\20\2\u0212\u0213"+
		"\7\13\2\2\u0213\u0214\5,\27\2\u0214\u0215\7\f\2\2\u0215\u0216\5\66\34"+
		"\2\u0216+\3\2\2\2\u0217\u0219\5.\30\2\u0218\u0217\3\2\2\2\u0218\u0219"+
		"\3\2\2\2\u0219-\3\2\2\2\u021a\u021f\5(\25\2\u021b\u021c\7\r\2\2\u021c"+
		"\u021e\5(\25\2\u021d\u021b\3\2\2\2\u021e\u0221\3\2\2\2\u021f\u021d\3\2"+
		"\2\2\u021f\u0220\3\2\2\2\u0220\u0224\3\2\2\2\u0221\u021f\3\2\2\2\u0222"+
		"\u0223\7\r\2\2\u0223\u0225\5\60\31\2\u0224\u0225\3\2\2\2\u0224\u0222\3"+
		"\2\2\2\u0225/\3\2\2\2\u0226\u022b\5\62\32\2\u0227\u0228\7\r\2\2\u0228"+
		"\u022a\5\62\32\2\u0229\u0227\3\2\2\2\u022a\u022d\3\2\2\2\u022b\u0229\3"+
		"\2\2\2\u022b\u022c\3\2\2\2\u022c\61\3\2\2\2\u022d\u022b\3\2\2\2\u022e"+
		"\u022f\5(\25\2\u022f\u0230\7\16\2\2\u0230\u0231\5&\24\2\u0231\63\3\2\2"+
		"\2\u0232\u0236\7%\2\2\u0233\u0235\58\35\2\u0234\u0233\3\2\2\2\u0235\u0238"+
		"\3\2\2\2\u0236\u0234\3\2\2\2\u0236\u0237\3\2\2\2\u0237\u0239\3\2\2\2\u0238"+
		"\u0236\3\2\2\2\u0239\u023a\7&\2\2\u023a\65\3\2\2\2\u023b\u023f\7%\2\2"+
		"\u023c\u023e\58\35\2\u023d\u023c\3\2\2\2\u023e\u0241\3\2\2\2\u023f\u023d"+
		"\3\2\2\2\u023f\u0240\3\2\2\2\u0240\u0243\3\2\2\2\u0241\u023f\3\2\2\2\u0242"+
		"\u0244\5\u0090I\2\u0243\u0242\3\2\2\2\u0243\u0244\3\2\2\2\u0244\u0245"+
		"\3\2\2\2\u0245\u0246\7&\2\2\u0246\67\3\2\2\2\u0247\u0255\5j\66\2\u0248"+
		"\u0255\5Z.\2\u0249\u0255\5\u0082B\2\u024a\u0255\5~@\2\u024b\u0255\5\u0080"+
		"A\2\u024c\u0255\5\b\5\2\u024d\u0255\5`\61\2\u024e\u0255\5\u009eP\2\u024f"+
		"\u0255\5\u00a2R\2\u0250\u0255\5\26\f\2\u0251\u0255\5\u016e\u00b8\2\u0252"+
		"\u0255\5\u0170\u00b9\2\u0253\u0255\5|?\2\u0254\u0247\3\2\2\2\u0254\u0248"+
		"\3\2\2\2\u0254\u0249\3\2\2\2\u0254\u024a\3\2\2\2\u0254\u024b\3\2\2\2\u0254"+
		"\u024c\3\2\2\2\u0254\u024d\3\2\2\2\u0254\u024e\3\2\2\2\u0254\u024f\3\2"+
		"\2\2\u0254\u0250\3\2\2\2\u0254\u0251\3\2\2\2\u0254\u0252\3\2\2\2\u0254"+
		"\u0253\3\2\2\2\u02559\3\2\2\2\u0256\u025a\5@!\2\u0257\u0259\5<\37\2\u0258"+
		"\u0257\3\2\2\2\u0259\u025c\3\2\2\2\u025a\u0258\3\2\2\2\u025a\u025b\3\2"+
		"\2\2\u025b\u0262\3\2\2\2\u025c\u025a\3\2\2\2\u025d\u025e\7\13\2\2\u025e"+
		"\u025f\5:\36\2\u025f\u0260\7\f\2\2\u0260\u0262\3\2\2\2\u0261\u0256\3\2"+
		"\2\2\u0261\u025d\3\2\2\2\u0262;\3\2\2\2\u0263\u0264\5T+\2\u0264\u0265"+
		"\5@!\2\u0265=\3\2\2\2\u0266\u0267\7\13\2\2\u0267\u0268\5:\36\2\u0268\u0269"+
		"\7\f\2\2\u0269?\3\2\2\2\u026a\u0272\5B\"\2\u026b\u0272\5D#\2\u026c\u0272"+
		"\7\u00b2\2\2\u026d\u0272\7g\2\2\u026e\u0272\5F$\2\u026f\u0272\5H%\2\u0270"+
		"\u0272\5\u0084C\2\u0271\u026a\3\2\2\2\u0271\u026b\3\2\2\2\u0271\u026c"+
		"\3\2\2\2\u0271\u026d\3\2\2\2\u0271\u026e\3\2\2\2\u0271\u026f\3\2\2\2\u0271"+
		"\u0270\3\2\2\2\u0272A\3\2\2\2\u0273\u0275\7$\2\2\u0274\u0273\3\2\2\2\u0274"+
		"\u0275\3\2\2\2\u0275\u0276\3\2\2\2\u0276\u0277\5\36\20\2\u0277C\3\2\2"+
		"\2\u0278\u027a\7$\2\2\u0279\u0278\3\2\2\2\u0279\u027a\3\2\2\2\u027a\u027b"+
		"\3\2\2\2\u027b\u027c\7\13\2\2\u027c\u027d\5@!\2\u027d\u027e\7\f\2\2\u027e"+
		"E\3\2\2\2\u027f\u0280\5J&\2\u0280\u0281\5R*\2\u0281\u0282\5J&\2\u0282"+
		"G\3\2\2\2\u0283\u0284\5N(\2\u0284\u0285\5T+\2\u0285\u0286\5N(\2\u0286"+
		"I\3\2\2\2\u0287\u0288\b&\1\2\u0288\u0289\7\13\2\2\u0289\u028a\5J&\2\u028a"+
		"\u028b\5P)\2\u028b\u028c\5J&\2\u028c\u028d\7\f\2\2\u028d\u0290\3\2\2\2"+
		"\u028e\u0290\5L\'\2\u028f\u0287\3\2\2\2\u028f\u028e\3\2\2\2\u0290\u0297"+
		"\3\2\2\2\u0291\u0292\f\4\2\2\u0292\u0293\5P)\2\u0293\u0294\5J&\5\u0294"+
		"\u0296\3\2\2\2\u0295\u0291\3\2\2\2\u0296\u0299\3\2\2\2\u0297\u0295\3\2"+
		"\2\2\u0297\u0298\3\2\2\2\u0298K\3\2\2\2\u0299\u0297\3\2\2\2\u029a\u02a6"+
		"\7\5\2\2\u029b\u02a6\5\36\20\2\u029c\u029d\7)\2\2\u029d\u029e\5\"\22\2"+
		"\u029e\u029f\7)\2\2\u029f\u02a6\3\2\2\2\u02a0\u02a6\5\32\16\2\u02a1\u02a6"+
		"\5\u009cO\2\u02a2\u02a6\5\u0084C\2\u02a3\u02a6\5r:\2\u02a4\u02a6\5t;\2"+
		"\u02a5\u029a\3\2\2\2\u02a5\u029b\3\2\2\2\u02a5\u029c\3\2\2\2\u02a5\u02a0"+
		"\3\2\2\2\u02a5\u02a1\3\2\2\2\u02a5\u02a2\3\2\2\2\u02a5\u02a3\3\2\2\2\u02a5"+
		"\u02a4\3\2\2\2\u02a6M\3\2\2\2\u02a7\u02a8\b(\1\2\u02a8\u02b4\5\36\20\2"+
		"\u02a9\u02aa\7)\2\2\u02aa\u02ab\5\"\22\2\u02ab\u02ac\7)\2\2\u02ac\u02b4"+
		"\3\2\2\2\u02ad\u02ae\7\13\2\2\u02ae\u02af\5N(\2\u02af\u02b0\5T+\2\u02b0"+
		"\u02b1\5N(\2\u02b1\u02b2\7\f\2\2\u02b2\u02b4\3\2\2\2\u02b3\u02a7\3\2\2"+
		"\2\u02b3\u02a9\3\2\2\2\u02b3\u02ad\3\2\2\2\u02b4\u02bb\3\2\2\2\u02b5\u02b6"+
		"\f\4\2\2\u02b6\u02b7\5T+\2\u02b7\u02b8\5N(\5\u02b8\u02ba\3\2\2\2\u02b9"+
		"\u02b5\3\2\2\2\u02ba\u02bd\3\2\2\2\u02bb\u02b9\3\2\2\2\u02bb\u02bc\3\2"+
		"\2\2\u02bcO\3\2\2\2\u02bd\u02bb\3\2\2\2\u02be\u02bf\t\4\2\2\u02bfQ\3\2"+
		"\2\2\u02c0\u02c1\t\5\2\2\u02c1S\3\2\2\2\u02c2\u02c3\t\6\2\2\u02c3U\3\2"+
		"\2\2\u02c4\u02c5\5:\36\2\u02c5\u02c6\7*\2\2\u02c6\u02c7\5X-\2\u02c7\u02c8"+
		"\7\t\2\2\u02c8\u02c9\5X-\2\u02c9W\3\2\2\2\u02ca\u02d0\5\"\22\2\u02cb\u02cc"+
		"\7\13\2\2\u02cc\u02cd\5V,\2\u02cd\u02ce\7\f\2\2\u02ce\u02d0\3\2\2\2\u02cf"+
		"\u02ca\3\2\2\2\u02cf\u02cb\3\2\2\2\u02d0Y\3\2\2\2\u02d1\u02d2\7p\2\2\u02d2"+
		"\u02d5\5> \2\u02d3\u02d6\58\35\2\u02d4\u02d6\5\64\33\2\u02d5\u02d3\3\2"+
		"\2\2\u02d5\u02d4\3\2\2\2\u02d6\u02da\3\2\2\2\u02d7\u02d9\5\\/\2\u02d8"+
		"\u02d7\3\2\2\2\u02d9\u02dc\3\2\2\2\u02da\u02d8\3\2\2\2\u02da\u02db\3\2"+
		"\2\2\u02db\u02de\3\2\2\2\u02dc\u02da\3\2\2\2\u02dd\u02df\5^\60\2\u02de"+
		"\u02dd\3\2\2\2\u02de\u02df\3\2\2\2\u02df[\3\2\2\2\u02e0\u02e1\7^\2\2\u02e1"+
		"\u02e2\5> \2\u02e2\u02e3\5\64\33\2\u02e3]\3\2\2\2\u02e4\u02e7\7]\2\2\u02e5"+
		"\u02e8\58\35\2\u02e6\u02e8\5\64\33\2\u02e7\u02e5\3\2\2\2\u02e7\u02e6\3"+
		"\2\2\2\u02e8_\3\2\2\2\u02e9\u02ea\7\u00a9\2\2\u02ea\u02eb\7\13\2\2\u02eb"+
		"\u02ec\5\"\22\2\u02ec\u02ed\7\f\2\2\u02ed\u02f1\7%\2\2\u02ee\u02f0\5b"+
		"\62\2\u02ef\u02ee\3\2\2\2\u02f0\u02f3\3\2\2\2\u02f1\u02ef\3\2\2\2\u02f1"+
		"\u02f2\3\2\2\2\u02f2\u02f5\3\2\2\2\u02f3\u02f1\3\2\2\2\u02f4\u02f6\5f"+
		"\64\2\u02f5\u02f4\3\2\2\2\u02f5\u02f6\3\2\2\2\u02f6\u02f7\3\2\2\2\u02f7"+
		"\u02f8\7&\2\2\u02f8a\3\2\2\2\u02f9\u02fa\7C\2\2\u02fa\u02fb\5d\63\2\u02fb"+
		"\u02fc\7\t\2\2\u02fc\u02fd\5h\65\2\u02fdc\3\2\2\2\u02fe\u0304\7\5\2\2"+
		"\u02ff\u0300\7)\2\2\u0300\u0301\5$\23\2\u0301\u0302\7)\2\2\u0302\u0304"+
		"\3\2\2\2\u0303\u02fe\3\2\2\2\u0303\u02ff\3\2\2\2\u0304e\3\2\2\2\u0305"+
		"\u0306\7S\2\2\u0306\u0307\7\t\2\2\u0307\u0308\5h\65\2\u0308g\3\2\2\2\u0309"+
		"\u030b\58\35\2\u030a\u0309\3\2\2\2\u030b\u030e\3\2\2\2\u030c\u030a\3\2"+
		"\2\2\u030c\u030d\3\2\2\2\u030d\u0311\3\2\2\2\u030e\u030c\3\2\2\2\u030f"+
		"\u0311\5\64\33\2\u0310\u030c\3\2\2\2\u0310\u030f\3\2\2\2\u0311i\3\2\2"+
		"\2\u0312\u0313\7h\2\2\u0313\u0314\7\13\2\2\u0314\u0315\5l\67\2\u0315\u0318"+
		"\7\f\2\2\u0316\u0319\58\35\2\u0317\u0319\5\64\33\2\u0318\u0316\3\2\2\2"+
		"\u0318\u0317\3\2\2\2\u0319k\3\2\2\2\u031a\u031b\5n8\2\u031b\u031c\7\b"+
		"\2\2\u031c\u031d\5p9\2\u031d\u031e\7\b\2\2\u031e\u031f\5z>\2\u031fm\3"+
		"\2\2\2\u0320\u0323\5(\25\2\u0321\u0322\7\16\2\2\u0322\u0324\5L\'\2\u0323"+
		"\u0321\3\2\2\2\u0323\u0324\3\2\2\2\u0324\u0329\3\2\2\2\u0325\u0326\7\r"+
		"\2\2\u0326\u0328\5n8\2\u0327\u0325\3\2\2\2\u0328\u032b\3\2\2\2\u0329\u0327"+
		"\3\2\2\2\u0329\u032a\3\2\2\2\u032ao\3\2\2\2\u032b\u0329\3\2\2\2\u032c"+
		"\u0331\5:\36\2\u032d\u032e\7\r\2\2\u032e\u0330\5:\36\2\u032f\u032d\3\2"+
		"\2\2\u0330\u0333\3\2\2\2\u0331\u032f\3\2\2\2\u0331\u0332\3\2\2\2\u0332"+
		"q\3\2\2\2\u0333\u0331\3\2\2\2\u0334\u0335\5\36\20\2\u0335\u0336\t\7\2"+
		"\2\u0336s\3\2\2\2\u0337\u0338\t\7\2\2\u0338\u0339\5\36\20\2\u0339u\3\2"+
		"\2\2\u033a\u033b\5\36\20\2\u033b\u033c\t\b\2\2\u033c\u033d\7\16\2\2\u033d"+
		"\u033e\5J&\2\u033ew\3\2\2\2\u033f\u0343\5r:\2\u0340\u0343\5t;\2\u0341"+
		"\u0343\5v<\2\u0342\u033f\3\2\2\2\u0342\u0340\3\2\2\2\u0342\u0341\3\2\2"+
		"\2\u0343y\3\2\2\2\u0344\u0349\5x=\2\u0345\u0346\7\r\2\2\u0346\u0348\5"+
		"x=\2\u0347\u0345\3\2\2\2\u0348\u034b\3\2\2\2\u0349\u0347\3\2\2\2\u0349"+
		"\u034a\3\2\2\2\u034a{\3\2\2\2\u034b\u0349\3\2\2\2\u034c\u034d\5x=\2\u034d"+
		"\u034e\7\b\2\2\u034e}\3\2\2\2\u034f\u0350\7\u00bd\2\2\u0350\u0351\7\13"+
		"\2\2\u0351\u0352\5:\36\2\u0352\u0355\7\f\2\2\u0353\u0356\58\35\2\u0354"+
		"\u0356\5\64\33\2\u0355\u0353\3\2\2\2\u0355\u0354\3\2\2\2\u0356\177\3\2"+
		"\2\2\u0357\u035a\7Z\2\2\u0358\u035b\58\35\2\u0359\u035b\5\64\33\2\u035a"+
		"\u0358\3\2\2\2\u035a\u0359\3\2\2\2\u035b\u035c\3\2\2\2\u035c\u035d\7\u00bd"+
		"\2\2\u035d\u035e\7\13\2\2\u035e\u035f\5:\36\2\u035f\u0360\7\f\2\2\u0360"+
		"\u0361\7\b\2\2\u0361\u0081\3\2\2\2\u0362\u0363\5\u0084C\2\u0363\u0364"+
		"\7\b\2\2\u0364\u0083\3\2\2\2\u0365\u0366\5\36\20\2\u0366\u0367\5\u0086"+
		"D\2\u0367\u0085\3\2\2\2\u0368\u036c\7\13\2\2\u0369\u036b\5\u008aF\2\u036a"+
		"\u0369\3\2\2\2\u036b\u036e\3\2\2\2\u036c\u036a\3\2\2\2\u036c\u036d\3\2"+
		"\2\2\u036d\u036f\3\2\2\2\u036e\u036c\3\2\2\2\u036f\u0370\7\f\2\2\u0370"+
		"\u0087\3\2\2\2\u0371\u0374\5\36\20\2\u0372\u0374\7\5\2\2\u0373\u0371\3"+
		"\2\2\2\u0373\u0372\3\2\2\2\u0374\u0379\3\2\2\2\u0375\u0379\5\u008cG\2"+
		"\u0376\u0379\5\u0084C\2\u0377\u0379\5J&\2\u0378\u0373\3\2\2\2\u0378\u0375"+
		"\3\2\2\2\u0378\u0376\3\2\2\2\u0378\u0377\3\2\2\2\u0379\u0089\3\2\2\2\u037a"+
		"\u037f\5\u0088E\2\u037b\u037c\7\r\2\2\u037c\u037e\5\u0088E\2\u037d\u037b"+
		"\3\2\2\2\u037e\u0381\3\2\2\2\u037f\u037d\3\2\2\2\u037f\u0380\3\2\2\2\u0380"+
		"\u008b\3\2\2\2\u0381\u037f\3\2\2\2\u0382\u0383\7k\2\2\u0383\u0384\7\13"+
		"\2\2\u0384\u0385\5\u008eH\2\u0385\u0386\7\f\2\2\u0386\u0387\5\64\33\2"+
		"\u0387\u008d\3\2\2\2\u0388\u038d\5\36\20\2\u0389\u038a\7\r\2\2\u038a\u038c"+
		"\5\36\20\2\u038b\u0389\3\2\2\2\u038c\u038f\3\2\2\2\u038d\u038b\3\2\2\2"+
		"\u038d\u038e\3\2\2\2\u038e\u008f\3\2\2\2\u038f\u038d\3\2\2\2\u0390\u0392"+
		"\7\u00a1\2\2\u0391\u0393\5\"\22\2\u0392\u0391\3\2\2\2\u0392\u0393\3\2"+
		"\2\2\u0393\u0394\3\2\2\2\u0394\u0395\7\b\2\2\u0395\u0091\3\2\2\2\u0396"+
		"\u0397\7%\2\2\u0397\u0398\5\u0094K\2\u0398\u0399\7&\2\2\u0399\u0093\3"+
		"\2\2\2\u039a\u039f\5\u0096L\2\u039b\u039c\7\r\2\2\u039c\u039e\5\u0096"+
		"L\2\u039d\u039b\3\2\2\2\u039e\u03a1\3\2\2\2\u039f\u039d\3\2\2\2\u039f"+
		"\u03a0\3\2\2\2\u03a0\u0095\3\2\2\2\u03a1\u039f\3\2\2\2\u03a2\u03a3\5\u0098"+
		"M\2\u03a3\u03a4\7\t\2\2\u03a4\u03a5\5\"\22\2\u03a5\u0097\3\2\2\2\u03a6"+
		"\u03a7\7)\2\2\u03a7\u03a8\5\36\20\2\u03a8\u03a9\7)\2\2\u03a9\u03ac\3\2"+
		"\2\2\u03aa\u03ac\5\36\20\2\u03ab\u03a6\3\2\2\2\u03ab\u03aa\3\2\2\2\u03ac"+
		"\u0099\3\2\2\2\u03ad\u03ae\7\'\2\2\u03ae\u03b3\5\u0092J\2\u03af\u03b0"+
		"\7\r\2\2\u03b0\u03b2\5\u0092J\2\u03b1\u03af\3\2\2\2\u03b2\u03b5\3\2\2"+
		"\2\u03b3\u03b1\3\2\2\2\u03b3\u03b4\3\2\2\2\u03b4\u03b6\3\2\2\2\u03b5\u03b3"+
		"\3\2\2\2\u03b6\u03b7\7(\2\2\u03b7\u009b\3\2\2\2\u03b8\u03b9\5\36\20\2"+
		"\u03b9\u03ba\7\n\2\2\u03ba\u03bb\5\"\22\2\u03bb\u009d\3\2\2\2\u03bc\u03bd"+
		"\5\u00a0Q\2\u03bd\u03be\7\n\2\2\u03be\u03bf\7,\2\2\u03bf\u03c5\7\13\2"+
		"\2\u03c0\u03c1\7\13\2\2\u03c1\u03c2\5\u00a0Q\2\u03c2\u03c3\7\f\2\2\u03c3"+
		"\u03c6\3\2\2\2\u03c4\u03c6\5\u00a0Q\2\u03c5\u03c0\3\2\2\2\u03c5\u03c4"+
		"\3\2\2\2\u03c6\u03c7\3\2\2\2\u03c7\u03c8\7\23\2\2\u03c8\u03cb\7\37\2\2"+
		"\u03c9\u03cc\58\35\2\u03ca\u03cc\5\64\33\2\u03cb\u03c9\3\2\2\2\u03cb\u03ca"+
		"\3\2\2\2\u03cc\u03cd\3\2\2\2\u03cd\u03ce\7\f\2\2\u03ce\u03cf\7\b\2\2\u03cf"+
		"\u009f\3\2\2\2\u03d0\u03d3\5\36\20\2\u03d1\u03d3\5\32\16\2\u03d2\u03d0"+
		"\3\2\2\2\u03d2\u03d1\3\2\2\2\u03d3\u00a1\3\2\2\2\u03d4\u03d5\7.\2\2\u03d5"+
		"\u03d6\7\13\2\2\u03d6\u03db\5\"\22\2\u03d7\u03d8\7\20\2\2\u03d8\u03da"+
		"\5\"\22\2\u03d9\u03d7\3\2\2\2\u03da\u03dd\3\2\2\2\u03db\u03d9\3\2\2\2"+
		"\u03db\u03dc\3\2\2\2\u03dc\u03de\3\2\2\2\u03dd\u03db\3\2\2\2\u03de\u03df"+
		"\7\f\2\2\u03df\u03e0\7\b\2\2\u03e0\u00a3\3\2\2\2\u03e1\u03e2\7\u00c8\2"+
		"\2\u03e2\u03e3\bS\1\2\u03e3\u00a5\3\2\2\2\u03e4\u03e6\7\b\2\2\u03e5\u03e4"+
		"\3\2\2\2\u03e6\u03e9\3\2\2\2\u03e7\u03e5\3\2\2\2\u03e7\u03e8\3\2\2\2\u03e8"+
		"\u03ea\3\2\2\2\u03e9\u03e7\3\2\2\2\u03ea\u03f3\5\u00a8U\2\u03eb\u03ed"+
		"\7\b\2\2\u03ec\u03eb\3\2\2\2\u03ed\u03ee\3\2\2\2\u03ee\u03ec\3\2\2\2\u03ee"+
		"\u03ef\3\2\2\2\u03ef\u03f0\3\2\2\2\u03f0\u03f2\5\u00a8U\2\u03f1\u03ec"+
		"\3\2\2\2\u03f2\u03f5\3\2\2\2\u03f3\u03f1\3\2\2\2\u03f3\u03f4\3\2\2\2\u03f4"+
		"\u03f9\3\2\2\2\u03f5\u03f3\3\2\2\2\u03f6\u03f8\7\b\2\2\u03f7\u03f6\3\2"+
		"\2\2\u03f8\u03fb\3\2\2\2\u03f9\u03f7\3\2\2\2\u03f9\u03fa\3\2\2\2\u03fa"+
		"\u00a7\3\2\2\2\u03fb\u03f9\3\2\2\2\u03fc\u0406\5\u00aaV\2\u03fd\u0406"+
		"\5\u00b0Y\2\u03fe\u0406\5\u00d2j\2\u03ff\u0406\5\u00d4k\2\u0400\u0406"+
		"\5\u00d6l\2\u0401\u0406\5\u00dco\2\u0402\u0406\5\u00e2r\2\u0403\u0406"+
		"\5\u00bc_\2\u0404\u0406\5\u00be`\2\u0405\u03fc\3\2\2\2\u0405\u03fd\3\2"+
		"\2\2\u0405\u03fe\3\2\2\2\u0405\u03ff\3\2\2\2\u0405\u0400\3\2\2\2\u0405"+
		"\u0401\3\2\2\2\u0405\u0402\3\2\2\2\u0405\u0403\3\2\2\2\u0405\u0404\3\2"+
		"\2\2\u0406\u00a9\3\2\2\2\u0407\u0408\7\65\2\2\u0408\u040c\7\u00aa\2\2"+
		"\u0409\u040a\5\u015a\u00ae\2\u040a\u040b\7\n\2\2\u040b\u040d\3\2\2\2\u040c"+
		"\u0409\3\2\2\2\u040c\u040d\3\2\2\2\u040d\u040e\3\2\2\2\u040e\u0419\5\u015c"+
		"\u00af\2\u040f\u0410\7\u009e\2\2\u0410\u0411\7\u00af\2\2\u0411\u041a\5"+
		"\u0160\u00b1\2\u0412\u041a\5\u00aeX\2\u0413\u041a\5\u00acW\2\u0414\u0416"+
		"\7\61\2\2\u0415\u0417\7G\2\2\u0416\u0415\3\2\2\2\u0416\u0417\3\2\2\2\u0417"+
		"\u0418\3\2\2\2\u0418\u041a\5\u00e6t\2\u0419\u040f\3\2\2\2\u0419\u0412"+
		"\3\2\2\2\u0419\u0413\3\2\2\2\u0419\u0414\3\2\2\2\u041a\u00ab\3\2\2\2\u041b"+
		"\u041c\7\61\2\2\u041c\u041d\7J\2\2\u041d\u041e\5\u016c\u00b7\2\u041e\u041f"+
		"\5\u011a\u008e\2\u041f\u00ad\3\2\2\2\u0420\u0421\7\61\2\2\u0421\u0422"+
		"\5\u011a\u008e\2\u0422\u00af\3\2\2\2\u0423\u0424\7L\2\2\u0424\u0428\7"+
		"\u00aa\2\2\u0425\u0426\7p\2\2\u0426\u0427\7\u0088\2\2\u0427\u0429\7d\2"+
		"\2\u0428\u0425\3\2\2\2\u0428\u0429\3\2\2\2\u0429\u042a\3\2\2\2\u042a\u042b"+
		"\5\u0106\u0084\2\u042b\u042c\5\u00b2Z\2\u042c\u042d\5\u00b6\\\2\u042d"+
		"\u042e\5\u00ba^\2\u042e\u00b1\3\2\2\2\u042f\u0430\7\13\2\2\u0430\u0435"+
		"\5\u00b4[\2\u0431\u0432\7\r\2\2\u0432\u0434\5\u00b4[\2\u0433\u0431\3\2"+
		"\2\2\u0434\u0437\3\2\2\2\u0435\u0433\3\2\2\2\u0435\u0436\3\2\2\2\u0436"+
		"\u0438\3\2\2\2\u0437\u0435\3\2\2\2\u0438\u0439\7\f\2\2\u0439\u00b3\3\2"+
		"\2\2\u043a\u043b\5\u0162\u00b2\2\u043b\u043c\5\u00eav\2\u043c\u00b5\3"+
		"\2\2\2\u043d\u043e\7\u00ab\2\2\u043e\u043f\7\16\2\2\u043f\u0440\7)\2\2"+
		"\u0440\u0441\5\u00b8]\2\u0441\u0442\7)\2\2\u0442\u00b7\3\2\2\2\u0443\u0444"+
		"\t\t\2\2\u0444\u00b9\3\2\2\2\u0445\u0446\7\u0093\2\2\u0446\u0447\7\16"+
		"\2\2\u0447\u0448\5\u00caf\2\u0448\u00bb\3\2\2\2\u0449\u044a\7L\2\2\u044a"+
		"\u044e\7\u00ab\2\2\u044b\u044c\7p\2\2\u044c\u044d\7\u0088\2\2\u044d\u044f"+
		"\7d\2\2\u044e\u044b\3\2\2\2\u044e\u044f\3\2\2\2\u044f\u0450\3\2\2\2\u0450"+
		"\u0451\5\u0106\u0084\2\u0451\u0452\5\u00b2Z\2\u0452\u00bd\3\2\2\2\u0453"+
		"\u0454\7L\2\2\u0454\u0455\7\63\2\2\u0455\u0456\5\u016c\u00b7\2\u0456\u0457"+
		"\5\u00c0a\2\u0457\u00bf\3\2\2\2\u0458\u0459\7\13\2\2\u0459\u045a\5\u00ca"+
		"f\2\u045a\u045b\7\r\2\2\u045b\u045c\5\u00c6d\2\u045c\u045d\7\r\2\2\u045d"+
		"\u045e\5\u00c8e\2\u045e\u045f\7\r\2\2\u045f\u0460\5\u00c4c\2\u0460\u0461"+
		"\7\r\2\2\u0461\u0462\5\u00c2b\2\u0462\u0463\7\f\2\2\u0463\u00c1\3\2\2"+
		"\2\u0464\u0465\7\'\2\2\u0465\u046a\5\u00c4c\2\u0466\u0467\7\r\2\2\u0467"+
		"\u0469\5\u00c4c\2\u0468\u0466\3\2\2\2\u0469\u046c\3\2\2\2\u046a\u0468"+
		"\3\2\2\2\u046a\u046b\3\2\2\2\u046b\u046d\3\2\2\2\u046c\u046a\3\2\2\2\u046d"+
		"\u046e\7(\2\2\u046e\u00c3\3\2\2\2\u046f\u0470\t\n\2\2\u0470\u00c5\3\2"+
		"\2\2\u0471\u0472\5\u016c\u00b7\2\u0472\u00c7\3\2\2\2\u0473\u0474\5\u016c"+
		"\u00b7\2\u0474\u00c9\3\2\2\2\u0475\u0476\5\u00ccg\2\u0476\u00cb\3\2\2"+
		"\2\u0477\u0478\7)\2\2\u0478\u0479\5$\23\2\u0479\u047a\7)\2\2\u047a\u00cd"+
		"\3\2\2\2\u047b\u0482\5\u00e6t\2\u047c\u047d\7\r\2\2\u047d\u0481\5\u011a"+
		"\u008e\2\u047e\u047f\7\r\2\2\u047f\u0481\5\u00e6t\2\u0480\u047c\3\2\2"+
		"\2\u0480\u047e\3\2\2\2\u0481\u0484\3\2\2\2\u0482\u0480\3\2\2\2\u0482\u0483"+
		"\3\2\2\2\u0483\u00cf\3\2\2\2\u0484\u0482\3\2\2\2\u0485\u0486\7\13\2\2"+
		"\u0486\u0487\5\u00ceh\2\u0487\u0488\7\f\2\2\u0488\u048c\3\2\2\2\u0489"+
		"\u048a\78\2\2\u048a\u048c\5\u00e0q\2\u048b\u0485\3\2\2\2\u048b\u0489\3"+
		"\2\2\2\u048c\u00d1\3\2\2\2\u048d\u048e\7V\2\2\u048e\u048f\7j\2\2\u048f"+
		"\u0492\5\u0126\u0094\2\u0490\u0491\7\u00bc\2\2\u0491\u0493\5\u00fe\u0080"+
		"\2\u0492\u0490\3\2\2\2\u0492\u0493\3\2\2\2\u0493\u00d3\3\2\2\2\u0494\u0495"+
		"\7[\2\2\u0495\u0498\7\u00aa\2\2\u0496\u0497\7p\2\2\u0497\u0499\7d\2\2"+
		"\u0498\u0496\3\2\2\2\u0498\u0499\3\2\2\2\u0499\u049a\3\2\2\2\u049a\u049b"+
		"\5\u0106\u0084\2\u049b\u00d5\3\2\2\2\u049c\u049e\5\u0140\u00a1\2\u049d"+
		"\u049f\5\u00d8m\2\u049e\u049d\3\2\2\2\u049e\u049f\3\2\2\2\u049f\u04a1"+
		"\3\2\2\2\u04a0\u04a2\5\u00dan\2\u04a1\u04a0\3\2\2\2\u04a1\u04a2\3\2\2"+
		"\2\u04a2\u00d7\3\2\2\2\u04a3\u04a4\7\u0091\2\2\u04a4\u04a5\7@\2\2\u04a5"+
		"\u04aa\5\u0128\u0095\2\u04a6\u04a7\7\r\2\2\u04a7\u04a9\5\u0128\u0095\2"+
		"\u04a8\u04a6\3\2\2\2\u04a9\u04ac\3\2\2\2\u04aa\u04a8\3\2\2\2\u04aa\u04ab"+
		"\3\2\2\2\u04ab\u00d9\3\2\2\2\u04ac\u04aa\3\2\2\2\u04ad\u04ae\7\u0082\2"+
		"\2\u04ae\u04b1\5\u00fe\u0080\2\u04af\u04b0\t\13\2\2\u04b0\u04b2\5\u00fe"+
		"\u0080\2\u04b1\u04af\3\2\2\2\u04b1\u04b2\3\2\2\2\u04b2\u00db\3\2\2\2\u04b3"+
		"\u04b4\7x\2\2\u04b4\u04b5\7{\2\2\u04b5\u04c1\5\u0106\u0084\2\u04b6\u04b7"+
		"\7\13\2\2\u04b7\u04bc\5\u0162\u00b2\2\u04b8\u04b9\7\r\2\2\u04b9\u04bb"+
		"\5\u0162\u00b2\2\u04ba\u04b8\3\2\2\2\u04bb\u04be\3\2\2\2\u04bc\u04ba\3"+
		"\2\2\2\u04bc\u04bd\3\2\2\2\u04bd\u04bf\3\2\2\2\u04be\u04bc\3\2\2\2\u04bf"+
		"\u04c0\7\f\2\2\u04c0\u04c2\3\2\2\2\u04c1\u04b6\3\2\2\2\u04c1\u04c2\3\2"+
		"\2\2\u04c2\u04c3\3\2\2\2\u04c3\u04c4\5\u00dep\2\u04c4\u00dd\3\2\2\2\u04c5"+
		"\u04ca\5\u0142\u00a2\2\u04c6\u04ca\5\u00e0q\2\u04c7\u04c8\7S\2\2\u04c8"+
		"\u04ca\7\u00b8\2\2\u04c9\u04c5\3\2\2\2\u04c9\u04c6\3\2\2\2\u04c9\u04c7"+
		"\3\2\2\2\u04ca\u00df\3\2\2\2\u04cb\u04cc\5\u00d6l\2\u04cc\u00e1\3\2\2"+
		"\2\u04cd\u04ce\7\u00b5\2\2\u04ce\u04cf\5\u0126\u0094\2\u04cf\u04d0\7\u00a7"+
		"\2\2\u04d0\u04d5\5\u00e4s\2\u04d1\u04d2\7\r\2\2\u04d2\u04d4\5\u00e4s\2"+
		"\u04d3\u04d1\3\2\2\2\u04d4\u04d7\3\2\2\2\u04d5\u04d3\3\2\2\2\u04d5\u04d6"+
		"\3\2\2\2\u04d6\u04da\3\2\2\2\u04d7\u04d5\3\2\2\2\u04d8\u04d9\7\u00bc\2"+
		"\2\u04d9\u04db\5\u00fe\u0080\2\u04da\u04d8\3\2\2\2\u04da\u04db\3\2\2\2"+
		"\u04db\u00e3\3\2\2\2\u04dc\u04dd\5\u0162\u00b2\2\u04dd\u04de\7\16\2\2"+
		"\u04de\u04df\5\u00fe\u0080\2\u04df\u00e5\3\2\2\2\u04e0\u04e4\5\u0162\u00b2"+
		"\2\u04e1\u04e3\5\u00e8u\2\u04e2\u04e1\3\2\2\2\u04e3\u04e6\3\2\2\2\u04e4"+
		"\u04e2\3\2\2\2\u04e4\u04e5\3\2\2\2\u04e5\u00e7\3\2\2\2\u04e6\u04e4\3\2"+
		"\2\2\u04e7\u04ea\5\u00eex\2\u04e8\u04ea\5\u00eav\2\u04e9\u04e7\3\2\2\2"+
		"\u04e9\u04e8\3\2\2\2\u04ea\u00e9\3\2\2\2\u04eb\u04f6\5\u0156\u00ac\2\u04ec"+
		"\u04ed\7\13\2\2\u04ed\u04ee\5\u00ecw\2\u04ee\u04ef\7\f\2\2\u04ef\u04f7"+
		"\3\2\2\2\u04f0\u04f1\7\13\2\2\u04f1\u04f2\5\u00ecw\2\u04f2\u04f3\7\r\2"+
		"\2\u04f3\u04f4\5\u00ecw\2\u04f4\u04f5\7\f\2\2\u04f5\u04f7\3\2\2\2\u04f6"+
		"\u04ec\3\2\2\2\u04f6\u04f0\3\2\2\2\u04f6\u04f7\3\2\2\2\u04f7\u04fa\3\2"+
		"\2\2\u04f8\u04fa\5\u00c4c\2\u04f9\u04eb\3\2\2\2\u04f9\u04f8\3\2\2\2\u04fa"+
		"\u00eb\3\2\2\2\u04fb\u04fd\5\u014c\u00a7\2\u04fc\u04fe\5\u016c\u00b7\2"+
		"\u04fd\u04fc\3\2\2\2\u04fd\u04fe\3\2\2\2\u04fe\u00ed\3\2\2\2\u04ff\u0500"+
		"\7J\2\2\u0500\u0502\5\u0156\u00ac\2\u0501\u04ff\3\2\2\2\u0501\u0502\3"+
		"\2\2\2\u0502\u050f\3\2\2\2\u0503\u0510\5\u00f0y\2\u0504\u0510\5\u00f2"+
		"z\2\u0505\u0510\5\u00f4{\2\u0506\u0510\5\u00f6|\2\u0507\u0508\7E\2\2\u0508"+
		"\u0509\7\13\2\2\u0509\u050a\5\u00fe\u0080\2\u050a\u050b\7\f\2\2\u050b"+
		"\u0510\3\2\2\2\u050c\u0510\5\u00f8}\2\u050d\u050e\7F\2\2\u050e\u0510\5"+
		"\u0164\u00b3\2\u050f\u0503\3\2\2\2\u050f\u0504\3\2\2\2\u050f\u0505\3\2"+
		"\2\2\u050f\u0506\3\2\2\2\u050f\u0507\3\2\2\2\u050f\u050c\3\2\2\2\u050f"+
		"\u050d\3\2\2\2\u0510\u00ef\3\2\2\2\u0511\u0512\7\u0096\2\2\u0512\u0514"+
		"\7\177\2\2\u0513\u0515\t\f\2\2\u0514\u0513\3\2\2\2\u0514\u0515\3\2\2\2"+
		"\u0515\u0517\3\2\2\2\u0516\u0518\7;\2\2\u0517\u0516\3\2\2\2\u0517\u0518"+
		"\3\2\2\2\u0518\u00f1\3\2\2\2\u0519\u051a\5\u010c\u0087\2\u051a\u00f3\3"+
		"\2\2\2\u051b\u051c\7\u0088\2\2\u051c\u051d\7\u008b\2\2\u051d\u00f5\3\2"+
		"\2\2\u051e\u051f\7\u008b\2\2\u051f\u00f7\3\2\2\2\u0520\u0521\7S\2\2\u0521"+
		"\u0528\5\u00fa~\2\u0522\u0524\7\4\2\2\u0523\u0525\5\u016c\u00b7\2\u0524"+
		"\u0523\3\2\2\2\u0525\u0526\3\2\2\2\u0526\u0524\3\2\2\2\u0526\u0527\3\2"+
		"\2\2\u0527\u0529\3\2\2\2\u0528\u0522\3\2\2\2\u0528\u0529\3\2\2\2\u0529"+
		"\u00f9\3\2\2\2\u052a\u0536\5\u00fc\177\2\u052b\u052c\7\13\2\2\u052c\u052d"+
		"\5\u00fe\u0080\2\u052d\u052e\7\f\2\2\u052e\u0536\3\2\2\2\u052f\u0530\7"+
		"\u0085\2\2\u0530\u0531\7\13\2\2\u0531\u0532\5\u00fe\u0080\2\u0532\u0533"+
		"\7\f\2\2\u0533\u0536\3\2\2\2\u0534\u0536\5\u016c\u00b7\2\u0535\u052a\3"+
		"\2\2\2\u0535\u052b\3\2\2\2\u0535\u052f\3\2\2\2\u0535\u0534\3\2\2\2\u0536"+
		"\u00fb\3\2\2\2\u0537\u053a\5\u014c\u00a7\2\u0538\u053a\5\u014e\u00a8\2"+
		"\u0539\u0537\3\2\2\2\u0539\u0538\3\2\2\2\u053a\u00fd\3\2\2\2\u053b\u053c"+
		"\b\u0080\1\2\u053c\u0548\5\u014e\u00a8\2\u053d\u0548\5\u0104\u0083\2\u053e"+
		"\u053f\5\u0150\u00a9\2\u053f\u0540\5\u00fe\u0080\16\u0540\u0548\3\2\2"+
		"\2\u0541\u0548\5\u0102\u0082\2\u0542\u0543\7\13\2\2\u0543\u0544\5\u00fe"+
		"\u0080\2\u0544\u0545\7\f\2\2\u0545\u0548\3\2\2\2\u0546\u0548\5\u0100\u0081"+
		"\2\u0547\u053b\3\2\2\2\u0547\u053d\3\2\2\2\u0547\u053e\3\2\2\2\u0547\u0541"+
		"\3\2\2\2\u0547\u0542\3\2\2\2\u0547\u0546\3\2\2\2\u0548\u0571\3\2\2\2\u0549"+
		"\u054a\f\r\2\2\u054a\u054b\7\25\2\2\u054b\u0570\5\u00fe\u0080\16\u054c"+
		"\u054d\f\f\2\2\u054d\u054e\t\r\2\2\u054e\u0570\5\u00fe\u0080\r\u054f\u0550"+
		"\f\13\2\2\u0550\u0551\t\b\2\2\u0551\u0570\5\u00fe\u0080\f\u0552\u0553"+
		"\f\n\2\2\u0553\u0554\t\16\2\2\u0554\u0570\5\u00fe\u0080\13\u0555\u0556"+
		"\f\t\2\2\u0556\u0557\t\17\2\2\u0557\u0570\5\u00fe\u0080\n\u0558\u0564"+
		"\f\b\2\2\u0559\u0565\7\16\2\2\u055a\u0565\7!\2\2\u055b\u0565\7\"\2\2\u055c"+
		"\u0565\7#\2\2\u055d\u0565\7|\2\2\u055e\u055f\7|\2\2\u055f\u0565\7\u0088"+
		"\2\2\u0560\u0565\7\u0081\2\2\u0561\u0565\7m\2\2\u0562\u0565\7\u0083\2"+
		"\2\u0563\u0565\7\u009b\2\2\u0564\u0559\3\2\2\2\u0564\u055a\3\2\2\2\u0564"+
		"\u055b\3\2\2\2\u0564\u055c\3\2\2\2\u0564\u055d\3\2\2\2\u0564\u055e\3\2"+
		"\2\2\u0564\u0560\3\2\2\2\u0564\u0561\3\2\2\2\u0564\u0562\3\2\2\2\u0564"+
		"\u0563\3\2\2\2\u0565\u0566\3\2\2\2\u0566\u0570\5\u00fe\u0080\t\u0567\u0568"+
		"\f\7\2\2\u0568\u0569\7\67\2\2\u0569\u0570\5\u00fe\u0080\b\u056a\u056b"+
		"\f\6\2\2\u056b\u056c\7\u0090\2\2\u056c\u0570\5\u00fe\u0080\7\u056d\u056e"+
		"\f\20\2\2\u056e\u0570\5\u010a\u0086\2\u056f\u0549\3\2\2\2\u056f\u054c"+
		"\3\2\2\2\u056f\u054f\3\2\2\2\u056f\u0552\3\2\2\2\u056f\u0555\3\2\2\2\u056f"+
		"\u0558\3\2\2\2\u056f\u0567\3\2\2\2\u056f\u056a\3\2\2\2\u056f\u056d\3\2"+
		"\2\2\u0570\u0573\3\2\2\2\u0571\u056f\3\2\2\2\u0571\u0572\3\2\2\2\u0572"+
		"\u00ff\3\2\2\2\u0573\u0571\3\2\2\2\u0574\u0576\7\u0088\2\2\u0575\u0574"+
		"\3\2\2\2\u0575\u0576\3\2\2\2\u0576\u0577\3\2\2\2\u0577\u0579\7d\2\2\u0578"+
		"\u0575\3\2\2\2\u0578\u0579\3\2\2\2\u0579\u057a\3\2\2\2\u057a\u057b\7\13"+
		"\2\2\u057b\u057c\5\u00e0q\2\u057c\u057d\7\f\2\2\u057d\u0101\3\2\2\2\u057e"+
		"\u057f\5\u0158\u00ad\2\u057f\u058c\7\13\2\2\u0580\u0582\7Y\2\2\u0581\u0580"+
		"\3\2\2\2\u0581\u0582\3\2\2\2\u0582\u0583\3\2\2\2\u0583\u0588\5\u00fe\u0080"+
		"\2\u0584\u0585\7\r\2\2\u0585\u0587\5\u00fe\u0080\2\u0586\u0584\3\2\2\2"+
		"\u0587\u058a\3\2\2\2\u0588\u0586\3\2\2\2\u0588\u0589\3\2\2\2\u0589\u058d"+
		"\3\2\2\2\u058a\u0588\3\2\2\2\u058b\u058d\7\17\2\2\u058c\u0581\3\2\2\2"+
		"\u058c\u058b\3\2\2\2\u058c\u058d\3\2\2\2\u058d\u058e\3\2\2\2\u058e\u058f"+
		"\7\f\2\2\u058f\u0103\3\2\2\2\u0590\u0591\5\u0106\u0084\2\u0591\u0592\7"+
		"\n\2\2\u0592\u0594\3\2\2\2\u0593\u0590\3\2\2\2\u0593\u0594\3\2\2\2\u0594"+
		"\u0595\3\2\2\2\u0595\u0596\5\u0162\u00b2\2\u0596\u0105\3\2\2\2\u0597\u0598"+
		"\5\u015e\u00b0\2\u0598\u0107\3\2\2\2\u0599\u05a3\5\u00e0q\2\u059a\u059f"+
		"\5\u00fe\u0080\2\u059b\u059c\7\r\2\2\u059c\u059e\5\u00fe\u0080\2\u059d"+
		"\u059b\3\2\2\2\u059e\u05a1\3\2\2\2\u059f\u059d\3\2\2\2\u059f\u05a0\3\2"+
		"\2\2\u05a0\u05a3\3\2\2\2\u05a1\u059f\3\2\2\2\u05a2\u0599\3\2\2\2\u05a2"+
		"\u059a\3\2\2\2\u05a2\u05a3\3\2\2\2\u05a3\u0109\3\2\2\2\u05a4\u05a6\7\u0088"+
		"\2\2\u05a5\u05a4\3\2\2\2\u05a5\u05a6\3\2\2\2\u05a6\u05a7\3\2\2\2\u05a7"+
		"\u05ad\7s\2\2\u05a8\u05a9\7\13\2\2\u05a9\u05aa\5\u0108\u0085\2\u05aa\u05ab"+
		"\7\f\2\2\u05ab\u05ae\3\2\2\2\u05ac\u05ae\5\u0106\u0084\2\u05ad\u05a8\3"+
		"\2\2\2\u05ad\u05ac\3\2\2\2\u05ae\u010b\3\2\2\2\u05af\u05b0\7\u009a\2\2"+
		"\u05b0\u05bc\5\u0114\u008b\2\u05b1\u05b2\7\13\2\2\u05b2\u05b7\5\u0116"+
		"\u008c\2\u05b3\u05b4\7\r\2\2\u05b4\u05b6\5\u0116\u008c\2\u05b5\u05b3\3"+
		"\2\2\2\u05b6\u05b9\3\2\2\2\u05b7\u05b5\3\2\2\2\u05b7\u05b8\3\2\2\2\u05b8"+
		"\u05ba\3\2\2\2\u05b9\u05b7\3\2\2\2\u05ba\u05bb\7\f\2\2\u05bb\u05bd\3\2"+
		"\2\2\u05bc\u05b1\3\2\2\2\u05bc\u05bd\3\2\2\2\u05bd\u05c1\3\2\2\2\u05be"+
		"\u05c0\5\u0112\u008a\2\u05bf\u05be\3\2\2\2\u05c0\u05c3\3\2\2\2\u05c1\u05bf"+
		"\3\2\2\2\u05c1\u05c2\3\2\2\2\u05c2\u05c5\3\2\2\2\u05c3\u05c1\3\2\2\2\u05c4"+
		"\u05c6\5\u010e\u0088\2\u05c5\u05c4\3\2\2\2\u05c5\u05c6\3\2\2\2\u05c6\u010d"+
		"\3\2\2\2\u05c7\u05c9\7\u0088\2\2\u05c8\u05c7\3\2\2\2\u05c8\u05c9\3\2\2"+
		"\2\u05c9\u05ca\3\2\2\2\u05ca\u05cf\7T\2\2\u05cb\u05cc\7v\2\2\u05cc\u05d0"+
		"\7U\2\2\u05cd\u05ce\7v\2\2\u05ce\u05d0\7r\2\2\u05cf\u05cb\3\2\2\2\u05cf"+
		"\u05cd\3\2\2\2\u05cf\u05d0\3\2\2\2\u05d0\u05d2\3\2\2\2\u05d1\u05d3\7`"+
		"\2\2\u05d2\u05d1\3\2\2\2\u05d2\u05d3\3\2\2\2\u05d3\u010f\3\2\2\2\u05d4"+
		"\u05d5\7\u008e\2\2\u05d5\u05de\t\20\2\2\u05d6\u05d7\7\u00a7\2\2\u05d7"+
		"\u05df\7\u008b\2\2\u05d8\u05d9\7\u00a7\2\2\u05d9\u05df\7S\2\2\u05da\u05df"+
		"\7B\2\2\u05db\u05df\7\u00a0\2\2\u05dc\u05dd\7\u0087\2\2\u05dd\u05df\7"+
		"\60\2\2\u05de\u05d6\3\2\2\2\u05de\u05d8\3\2\2\2\u05de\u05da\3\2\2\2\u05de"+
		"\u05db\3\2\2\2\u05de\u05dc\3\2\2\2\u05df\u0111\3\2\2\2\u05e0\u05e4\5\u0110"+
		"\u0089\2\u05e1\u05e2\7\u0083\2\2\u05e2\u05e4\5\u0156\u00ac\2\u05e3\u05e0"+
		"\3\2\2\2\u05e3\u05e1\3\2\2\2\u05e4\u0113\3\2\2\2\u05e5\u05e6\5\u015a\u00ae"+
		"\2\u05e6\u05e7\7\n\2\2\u05e7\u05e9\3\2\2\2\u05e8\u05e5\3\2\2\2\u05e8\u05e9"+
		"\3\2\2\2\u05e9\u05ea\3\2\2\2\u05ea\u05eb\5\u0166\u00b4\2\u05eb\u0115\3"+
		"\2\2\2\u05ec\u05ed\5\u0156\u00ac\2\u05ed\u0117\3\2\2\2\u05ee\u05f1\5\u0162"+
		"\u00b2\2\u05ef\u05f0\7F\2\2\u05f0\u05f2\5\u0164\u00b3\2\u05f1\u05ef\3"+
		"\2\2\2\u05f1\u05f2\3\2\2\2\u05f2\u05f4\3\2\2\2\u05f3\u05f5\t\f\2\2\u05f4"+
		"\u05f3\3\2\2\2\u05f4\u05f5\3\2\2\2\u05f5\u0119\3\2\2\2\u05f6\u05f7\7J"+
		"\2\2\u05f7\u05f9\5\u0156\u00ac\2\u05f8\u05f6\3\2\2\2\u05f8\u05f9\3\2\2"+
		"\2\u05f9\u0603\3\2\2\2\u05fa\u0604\5\u011c\u008f\2\u05fb\u0604\5\u0122"+
		"\u0092\2\u05fc\u0604\5\u0120\u0091\2\u05fd\u05fe\7E\2\2\u05fe\u05ff\7"+
		"\13\2\2\u05ff\u0600\5\u00fe\u0080\2\u0600\u0601\7\f\2\2\u0601\u0604\3"+
		"\2\2\2\u0602\u0604\5\u011e\u0090\2\u0603\u05fa\3\2\2\2\u0603\u05fb\3\2"+
		"\2\2\u0603\u05fc\3\2\2\2\u0603\u05fd\3\2\2\2\u0603\u0602\3\2\2\2\u0604"+
		"\u011b\3\2\2\2\u0605\u0606\7\u0096\2\2\u0606\u0607\7\177\2\2\u0607\u0608"+
		"\7\13\2\2\u0608\u060d\5\u0118\u008d\2\u0609\u060a\7\r\2\2\u060a\u060c"+
		"\5\u0118\u008d\2\u060b\u0609\3\2\2\2\u060c\u060f\3\2\2\2\u060d\u060b\3"+
		"\2\2\2\u060d\u060e\3\2\2\2\u060e\u0610\3\2\2\2\u060f\u060d\3\2\2\2\u0610"+
		"\u0611\7\f\2\2\u0611\u011d\3\2\2\2\u0612\u0613\7i\2\2\u0613\u0614\7\177"+
		"\2\2\u0614\u0615\7\13\2\2\u0615\u061a\5\u0124\u0093\2\u0616\u0617\7\r"+
		"\2\2\u0617\u0619\5\u0124\u0093\2\u0618\u0616\3\2\2\2\u0619\u061c\3\2\2"+
		"\2\u061a\u0618\3\2\2\2\u061a\u061b\3\2\2\2\u061b\u061d\3\2\2\2\u061c\u061a"+
		"\3\2\2\2\u061d\u061e\7\f\2\2\u061e\u061f\5\u010c\u0087\2\u061f\u011f\3"+
		"\2\2\2\u0620\u0622\7\u00b4\2\2\u0621\u0623\7\177\2\2\u0622\u0621\3\2\2"+
		"\2\u0622\u0623\3\2\2\2\u0623\u0625\3\2\2\2\u0624\u0626\5\u0156\u00ac\2"+
		"\u0625\u0624\3\2\2\2\u0625\u0626\3\2\2\2\u0626\u0627\3\2\2\2\u0627\u0628"+
		"\7\13\2\2\u0628\u062d\5\u0118\u008d\2\u0629\u062a\7\r\2\2\u062a\u062c"+
		"\5\u0118\u008d\2\u062b\u0629\3\2\2\2\u062c\u062f\3\2\2\2\u062d\u062b\3"+
		"\2\2\2\u062d\u062e\3\2\2\2\u062e\u0630\3\2\2\2\u062f\u062d\3\2\2\2\u0630"+
		"\u0631\7\f\2\2\u0631\u0121\3\2\2\2\u0632\u0634\7\177\2\2\u0633\u0635\5"+
		"\u0156\u00ac\2\u0634\u0633\3\2\2\2\u0634\u0635\3\2\2\2\u0635\u0636\3\2"+
		"\2\2\u0636\u0637\7\13\2\2\u0637\u063c\5\u0118\u008d\2\u0638\u0639\7\r"+
		"\2\2\u0639\u063b\5\u0118\u008d\2\u063a\u0638\3\2\2\2\u063b\u063e\3\2\2"+
		"\2\u063c\u063a\3\2\2\2\u063c\u063d\3\2\2\2\u063d\u063f\3\2\2\2\u063e\u063c"+
		"\3\2\2\2\u063f\u0640\7\f\2\2\u0640\u0123\3\2\2\2\u0641\u0642\5\u0162\u00b2"+
		"\2\u0642\u0125\3\2\2\2\u0643\u0649\5\u0106\u0084\2\u0644\u0645\7u\2\2"+
		"\u0645\u0646\7@\2\2\u0646\u064a\5\u0168\u00b5\2\u0647\u0648\7\u0088\2"+
		"\2\u0648\u064a\7u\2\2\u0649\u0644\3\2\2\2\u0649\u0647\3\2\2\2\u0649\u064a"+
		"\3\2\2\2\u064a\u0127\3\2\2\2\u064b\u064e\5\u00fe\u0080\2\u064c\u064d\7"+
		"F\2\2\u064d\u064f\5\u0164\u00b3\2\u064e\u064c\3\2\2\2\u064e\u064f\3\2"+
		"\2\2\u064f\u0651\3\2\2\2\u0650\u0652\t\f\2\2\u0651\u0650\3\2\2\2\u0651"+
		"\u0652\3\2\2\2\u0652\u0129\3\2\2\2\u0653\u065a\7\17\2\2\u0654\u0655\5"+
		"\u015e\u00b0\2\u0655\u0656\7\n\2\2\u0656\u0657\7\17\2\2\u0657\u065a\3"+
		"\2\2\2\u0658\u065a\5\u012c\u0097\2\u0659\u0653\3\2\2\2\u0659\u0654\3\2"+
		"\2\2\u0659\u0658\3\2\2\2\u065a\u012b\3\2\2\2\u065b\u0660\5\u00fe\u0080"+
		"\2\u065c\u065e\78\2\2\u065d\u065c\3\2\2\2\u065d\u065e\3\2\2\2\u065e\u065f"+
		"\3\2\2\2\u065f\u0661\5\u0152\u00aa\2\u0660\u065d\3\2\2\2\u0660\u0661\3"+
		"\2\2\2\u0661\u012d\3\2\2\2\u0662\u0666\5\u0134\u009b\2\u0663\u0666\5\u0130"+
		"\u0099\2\u0664\u0666\5\u0132\u009a\2\u0665\u0662\3\2\2\2\u0665\u0663\3"+
		"\2\2\2\u0665\u0664\3\2\2\2\u0666\u012f\3\2\2\2\u0667\u0668\7\13\2\2\u0668"+
		"\u0669\5\u014a\u00a6\2\u0669\u066e\7\f\2\2\u066a\u066c\78\2\2\u066b\u066a"+
		"\3\2\2\2\u066b\u066c\3\2\2\2\u066c\u066d\3\2\2\2\u066d\u066f\5\u016a\u00b6"+
		"\2\u066e\u066b\3\2\2\2\u066e\u066f\3\2\2\2\u066f\u0131\3\2\2\2\u0670\u0671"+
		"\7\13\2\2\u0671\u0672\5\u00e0q\2\u0672\u0677\7\f\2\2\u0673\u0675\78\2"+
		"\2\u0674\u0673\3\2\2\2\u0674\u0675\3\2\2\2\u0675\u0676\3\2\2\2\u0676\u0678"+
		"\5\u016a\u00b6\2\u0677\u0674\3\2\2\2\u0677\u0678\3\2\2\2\u0678\u0133\3"+
		"\2\2\2\u0679\u067e\5\u0106\u0084\2\u067a\u067c\78\2\2\u067b\u067a\3\2"+
		"\2\2\u067b\u067c\3\2\2\2\u067c\u067d\3\2\2\2\u067d\u067f\5\u016a\u00b6"+
		"\2\u067e\u067b\3\2\2\2\u067e\u067f\3\2\2\2\u067f\u0681\3\2\2\2\u0680\u0682"+
		"\5\u0136\u009c\2\u0681\u0680\3\2\2\2\u0681\u0682\3\2\2\2\u0682\u0135\3"+
		"\2\2\2\u0683\u0684\7u\2\2\u0684\u0685\7@\2\2\u0685\u0689\5\u0168\u00b5"+
		"\2\u0686\u0687\7\u0088\2\2\u0687\u0689\7u\2\2\u0688\u0683\3\2\2\2\u0688"+
		"\u0686\3\2\2\2\u0689\u0137\3\2\2\2\u068a\u068e\5\u012e\u0098\2\u068b\u068d"+
		"\5\u013a\u009e\2\u068c\u068b\3\2\2\2\u068d\u0690\3\2\2\2\u068e\u068c\3"+
		"\2\2\2\u068e\u068f\3\2\2\2\u068f\u0139\3\2\2\2\u0690\u068e\3\2\2\2\u0691"+
		"\u0692\5\u013c\u009f\2\u0692\u0693\5\u012e\u0098\2\u0693\u0694\5\u013e"+
		"\u00a0\2\u0694\u013b\3\2\2\2\u0695\u069f\7\r\2\2\u0696\u0698\7\u0080\2"+
		"\2\u0697\u0699\7\u0092\2\2\u0698\u0697\3\2\2\2\u0698\u0699\3\2\2\2\u0699"+
		"\u069c\3\2\2\2\u069a\u069c\7w\2\2\u069b\u0696\3\2\2\2\u069b\u069a\3\2"+
		"\2\2\u069b\u069c\3\2\2\2\u069c\u069d\3\2\2\2\u069d\u069f\7~\2\2\u069e"+
		"\u0695\3\2\2\2\u069e\u069b\3\2\2\2\u069f\u013d\3\2\2\2\u06a0\u06a1\7\u008e"+
		"\2\2\u06a1\u06a3\5\u00fe\u0080\2\u06a2\u06a0\3\2\2\2\u06a2\u06a3\3\2\2"+
		"\2\u06a3\u013f\3\2\2\2\u06a4\u06a7\5\u0148\u00a5\2\u06a5\u06a6\7j\2\2"+
		"\u06a6\u06a8\5\u014a\u00a6\2\u06a7\u06a5\3\2\2\2\u06a7\u06a8\3\2\2\2\u06a8"+
		"\u06ab\3\2\2\2\u06a9\u06aa\7\u00bc\2\2\u06aa\u06ac\5\u00fe\u0080\2\u06ab"+
		"\u06a9\3\2\2\2\u06ab\u06ac\3\2\2\2\u06ac\u06ae\3\2\2\2\u06ad\u06af\5\u0146"+
		"\u00a4\2\u06ae\u06ad\3\2\2\2\u06ae\u06af\3\2\2\2\u06af\u06b2\3\2\2\2\u06b0"+
		"\u06b2\5\u0142\u00a2\2\u06b1\u06a4\3\2\2\2\u06b1\u06b0\3\2\2\2\u06b2\u0141"+
		"\3\2\2\2\u06b3\u06b4\7\u00b8\2\2\u06b4\u06b5\7\13\2\2\u06b5\u06b6\5\u0144"+
		"\u00a3\2\u06b6\u06be\7\f\2\2\u06b7\u06b8\7\r\2\2\u06b8\u06b9\7\13\2\2"+
		"\u06b9\u06ba\5\u0144\u00a3\2\u06ba\u06bb\7\f\2\2\u06bb\u06bd\3\2\2\2\u06bc"+
		"\u06b7\3\2\2\2\u06bd\u06c0\3\2\2\2\u06be\u06bc\3\2\2\2\u06be\u06bf\3\2"+
		"\2\2\u06bf\u0143\3\2\2\2\u06c0\u06be\3\2\2\2\u06c1\u06c6\5\u00fe\u0080"+
		"\2\u06c2\u06c3\7\r\2\2\u06c3\u06c5\5\u00fe\u0080\2\u06c4\u06c2\3\2\2\2"+
		"\u06c5\u06c8\3\2\2\2\u06c6\u06c4\3\2\2\2\u06c6\u06c7\3\2\2\2\u06c7\u0145"+
		"\3\2\2\2\u06c8\u06c6\3\2\2\2\u06c9\u06ca\7n\2\2\u06ca\u06cb\7@\2\2\u06cb"+
		"\u06d0\5\u00fe\u0080\2\u06cc\u06cd\7\r\2\2\u06cd\u06cf\5\u00fe\u0080\2"+
		"\u06ce\u06cc\3\2\2\2\u06cf\u06d2\3\2\2\2\u06d0\u06ce\3\2\2\2\u06d0\u06d1"+
		"\3\2\2\2\u06d1\u06d5\3\2\2\2\u06d2\u06d0\3\2\2\2\u06d3\u06d4\7o\2\2\u06d4"+
		"\u06d6\5\u00fe\u0080\2\u06d5\u06d3\3\2\2\2\u06d5\u06d6\3\2\2\2\u06d6\u0147"+
		"\3\2\2\2\u06d7\u06d9\7\u00a6\2\2\u06d8\u06da\t\21\2\2\u06d9\u06d8\3\2"+
		"\2\2\u06d9\u06da\3\2\2\2\u06da\u06db\3\2\2\2\u06db\u06e0\5\u012a\u0096"+
		"\2\u06dc\u06dd\7\r\2\2\u06dd\u06df\5\u012a\u0096\2\u06de\u06dc\3\2\2\2"+
		"\u06df\u06e2\3\2\2\2\u06e0\u06de\3\2\2\2\u06e0\u06e1\3\2\2\2\u06e1\u0149"+
		"\3\2\2\2\u06e2\u06e0\3\2\2\2\u06e3\u06e8\5\u012e\u0098\2\u06e4\u06e5\7"+
		"\r\2\2\u06e5\u06e7\5\u012e\u0098\2\u06e6\u06e4\3\2\2\2\u06e7\u06ea\3\2"+
		"\2\2\u06e8\u06e6\3\2\2\2\u06e8\u06e9\3\2\2\2\u06e9\u06ed\3\2\2\2\u06ea"+
		"\u06e8\3\2\2\2\u06eb\u06ed\5\u0138\u009d\2\u06ec\u06e3\3\2\2\2\u06ec\u06eb"+
		"\3\2\2\2\u06ed\u014b\3\2\2\2\u06ee\u06f0\t\b\2\2\u06ef\u06ee\3\2\2\2\u06ef"+
		"\u06f0\3\2\2\2\u06f0\u06f1\3\2\2\2\u06f1\u06f4\7\5\2\2\u06f2\u06f4\7\17"+
		"\2\2\u06f3\u06ef\3\2\2\2\u06f3\u06f2\3\2\2\2\u06f4\u014d\3\2\2\2\u06f5"+
		"\u06f6\t\22\2\2\u06f6\u014f\3\2\2\2\u06f7\u06f8\t\23\2\2\u06f8\u0151\3"+
		"\2\2\2\u06f9\u06fc\t\24\2\2\u06fa\u06fc\7\u00c4\2\2\u06fb\u06f9\3\2\2"+
		"\2\u06fb\u06fa\3\2\2\2\u06fc\u0153\3\2\2\2\u06fd\u06fe\t\25\2\2\u06fe"+
		"\u0155\3\2\2\2\u06ff\u0700\5\u016c\u00b7\2\u0700\u0157\3\2\2\2\u0701\u0702"+
		"\5\u016c\u00b7\2\u0702\u0159\3\2\2\2\u0703\u0704\5\u016c\u00b7\2\u0704"+
		"\u015b\3\2\2\2\u0705\u0706\5\u016c\u00b7\2\u0706\u015d\3\2\2\2\u0707\u0708"+
		"\5\u016c\u00b7\2\u0708\u015f\3\2\2\2\u0709\u070a\5\u016c\u00b7\2\u070a"+
		"\u0161\3\2\2\2\u070b\u070c\5\u016c\u00b7\2\u070c\u0163\3\2\2\2\u070d\u070e"+
		"\5\u016c\u00b7\2\u070e\u0165\3\2\2\2\u070f\u0710\5\u016c\u00b7\2\u0710"+
		"\u0167\3\2\2\2\u0711\u0712\5\u016c\u00b7\2\u0712\u0169\3\2\2\2\u0713\u0714"+
		"\5\u016c\u00b7\2\u0714\u016b\3\2\2\2\u0715\u071c\t\24\2\2\u0716\u071c"+
		"\7\u00c4\2\2\u0717\u0718\7\13\2\2\u0718\u0719\5\u016c\u00b7\2\u0719\u071a"+
		"\7\f\2\2\u071a\u071c\3\2\2\2\u071b\u0715\3\2\2\2\u071b\u0716\3\2\2\2\u071b"+
		"\u0717\3\2\2\2\u071c\u016d\3\2\2\2\u071d\u071e\7?\2\2\u071e\u071f\7\b"+
		"\2\2\u071f\u016f\3\2\2\2\u0720\u0721\7K\2\2\u0721\u0722\7\b\2\2\u0722"+
		"\u0171\3\2\2\2\u00b3\u0175\u017d\u0181\u0194\u019a\u019e\u01a5\u01a9\u01b5"+
		"\u01cc\u01d1\u01e9\u01f0\u01f3\u01f5\u01fb\u0201\u020c\u0218\u021f\u0224"+
		"\u022b\u0236\u023f\u0243\u0254\u025a\u0261\u0271\u0274\u0279\u028f\u0297"+
		"\u02a5\u02b3\u02bb\u02cf\u02d5\u02da\u02de\u02e7\u02f1\u02f5\u0303\u030c"+
		"\u0310\u0318\u0323\u0329\u0331\u0342\u0349\u0355\u035a\u036c\u0373\u0378"+
		"\u037f\u038d\u0392\u039f\u03ab\u03b3\u03c5\u03cb\u03d2\u03db\u03e7\u03ee"+
		"\u03f3\u03f9\u0405\u040c\u0416\u0419\u0428\u0435\u044e\u046a\u0480\u0482"+
		"\u048b\u0492\u0498\u049e\u04a1\u04aa\u04b1\u04bc\u04c1\u04c9\u04d5\u04da"+
		"\u04e4\u04e9\u04f6\u04f9\u04fd\u0501\u050f\u0514\u0517\u0526\u0528\u0535"+
		"\u0539\u0547\u0564\u056f\u0571\u0575\u0578\u0581\u0588\u058c\u0593\u059f"+
		"\u05a2\u05a5\u05ad\u05b7\u05bc\u05c1\u05c5\u05c8\u05cf\u05d2\u05de\u05e3"+
		"\u05e8\u05f1\u05f4\u05f8\u0603\u060d\u061a\u0622\u0625\u062d\u0634\u063c"+
		"\u0649\u064e\u0651\u0659\u065d\u0660\u0665\u066b\u066e\u0674\u0677\u067b"+
		"\u067e\u0681\u0688\u068e\u0698\u069b\u069e\u06a2\u06a7\u06ab\u06ae\u06b1"+
		"\u06be\u06c6\u06d0\u06d5\u06d9\u06e0\u06e8\u06ec\u06ef\u06f3\u06fb\u071b";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}