package Visitor.gen;// Generated from C:/Users/mhmas/Desktop/My File/CompilerProject/src\Rules.g4 by ANTLR 4.8
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link RulesParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface RulesVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link RulesParser#parse}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParse(RulesParser.ParseContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#all_statement_starts}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAll_statement_starts(RulesParser.All_statement_startsContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#java_stmt_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJava_stmt_list(RulesParser.Java_stmt_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#create_variable_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreate_variable_stmt(RulesParser.Create_variable_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#post_square}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPost_square(RulesParser.Post_squareContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#previous_square}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrevious_square(RulesParser.Previous_squareContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#assign_variable_in_creation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssign_variable_in_creation(RulesParser.Assign_variable_in_creationContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#assign_variable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssign_variable(RulesParser.Assign_variableContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#assign_array}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssign_array(RulesParser.Assign_arrayContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#assign_array_variable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssign_array_variable(RulesParser.Assign_array_variableContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#assign_variable_without_var}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssign_variable_without_var(RulesParser.Assign_variable_without_varContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#direct_array_values}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDirect_array_values(RulesParser.Direct_array_valuesContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#var_name_item_in_array}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar_name_item_in_array(RulesParser.Var_name_item_in_arrayContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#value_of_one_item_array}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValue_of_one_item_array(RulesParser.Value_of_one_item_arrayContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#variable_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable_name(RulesParser.Variable_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#array_value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArray_value(RulesParser.Array_valueContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#variable_value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable_value(RulesParser.Variable_valueContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#value_in_quote}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValue_in_quote(RulesParser.Value_in_quoteContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#default_value_parameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefault_value_parameter(RulesParser.Default_value_parameterContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#initial_variable_declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInitial_variable_declaration(RulesParser.Initial_variable_declarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#prototype_function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrototype_function(RulesParser.Prototype_functionContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#prototype_function_parameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrototype_function_parameter(RulesParser.Prototype_function_parameterContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#function_parameter_in_prototype}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_parameter_in_prototype(RulesParser.Function_parameter_in_prototypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#function_parameter_with_default}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_parameter_with_default(RulesParser.Function_parameter_with_defaultContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#initial_variable_declaration_with_default_value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInitial_variable_declaration_with_default_value(RulesParser.Initial_variable_declaration_with_default_valueContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#body_function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBody_function(RulesParser.Body_functionContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#body_prototype_function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBody_prototype_function(RulesParser.Body_prototype_functionContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#java_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJava_stmt(RulesParser.Java_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#full_condition_java}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFull_condition_java(RulesParser.Full_condition_javaContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#multi_condition_java}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMulti_condition_java(RulesParser.Multi_condition_javaContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#header_if}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHeader_if(RulesParser.Header_ifContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#one_condition_in_java}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOne_condition_in_java(RulesParser.One_condition_in_javaContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#not_var_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNot_var_name(RulesParser.Not_var_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#not_one_condition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNot_one_condition(RulesParser.Not_one_conditionContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#number_condition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumber_condition(RulesParser.Number_conditionContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#logical_condition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogical_condition(RulesParser.Logical_conditionContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#expr_java}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr_java(RulesParser.Expr_javaContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#number_value_java}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumber_value_java(RulesParser.Number_value_javaContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#logic_operation_statement_in_java}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogic_operation_statement_in_java(RulesParser.Logic_operation_statement_in_javaContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#operation_in_java}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOperation_in_java(RulesParser.Operation_in_javaContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#comparison_in_java}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComparison_in_java(RulesParser.Comparison_in_javaContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#logical_in_java}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogical_in_java(RulesParser.Logical_in_javaContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#line_condition__expression_java}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLine_condition__expression_java(RulesParser.Line_condition__expression_javaContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#line_condition_result}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLine_condition_result(RulesParser.Line_condition_resultContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#if_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_statement(RulesParser.If_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#else_if_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElse_if_statement(RulesParser.Else_if_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#else_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElse_statement(RulesParser.Else_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#switch_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSwitch_statement(RulesParser.Switch_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#case_statenemt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCase_statenemt(RulesParser.Case_statenemtContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#case_value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCase_value(RulesParser.Case_valueContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#default_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefault_statement(RulesParser.Default_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#body_for_case_and_default}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBody_for_case_and_default(RulesParser.Body_for_case_and_defaultContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#for_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFor_stmt(RulesParser.For_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#for_parameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFor_parameter(RulesParser.For_parameterContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#initial_start_count_in_for}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInitial_start_count_in_for(RulesParser.Initial_start_count_in_forContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#condition_for}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCondition_for(RulesParser.Condition_forContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#advanced_increase}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdvanced_increase(RulesParser.Advanced_increaseContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#delayed_increase}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDelayed_increase(RulesParser.Delayed_increaseContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#specific_increase}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSpecific_increase(RulesParser.Specific_increaseContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#increment_type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIncrement_type(RulesParser.Increment_typeContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#increment_count_for}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIncrement_count_for(RulesParser.Increment_count_forContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#increment_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIncrement_stmt(RulesParser.Increment_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#while_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhile_statement(RulesParser.While_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#do_while_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDo_while_statement(RulesParser.Do_while_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#callback_function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCallback_function(RulesParser.Callback_functionContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#callback_function_without_scol}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCallback_function_without_scol(RulesParser.Callback_function_without_scolContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#callback_function_heder}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCallback_function_heder(RulesParser.Callback_function_hederContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#callback_function_parameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCallback_function_parameter(RulesParser.Callback_function_parameterContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#callback_function_parameter_in_header}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCallback_function_parameter_in_header(RulesParser.Callback_function_parameter_in_headerContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#function_parameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_parameter(RulesParser.Function_parameterContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#var_name_in_parameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar_name_in_parameter(RulesParser.Var_name_in_parameterContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#return_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturn_stmt(RulesParser.Return_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#json_declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJson_declaration(RulesParser.Json_declarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#all_fileds_in_json}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAll_fileds_in_json(RulesParser.All_fileds_in_jsonContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#filed_in_json}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFiled_in_json(RulesParser.Filed_in_jsonContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#key_filed}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKey_filed(RulesParser.Key_filedContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#array_json_declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArray_json_declaration(RulesParser.Array_json_declarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#access_to_data_in_json}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAccess_to_data_in_json(RulesParser.Access_to_data_in_jsonContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#foreach_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForeach_statement(RulesParser.Foreach_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#variable_in_foreach}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable_in_foreach(RulesParser.Variable_in_foreachContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#print_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrint_statement(RulesParser.Print_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#error}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitError(RulesParser.ErrorContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#sql_stmt_list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSql_stmt_list(RulesParser.Sql_stmt_listContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#sql_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSql_stmt(RulesParser.Sql_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#alter_table_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlter_table_stmt(RulesParser.Alter_table_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#alter_table_add_constraint}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlter_table_add_constraint(RulesParser.Alter_table_add_constraintContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#alter_table_add}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAlter_table_add(RulesParser.Alter_table_addContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#create_table_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreate_table_stmt(RulesParser.Create_table_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#columns_definition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumns_definition(RulesParser.Columns_definitionContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#column_definition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_definition(RulesParser.Column_definitionContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#create_table_stmt_type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreate_table_stmt_type(RulesParser.Create_table_stmt_typeContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#type_faild}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType_faild(RulesParser.Type_faildContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#create_table_stmt_path}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreate_table_stmt_path(RulesParser.Create_table_stmt_pathContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#create_type_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreate_type_stmt(RulesParser.Create_type_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#create_aggregation_function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreate_aggregation_function(RulesParser.Create_aggregation_functionContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#create_aggregation_function_elements}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreate_aggregation_function_elements(RulesParser.Create_aggregation_function_elementsContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#return_type_array}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturn_type_array(RulesParser.Return_type_arrayContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#return_type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturn_type(RulesParser.Return_typeContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#class_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClass_name(RulesParser.Class_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#method_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethod_name(RulesParser.Method_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#path_string}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPath_string(RulesParser.Path_stringContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#any_name_with_quote}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAny_name_with_quote(RulesParser.Any_name_with_quoteContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#columns_def_tabels_constraint}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumns_def_tabels_constraint(RulesParser.Columns_def_tabels_constraintContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#columns_def_or_select_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumns_def_or_select_stmt(RulesParser.Columns_def_or_select_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#delete_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDelete_stmt(RulesParser.Delete_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#drop_table_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDrop_table_stmt(RulesParser.Drop_table_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#factored_select_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFactored_select_stmt(RulesParser.Factored_select_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#order_by}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrder_by(RulesParser.Order_byContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#limit_select}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLimit_select(RulesParser.Limit_selectContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#insert_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInsert_stmt(RulesParser.Insert_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#insert_stmt_values}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInsert_stmt_values(RulesParser.Insert_stmt_valuesContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#select_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelect_stmt(RulesParser.Select_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#update_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUpdate_stmt(RulesParser.Update_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#column_name_equals_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_name_equals_expr(RulesParser.Column_name_equals_exprContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#column_def}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_def(RulesParser.Column_defContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#column_constraint_or_type_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_constraint_or_type_name(RulesParser.Column_constraint_or_type_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#type_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType_name(RulesParser.Type_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#signed_number_any_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSigned_number_any_name(RulesParser.Signed_number_any_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#column_constraint}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_constraint(RulesParser.Column_constraintContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#column_constraint_primary_key}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_constraint_primary_key(RulesParser.Column_constraint_primary_keyContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#column_constraint_foreign_key}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_constraint_foreign_key(RulesParser.Column_constraint_foreign_keyContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#column_constraint_not_null}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_constraint_not_null(RulesParser.Column_constraint_not_nullContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#column_constraint_null}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_constraint_null(RulesParser.Column_constraint_nullContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#column_default}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_default(RulesParser.Column_defaultContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#column_default_content}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_default_content(RulesParser.Column_default_contentContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#column_default_value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_default_value(RulesParser.Column_default_valueContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr(RulesParser.ExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#exists_case}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExists_case(RulesParser.Exists_caseContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#function_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_expr(RulesParser.Function_exprContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#column_name_faild}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_name_faild(RulesParser.Column_name_faildContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#table_name_faild}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_name_faild(RulesParser.Table_name_faildContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#select_or_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelect_or_expr(RulesParser.Select_or_exprContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#in_case}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIn_case(RulesParser.In_caseContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#foreign_key_clause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForeign_key_clause(RulesParser.Foreign_key_clauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#foreign_deferrable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForeign_deferrable(RulesParser.Foreign_deferrableContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#foreign_on}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForeign_on(RulesParser.Foreign_onContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#foreign_on_or_match}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForeign_on_or_match(RulesParser.Foreign_on_or_matchContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#foreign_table_faild}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForeign_table_faild(RulesParser.Foreign_table_faildContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#fk_target_column_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFk_target_column_name(RulesParser.Fk_target_column_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#indexed_column}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndexed_column(RulesParser.Indexed_columnContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#table_constraint}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_constraint(RulesParser.Table_constraintContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#table_constraint_primary_key}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_constraint_primary_key(RulesParser.Table_constraint_primary_keyContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#table_constraint_foreign_key}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_constraint_foreign_key(RulesParser.Table_constraint_foreign_keyContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#table_constraint_unique}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_constraint_unique(RulesParser.Table_constraint_uniqueContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#table_constraint_key}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_constraint_key(RulesParser.Table_constraint_keyContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#fk_origin_column_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFk_origin_column_name(RulesParser.Fk_origin_column_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#qualified_table_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitQualified_table_name(RulesParser.Qualified_table_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#ordering_term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrdering_term(RulesParser.Ordering_termContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#result_column}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitResult_column(RulesParser.Result_columnContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#result_column_with_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitResult_column_with_expr(RulesParser.Result_column_with_exprContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#table_or_subquery}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_or_subquery(RulesParser.Table_or_subqueryContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#from_itmes_as_table}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFrom_itmes_as_table(RulesParser.From_itmes_as_tableContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#select_stmt_as_table}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelect_stmt_as_table(RulesParser.Select_stmt_as_tableContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#table_as_alias}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_as_alias(RulesParser.Table_as_aliasContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#index_name_or_not_index}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndex_name_or_not_index(RulesParser.Index_name_or_not_indexContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#join_clause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJoin_clause(RulesParser.Join_clauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#operator_table_constraint}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOperator_table_constraint(RulesParser.Operator_table_constraintContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#join_operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJoin_operator(RulesParser.Join_operatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#join_constraint}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJoin_constraint(RulesParser.Join_constraintContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#select_core}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelect_core(RulesParser.Select_coreContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#values_items}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValues_items(RulesParser.Values_itemsContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#expr_comma_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr_comma_expr(RulesParser.Expr_comma_exprContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#group_by}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGroup_by(RulesParser.Group_byContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#select_items}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelect_items(RulesParser.Select_itemsContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#from_itmes}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFrom_itmes(RulesParser.From_itmesContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#signed_number}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSigned_number(RulesParser.Signed_numberContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#literal_value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteral_value(RulesParser.Literal_valueContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#unary_operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnary_operator(RulesParser.Unary_operatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#column_alias}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_alias(RulesParser.Column_aliasContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#keyword}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKeyword(RulesParser.KeywordContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitName(RulesParser.NameContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#function_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_name(RulesParser.Function_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#database_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDatabase_name(RulesParser.Database_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#source_table_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSource_table_name(RulesParser.Source_table_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#table_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_name(RulesParser.Table_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#new_table_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNew_table_name(RulesParser.New_table_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#column_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn_name(RulesParser.Column_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#collation_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCollation_name(RulesParser.Collation_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#foreign_table}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForeign_table(RulesParser.Foreign_tableContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#index_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIndex_name(RulesParser.Index_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#table_alias}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTable_alias(RulesParser.Table_aliasContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#any_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAny_name(RulesParser.Any_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#break_java}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBreak_java(RulesParser.Break_javaContext ctx);
	/**
	 * Visit a parse tree produced by {@link RulesParser#continue_java}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitContinue_java(RulesParser.Continue_javaContext ctx);
}